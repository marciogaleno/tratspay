<?php

use Illuminate\Database\Seeder;
use App\Models\Plataforma\Categorie;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['desc' => 'Administração e Negócios'],
            ['desc' => 'Animais'],
            ['desc' => 'Artes e Música'],
            ['desc' => 'Auto Ajuda e Similares'],
            ['desc' => 'Beleza'],
            ['desc' => 'Blogs e Redes Sociais'],
            ['desc' => 'Casa, Jardinagem e Similares'],
            ['desc' => 'Culinária e Gastronomia'],
            ['desc' => 'Design de Templates'],
            ['desc' => 'Edições Audivisuais'],
            ['desc' => 'Esporte e Fitness'],
            ['desc' => 'Educacional Profissionalizante'],
            ['desc' => 'Entretenimento e Diversão'],
            ['desc' => 'Informática'],
            ['desc' => 'Idiomas'],
            ['desc' => 'Internet Marketing'],
            ['desc' => 'Investimento e Finanças'],
            ['desc' => 'Jurídico'],
            ['desc' => 'Marketing de Rede'],
            ['desc' => 'Moda e Vestuário'],
            ['desc' => 'Paquera e Relacionamentos'],
            ['desc' => 'Plugins, Widgets e Similares'],
            ['desc' => 'Produtos Infantis'],
            ['desc' => 'Religiões e crenças'],
            ['desc' => 'Saúde'],
            ['desc' => 'Turismo'],
            ['desc' => 'Sexologia e Sexualidade'],
            ['desc' => 'Outros Segmentos']
        ];

        foreach ($categories as $category) {
            Categorie::create($category);
        }

        
    }
}
