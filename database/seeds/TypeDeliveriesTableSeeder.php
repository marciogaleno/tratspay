<?php

use Illuminate\Database\Seeder;
use App\Models\Plataforma\TypeDeliverie;

class TypeDeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typeDeliveries = [
            ['desc' => 'Área de Membros'],
            //['desc' => 'Área de Membros da TratsClub'],
            ['desc' => 'Disponível para Baixar'],
            ['desc' => 'Correios / Transportadora'],
            ['desc' => 'Em mãos / Outros meios']
        ];

        foreach ($typeDeliveries as $delivery) {
            TypeDeliverie::create($delivery);
        }
        
    }
}
