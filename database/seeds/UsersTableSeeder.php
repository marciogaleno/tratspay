<?php

use Illuminate\Database\Seeder;
use App\Tenant;
use App\User;
use App\Models\Plataforma\Client;
use App\Models\Plataforma\Account;

class UsersTableSeeder extends Seeder
{
   /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tenant = Tenant::create([
            'email' => 'marcio@teste.com'
        ]);

        $user = User::create([
            'name' => 'Marcio Galeno',
            'email' => 'marcio@teste.com',
            'accept_terms' => 1,
            'password' => Hash::make('12345678'),
            'tenant_id' => $tenant->id
        ]);

        Client::create(['user_id'=>$user->id]);
        
        Account::create([
            'balance' => 0,
            'user_id'=> $user->id
        ]);
    }
}