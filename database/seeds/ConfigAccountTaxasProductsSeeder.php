<?php

use Illuminate\Database\Seeder;
use App\Src\Model\ConfigAccount;

class ConfigAccountTaxasProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConfigAccount::insert([
            [
                'key' => 'taxa_product_fisico',
                'value' => '6.9',
                'status' => ConfigAccount::STATUS_ENABLED
            ],
            [
                'key' => 'taxa_product_virtual',
                'value' => '9.9',
                'status' => ConfigAccount::STATUS_ENABLED
            ],
            [
                'key' => 'background_login',
                'value' => '',
                'status' => ConfigAccount::STATUS_ENABLED
            ]
        ]);
    }
}