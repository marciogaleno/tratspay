<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoAffiliatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('co_affiliates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 100);
            $table->enum('format_commission', ['percent', 'valuefixed']);
            $table->decimal('value_commission', 10, 2);
            $table->date('deadline')->nullable();
            $table->enum('status', ['A', 'I']);
            $table->string('code_affiliate', 12);
            $table->string('user_name', 100);
            $table->integer('user_id')->unsigned();
            $table->integer('product_id')->unsigned();           
            $table->integer('tenant_id')->nullable();          
            $table->softDeletes();            
            $table->timestamps();


            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('co_affiliates');
    }
}
