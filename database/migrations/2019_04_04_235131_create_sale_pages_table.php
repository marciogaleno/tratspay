<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 12)->unique();
            $table->enum('local_page_sale', ['PE', 'PI']); // PE: PÁGINA EXTERNA; PI:PÁGINA INTERNA;
            $table->text('url_page_sale')->nullable();
            $table->text('url_page_thank')->nullable();
            $table->text('url_page_print_ticket')->nullable();
            $table->string('title', 100);
            $table->text('desc', 100)->nullable();
            $table->integer('product_id')->unsigned();
            $table->integer('tenant_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_pages');
    }
}
