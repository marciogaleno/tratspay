<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postbacks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned();
            $table->enum('type', [1, 2]);
            $table->string('url', 250);
            $table->boolean('status')->default(1);
            $table->boolean('pending')->default(0);
            $table->boolean('refund')->default(0);
            $table->boolean('abandon_checkout')->default(0);
            $table->boolean('finished')->default(0);
            $table->boolean('blocked')->default(0);
            $table->boolean('canceled')->default(0);
            $table->boolean('completed')->default(0);
            $table->integer('tenant_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postbacks');
    }
}
