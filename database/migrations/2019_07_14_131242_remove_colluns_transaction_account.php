<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveCollunsTransactionAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('transaction_accounts', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('commission_id');
            $table->dropColumn('user_id');
            $table->dropColumn('updated_at');
            $table->dropColumn('created_at');
        });
        
        Schema::table('transaction_accounts', function (Blueprint $table) {
            $table->string('type', 20); // 'commission', 'system', 'transfer', 'refund', 'purchase', 'cashout;
            $table->string('type_transaction', 10); // 'IN', 'OUT'
            $table->integer('source_account')->nullable();
            $table->integer('destination_account')->nullable();
            $table->integer('commission_id')->nullable();
            $table->integer('transaction_id')->nullable();
            $table->integer('chashout_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_accounts');
    }
}
