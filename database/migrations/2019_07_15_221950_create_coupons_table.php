<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 10);
            $table->string('coupon', 30)->nullable();
            $table->boolean('active')->default(1);
            $table->decimal('price', 10, 2);
            $table->boolean('free')->default(0);
            $table->text('desc')->nullable();    
            $table->string('url_thank_you', 255)->nullable();    
            $table->string('url_ticket', 255)->nullable();    
            $table->dateTime('validity')->nullable();    
            $table->integer('max_purchase')->nullable();    
            $table->string('email_buyer')->nullable();    
            $table->boolean('counter')->default(0);   
            $table->string('text_counter')->nullable();    
            $table->string('background_counter', 6)->nullable();    
            $table->string('color_text_counter', 6)->nullable();                
            $table->time('time_counter')->nullable();                
            $table->boolean('take_interest')->default(0);
            $table->integer('plan_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->softDeletes();               
            $table->timestamps();

            $table->foreign('plan_id')
            ->references('id')
            ->on('plans')
            ->onDelete('cascade');

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
