<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->boolean('accept_terms')->default(1);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('image')->nullable();
            $table->string('password');
            $table->uuid('token');
            $table->integer('tenant_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('tenant_id')->references('id')->on('tenants')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_indications');
        Schema::dropIfExists('transaction_accounts');
        Schema::dropIfExists('accounts');
        Schema::dropIfExists('checkouts');
        Schema::dropIfExists('clicks');
        Schema::dropIfExists('plans');
        Schema::dropIfExists('config_affiliations');
        Schema::dropIfExists('url_alternatives');
        Schema::dropIfExists('campaigns');
        Schema::dropIfExists('sale_pages');
        Schema::dropIfExists('files');
        Schema::dropIfExists('co_producers');
        Schema::dropIfExists('media');
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('type_deliveries');
        Schema::dropIfExists('coupons');
        Schema::dropIfExists('users');
        
    }
}
