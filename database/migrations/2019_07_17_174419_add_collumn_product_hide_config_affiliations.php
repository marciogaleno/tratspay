<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumnProductHideConfigAffiliations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_affiliations', function($table) {
            $table->boolean('product_hide')->default(0);// ocultar o produto na pagina de mercado
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_affiliations', function($table) {
            $table->dropColumn('product_hide');
        });
       
    }
}
