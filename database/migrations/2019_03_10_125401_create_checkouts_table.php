<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkouts', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('default')->default(0);
            $table->string('code', 12)->unique();
            $table->string('desc', 100)->nullable();
            $table->string('invoice_desc', 12)->nullable();
            $table->boolean('pay_card')->default(1)->nullable();
            $table->boolean('pay_ticket')->default(1)->nullable();
            $table->boolean('request_address')->default(0)->nullable();
            $table->integer('days_expir_ticket')->default(3)->nullable();
            $table->boolean('request_cpf')->default(0)->nullable();
            $table->boolean('request_phone')->default(0)->nullable();
            $table->integer('plan_id')->unsigned();
            $table->integer('tenant_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('plan_id')
            ->references('id')
            ->on('plans')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkouts');
    }
}
