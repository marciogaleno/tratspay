<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('value', 10, 2);
            $table->enum('type', ['IN', 'OUT']);
            $table->integer('bank_account_id')->nullable();
            $table->enum('status', ['finalized', 'processing']);
            $table->integer('user_id');
            $table->integer('commission_id');
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->foreign('commission_id')
            ->references('id')
            ->on('commissions')
            ->onDelete('cascade');

            $table->foreign('bank_account_id')
            ->references('id')
            ->on('bank_account')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_accounts');
    }
}
