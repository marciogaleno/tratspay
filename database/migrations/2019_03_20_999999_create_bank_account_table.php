<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('bank_account', function (Blueprint $table) {
            $table->increments('id');
            //MAIN
            $table->integer('user_id')->nullable();
            $table->integer('type')->nullable();
            $table->enum('type_people', ['PF', 'PJ']);
            $table->integer('bank')->nullable();
            $table->integer('bank_branch')->nullable();
            $table->integer('bank_branch_digit')->nullable();
            $table->integer('bank_account')->nullable();
            $table->integer('bank_account_digit')->nullable();
            $table->integer('variation')->nullable();
            $table->string('attachment1')->nullable();
            $table->string('attachment2')->nullable();
            $table->integer('tenant_id')->unsigned();
            $table->integer('status')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('bank_account');
    }

}
