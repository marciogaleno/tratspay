<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProducersAffiliates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producer_affiliate', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 12);
            $table->integer('user_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->enum('relation_type', ['producer', 'affiliate']);
            $table->dateTime('request_date');
            $table->dateTime('approval_date')->nullable();
            $table->enum('status', ['A', 'I']);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producer_affiliate');
    }
}
