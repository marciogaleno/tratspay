<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 12)->unique();
            $table->string('name', 100);
            $table->boolean('default')->default(0);
            $table->decimal('price', 10, 2)->nullable();
            $table->integer('quantity')->nullable();
            $table->text('desc')->nullable();
            $table->enum('frequency', ['UNI', 'WEE', 'MON', 'BIM', 'QUA', 'SEM', 'YEA'])->nullable();
            $table->enum('first_installment', [1, 2, 3, 4])->nullable(); // 1 IGUAS AS DEAMIS 2: GRÁTIS - 3: PRECO DIFERENCIADO - 4: GRÁTIS POR UM PERÍOD
            $table->integer('free_days')->nullable();
            $table->decimal('different_value', 10, 2)->nullable();
            $table->decimal('amount_recurrence', 10, 2)->nullable();
            $table->boolean('free')->nullable();
            $table->boolean('active')->default(1);
            $table->integer('product_id')->unsigned();
            $table->integer('tenant_id')->unsigned()->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planos');
    }
}
