<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigAffiliationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_affiliations', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('accept_affiliation')->default(0);
            $table->enum('format_commission', ['P', 'V'])->nullable(); // TIPO DE COMISSÃO PARA O AFILIADO P: POCENTAGEM, V: VALOR FIXO
            $table->decimal('value_commission', 10, 2)->nullable();
            $table->integer('expiration_coockie')->nullable();
            $table->text('desc_affiliate')->nullable();
            $table->boolean('producer_refund')->default(0); // Só produto paga reembolo ou produtor mais afiliado
            $table->boolean('afil_link_chk')->default(1); // oculta dado do cliente para o afiliado
            $table->boolean('hide_data_customer')->default(0); // Afiliado pode ter link do checkout direto ou não
            $table->boolean('coupon')->default(0);// Afiliado pode criar cupons de desconto
            $table->enum('assignment_type', ['1', '2', '3', '4'])->nullable();
            $table->integer('product_id')->unsigned();
            $table->integer('tenant_id')->unsigned()->nullable();
            $table->timestamps();

                       
            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_afiliacaos');
    }
}
