CREATE OR REPLACE FUNCTION TRANSFER_CURRENT_ACCOUNT()
RETURNS trigger
AS $$
	DECLARE 
		comission  RECORD;				
    BEGIN
	    IF (NEW.status = 'completed') THEN
		   
		   	FOR comission in select b.* from transaction_mp a
						inner join commissions b on b.transaction_id = a.id
						where a.transaction_id = new.transaction_id
		 	LOOP
		   	
		   			insert into transaction_accounts (value, type, status, user_id, commission_id, created_at, updated_at) values (comission.value, 'IN', 'finalized', comission.user_id, comission.id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP );
		   			update accounts set balance = balance + comission.value where user_id = comission.user_id;
		   	END LOOP;
	 
	 END IF;
	
	RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER TRG_TRANSFER_CURRENT_ACCOUNT AFTER UPDATE ON transaction_mp FOR EACH ROW EXECUTE PROCEDURE TRANSFER_CURRENT_ACCOUNT();