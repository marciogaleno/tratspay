CREATE OR REPLACE PROCEDURE UPDATE_TRANSACTION_COMPLETE()
LANGUAGE plpgsql    
AS $$
declare
	transcation_tmp  RECORD;
	transactions  CURSOR FOR  select * from transaction_mp a where a.end_warranty < NOW() and a.status = 'finished';
BEGIN
    open transactions;
   
   loop
   		 FETCH transactions INTO transcation_tmp;
   			EXIT WHEN NOT FOUND;
   		
   			update transaction_mp set status = 'completed' where transaction_id = transcation_tmp.transaction_id;
   end loop;
   
   close transactions;
END;
$$;