ALTER TABLE products DROP CONSTRAINT products_product_type_check;
  
ALTER TABLE products ADD CONSTRAINT products_product_type_check CHECK (product_type::TEXT = ANY (ARRAY['V'::CHARACTER VARYING, 'F'::CHARACTER varying, 'P'::CHARACTER varying, 'p'::CHARACTER VARYING]::TEXT[]))