<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->unique();
            $table->enum('product_type', ['V', 'F']);
            $table->string('name', 100);
            $table->text('desc')->nullable();
            $table->integer('warranty');
            $table->enum('payment_type', ['SINGPRICE', 'PLAN']);
            $table->string('email_support', 100);
            $table->boolean('several_purchases')->default(0); // Permite várias compras por cliente
            $table->integer('max_item_by_order')->unsigned()->default(1); // quantidade de item por compra
            $table->boolean('active')->default(1);
            $table->boolean('approved')->default(0);
            $table->text('image', 250)->nullable();
            $table->integer('category_id')->unsigned();
            $table->integer('type_deliverie_id')->unsigned();
            $table->integer('tenant_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('type_deliverie_id')
            ->references('id')
            ->on('type_deliveries')
            ->onDelete('cascade');

            $table->foreign('category_id')
            ->references('id')
            ->on('categories')
            ->onDelete('cascade');
        });

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
