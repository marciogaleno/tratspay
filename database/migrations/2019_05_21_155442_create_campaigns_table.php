<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 12);
            $table->string('name');
            $table->enum('type_url', [1, 2, 3]);
            $table->string('checkout_code', 12)->nullable();
            $table->string('url_alternative_code', 12)->nullable();
            $table->enum('pixel', ['fb1', 'fb2', 'adw', 'bing']);
            $table->string('pixel_id');
            $table->string('label', 15)->nullable();
            $table->decimal('value', 10, 2)->nullable();
            $table->boolean('exec_ticket')->default(false)->nullable();
            $table->boolean('exec_checkout')->default(false)->nullable();
            $table->integer('product_id')->unsigned();
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
