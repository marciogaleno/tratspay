<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoproducersPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coproducers_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('coproducer_id');
            $table->integer('plan_id');
            $table->integer('product_id');
            $table->float('value')->default(0);
            $table->string('type');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coproducers_plans');
    }
}
