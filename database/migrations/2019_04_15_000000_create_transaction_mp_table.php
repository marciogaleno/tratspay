<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionMpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_mp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('transaction_id');
            $table->string('gateway', 20);
            $table->datetime('date_created');
            $table->datetime('date_approved')->nullable();
            $table->string('operation_type');
            $table->string('payment_method_id');
            $table->string('payment_type_id');
            $table->string('last_four_digits', 4);
            $table->integer('expiration_month');
            $table->integer('expiration_year');
            $table->string('status_gateway');
            $table->string('status_detail_gateway');
            $table->string('product_description');
            $table->string('captured')->nullable();
            $table->string('installments');
            $table->string('shipping_amount')->nullable();
            $table->string('external_reference')->unique();
            $table->decimal('transaction_amount', 10, 2)->nullable();
            $table->integer('quantity')->nullable();
            $table->string('external_resource_url')->nullable();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('cpf_cnpj');
            $table->string('phone')->nullable();
            $table->string('zipcode')->nullable();
            $table->string('address')->nullable();
            $table->string('address_number')->nullable();
            $table->string('address_complement')->nullable();
            $table->string('address_district')->nullable();
            $table->string('address_city')->nullable();
            $table->string('address_state')->nullable();
            $table->string('address_country')->nullable();

            $table->string('traking_code')->nullable();
            $table->integer('checkout_id')->nullable();
            $table->string('checkout_code', 12)->nullable();
            $table->integer('product_id')->unsigned();
            $table->uuid('product_uuid')->nullable();
            $table->string('product_name', 100)->nullable();
            $table->string('product_type', 1)->nullable();
            $table->string('email_support', 100)->nullable();
            $table->integer('plan_id')->nullable();
            $table->string('plan_code', 12)->nullable();
            $table->string('plan_name', 100)->nullable();        
            $table->decimal('plan_price', 10, 2)->nullable();        
            $table->integer('plan_quantity')->nullable();        
            $table->string('plan_frequency', 10)->nullable();        
            $table->decimal('rate_percent', 10, 2)->nullable();
            $table->decimal('fixed_rate', 10, 2)->nullable();
            $table->decimal('rate_total', 10, 2)->nullable();
            $table->integer('warranty')->nullable();
            $table->dateTime('end_warranty')->nullable();
            $table->string('status', 20);//pending', 'finished', 'canceled', 'blocked', 'complete
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_mp');
    }
}
