<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->integer('read')->default(0);
            $table->integer('user_id');
            $table->integer('product_id');
            $table->enum('type', ['sale', 'refund']);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('cpf_cnpj');
            $table->string('phone')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
