<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('value', 10, 2);
            $table->decimal('percent', 10, 2)->nullable();
            $table->decimal('fixed_rate', 10, 2)->nullable();
            $table->decimal('value_transaction', 10, 2)->nullable();
            $table->char('assignment_type')->nullable();
            $table->string('commission_type', 20);
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('product_id')->unsigned();
            $table->string('reference_code')->nullable();
            $table->jsonb('source')->nullable();
            $table->integer('transaction_id')->unsigned();           
            $table->timestamps();

            $table->foreign('product_id')
            ->references('id')
            ->on('products')
            ->onDelete('cascade');

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
