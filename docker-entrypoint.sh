#!/bin/bash

if [ ! -d vendor ]; then
  composer -n install
fi

if [ ! -f /var/www/html/.env ]; then
  cp /var/www/html/.env-example /var/www/html/.env
  php artisan key:generate
fi

php artisan migrate

php artisan db:seed