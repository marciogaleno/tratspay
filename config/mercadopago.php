<?php

/*
|--------------------------------------------------------------------------
| MERCADOPAGO Credenciais
|--------------------------------------------------------------------------
*/

$producao = env('MERCADOPAGO_PRODUCAO', false);

if ($producao) {
    // PRODUÇÃO
    return [
        'credencial' => [
            'public_key' => 'APP_USR-e4befee0-9e23-4920-9ead-34c1afa9ed85',
            'access_token' => 'APP_USR-6111661243509728-072001-6b802343c06510078d696f09d0126f26-419846982'
        ],
    ];
} else {
    // SANDBOX
    return [
        'credencial' => [
            'public_key' => 'TEST-d6261acb-6ef2-4be8-9472-ba6e80949192',
            'access_token' => 'TEST-6111661243509728-072001-eb4987129d0a6e180b7fde4546aca62f-419846982'
        ],
    ];
}



