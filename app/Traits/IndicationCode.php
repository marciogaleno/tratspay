<?php

namespace App\Traits;


trait IndicationCode
{
    public static function bootIndicationCode()
    {
        static::creating(function ($model) {
            $model->indication_code = strtoupper(bin2hex(openssl_random_pseudo_bytes(5)));
        });
    }
}