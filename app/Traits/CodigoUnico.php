<?php

namespace App\Traits;


trait CodigoUnico
{
    public static function bootCodigoUnico()
    {
        static::creating(function ($model) {
            $model->code = strtoupper(bin2hex(openssl_random_pseudo_bytes(5)));

            if ($model->id) {
                $model->id = strtoupper(bin2hex(openssl_random_pseudo_bytes(5)));
            }
        });
    }
}