<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait GenerateId
{
    public static function bootGenerateId()
    {
        static::creating(function ($model) {
            $model->id = strtoupper(Str::random());    
        });
    }
}