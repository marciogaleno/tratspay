<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait Token
{
    public static function bootToken()
    {
        static::creating(function ($model) {
            $model->token = (string) Str::uuid();
        });
    }
}