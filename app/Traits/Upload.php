<?php
namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Upload
{
    public function saveImage(UploadedFile $file, $path, $disk = 'images')
    {
        $filename = Storage::disk($disk)->put($path, $file);
        if ($filename) {
            return $filename;
        }

        return null;
    }
}