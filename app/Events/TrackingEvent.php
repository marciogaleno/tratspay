<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrackingEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $data;
    private $params;
    private $trackingCode;
    private $ip;
    private $typeUrl;
    private $codeUrl;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $params, $trackingCode, $ip, $typeUrl, $codeUrl)
    {
        $this->data = $data;
        $this->params = $params;
        $this->trackingCode = $trackingCode;
        $this->ip = $ip;
        $this->typeUrl = $typeUrl;
        $this->codeUrl = $codeUrl;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function getData()
    {
        return $this->data;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    public function getIp()
    {
        return  $this->ip;
    }

    public function getTypeUrl()
    {
        return $this->typeUrl;
    }

    public function getCodeUrl()
    {
        return $this->codeUrl;
    }
}
