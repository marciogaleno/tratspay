<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Plataforma\TransactionMp;

class ProductSold
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $transaction;
    public $affiliateId;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(TransactionMp $transaction, $affiliateId = null)
    {
        $this->transaction = $transaction;
    }

    public function getTransaction() : TransactionMp
    {
        return $this->transaction;
    }

    public function getAffiliateId()
    {
        return $this->affiliateId;
    }
    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
