<?php

namespace App\Repository;

use App\Models\Plataforma\Commission;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Messages;
use App\Repository\BaseRepository;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;

class ReportRepository extends BaseRepository
{
    public function getSalesAffiliate($request)
    {

        if (isset($request->product_id) && !empty($request->product_id)) {
            $productIds = implode(",", $request->product_id);
        } else {
            $productIds = '0';
        }

        //data_inicio
        //data_fim
        $sales = DB::select(
            DB::raw(
                "SELECT C.id, C.name,SUM(A.value) totaldesconto, SUM(A.value_transaction) totalvenda  FROM  commissions A 
                inner join transaction_mp B 
                ON B.id = A.transaction_id 
                inner join users C 
                ON C.id = A.user_id 
                WHERE  B.status IN ( 'finished','completed' ) 
                AND A.commission_type = 'affiliate'
                AND A.PRODUCT_ID IN ($productIds)
                GROUP  BY C.id, C.name"
            )
        );

        return $this->paginateForce($sales, $request);
    }

    public function getSalesIndication($request)
    {
        $userId = Auth::user()->id;

        $sql = "SELECT DISTINCT D.id as code_transaction, A.*, C.*, D.*
                FROM COMMISSIONS A, COMMISSIONS B, USERS C, TRANSACTION_MP D
                WHERE A.USER_ID = {$userId}
                AND A.commission_type = 'indication'
                AND B.user_id = C.user_indication
                AND A.ID = B.ID
                AND A.transaction_id = D.ID
                AND D.STATUS IN ('finished', 'completed')
                AND EXISTS (
                    SELECT * FROM COMMISSIONS X WHERE X.USER_ID = C.ID AND X.ID = X.ID
                )";


        //-----filters------
        if(!empty($request['transaction_id']) && isset($request['transaction_id'])) {
            $sql .= "AND D.ID =".$request['transaction_id'];
        }

        if(!empty($request['product_name']) && isset($request['product_name'])) {
            $sql .= "AND D.product_id IN(".implode(',', $request->product_name).")";
        }

        if(!empty($request['status']) && isset($request['status']) ) {
            //error
            $sql .= "AND D.status IN(".implode(',', $request->status).")";
        }

        if (!empty($request->indication_id && isset($request->indication_id))) {
            //testar
            $sql .= "AND C.ID IN (".implode(',', $request->indication_id).")";
        }

        if(!empty($request['form_payment']) && isset($request['form_payment'])&& $request['form_payment'] != 'todos') {
            //testar
            $formPayment = $request['form_payment'];
        }

        //data_inicio
        //data_fim
        //cpf
        //email comprador
        //nome comprador

        //-----end filters------

        $sales = DB::select(
            DB::raw($sql)
        );

        return $this->paginateForce($sales, $request);
    }

    public function getSalesCoproducer($request)
    {
        if (isset($request->product_id) && !empty($request->product_id)) {
            $productIds = implode(",", $request->product_id);
        } else {
            $productIds = '0';
        }

        $sales = DB::select(
            DB::raw(
                "SELECT C.id, C.name,SUM(A.value) totaldesconto, SUM(A.value_transaction) totalvenda  FROM  commissions A 
                inner join transaction_mp B 
                ON B.id = A.transaction_id 
                inner join users C 
                ON C.id = A.user_id 
                WHERE  B.status IN ( 'finished','completed' ) 
                AND A.commission_type = 'coproducer'
                AND A.PRODUCT_ID IN ($productIds)
                GROUP  BY C.id, C.name"
            )
        );

        return $this->paginateForce($sales, $request);
    }

    public function getSalesCoAffiliate($request)
    {
        $sales = DB::select(
            DB::raw(
                "SELECT C.id, 
                       C.name, 
                       SUM(A.value) VENDAS 
                FROM   commissions A 
                       inner join transaction_mp B 
                               ON B.id = A.transaction_id 
                       inner join users C 
                               ON C.id = A.user_id 
                WHERE  B.status IN ( 'finished', 'completed' ) 
                       AND A.commission_type = 'coaffiliate' 
                       AND A.product_id in (SELECT X.product_id 
                                               FROM   co_producers X 
                                               WHERE  x.product_id = a.product_id
                                               and x.type = 'coproducer')
                GROUP  BY C.id, 
                          C.name"
            )
        );
//AND X.product_id IN (". implode(    ',', $request["product_id"]).")
        return $this->paginateForce($sales, $request);
    }

    public function getSalesManager($request)
    {
        if (isset($request->product_id) && !empty($request->product_id)) {
            $productIds = implode(",", $request->product_id);
        } else {
            $productIds = '0';
        }
        
        $sales = DB::select(
            DB::raw(
                "SELECT C.id, C.name,SUM(A.value) totaldesconto, SUM(A.value_transaction) totalvenda  FROM  commissions A 
                inner join transaction_mp B 
                ON B.id = A.transaction_id 
                inner join users C 
                ON C.id = A.user_id 
                WHERE  B.status IN ( 'finished','completed') 
                AND A.commission_type = 'manager'
                AND A.PRODUCT_ID IN ($productIds)
                GROUP  BY C.id, C.name"
            )
        );

        return $this->paginateForce($sales, $request);
    }
    public function paginateForce($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }
}
//
//SELECT C.id,
//                       C.name,
//                       SUM(A.value) VENDAS
//                FROM   commissions A
//                       inner join transaction_mp B
//                               ON B.id = A.transaction_id
//                       inner join users C
//                               ON C.id = A.user_id
//                WHERE   A.commission_type = 'manager'
//AND A.reference_code IN (SELECT X.code
//                                               FROM   producer_affiliate X
//                                               WHERE  X.user_id = ".Auth::user()->id."
//AND X.product_id IN (". implode(    ',', $request["product_id"]).")
//                                                      AND X.relation_type = 'producer')
//                GROUP  BY C.id,
//                          C.name