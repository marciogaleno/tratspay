<?php

namespace App\Repository;

use App\Models\Plataforma\Product;
use App\Models\Plataforma\TransactionAccount;
use App\Repository\BaseRepository;
use DB;


class MarketRepository extends BaseRepository
{

    public function __construct(Product $model, TransactionAccount $transactionAccount)
    {
        $this->model = $model;
        $this->transactionAccount = $transactionAccount;

    }

    public function getFiltered($filtros)
    {

        $data = Product::selectRaw("DISTINCT(products.id),
               products.name,
               COUNT(*) vendas,
               products.product_type,
               products.category_id,
               products.id,
               products.image,
               products.payment_type"
            )
            ->leftjoin('transaction_mp', function ($join) {
                $join->on('products.id', '=', 'transaction_mp.product_id')
                    ->where('transaction_mp.status', '=', 'finished');
            })
            ->join('config_affiliations', function ($join) {
                $join->on('products.id', '=', 'config_affiliations.product_id')
                    ->where('config_affiliations.product_hide', '=', false);
            })
            ->groupBy('products.id')
            ->orderBy('vendas', 'desc');

        if(!empty($filtros['product_type']) && isset($filtros['product_type'])) {
            $data = $data->where('products.product_type',$filtros['product_type']);
        }
        if(!empty($filtros['category_id']) && isset($filtros['category_id'])) {
            $data = $data->where('products.category_id', $filtros['category_id']);
        }

        if(!empty($filtros['name']) && isset($filtros['name'])) {
            $data = $data->where('products.name', 'ILIKE', '%' . $filtros['name'] . '%');
        }

        return $data->paginate(8);
    }

    public function getFilteredLast($filtros)
    {
        $data = $this->model->join('config_affiliations', function ($join) {
            $join->on('products.id', '=', 'config_affiliations.product_id')
                ->where('config_affiliations.product_hide', '=', false);
        })
        ->select('products.*');
       
        if(!empty($filtros['product_type']) && isset($filtros['product_type'])) {
            $data = $data->where('products.product_type',$filtros['product_type']);
        }
        if(!empty($filtros['category_id']) && isset($filtros['category_id'])) {
            $data = $data->where('category_id', $filtros['category_id']);
        }
        if(!empty($filtros['name']) && isset($filtros['name'])) {
            $data = $data->where('products.name', 'ILIKE', '%' . $filtros['name'] . '%');
        }

        return $data->orderBy('products.id', 'desc')->paginate(8);
    }

}