<?php

namespace App\Repository\Plataforma;

use App\Repository\BaseRepository;
use DB;
use App\User;
use App\Models\Plataforma\TrackingIndication;
use Illuminate\Support\Carbon;
use App\Tenant;
use Illuminate\Support\Facades\Hash;
use App\Models\Plataforma\Account;
use App\Models\Plataforma\Client;

class UserRepository extends BaseRepository
{
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create($data)
    {    
        try {
            DB::beginTransaction();

            $tenant = Tenant::create([
                'email' => $data['email']
            ]);
            
            $dataUser = [
                'name' => $data['name'],
                'email' => $data['email'],
                'accept_terms' => $data['accept_terms'],
                'password' => Hash::make($data['password']),
                'tenant_id' => $tenant->id
            ];

            $userWhoIndicated =  $this->getUserWhoIndicated();

            if ($userWhoIndicated) {
                $dataUser['user_indication'] = $userWhoIndicated->id;
                $dataUser['indication_end'] = date('Y-m-d H:i:s', strtotime('+1 year',strtotime('now')));
            }

            $user =  User::create($dataUser);
        
            DB::commit();
            return $user;
        } catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

        return false;
        
    }

    public function getUserWhoIndicated()
    {
        $track_indication = null;
        $track_indication = request()->cookie('track_indication');

        if ($track_indication) {
            $trackinIndicated = TrackingIndication::where('tracking_code', $track_indication)
                            ->orderBy('updated_at', 'desc')
                            ->first();
            
            if (!$trackinIndicated) 
                return 0;

            $date = Carbon::parse($trackinIndicated->updated_at);
            $now = Carbon::now();
            $diffDays = $date->diffInDays($now);
            // Indicação deve está entre 3 dias
            if ($diffDays >= 0 && $diffDays <= 3) {
                $user = $this->model->where('indication_code', $trackinIndicated->indication_code)->first();
                return $user;
            }
        }

        return null;
    }
    

}