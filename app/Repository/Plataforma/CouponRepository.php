<?php

namespace App\Repository\Plataforma;

use App\Models\Plataforma\Coupon;
use App\Repository\BaseRepository;
use DB;
use Illuminate\Support\Facades\Auth;

class CouponRepository extends BaseRepository
{

    public function __construct(Coupon $model)
    {
        $this->model = $model;
    }

    public function create($data)
    {
        try {
            $this->model->create($data);
            return true;
        } catch (\Illuminate\Database\QueryException $ex) {
            throw $ex;
            return false;
        }
    }

    public function update($id, $data)
    {
        $this->model = $this->model::where('id', $id)->first();
        
        try {
            $this->model->update($data);
            return true;
        } catch (\Illuminate\Database\QueryException $ex) {
            throw $ex;
            return false;
        }
    }

    public function delete($id)
    {
        $this->model = $this->model::where('id', $id)->first();
        
        try {
            $this->model->delete();
            return true;
        } catch (\Illuminate\Database\QueryException $ex) {
            throw $ex;
            return false;
        }
    }


}