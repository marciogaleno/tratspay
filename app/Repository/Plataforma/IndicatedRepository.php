<?php

namespace App\Repository\Plataforma;

use App\Models\Plataforma\Commission;
use App\Repository\BaseRepository;
use DB;
use Illuminate\Support\Facades\Auth;

class IndicatedRepository extends BaseRepository
{

    public function __construct(Commission $model)
    {
        $this->model = $model;
    }

    public function getFiltered($filtros)
    {
        $data = $this->model->join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
//            ->with('transaction','transaction.commissions')
            ->where('user_id', Auth::user()->id);

        if(!empty($filtros['transaction_id']) && isset($filtros['transaction_id'])) {
            $data = $data->where('commissions.id', $filtros['transaction_id']);
        }

        if(!empty($filtros['form_payment']) && isset($filtros['form_payment'])&& $filtros['form_payment'] != 'todos') {
            $data = $data->where('transaction_mp.payment_type_id', $filtros['form_payment']);
        }

        if(!empty($filtros['product_name']) && isset($filtros['product_name'])) {
            $data = $data->whereIn('products.id', $filtros['product_name']);
        }

        if(!empty($filtros['status']) && isset($filtros['status']) ) {
            $data = $data->whereIn('status',$filtros['status']);
        }

        
//        if(!empty($filtros['co_affiliates']) && isset($filtros['co_affiliates']) ) {
//            $data = $data->whereIn('co_affiliates',$filtros['co_affiliates']);
//        }

//        if(!empty($filtros['origem_venda']) && isset($filtros['origem_venda'])) {
//            $data = $data->where('origem_venda', $filtros['origem_venda']);
//        }

        if(!empty($filtros['data_pedido']) && isset($filtros['data_pedido'])) {
            $data = $data->whereDate('transaction_mp.created_at', $filtros['data_pedido']);
        }

//        como temos a data de finalizada?
//        if(!empty($filtros['data_finalizada']) && isset($filtros['data_finalizada'])) {
//            $data = $data->whereDate('transaction_mp.created_at', $filtros['data_finalizada']);
//        }

        return $data->paginate(10);
    }

    public function create($data)
    {
        try {
            return $this->model->create($data);
        } catch (\Illuminate\Database\QueryException $ex) {
            return false;
        }
        
    }
    

}