<?php

namespace App\Repository;

use App\Models\Plataforma\Product;
use App\Models\Plataforma\Messages;
use App\Repository\BaseRepository;
use DB;
use Auth;
use App\User;

class TalkAffiliatesRepository extends BaseRepository
{

    public function __construct(Messages $messages)
    {
        $this->messages = $messages;

    }

    public function existsAffiliate($request)
    {
        $affiliates = DB::select(
            "select distinct a.user_id from producer_affiliate a
            join products p on p.id = a.product_id 
            join users u on u.id =  a.user_id
            where a.status = 'A'
            and a.product_id IN (".implode(",", $request['product_id']).")
            and a.relation_type = 'affiliate'"
        );

        if(!count($affiliates)) {
            return false;
        }

        return $affiliates;
    }

    public function sendMessages($affiliates, $message)
    {
        $itens = [];

        foreach ($affiliates as $affiliate) {
            $item['content'] = $message;
            $item['user_id'] = Auth::user()->id;
            $item['id_affiliate'] = $affiliate->user_id;
//            $item['reference_code'] = $affiliate->code;
            $item['created_at'] = date('Y-m-d H:i:s');
            $itens[] = $item;
        }

        if(!Messages::insert($itens)){
            return false;
        }

        return true;
    }

    public function getMessages($filtros)
    {
        //$user->products()->where('relation_type', 'affiliate')->get()
        $messages = $this->messages->where('id_affiliate', Auth::user()->id);

        if(!empty($filtros['content']) && isset($filtros['content'])) {
            $messages = $messages->where('content', 'ilike', '%'.$filtros['content'].'%');
        }

        if(isset($filtros['trash'])) {
            $messages = $messages->where('trash', 1);
        }else{
            $messages = $messages->where('trash', 0);
        }

        return $messages->paginate(10);
    }

    public function getMessagesApi()
    {
        return $this->messages->join('users', 'users.id', 'id_affiliate')
            ->where('id_affiliate', Auth::user()->id)
            ->orderBy('messages.id', 'desc')
            ->where('trash', 0)
            ->select('messages.*', 'users.name', 'users.image')
            ->limit(5)
            ->get();
    }
}