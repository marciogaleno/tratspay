<?php

namespace App\Repository;

use App\Models\Plataforma\Product;
use App\Models\Plataforma\TransactionAccount;
use App\Repository\BaseRepository;
use DB;
use Auth;
use App\User;

class MyAffiliatesRepository extends BaseRepository
{
    public function getFiltered($filtros)
    {
        $user = User::find(Auth::user()->id);
        $ids = [];

        $productsProducer = DB::select(
            "SELECT distinct p.id
            FROM products as p 
            INNER JOIN producer_affiliate as a ON p.id = a.product_id
            WHERE a.user_id = ". $user->id."
              AND a.relation_type = 'producer'"
        );

        foreach ($productsProducer  as $item) {
            $ids[] = $item->id;
        }

        $sql = "SELECT DISTINCT u.*,
                  (SELECT COUNT(*)
                   FROM commissions
                   WHERE a.user_id = commissions.user_id
                     AND a.product_id = commissions.product_id) AS total
                FROM users u
                INNER JOIN producer_affiliate a ON a.user_id = u.id
                WHERE a.product_id IN (".implode(',',$ids).")
                  AND a.relation_type = 'affiliate'
                  AND a.status = 'A'";

        if(isset($filtros['affiliates']) && $filtros['affiliates'] != 'ALL_AFFILIATES' ) {

            if ($filtros['affiliates'] == 'SALES_AFFILIATES') {
                $sql .= "AND EXISTS (SELECT 1 FROM commissions b WHERE a.user_id = b.user_id AND a.product_id = b.product_id)";
            }
            if ($filtros['affiliates'] == 'SALES_WITHOUT_AFFILIATES') {
                $sql .= "AND NOT EXISTS (SELECT 1 FROM commissions b WHERE a.user_id = b.user_id AND a.product_id = b.product_id)";
            }
        }

        if(!empty($filtros['product_id']) && isset($filtros['product_id'])) {
            $sql .= "AND product_id = ".$filtros['product_id'];
        }


        if(isset($filtros['affiliates_sub'])) {
            if($filtros['affiliates_sub']  == 'NAME') {
                $sql .= "order by u.name asc";
            }

            if($filtros['affiliates_sub']  == 'MORE_SALES') {
                $sql .= "order by total desc";
            }

            if($filtros['affiliates_sub']  == 'LESS_SALES') {
                $sql .= "order by total asc";
            }
        }

        $data = db::SELECT($sql);

        return $data;
    }
}