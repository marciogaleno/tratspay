<?php

namespace App\Repository;

use App\Models\Plataforma\Click;
use App\Models\Plataforma\Commission;
use App\Repository\BaseRepository;
use DB;
use Illuminate\Support\Facades\Auth;
use \Carbon\Carbon;

class SalesRepository extends BaseRepository
{

    public function __construct(Commission $model)
    {
        $this->model = $model;
    }

    public function getFiltered($filtros)
    {
        $data = $this->model->join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
//            ->join('clients', 'clients.user_id', '=', 'commissions.user_id')
//            ->with('transaction','transaction.commissions')
            ->where('commissions.user_id', Auth::user()->id)
            ->where('transaction_mp.gateway', 'gerencianet')
            ->select('transaction_mp.id as code_transaction', 'transaction_mp.*', 'commissions.*', 'products.*');

        if(!empty($filtros['transaction_id']) && isset($filtros['transaction_id'])) {
            $data = $data->where('commissions.transaction_id', $filtros['transaction_id']);
        }

        if(!empty($filtros['form_payment']) && isset($filtros['form_payment'])&& $filtros['form_payment'] != 'todos') {
            $data = $data->where('transaction_mp.payment_type_id', $filtros['form_payment']);
        }
        if(!empty($filtros['product_name']) && isset($filtros['product_name'])) {
            $data = $data->whereIn('products.id', $filtros['product_name']);
        }

        if(!empty($filtros['status_transaction']) && isset($filtros['status_transaction']) ) {
            $data = $data->whereIn('transaction_mp.status',$filtros['status_transaction']);
        }


        if(!empty($filtros['affiliates']) && isset($filtros['affiliates'])) {

            /*$data>whereExists(function ($query) {
                $query->select(DB::raw(1))
                      ->from('commissions')
                      ->whereRaw('orders.user_id = users.id');
            })*/
            $data = $data->whereRaw(DB::raw("EXISTS ( SELECT * FROM COMMISSIONS X WHERE  X.commission_type = 'affiliate' and X.transaction_id = COMMISSIONS.transaction_id and X.USER_ID IN (".implode(",", $filtros['affiliates'])."))"));
        }

        if(!empty($filtros['origem_venda']) && isset($filtros['origem_venda']) && $filtros['origem_venda'] != 'todos') {
            $data = $data->where('transaction_mp.payment_type_id', $filtros['origem_venda']);
        }

        if((!empty($filtros['data_pedido']) && isset($filtros['data_pedido']))
            && (!empty($filtros['data_finalizada']) && isset($filtros['data_finalizada']))) {
            $data = $data->whereBetween('transaction_mp.date_created', [$filtros['data_pedido'], $filtros['data_finalizada']]);
        }

        if(!empty($filtros['vendashoje']) && isset($filtros['vendashoje'])) {
            $data = $data->whereDate('transaction_mp.date_created', Carbon::now()->format('Y-m-d'))->whereIn('transaction_mp.status',['finished','completed']);
        }

        if(!empty($filtros['vendasmes']) && isset($filtros['vendasmes'])) {
            $data = $data->whereMonth("transaction_mp.date_created",'=',Carbon::now()->format('m'))->whereIn('transaction_mp.status',['finished','completed']);
        }

        if(!empty($filtros['boletosimpressos']) && isset($filtros['boletosimpressos'])) {
            $data = $data->whereDate('transaction_mp.date_created', Carbon::now()->format('Y-m-d'))->where('transaction_mp.payment_type_id', 'ticket');
        }

        if(!empty($filtros['boletospendentes']) && isset($filtros['boletospendentes'])) {
            $data = $data->where('transaction_mp.payment_type_id', 'ticket')->where('transaction_mp.status','pending');
        }

        if(!empty($filtros['cpf_cnpj']) && isset($filtros['cpf_cnpj'])) {
            $data = $data->where('cpf_cnpj', '=', $filtros['cpf_cnpj']);
        }

        if(!empty($filtros['email']) && isset($filtros['email'])) {
            $data = $data->where('email', '=', $filtros['email']);
        }

        if(!empty($filtros['clients']) && isset($filtros['clients'])) {
            $data = $data->where('first_name', 'like', '%'.$filtros['clients'].'%');
        }

        return $data->orderBy('transaction_mp.id', 'desc')->paginate(10);
    }

    public function getFilteredSumTotal($filtros, $field)
    {
        $data = $this->model->join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
//            ->join('clients', 'clients.user_id', '=', 'commissions.user_id')
//            ->with('transaction','transaction.commissions')
            ->where('commissions.user_id', Auth::user()->id);

        if(!empty($filtros['transaction_id']) && isset($filtros['transaction_id'])) {
            $data = $data->where('commissions.id', $filtros['transaction_id']);
        }

        if(!empty($filtros['form_payment']) && isset($filtros['form_payment'])&& $filtros['form_payment'] != 'todos') {
            $data = $data->where('transaction_mp.payment_type_id', $filtros['form_payment']);
        }

        if(!empty($filtros['product_name']) && isset($filtros['product_name'])) {
            $data = $data->whereIn('products.id', $filtros['product_name']);
        }

        if(!empty($filtros['status']) && isset($filtros['status']) ) {
            $data = $data->whereIn('status',$filtros['status']);
        }


        if(!empty($filtros['affiliates']) && isset($filtros['affiliates']) ) {
            $data = $data->whereIn('commissions.user_id',$filtros['affiliates']);
        }

        if(!empty($filtros['origem_venda']) && isset($filtros['origem_venda']) && $filtros['origem_venda'] != 'todos') {
            $data = $data->where('transaction_mp.payment_type_id', $filtros['origem_venda']);
        }

        if((!empty($filtros['data_pedido']) && isset($filtros['data_pedido']))
            && (!empty($filtros['data_finalizada']) && isset($filtros['data_finalizada']))) {
            $data = $data->whereBetween('transaction_mp.date_created', [$filtros['data_pedido'], $filtros['data_finalizada']]);
        }

        if(!empty($filtros['vendashoje']) && isset($filtros['vendashoje'])) {
            $data = $data->whereDate('transaction_mp.date_created', Carbon::now()->format('Y-m-d'))->whereIn('transaction_mp.status',['finished','completed']);
        }

        if(!empty($filtros['vendasmes']) && isset($filtros['vendasmes'])) {
            $data = $data->whereMonth("transaction_mp.date_created",'=',Carbon::now()->format('m'))->whereIn('transaction_mp.status',['finished','completed']);
        }


        if(!empty($filtros['boletosimpressos']) && isset($filtros['boletosimpressos'])) {
            $data = $data->whereDate('transaction_mp.date_created', Carbon::now()->format('Y-m-d'))->where('transaction_mp.payment_type_id', 'ticket');
        }

        if(!empty($filtros['boletospendentes']) && isset($filtros['boletospendentes'])) {
            $data = $data->where('transaction_mp.payment_type_id', 'ticket')->where('transaction_mp.status','pending');
        }

        if(!empty($filtros['cpf_cnpj']) && isset($filtros['cpf_cnpj'])) {
            $data = $data->where('cpf_cnpj', '=', $filtros['cpf_cnpj']);
        }

        if(!empty($filtros['email']) && isset($filtros['email'])) {
            $data = $data->where('email', '=', $filtros['email']);
        }

        if(!empty($filtros['first_name']) && isset($filtros['first_name'])) {
            $data = $data->where('first_name', 'like', '%'.$filtros['first_name'].'%');
        }

        return $data->orderBy('transaction_mp.id', 'desc')->sum($field);
    }

    public static function getQtdVenda($idProduto)
    {
        return Commission::join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
            ->where('user_id', Auth::user()->id)
            ->where('status', 'finished')
            ->where('products.id', $idProduto)
            ->count();
    }
    public function getDataFiltered($filtros)
    {
        $data = $this->model->join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
//            ->with('transaction','transaction.commissions')
            ->where('user_id', Auth::user()->id);

        if(!empty($filtros['product_name']) && isset($filtros['product_name'])) {
            $data = $data->whereIn('products.id', $filtros['product_name']);
        }

        if(!empty($filtros['data_inicio']) && isset($filtros['data_inicio'])) {
            $dataInicio = Carbon::createFromFormat('d/m/Y', $filtros['data_inicio'])->format('Y-m-d');
            $data = $data->whereDate('transaction_mp.date_created', '>=',$dataInicio);
        }

        if(!empty($filtros['data_fim']) && isset($filtros['data_fim'])) {
            $dataFim = Carbon::createFromFormat('d/m/Y', $filtros['data_fim'])->format('Y-m-d');
            $data = $data->whereDate('transaction_mp.date_created', '<=', $dataFim);
        }

        return $data->get();
    }

    public function getFilteredItensChart($filtros)
    {
        $data = $this->model->join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
//            ->join('clicks', 'clicks.product_id', '=', 'products.id')
//            ->with('transaction','transaction.commissions')
            ->where('commissions.user_id', Auth::user()->id)
//            ->where('clicks.user_id', Auth::user()->id)
            ->distinct();


        if(!empty($filtros['product_name']) && isset($filtros['product_name'])) {
            $data = $data->whereIn('products.id', $filtros['product_name']);
        }

        if(!empty($filtros['data_inicio']) && isset($filtros['data_inicio'])) {
            $dataInicio = Carbon::createFromFormat('d/m/Y', $filtros['data_inicio'])->format('Y-m-d');
            $data = $data->whereDate('commissions.date_created', '>=', $filtros['data_inicio']);
        }

        if(!empty($filtros['data_fim']) && isset($filtros['data_fim'])) {
            $dataFim = Carbon::createFromFormat('d/m/Y', $filtros['data_fim'])->format('Y-m-d');
            $data = $data->whereDate('commissions.date_created', '<=', $filtros['data_fim']);
        }

        return $data;
    }

    public function getDataChart($filter)
    {
        $boletos = $this->getFilteredItensChart($filter);
        $reembolso = $this->getFilteredItensChart($filter);

        //boleto
        $qtdVendaBoleto = $boletos->where('transaction_mp.payment_type_id', 'ticket')->count();
        $qtdVendaBoletoFinalizadas = $boletos->where('transaction_mp.status', 'finished')->count();
        $conversaoBoleto = ($qtdVendaBoleto / 100 * $qtdVendaBoletoFinalizadas);
        //reembolso
        $qtdVendas = $reembolso->count();
        $qtdVendaReembolso = $reembolso->join('reembolsos', 'reembolsos.product_id', 'products.id')
            ->where('reembolsos.user_id', Auth::user()->id)
            ->count();
        $conversaoReembolso = ($qtdVendas / 100 * $qtdVendaReembolso);

        //produto
        $qtdClick = $this->getFilteredCliques($filter)->count();
        $conversaoProduto = ($qtdVendas / 100 * $qtdClick);

        $data = [
        [
            "name"=> 'Conversão de boletos',
            "data"=> $conversaoBoleto
        ],
        [
            "name"=> 'Reembolso em produto',
            "data"=> $conversaoReembolso
        ],
        [
            "name"=> 'Conversão do produto',
            "data"=> $conversaoProduto
        ]];

        return $data;
    }
    public function getFilteredCliques($filtros)
    {
        $data = Click::join('products', 'product_id', '=', 'products.id')
            ->where('clicks.user_id', Auth::user()->id);

        if(!empty($filtros['product_name']) && isset($filtros['product_name'])) {
            $data = $data->whereIn('products.id', $filtros['product_name']);
        }

        if(!empty($filtros['data_inicio']) && isset($filtros['data_inicio'])) {
            $dataInicio = Carbon::createFromFormat('d/m/Y', $filtros['data_inicio'])->format('Y-m-d');
            $data = $data->whereDate('clicks.date_created', '>=', $dataInicio);
        }

        if(!empty($filtros['data_fim']) && isset($filtros['data_fim'])) {
            $dataFim = Carbon::createFromFormat('d/m/Y', $filtros['data_fim'])->format('Y-m-d');
            $data = $data->whereDate('clicks.date_created', '<=', $dataFim);
        }

        return $data->get();
    }

    public static function getAffiliatesSales($transaction_id)
    {
        $affiliate =  Commission::leftjoin('users', 'commissions.user_id', '=', 'users.id')
            ->where('commissions.commission_type', 'affiliate')
            ->where('commissions.transaction_id', $transaction_id)
            ->distinct()
            ->pluck('users.name')
            ->toArray();

        if(!count($affiliate)) {
           return '-';
        }
        return implode(',',$affiliate);
    }
}
