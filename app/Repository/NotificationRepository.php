<?php

namespace App\Repository;

use App\Models\Plataforma\Notification;
use App\Repository\BaseRepository;
use DB;
use Auth;
use App\User;

class NotificationRepository extends BaseRepository
{

    public function __construct(Notification $notifications)
    {
        $this->notifications = $notifications;

    }

    public function getNotifications($filtros)
    {
        $notifications = $this->notifications->where('user_id', Auth::user()->id)->orderBy('id', 'desc');

//        filter
//        if(!empty($filtros['content']) && isset($filtros['content'])) {
//            $notifications = $notifications->where('content', 'ilike', '%'.$filtros['content'].'%');
//        }

        return $notifications->paginate(10);
    }

    public function getNotificationsApi()
    {
        return $this->notifications->join('users', 'users.id', 'user_id')
            ->where('user_id', Auth::user()->id)
            ->orderBy('notifications.id', 'desc')
            ->select('notifications.*', 'users.name', 'users.image')
            ->limit(5)
            ->get();
    }
}