<?php
namespace App\Repository;

use App\Models\Plataforma\Product;
use App\Repository\BaseRepository;
use DB;

class ProductRepository extends BaseRepository {
	protected $modelClass = \App\Models\Plataforma\Product::class;

	public function getProducts($limit = 8, $input = []) {
		// return $this->modelClass::select('especialidades.*', DB::raw('count(produtos.id) as total_produtos'))
		//     ->join('produtos', 'especialidades.id', '=', 'produtos.especialidade_id')
		//     ->with('categorias')
		//     ->groupBy('especialidades.id')
		//     ->get();

		$model = $this->modelClass::select('products.*', DB::raw('(SELECT COUNT(id) from products)'))
			->where('name')
			->where('deleted_at',NULL)
			->having('id', '>', 0)
		// ->limit($limit)
			->groupBy('products.id')
			->inRandomOrder();
		// ->simplePaginate(8);
		$showPerPag = $limit;
		$curPage = \Illuminate\Pagination\Paginator::resolveCurrentPage();
		$total = $model->get()->count();
		$items = $model->forPage($curPage, $showPerPag)->get();
		$products = new \Illuminate\Pagination\LengthAwarePaginator($items, $total, $showPerPag, $curPage, ['path' => request()->url(), 'query' => request()->query()]);

		// dd($paginated);

		return $products;
	}

	// public function getProductsDestaque() {
	// 	return $this->modelClass::select('produtos.*', DB::raw('(SELECT COUNT(id) from produto_imagems where produto_imagems.produto_id = produtos.id) as id'))
	// 		->with('imagem')
	// 		->whereAtivo(1)
	// 		->where('status', 'disponivel')
	// 		->having('fotosTotal', '>', 0)
	// 	// ->orderBy(DB::raw('RAND()'))
	// 		->limit(8)
	// 		->groupBy('produtos.id')
	// 		->get();
	// }

	// public function getProductsSemFoto() {

	// 	$model = $this->modelClass::select('produtos.*', DB::raw('(SELECT COUNT(id) from produto_imagems where produto_imagems.produto_id = produtos.id) as fotosTotal'))
	// 		->with('imagem')
	// 		->whereAtivo(1)
	// 		->where('status', 'disponivel')
	// 		->having('fotosTotal', '=', 0)
	// 	// ->limit($limit)
	// 		->groupBy('produtos.id');
	// 	$produtos = $model->get();

	// 	/*$showPerPag = $limit;
	// 		        $curPage    = \Illuminate\Pagination\Paginator::resolveCurrentPage();
	// 		        $total      = $model->get()->count();
	// 		        $items      = $model->forPage($curPage, $showPerPag)->get();
	// 		        $produtos   = new \Illuminate\Pagination\LengthAwarePaginator($items, $total, $showPerPag, $curPage, ['path' => request()->url(), 'query' => request()->query()]);
	// 	*/
	// 	// dd($paginated);

	// 	return $produtos;
	// }
}
