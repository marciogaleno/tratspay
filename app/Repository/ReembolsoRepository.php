<?php

namespace App\Repository;

use App\Models\Plataforma\ProducerAffiliate;
use App\Models\Plataforma\Reembolso;
use App\Repository\BaseRepository;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ReembolsoRepository extends BaseRepository
{

    public function __construct(Reembolso $model)
    {
        $this->model = $model;
    }

    public function getFiltered($filtros)
    {
        $data = $this->model;

        $productsProducer = ProducerAffiliate::where('user_id', Auth::user()->id)
                                ->where('relation_type', 'producer')
                                ->select('product_id')
                                ->distinct()
                                ->pluck('product_id')
                                ->toArray();

        $data = $data->whereIn("product_id", $productsProducer);

//        if(!empty($filtros['product_type']) && isset($filtros['product_type'])) {
//            $data = $data->where('product_type',$filtros['product_type']);
//        }
//
//        if(!empty($filtros['category_id']) && isset($filtros['category_id'])) {
//            $data = $data->where('category_id', $filtros['category_id']);
//        }
//
//        if(!empty($filtros['name']) && isset($filtros['name'])) {
//            $data = $data->where('name', 'LIKE', '%' . $filtros['name'] . '%');
//        }

        return $data->orderBy('created_at', 'desc')->withoutGlobalScopes()->paginate(8);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function create($request)
    {
        $exists = (bool) $this->model
            ->where('user_id',$request['user_id'])
            ->where('product_id',$request['product_id'])
            ->where('transaction_id',$request['transaction_id'])
            ->where('status', "P")
            ->count();

        if($exists) {
            return false;
        }
        if(empty($request['user_id']) || empty($request['product_id']) || empty($request['motivo']) ) {
            return false;
        }
        $request['status'] = "P";

        $request['prazo'] = Carbon::now()->addDay(7)->format('Y-m-d');

        $model = $this->model->create($request);

        if(!$model){
            return false;
        }

        return true;
    }

    public function getReembolsosForJob()
    {
        return $this->model
            ->where('status', "P")
            ->where('prazo', "<", date('Y-m-d'))
            ->get();
    }
}