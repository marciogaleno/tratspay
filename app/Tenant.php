<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Tenant extends Model
{
    use Uuid;

    protected $fillable = [
        'email'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
