<?php

namespace App\Helpers;
use Collective\Html\FormFacade as Form;
use Collective\Html\HtmlFacade as Html;

class FormHelper
{
    public static function text($field, $label, $errors = null, $options = [])
    {
        $optionsDefault = ['class' => 'form-control'];
        $options = array_merge($optionsDefault, $options);
        
        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';

        if ($label) {
            $html .= Form::label($field, $label, ['for' => $field]);
        }
        
        $html .= Form::text($field, old($field),  $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    public static function url($field, $label, $errors = null, $options = [])
    {
        $optionsDefault = ['class' => 'form-control'];
        $options = array_merge($optionsDefault, $options);
        
        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);
        $html .= Form::url($field, old($field),  $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    public static function number($field, $label, $errors = null, $options = [])
    {
        $optionsDefault = ['class' => 'form-control'];
        $options = array_merge($optionsDefault, $options);
        
        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);
        $html .= Form::number($field, old($field),  $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    public static function colorPicker($field, $label, $errors = null, $options = [])
    {
        $optionsDefault = ['class' => 'form-control'];
        $options = array_merge($optionsDefault, $options);
        
        $html = '';
        $html .= '<div class="form-group colorpicker-component has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);

        $html .= '<div class="input-group mb-3">';

        $html .= '<div class="input-group-prepend input-group-addon">';
        $html .= '<span class="input-group-text"> <i></i></span>';
        $html .= '</div>';
        $html .= Form::text($field, null,  $options);

        $html .= '</div>';
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    public static function time($field, $label, $errors = null, $options = [])
    {
        $optionsDefault = ['class' => 'form-control'];
        $options = array_merge($optionsDefault, $options);
        
        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);
        $html .= Form::time($field, old($field),  $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    public static function textarea($field, $label, $errors = null,  $options = [])
    {
        $optionsDefault = ['class' => 'form-control', 'rows' => 5];
        $options = array_merge($optionsDefault, $options);

        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);
        $html .= Form::textarea($field, old($field),  $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    public static function select($field, $label, $data = [], $errors = null,  $options = [])
    {
        $optionsDefault = ['class' => 'form-control'];
        $options = array_merge($optionsDefault, $options);

        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);
        $html .= Form::select($field, $data, old($field), $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }

    /**
     * Esse select facilita a busca de da opção desejada através de uma busca.
     * 
     * Para funcionar precia adicionar os seguinte  scrips na página
     * 
     * <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
     * <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
     * 
     * E adicionar o código abaixo:
     * 
     * <script>
     *      $('.single-select').select2();
     * </script>
     *
     * @param [type] $field
     * @param [type] $label
     * @param array $data
     * @param [type] $errors
     * @param array $options
     * @return void
     */
    public static function selectSearch($field, $label, $data = [], $errors = null,  $options = [])
    {
        $optionsDefault = ['class' => 'form-control single-select', 'rows' => 5];
        $options = array_merge($optionsDefault, $options);

        $html = '';
        $html .= '<div class="form-group has-feedback' . $errors->has($field) ?? 'has-error';
        $html .= '" >';
        $html .= Form::label($field, $label, ['for' => $field]);
        $html .= Form::select($field, $data, old($field), $options);
        
        if ($errors->has($field)) {
            $html .= '<label class="error" for="' . $field . '">' . $errors->first($field) . '</label>';
        }

       $html .= '</div>';
       return $html;
    }
}