<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Request;
use App\Models\Plataforma\Product;

class TabsAffiliateHelper
{
    public static function openBoxTabs($product)
    {   
        // Codigo para pegar o produto aumaticamente
        //$request = Request::instance();
        //$product = Product$request->productUuid);
        
        $user = $product->affiliates->where('id',auth()->user()->id)->first();
        $status = $user->pivot->status;

        $html  = '<div class="col-12">';
        $html .= '<div class="card">';
        $html .= '<div class="card-header bg-warning text-white">';
        $html .= '<div class="row">';
        $html .= '<div class="col-sm-9">';
        $html .= '<h5 class="text-white">' .$product->name . '</h5>';
        $html .= '</div>';
        if($status == "A"){
        $html .= '<div class="col-sm-3">';
        $html .= '<button class="btn btn-primary waves-effect waves-light m-1 text-right cancelar_afiliacao">Cancelar Afiliação</button>';
        $html .= '</div>';
        }
        $html .= '</div>';
        $html .= '</div>';
        $html .= '<div class="card-body">';

        return $html;
    }

    public static function closeBoxTabs()
    {
        $html  = '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public static function openHeader($product = null, $tabAcitve = 0) {

        $tabNotAppear = [];

        $tabs = [
            ['desc' => 'Dados Gerais', 'icon' => 'ti-home', 'url' => 'affiliates/detailsProduct/' . ($product ? $product->id : '')], 
            ['desc' => 'URLs', 'icon' => 'fa fa-link', 'url' => 'affiliates/urls/' .  ($product ? $product->id : '')], 
            ['desc' => 'Co-Afiliação', 'icon' => 'fa fa-share-alt', 'url' => 'coaffiliates/list/' .  ($product ? $product->id : '')]
        ];
        

        $html  = '<div class="row">';
        $html .= '<div class="col-md-2" style="padding: 0px">';
        $html .= '<div class="tabs-vertical tabs-vertical-warning">';
        $html .= '<ul class="nav nav-tabs flex-column">';

        foreach ($tabs as $key => $tab) {
            //Verify if key not exists in $tabNotAppear. If exists not generate tab. 
            if (!array_keys($tabNotAppear, $key)) {
                $html .= ' <li class="nav-item"> ';
                $html .= ' <a class="nav-link py-4 '. ($tabAcitve == $key ? 'active show' : '')  .'"  href="' . url($tab['url']) .'" role="tab" style="text-align: left">';
                $html .= '<i class="fa ' . $tab['icon'] . '" style="margin-left: 10%"></i>&nbsp';
                $html .= '<span class="hidden-xs">';
                $html .=  $tab['desc'];
                $html .= '</span>';
                $html .= '</a>';
                $html .= '</li>';
            }
        }

        $html .= '</ul>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
        
    }

    public static function closeHeader() 
    {
        return '</div>';
    }

    public static function openContent()
    {
        $html  = '<div class="col-md-10">';
        $html .= '<div class="tab-content">';
        $html .= '<div class="tabe-21">';

        return $html;
          
    }

    public static function closeContent()
    {
        $html  = '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}