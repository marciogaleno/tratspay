<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;
use App\Models\Plataforma\Product;
use Illuminate\Support\Facades\Request;

class TabsHelper
{
    private static $product = null;

    public static function openBoxTabs()
    {
        $request = Request::instance();
        self::$product = $request->product;

        $html  = '<div class="col-12">';
        $html .= '<div class="card">';
        $html .= '<div class="card-header bg-warning text-white">';
        $html .= '<h5 class="text-white">' . self::$product->name . '</h5>';
        $html .= '</div>';
        $html .= '<div class="card-body">';

        return $html;
    }

    public static function closeBoxTabs()
    {
        $html  = '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public static function openHeader($tabAcitve = 0)
    {

        $product = self::$product;

        $tabNotAppear = [];

        $urlBase = 'product/' . ($product ? $product->id : '');
        $tabs = [
            ['desc' => 'Dados Gerais', 'icon' => 'ti-home', 'url' => $urlBase . '/edit'],
            ['desc' => 'Planos', 'icon' => 'fa-cart-arrow-down', 'url' =>  $urlBase . '/plans'],
            ['desc' => 'Checkout', 'icon' => 'fa-credit-card', 'url' =>  $urlBase .'/checkouts'],
            ['desc' => 'URLs', 'icon' => 'fa-anchor', 'url' => $urlBase . '/urls'],
            ['desc' => 'Arquivos', 'icon' => 'fa-file-pdf-o', 'url' => $urlBase . '/files'],
            ['desc' => 'Área de Membros', 'icon' => 'fa-users', 'url' => $urlBase . '/memberArea'],
            ['desc' => 'Co-Produções', 'icon' => 'fa fa-share-alt', 'url' =>  $urlBase . '/coproducers'],
            /*['desc'  => 'Afiliados', 'icon' => 'fa-money',  'url' =>  $urlBase . '/configaffiliation'],*/
            ['desc' => 'Mídia', 'icon' => 'fa-picture-o', 'url' => $urlBase . '/media/add'],
            ['desc' => 'Campanha','icon' => 'fa-bar-chart', 'url' => $urlBase . '/campaigns'],
            ['desc' => 'Cupons', 'icon' => 'fa-handshake-o', 'url' => $urlBase . '/coupons'],
            ['desc' => 'Vendas', 'icon' => 'fa-key', 'url' => $urlBase.'/sales']
        ];

        if ($product->payment_type != 'PLAN') {
            unset($tabs[1]);
        }


        $html  = '<div class="row">';
        $html .= '<div class="col-md-2"  style="padding: 0px">';
        $html .= '<div class="tabs-vertical tabs-vertical-warning">';
        $html .= '<ul class="nav nav-tabs flex-column">';

        if ($product && isset($product->payment_type) && $product->payment_type != 'PLAN') {
            $tabNotAppear[] = 1;
        }

        if ($product && isset($product->type_deliverie_id) && $product->type_deliverie_id == 1 ) {
            $tabNotAppear[] = 4;
        }

        if ($product && isset($product->type_deliverie_id) && $product->type_deliverie_id == 3 ) {
            $tabNotAppear[] = 5;
        }





        foreach ($tabs as $key => $tab) {
            //Verify if key not exists in $tabNotAppear. If exists not generate tab.
            if (!array_keys($tabNotAppear, $key)) {


                $html .= ' <li class="nav-item"> ';
                $html .= ' <a class="nav-link py-4 '. ($tabAcitve == $key ? 'active show' : '')  .'"  href="' . url($tab['url']) .'" role="tab" style="text-align: left">';
                $html .= '<i class="fa ' . $tab['icon'] . '" style="margin-left: 10%"></i>&nbsp';
                $html .= '<span class="hidden-xs">';
                $html .=  $tab['desc'];
                $html .= '</span>';
                $html .= '</a>';
                $html .= '</li>';
            }
        }

        $html .= '</ul>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;

    }

    public static function closeHeader()
    {
        return '</div>';
    }

    public static function openContent()
    {
        $html  = '<div class="col-md-10">';
        $html .= '<div class="tab-content">';
        $html .= '<div class="tabe-21">';

        return $html;

    }

    public static function closeContent()
    {
        $html  = '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}
