<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Plataforma\Bank;

class Helper
{
    /**
     * @param $date
     * @param bool $hours
     * @return string
     */
    public static function dateToBr($date, $hours = false)
    {
        if ($date) {
            if ($hours) {
                return Carbon::parse($date)->format('d/m/Y H:i:s');
            } else {
                return Carbon::parse($date)->format('d/m/Y');
            }
        } else {
            return "-";
        }
    }

    /**
     * @param $date
     * @param bool $hours
     * @return string
     */
    public static function dateToEn($date, $hours = true)
    {
        if ($date) {
            $d = Carbon::createFromFormat('d/m/Y', $date);
            if ($d) {
                if ($hours) {
                    return Carbon::parse($d)->format('Y-m-d H:i:s');
                }
                return Carbon::parse($d)->format('Y-m-d');
            }
        }
    }

    /**
     * @param $date
     * @param bool $hours
     * @return string
     */
    public static function datetimeToEn($date)
    {
        if ($date) {
            $d = Carbon::createFromFormat('d/m/Y H:i:s', $date);
            return Carbon::parse($d)->format('Y-m-d H:i:s');
        }

        return null;
    }

    /**
     * @param $date
     * @param bool $hours
     * @return string
     */
    public static function dateToAmerican($date)
    {
        $d = Carbon::createFromFormat('d/m/Y', $date);
        if ($d) {
            return Carbon::parse($d)->format('Y-m-d');
        }
    }

    /**
     * @return array
     */
    public static function gender()
    {
        return [
            'm' => 'Masculino',
            'f' => 'Feminiino'
        ];
    }

    /**
     * @param null $type
     * @return array|mixed
     */
    public static function typePerson($type = null)
    {
        $listType = [
            1 => 'Física',
            2 => 'Jurídica',
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    /**
     * @param null $type
     * @return array
     */
    public static function statusDefault($type = null)
    {
        $listType = [
            1 => '<span class="label label-success">Ativo</span>',
            0 => '<span class="label label-danger">Inativo</span>',
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }


    /**
     * @param null $type
     * @return array
     */
    public static function ListStates($type = null)
    {
        $listType = [
            'AC' => 'Acre',
            'AL' => 'Alagoas',
            'AP' => 'Amapá',
            'AM' => 'Amazonas',
            'BA' => 'Bahia',
            'CE' => 'Ceará',
            'DF' => 'Distrito Federal',
            'ES' => 'Espírito Santo',
            'GO' => 'Goiás',
            'MA' => 'Maranhão',
            'MT' => 'Mato Grosso',
            'MS' => 'Mato Grosso do Sul',
            'MG' => 'Minas Gerais',
            'PA' => 'Pará',
            'PB' => 'Paraíba',
            'PR' => 'Paraná',
            'PE' => 'Pernambuco',
            'PI' => 'Piauí',
            'RJ' => 'Rio de Janeiro',
            'RN' => 'Rio Grande do Norte',
            'RS' => 'Rio Grande do Sul',
            'RO' => 'Rondônia',
            'RR' => 'Roraima',
            'SC' => 'Santa Catarina',
            'SP' => 'São Paulo',
            'SE' => 'Sergipe',
            'TO' => 'Tocantins'
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function ListCountries($type = null)
    {
        $listType = [
            "África do Sul"=>"África do Sul",
            "Albânia"=>"Albânia",
            "Alemanha"=>"Alemanha",
            "Andorra"=>"Andorra",
            "Angola"=>"Angola",
            "Anguilla"=>"Anguilla",
            "Antigua"=>"Antigua",
            "Arábia Saudita"=>"Arábia Saudita",
            "Argentina"=>"Argentina",
            "Armênia"=>"Armênia",
            "Aruba"=>"Aruba",
            "Austrália"=>"Austrália",
            "Áustria"=>"Áustria",
            "Azerbaijão"=>"Azerbaijão",
            "Bahamas"=>"Bahamas",
            "Bahrein"=>"Bahrein",
            "Bangladesh"=>"Bangladesh",
            "Barbados"=>"Barbados",
            "Bélgica"=>"Bélgica",
            "Benin"=>"Benin",
            "Bermudas"=>"Bermudas",
            "Botsuana"=>"Botsuana",
            "Brasil"=>"Brasil",
            "Brunei"=>"Brunei",
            "Bulgária"=>"Bulgária",
            "Burkina Fasso"=>"Burkina Fasso",
            "botão"=>"botão",
            "Cabo Verde"=>"Cabo Verde",
            "Camarões"=>"Camarões",
            "Camboja"=>"Camboja",
            "Canadá"=>"Canadá",
            "Cazaquistão"=>"Cazaquistão",
            "Chade"=>"Chade",
            "Chile"=>"Chile",
            "China"=>"China",
            "Cidade do Vaticano"=>"Cidade do Vaticano",
            "Colômbia"=>"Colômbia",
            "Congo"=>"Congo",
            "Coréia do Sul"=>"Coréia do Sul",
            "Costa do Marfim"=>"Costa do Marfim",
            "Costa Rica"=>"Costa Rica",
            "Croácia"=>"Croácia",
            "Dinamarca"=>"Dinamarca",
            "Djibuti"=>"Djibuti",
            "Dominica"=>"Dominica",
            "EUA"=>"EUA",
            "Egito"=>"Egito",
            "El Salvador"=>"El Salvador",
            "Emirados Árabes"=>"Emirados Árabes",
            "Equador"=>"Equador",
            "Eritréia"=>"Eritréia",
            "Escócia"=>"Escócia",
            "Eslováquia"=>"Eslováquia",
            "Eslovênia"=>"Eslovênia",
            "Espanha"=>"Espanha",
            "Estônia"=>"Estônia",
            "Etiópia"=>"Etiópia",
            "Fiji"=>"Fiji",
            "Filipinas"=>"Filipinas",
            "Finlândia"=>"Finlândia",
            "França"=>"França",
            "Gabão"=>"Gabão",
            "Gâmbia"=>"Gâmbia",
            "Gana"=>"Gana",
            "Geórgia"=>"Geórgia",
            "Gibraltar"=>"Gibraltar",
            "Granada"=>"Granada",
            "Grécia"=>"Grécia",
            "Guadalupe"=>"Guadalupe",
            "Guam"=>"Guam",
            "Guatemala"=>"Guatemala",
            "Guiana"=>"Guiana",
            "Guiana Francesa"=>"Guiana Francesa",
            "Guiné-bissau"=>"Guiné-bissau",
            "Haiti"=>"Haiti",
            "Holanda"=>"Holanda",
            "Honduras"=>"Honduras",
            "Hong Kong"=>"Hong Kong",
            "Hungria"=>"Hungria",
            "Iêmen"=>"Iêmen",
            "Ilhas Cayman"=>"Ilhas Cayman",
            "Ilhas Cook"=>"Ilhas Cook",
            "Ilhas Curaçao"=>"Ilhas Curaçao",
            "Ilhas Marshall"=>"Ilhas Marshall",
            "Ilhas Turks & Caicos"=>"Ilhas Turks & Caicos",
            "Ilhas Virgens (brit.)"=>"Ilhas Virgens (brit.)",
            "Ilhas Virgens(amer.)"=>"Ilhas Virgens(amer.)",
            "Ilhas Wallis e Futuna"=>"Ilhas Wallis e Futuna",
            "Índia"=>"Índia",
            "Indonésia"=>"Indonésia",
            "Inglaterra"=>"Inglaterra",
            "Irlanda"=>"Irlanda",
            "Islândia"=>"Islândia",
            "Israel"=>"Israel",
            "Itália"=>"Itália",
            "Jamaica"=>"Jamaica",
            "Japão"=>"Japão",
            "Jordânia"=>"Jordânia",
            "Kuwait"=>"Kuwait",
            "Latvia"=>"Latvia",
            "Líbano"=>"Líbano",
            "Liechtenstein"=>"Liechtenstein",
            "Lituânia"=>"Lituânia",
            "Luxemburgo"=>"Luxemburgo",
            "Macau"=>"Macau",
            "Macedônia"=>"Macedônia",
            "Madagascar"=>"Madagascar",
            "Malásia"=>"Malásia",
            "Malaui"=>"Malaui",
            "Mali"=>"Mali",
            "Malta"=>"Malta",
            "Marrocos"=>"Marrocos",
            "Martinica"=>"Martinica",
            "Mauritânia"=>"Mauritânia",
            "Mauritius"=>"Mauritius",
            "México"=>"México",
            "Moldova"=>"Moldova",
            "Mônaco"=>"Mônaco",
            "Montserrat"=>"Montserrat",
            "Nepal"=>"Nepal",
            "Nicarágua"=>"Nicarágua",
            "Niger"=>"Niger",
            "Nigéria"=>"Nigéria",
            "Noruega"=>"Noruega",
            "Nova Caledônia"=>"Nova Caledônia",
            "Nova Zelândia"=>"Nova Zelândia",
            "Omã"=>"Omã",
            "Palau"=>"Palau",
            "Panamá"=>"Panamá",
            "Papua-nova Guiné"=>"Papua-nova Guiné",
            "Paquistão"=>"Paquistão",
            "Peru"=>"Peru",
            "Polinésia Francesa"=>"Polinésia Francesa",
            "Polônia"=>"Polônia",
            "Porto Rico"=>"Porto Rico",
            "Portugal"=>"Portugal",
            "Qatar"=>"Qatar",
            "Quênia"=>"Quênia",
            "Rep. Dominicana"=>"Rep. Dominicana",
            "Rep. Tcheca"=>"Rep. Tcheca",
            "Reunion"=>"Reunion",
            "Romênia"=>"Romênia",
            "Ruanda"=>"Ruanda",
            "Rússia"=>"Rússia",
            "Saipan"=>"Saipan",
            "Samoa Americana"=>"Samoa Americana",
            "Senegal"=>"Senegal",
            "Serra Leone"=>"Serra Leone",
            "Seychelles"=>"Seychelles",
            "Singapura"=>"Singapura",
            "Síria"=>"Síria",
            "Sri Lanka"=>"Sri Lanka",
            "St. Kitts & Nevis"=>"St. Kitts & Nevis",
            "St. Lúcia"=>"St. Lúcia",
            "St. Vincent"=>"St. Vincent",
            "Sudão"=>"Sudão",
            "Suécia"=>"Suécia",
            "Suiça"=>"Suiça",
            "Suriname"=>"Suriname",
            "Tailândia"=>"Tailândia",
            "Taiwan"=>"Taiwan",
            "Tanzânia"=>"Tanzânia",
            "Togo"=>"Togo",
            "Trinidad & Tobago"=>"Trinidad & Tobago",
            "Tunísia"=>"Tunísia",
            "Turquia"=>"Turquia",
            "Ucrânia"=>"Ucrânia",
            "Uganda"=>"Uganda",
            "Uruguai"=>"Uruguai",
            "Venezuela"=>"Venezuela",
            "Vietnã"=>"Vietnã",
            "Zaire"=>"Zaire",
            "Zâmbia"=>"Zâmbia",
            "Zimbábue"=>"Zimbábue",
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    /**
     * @param null $type
     * @return array
     */
    public static function ListBank($type = null)
    {
        $listType = Bank::pluck('name', 'code');

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function typeBankAccount($type = null)
    {
        $listType = [
            1 => 'Corrente',
            2 => 'Poupança',
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    /**
     * @param null $type
     * @return array
     */

    public static function statusBankAccount($type = null, $label = false)
    {
        $listType = [
            1 => '<span class="label label-success">Aprovada</span>',
            0 => '<span class="label label-default">Em Avaliação</span>',
            2 => '<span class="label label-danger">Reprovado</span>',
        ];

        if (!is_null($type)) {
            if ($label) {
                return $listType[$type];
            } else {
                return strip_tags($listType[$type]);
            }
        } else {
            foreach ($listType as $value => $item) {
                $listType[$value] = strip_tags($item);
            }
            return $listType;
        }
    }

    public static function imageProfile()
    {
        //dd($client->image);
        $image = auth()->user()->image;
        if ($image && Storage::disk('public')->exists('profile/'.$image)) {
            return url('storage/profile/'.$image);
        } else {
            return url('storage/profile/no_user_photo.png');
        }
    }

    public static function imageProduct($product)
    {
        $image = $product->image;
        if ($image && Storage::disk('public')->exists('products/'.$image)) {
            return url('storage/products/'.$image);
        }
        return '';
    }

    public static function frequency($type = null, $viewList = false)
    {
        $listType = [
            'UNI' => 'Cobrança ùnica',
            'WEE' => 'Semanal',
            'MON' => 'Mensal',
            'BIM' => 'Bimestral',
            'QUA' => 'Trimestral',
            'SEM' => 'Semestral',
            'YEA' => 'Anual'
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } elseif (is_null($type) && $viewList) {
            return $type;
        } else {
            return $listType;
        }
    }

    public static function warranty($type = null)
    {
        $listType = [
            '7' => '7 Dias',
            '15' => '15 dias',
            '21' => '21 Dias',
            '30' => '30 Dias'
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function firstInstallment($type = null, $viewList = false)
    {
        $listType = [
            1 => 'Igual as demais',
            2 => 'Grátis',
            3 => 'Valor diferenciado',
            4 => 'Grátis por um período'
        ];


        if (!is_null($type)) {
            return $listType[$type];
        } elseif (is_null($type) && $viewList) {
            return $type;
        } else {
            return $listType;
        }
    }

    public static function paymentType($type = null)
    {
        $listType = [
            'SINGPRICE' => 'Preço Único',
            'PLAN' => 'Planos'
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function productTypes($type = null)
    {
        $listType = [
            'V' => 'Virtual',
            'F' => 'Físico'
        ];


        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function getValueCommissionByType($product)
    {
        $valueComission = 0;

        if ($product->payment_type == 'SINGPRICE' &&
            $product->configAffiliationWithoutGlobalScope->format_commission == 'P') {
            $valueComission = (float) $product->plansWithoutGlobalScope[0]->price * ((float) $product->configAffiliationWithoutGlobalScope->value_commission / 100);
            $valueComission = number_format($valueComission, 2, ',', '.');

        }

        if ($product->payment_type == 'SINGPRICE' &&
            $product->configAffiliationWithoutGlobalScope->format_commission == 'V') {
            $valueComission = (float) $product->configAffiliationWithoutGlobalScope->value_commission;
            $valueComission = number_format($valueComission, 2, ',', '.');

        }

        return $valueComission;
    }

    public static function formatCommission($type = null)
    {
        $listType = ['P' => 'Porcentagem (%)', 'V' => 'Valor fixo (R$)'];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function localPageSale($type = null)
    {
        $listType = [
            'PE' => 'Meu Site / Página de vendas Própria',
            'PI' => 'Construtor de Página TratsPay'
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function severalPurchases($type = null)
    {
        $listType = [1 => 'Sim', 0 => 'Não'];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function acceptAffiliation($type = null)
    {
        $listType = [1 => 'Sim', 0 => 'Não'];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }
    public static function expirationCoockie($type = null)
    {
        $listType = [
            30 => '30 Dias',
            90 => '90 Dias',
            180 => '180 Dias',
            999999 => 'Eterno'
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function splitname($name)
    {
        $name = explode(' ', $name);

        if (count($name) > 1) {
            $firstName = $name[0];
            $lastName = (isset($name[count($name)-1])) ? $name[count($name)-1] : '';

            return $firstName . ' ' .  $lastName;
        }

        return current($name);
    }


    public static function jurosComposto($valor, $parcelas, $taxa = 2.99)
    {
        $taxa = $taxa / 100;

        if ($parcelas > 0) {
            $valParcela = $valor * pow((1 + $taxa), $parcelas-1);
            $valParcela = number_format($valParcela / $parcelas, 2);
            return (float)$valParcela;
        }
        return $valor;
    }

    public static function jurosSimples($valor, $parcelas, $taxa = 2.99)
    {
        $taxa = $taxa / 100;

        $m = $valor * (1 + $taxa * $parcelas);
        $valParcela = number_format($m / $parcelas, 2);

        return (float)$valParcela;
    }


    public static function parcelas($valorTotal)
    {
        $parcelaMinima = 5;
        $parcelasTotal = 12;
        $taxa = 2.99;
        $options = [];

        for ($x = 1; $x <= $parcelasTotal; $x++) {
            $parcela = self::jurosComposto($valorTotal, $x, $taxa);
            if ($parcela >=$parcelaMinima) {
                $options[$x] = $parcela;
            } else {
                break;
            }
        }
        return $options;
    }

    public static function statusPaymentGerenciaNet($type = null, $label = false)
    {
        switch ($type) {
            case 'waiting':
                return '<span class="text-warning">Pendente</span>';
                break;
            case 'paid':
                return '<span class="text-success">Aprovada</span>';
                break;
            case 'unpaid':
                return  '<span class="text-warning">Em processo</span>';
                break;
            case 'refunded':
                return '<span class="text-danger">Reembolsado</span>';
                break;
            case 'canceled':
                return '<span class="text-danger">Cancelado</span>';
                break;
            default:
                return  '<span class="text-warning">Status não mapeado</span>';
                break;
        }
    }

    public static function statusPaymentMp($type = null, $label = false)
    {
        $listType = [
            'approved' => '<span class="text-success">Aprovada</span>',
            'pending' => '<span class="text-warning">Pendente</span>',
            'authorized' => '<span class="text-success">Autorizado</span>',
            'in_process' => '<span class="text-warning">Em processo</span>',
            'in_mediation' => '<span class="text-warning">Em desputa</span>',
            'rejected' => '<span class="text-danger">Recusado</span>',
            'cancelled' => '<span class="text-danger">Cancelado</span>',
            'refunded' => '<span class="text-danger">Reembolsado</span>',
            'charged_back' => '<span class="text-danger">Estornado</span>',
        ];

        if (!is_null($type)) {
            if ($label) {
                return $listType[$type];
            } else {
                return strip_tags($listType[$type]);
            }
        } else {
            foreach ($listType as $value => $item) {
                $listType[$value] = strip_tags($item);
            }
            return $listType;
        }
    }

    public static function statusDetailPaymentMp($type = null)
    {
        $listType = [
            'accredited' => 'Pagamento foi aprovado! ',
            'pending_contingency' => 'Estamos processando o pagamento. Em até 2 dias úteis informaremos por e-mail o resultado.',
            'pending_review_manual' => 'Estamos processando o pagamento. Em até 2 dias úteis informaremos por e-mail o resultado.',
            'pending_waiting_payment' => 'Aguardando pagamento do Boleto.',
            'cc_rejected_bad_filled_card_number' => 'Confira o número do cartão.',
            'cc_rejected_bad_filled_date' => 'Confira a data de validade.',
            'cc_rejected_bad_filled_other' => 'Confira os dados.',
            'cc_rejected_bad_filled_security_code' => 'Confira o código de segurança.',
            'cc_rejected_blacklist' => 'Não conseguimos processar seu pagamento.',
            'cc_rejected_call_for_authorize' => 'Você deve autorizar com sua operadora o pagamento do valor ao Mercado Pago',
            'cc_rejected_card_disabled' => 'Ligue para sua operadora para ativar seu cartão. O telefone está no verso do seu cartão.',
            'cc_rejected_card_error' => 'Não conseguimos processar seu pagamento.',
            'cc_rejected_duplicated_payment' => 'Você já efetuou um pagamento com esse valor. Caso precise pagar novamente, utilize outro cartão ou outra forma de pagamento.',
            'cc_rejected_high_risk' => 'Seu pagamento foi recusado. Escolha outra forma de pagamento. Recomendamos meios de pagamento em dinheiro.',
            'cc_rejected_insufficient_amount' => 'O cartão possui saldo insuficiente.',
            'cc_rejected_invalid_installments' => 'O cartão não processa pagamentos parcelados.',
            'cc_rejected_max_attempts' => 'Você atingiu o limite de tentativas permitido. Escolha outro cartão ou outra forma de pagamento.',
            'cc_rejected_other_reason' => 'A operadora não processou seu pagamento.',
        ];

        if (!is_null($type)) {
            return $listType[$type];
        } else {
            return $listType;
        }
    }

    public static function getType($value)
    {
        switch ($value) {
            case 'coproducer':
                return 'Co-produtor';
            case 'manager':
                return 'Gerente';
            default:
                return '';
        }
    }

    public static function getValueCommission($value)
    {
        return number_format($value, 2, ',', '');
    }

    public static function getStatusDetailGerenciaNetTratspay($status)
    {
        switch ($status) {
            case 'waiting':
                return 'Aguardando a confirmação do pagamento';
                break;
            case 'paid':
                return 'Pagamento confirmado';
                break;
            case 'unpaid':
                return 'Não foi possível confirmar o pagamento da cobrança';
                break;
            case 'refunded':
                return 'Pagamento devolvido pelo lojista ou pelo intermediador Gerencianet';
                break;
            case 'canceled':
                return 'Cobrança cancelada pelo vendedor ou pelo pagador';
                break;
            default:
                return 'Status não mapeado';
                break;
        }
    }

    public static function getCoproducerPlan($idPlan, $idProduct, $idCoproducer)
    {
        return DB::table('coproducers_plans')->where('coproducer_id', $idCoproducer)
                ->where('plan_id', $idPlan)
                ->where('product_id', $idProduct)
                ->first();
    }

    public static function getStatusPaymentGerenciaNetTratspay($statusGateway)
    {
        $status = ['pending', 'finished', 'canceled'];

        switch ($statusGateway) {
            case 'waiting':
                return $status[0];
            case 'paid':
                return $status[1];
            case 'unpaid':
                return $status[2];
            case 'refunded':
                return $status[2];
            case 'canceled':
                return $status[2];
            default:
                return $status[2];
        }
    }
    public static function getStatusPaymentTratspay($statusGateway, $statusDetailGateway)
    {
        $status = ['pending', 'finished', 'canceled'];

        if ($statusGateway == 'approved' && $statusDetailGateway == 'accredited') {
            return $status[1];
        }

        if ($statusGateway == 'in_process') {
            switch ($statusDetailGateway) {
                case 'pending_contingency':
                    return $status[0];
                case 'pending_review_manual':
                    return $status[0];
                default:
                    return $status[2];
            }
        }
        return $status[0];
    }

    public static function getAllStatusPayment()
    {
        return [
            'pending' => 'Aguardando Pagamento',
            'finished' => 'Finalizada',
            'canceled' => 'Cancelada',
            'refund' => 'Devolvida',
            'blocked' => 'Bloqueada',
            'completed' => 'Completa'
        ];
    }
    public static function getStatusPayment($type)
    {
        switch ($type) {
            case 'pending':
                return 'Pendente';
                break;
            case 'finished':
                return 'Finalizada';
                break;
            case 'canceled':
                return 'Cancelada';
                break;
            case 'refund':
                return 'Devolvida';
                break;
            case 'blocked':
                return 'Bloqueada';
                break;
            case 'completed':
                return 'Completa';
                break;
            default:
                # code...
                break;
        }
    }

    public static function getCommissionType($type)
    {
        switch ($type) {
            case 'producer':
                return 'Produtor';
                break;
            case 'affiliate':
                return 'Afiliado';
                break;
            case 'coproducer':
                return 'Co-Produtor';
                break;
            case 'coaffiliate':
                return 'Co-Afiliado';
                break;
            default:
                return '';
                break;
        }
    }

    public static function assignmentType($type)
    {
        switch ($type) {
            case '1':
                return 'Primeiro clique';
                break;
            case '2':
                return 'Último clique';
                break;
            case '3':
                return 'Primeiro e ultimo clique';
                break;
            default:
                break;
        }

        return '';
    }

    public static function paymentForms($type)
    {
        switch ($type) {
            case 'credit_card':
                return 'Cartão de crédito';
                break;
            default:
                return 'Boleto';
                break;
        }
    }

    public static function getFormPayment()
    {
        return ['todos' => 'Todos','credit_card' => 'Cartão', 'ticket' => 'Boleto', 'gratis' => 'Grátis'];
    }

    public static function getStatusTransaction()
    {
        return ['approved' => 'Aprovado', 'awaiting_payment' => 'Aguardando Pagamento', 'refunded' => 'Reembolsado', 'unsolved' => 'Reclamação em Aberto'];
    }

    public static function getStatusTransactionGerencianet()
    {
        return ['waiting' => 'Aguardando Pagamento', 'paid' => 'Aprovada', 'unpaid' => 'Em processo','refunded' => 'Reembolsado', 'canceled' => 'Cancelado'];
    }

    public static function typesMedia()
    {
        return [
            '160X160' => 'Banner 160X160',
            '250X250' => 'Banner 250X250',
            '300X250' => 'Banner 300X250',
            '336X280' => 'Banner 336X280',
            '468X60' =>  'Banner 468X60',
            '728X90' =>  'Banner 728X90'
        ];
    }

    public static function typesURL()
    {
        return [
            1 => 'Página de Venda',
            2 => 'Checkout',
            3 => 'Links Alternativos',
        ];
    }

    public static function typesPixel()
    {
        return [
            'fb1' => 'Facebook (Antigo)',
            'fb2' => 'Facebook (Novo)',
            'adw' => 'Google',
            'bing' => 'Bing',
        ];
    }

    public static function statusCashout($status)
    {
        switch ($status) {
            case 'finished':
                return 'Finalizado';
                break;
            case 'processing':
                return 'Processando';
                break;
            default:
                # code...
                break;
        }
    }

    public static function getAllMotivosReembolso()
    {
        return [
            'produto_de_ma_qualidade' => 'Produto de má qualidade',
            'produto_entregue_pela_metade' => 'Produto entregue pela metade',
            'falta_de_suporte' => 'Falta de Suporte',
            'promessa_enganosa' => 'Promessa Enganosa',
            'outros' => 'Outros'
        ];
    }

    public static function getMotivosReembolso($motivo)
    {
        switch ($motivo) {
            case 'produto_de_ma_qualidade':
                return 'Produto de má qualidade';
                break;
            case 'produto_entregue_pela_metade':
                return 'Produto entregue pela metade';
                break;
            case 'falta_de_suporte':
                return 'Falta de Suporte';
                break;
            case 'promessa_enganosa':
                return 'Promessa Enganosa';
                break;
            case 'outros':
                return 'Outros';
                break;
         }
    }

    public static function commissionType($type)
    {
        switch ($type) {
            case 'affiliates':
                return 'Afiliado';
                break;
            case 'coaffiliates':
                return 'Co-afiliado';
                break;
            case 'managers':
                return 'Gerente';
                break;
            case 'coproducers':
                return 'Co-produtor';
                break;
            case 'producer':
                return 'Produtor';
                break;
            case 'producers':
                return 'Produtor';
                break;
            default:
                # code...
                break;
        }
    }

    public static function getPathFileS3($bucket, $pathFile)
    {
        switch ($bucket) {
            case 'images':
                $bucket = env('AWS_BUCKET_IMAGES');

                if ($bucket == 'tratspay-images') {
                    return 'https://tratspay-images.s3-sa-east-1.amazonaws.com/' . $pathFile;
                } else {
                    return 'https://tratspay-images-test.s3-sa-east-1.amazonaws.com/' . $pathFile;
                }
                break;
            case 'products':
                $bucket = env('AWS_BUCKET_PRODUCTS');

                if ($bucket == 'tratspay-products') {
                    return 'https://tratspay-products.s3-sa-east-1.amazonaws.com/' . $pathFile;
                } else {
                    return 'https://tratspay-products-test.s3-sa-east-1.amazonaws.com/' . $pathFile;
                }
                    // no break
            default:
                return null;
                break;
        }

        return false;
    }

    public static function getRemainingWarrantyDays($date)
    {
        // Se a data da garantia for menor que a data de hoje
        if (Carbon::parse($date)->lt(Carbon::now())) {
            return 'expirada';
        }

        $date = Carbon::parse($date);
        $now = Carbon::now();

        $diff = $date->diffInDays($now);

        return $diff . ' dias restantes';
    }


    public static function moneyUS($valor)
    {
        return str_replace(['.', ',', 'R$ '], ['', '.', ''], $valor);
    }

    public static function moneyBR($valor)
    {
        return number_format($valor, 2, ',', '.');
    }
}
