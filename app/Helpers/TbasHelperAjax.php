<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;

class TabsHelperAjax
{
    public static function openBoxTabs($product)
    {
        $html  = '<div class="col-12">';
        $html .= '<div class="card">';
        $html .= '<div class="card-header bg-warning text-white">';
        $html .= '<h5 class="text-white">' .$product->name . '</h5>';
        $html .= '</div>';
        $html .= '<div class="card-body">';

        return $html;
    }

    public static function closeBoxTabs()
    {
        $html  = '</div>';
        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public static function openHeader($product = null, $tabAcitve = 0) {

        $tabNotAppear = [];

        $tabs = [
            ['desc' => 'Dados Gerais', 'icon' => 'ti-home', 'id' => 'tab01','url' => 'products/edit/' . ($product ? $product->uuid : '')], 
            ['desc' => 'Planos', 'icon' => 'fa-cart-arrow-down', 'id' => 'tab02','url' => 'plans/view/' .  ($product ? $product->uuid : '')], 
            ['desc' => 'Checkout', 'icon' => 'fa-credit-card', 'id' => 'tab03','url' => 'checkouts/view/' .  ($product ? $product->uuid : '')], 
            ['desc' => 'URLs', 'icon' => 'fa-anchor', 'id' => 'tab04','url' => 'url/' .  ($product ? $product->uuid : '')], 
            ['desc' => 'Arquivos', 'icon' => 'fa-file-pdf-o', 'id' => 'tab05','url' => 'products/files/' .  ($product ? $product->uuid : '')],
            ['desc' => 'Área de Membros', 'icon' => 'fa-users', 'id' => 'tab06', 'url' =>  '/members'],
            ['desc' => 'Co-Producões', 'icon' => 'fa-users', 'id' => 'tab07','url' => 'coproducers/add/' .  ($product ? $product->uuid : '')],
            ['desc'  => 'Afiliados', 'icon' => 'fa-money', 'id' => 'tab08', 'url' => '/configaffiliation'],
            ['desc' => 'Mídia', 'icon' => 'fa-picture-o', 'url' => '#', 'id' => 'tab09',],
            ['desc' => 'Campanha','icon' => 'fa-bar-chart', 'url' => '#', 'id' => 'tab10',],
            ['desc' => 'Cupons', 'icon' => 'fa-handshake-o', 'url' => '#', 'id' => 'tab11',],
            ['desc' => 'Vendas', 'icon' => 'fa-key', 'url' => '#', 'id' => 'tab12',]

        ];

        if ($product->payment_type != 'PLAN') {
            unset($tabs[1]);
        }
        

        $html  = '<div class="row">';
        $html .= '<div class="col-md-3">';
        $html .= '<div class="tabs-vertical tabs-vertical-warning">';
        $html .= '<ul class="nav nav-tabs flex-column" id="myTabs">';

        if ($product && isset($product->payment_type) && $product->payment_type != 'PLAN') {
            $tabNotAppear[] = 1;
        }

        foreach ($tabs as $key => $tab) {
            dd($tabs);
            //Verify if key not exists in $tabNotAppear. If exists not generate tab. 
            if (!array_keys($tabNotAppear, $key)) {
                $html .= ' <li class="nav-item">';
                $html .= ' <a class="nav-link py-4 '. ($tabAcitve == $key ? 'active show' : '')  .'"  href="' . url($tab['url']) .'"' . 'data-toggle="tabajax"  data-target="#' . $tab['id']   .'" role="tab" style="text-align: left">';
                $html .= '<i class="fa ' . $tab['icon'] . '" style="margin-left: 10%"></i>&nbsp';
                $html .= '<span class="hidden-xs">';
                $html .=  $tab['desc'];
                $html .= '</span>';
                $html .= '</a>';
                $html .= '</li>';
            }
        }

        $html .= '</ul>';
        $html .= '</div>';
        $html .= '</div>';
        
        return $html;
        
    }

    public static function closeHeader() 
    {
        return '</div>';
    }

    public static function openContent()
    {
        $html  = '<div class="col-md-9">';
        $html .= '<div class="tab-content">';
        $html .= '<div id="tab01">';
        $html .= '<div id="tab02">';

        return $html;
          
    }

    public static function closeContent()
    {
        $html  = '</div>';
        $html  = '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}