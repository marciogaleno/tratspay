<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Src\Security\AbstractSecurityMiddleware;

class SecurityMiddleware extends AbstractSecurityMiddleware
{

    /**
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next)
    {
        if (!env('IS_SECURITY_ENNABLED')) {
            return $next($request);
        }

        $path = $this->getPathInfoArray($request->path());

        if ($this->security->haspermission($path)) {
	        return $next($request);
        } else {

            $request->session()->flash('error', 'Você não tem permissão para acessar esse recurso.');

	        if(!\Auth::check()) {
	            return redirect()->route('admin.login.form');
	        }

	        return redirect()->back();
        }
    }

    /**
     * Retorna os elementos do path info.
     *
     * @param string $pathInfo
     *
     * @return array
     */
    private function getPathInfoArray($pathInfo)
    {
        $path = preg_split('/\//', $pathInfo);

        $pathArray = array_values(array_filter($path, function ($item) {
            return !empty($item);
        }));

        $retorno[0] = isset($pathArray[0]) ? $pathArray[0] : 'index';
        $retorno[1] = isset($pathArray[1]) ? $pathArray[1] : 'index';
        $retorno[2] = isset($pathArray[2]) ? $pathArray[2] : 'index';

        return $retorno;
    }
}
