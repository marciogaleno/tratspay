<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Plataforma\Product;
use Illuminate\Support\Facades\Auth;

class ProductExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->product_id) abort(404);

        $product = Product::where('id', $request->product_id)->with('producer')->first();
        
        if (!$product || $product->producer[0]->id <> Auth::user()->id) abort(404);

        $request->product = $product;
        
        return $next($request);
    }
}
