<?php
namespace App\Http\Controllers\Admin;

use App\Src\Repositories\TypeAccountUserRepository;
use App\Src\Repositories\ModuleRepository;
use App\Src\Repositories\UserRepository;
use App\Src\Repositories\TypeAccountRepository;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TypeAccountUserController extends Controller
{
    protected $typeAccountUserRepository;
    protected $userRepository;
    protected $typeAccountRepository;
    protected $moduleRepository;

    public function __construct(
        TypeAccountUserRepository $typeAccountUserRepository,
        UserRepository $userRepository,
        TypeAccountRepository $typeAccountRepository,
        ModuleRepository $moduleRepository,
        User $user
    ){
        $this->typeAccountUserRepository = $typeAccountUserRepository;
        $this->userRepository = $userRepository;
        $this->typeAccountRepository = $typeAccountRepository;
        $this->moduleRepository = $moduleRepository;
        $this->user = $user;
    }

    public function index(Request $request)
    {

        $users = $this->filterUsers($request->all());

        return view('admin.type-account-user.index', compact('users'));
    }

    private function filterUsers ($request)
    {
        $data = $this->user;

        if(!empty($request['user_name']) && isset($request['user_name'])) {
            $data = $data->whereRaw("name ilike '%".$request['user_name']."%'");
        }

        return $data->paginate(10);
    }

    public function assign(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (DecryptException $e) {
            $request->session()->flash('error','Woop! Ocorreu um erro.');
            return redirect()->back();
        }

        $user = User::find($id);

        $typeAccounts = $this->typeAccountRepository->getTypeAccountWithModuleByUserId($id);

        $modulesLinked = $this->typeAccountRepository->getModulesByUserId($id);

        $params = array();
        $i = 0;

        foreach ($modulesLinked as $module) {
            $params[$i] = $module->module_id;
            ++$i;
        }

        $result = $this->moduleRepository->getModulesNoLinked($params);

        $modulesNoLinked = array();
        $modulesNoLinked[0] = '';

        foreach ($result as $module) {
            $modulesNoLinked[$module->id] = $module->name;
        }

        return view('admin.type-account-user.assign', compact('user', 'typeAccounts', 'modulesNoLinked'));
    }

    public function typeAccounts(Request $request)
    {
        $requestData = $request->all();

        $perfisNaoAtribuidos = $this->typeAccountRepository->getPerfisNaoAtribuidos($requestData['id']);

        $perfis = array();

        foreach ($perfisNaoAtribuidos as $perfil) {
            $perfis[$perfil->id] = $perfil->name;
        }

        //$json = json_encode($perfis);

        return response()->json($perfis);
    }

    public function detach(Request $request)
    {
        $userId = (int) $request->get('id');
        $typeAccountId = (int) $request->get('type_account_id');

        $result = $this->typeAccountUserRepository->detachTypeAccount($userId, $typeAccountId);

        if ($result) {
            $request->session()->flash('success','Perfil desvinculado com sucesso.');

            return redirect()->route('admin.type.account.user.assign',['id' => encrypt($userId)]);
        } else {
            $request->session()->flash('error','Algo deu errado, tente novamente.');

            return redirect()->route('admin.type.account.user.assign',['id' => encrypt($userId)]);
        }
    }

    public function attach(Request $request)
    {
        $userId = (int) $request->get('id');
        $typeAccountId = (int) $request->get('type_account_id');

        $result = $this->typeAccountUserRepository->attachTypeAccount($userId, $typeAccountId);

        if ($result) {
            $request->session()->flash('success','Perfil atribuído com sucesso.');

            return redirect()->route('admin.type.account.user.assign',['id' => encrypt($userId)]);
        } else {
            $request->session()->flash('error','Algo deu errado, tente novamente.');

            return redirect()->route('admin.type.account.user.assign',['id' => encrypt($userId)]);
        }
    }
}
