<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreRoleRequest;
use App\Src\Repositories\ResourceRepository;
use App\Src\Repositories\RoleRepository;
use App\Src\Model\Roles;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    protected $resourceRepository;
    protected $roleRepository;

    public function __construct(ResourceRepository $resourceRepository, RoleRepository $roleRepository)
    {
        $this->resourceRepository = $resourceRepository;
        $this->roleRepository = $roleRepository;
    }

    public function index()
    {
        $roles = $this->roleRepository->getAll();

        $rolesGruop = collect($roles)->groupBy('resource');

        return view('admin.role.index', compact('rolesGruop'));
    }

    public function create()
    {
        $resources = $this->resourceRepository->lists('name', 'id');

        return view('admin.role.create', compact('resources'));
    }

    public function store(StoreRoleRequest $request)
    {
        $data = [
            'name' => $request->name,
            'label' => $request->label,
            'resource_id' => $request->resource,
        ];

        $role = $this->roleRepository->create($data);

        if (!$role) {
            $request->session()->flash('error','Erro ao tentar salvar.');

            return redirect()->back()->withInput($request->all());
        }

        $request->session()->flash('success','Permissao criada com sucesso.');

        return redirect()->route('admin.role.index');
    }

    public function destroy(Request $request)
    {
        $id = $request->input('id');

        if ($this->roleRepository->delete($id)) {
            $request->session()->flash('success','Permissão excluída com sucesso.');
        } else {
            $request->session()->flash('error','Erro ao tentar excluir a permissão');
        }

        return redirect()->back();
    }
}
