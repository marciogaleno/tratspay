<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Plataforma\Client;
use App\Models\Plataforma\BankAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends Controller
{

    public function index(User $users, Request $request)
    {
        if ($request->has('q')) {
            $users = $users->where('email','ilike', sprintf('%s%%', $request->get('q')));
        }

        $users = $users->paginate();

        return view('admin.users.all', compact('users'))->with('titlePage', 'Todos os usuários');
    }

    public function show($id, BankAccount $bankAccount)
    {
        try {
            $id = decrypt($id);
        } catch (DecryptException $e) {
            $request->session()->flash('error','Woop! Ocorreu um erro.');
            return redirect()->back();
        }

        $user = Client::with(['user','bankAccounts'])->where('user_id', $id)->first();

        return view('admin.client.index', compact('user','bankAccount'))->with('titlePage', 'Dados Cadastrais');
    }
}