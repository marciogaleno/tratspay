<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(User $user)
    {
        $produtores = collect();
        $afiliados = collect();

        return view('admin.profile.index', compact('produtores','afiliados'))->with('titlePage', 'Perfis de Usuários');
    }
}