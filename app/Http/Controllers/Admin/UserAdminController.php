<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Src\Model\TypeAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAdminController extends Controller
{

    public function index()
    {
        $typeAccounts = TypeAccount::with('vincular')->get();

        return view('admin.users.index', compact('typeAccounts'))->with('titlePage', 'Usuários Administrativos');
    }
}