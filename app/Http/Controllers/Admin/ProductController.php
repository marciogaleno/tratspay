<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Categorie;
use App\Http\Requests\Plataforma\ProductFormRequest;
use App\Services\ProductService;

class ProductController extends Controller
{
    private $productService;

    function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index(Product $product, Categorie $categories)
    {
        $products = $product->paginate();

        $categories = $categories->orderBy('desc')->get();

        return view('admin.product.index', compact('products','categories'))->with('titlePage', 'Todos os produtos');
    }
    public function edit($product_id)
    {
        $product = Product::where('id', $product_id)->first();
        $categories = Categorie::pluck('desc', 'id');
        $severalPurchases = [1 => 'Sim', 0 => 'Não'];

        return view('admin.product.edit', compact('product', 'categories', 'severalPurchases'))->with('titlePage', 'Editar produto');
    }

    public function update(ProductFormRequest $request, $id)
    {   
        // Important because editing can not change the payment type.
        $data = $request->except(['payment_type']);

        if ($this->productService->updateProduct($data, $id, $request)) {
            session()->flash("success", "Dados atualizados com sucesso.");
            return redirect()->back();
            
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput()->withErrors($request->validator);

    }

    public function pendent(Product $product)
    {
        $products = $product->where('active', Product::ACTIVE)
                    ->where('approved', Product::DISAPPROVED)
                    ->with('categorie')
                    ->paginate();

        return view('admin.product.pendent', compact('products'))->with('titlePage', 'Produtos Pendentes');
    }

    public function disapprove(Product $product)
    {
        $products = $product->where('active', Product::ACTIVE)
                    ->where('approved', Product::DISAPPROVED)
                    ->with('categorie')
                    ->paginate();


        return view('admin.product.disapprove', compact('products'))->with('titlePage', 'Produtos Reprovados');
    }
}