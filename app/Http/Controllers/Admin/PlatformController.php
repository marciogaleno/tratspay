<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlatformController extends Controller
{
    public function index(User $user)
    {
        $data = [];

        return view('admin.platform.index', $data)->with('titlePage', 'Dados Gerais da Plataorma');
    }
}