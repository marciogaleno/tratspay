<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreTypeAccountRequest;
use App\Src\Repositories\TypeAccountRepository;
use App\Src\Repositories\RoleRepository;
use App\Src\Repositories\ModuleRepository;
use App\Src\Model\TypeAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TypeAccountController extends Controller
{
    protected $typeAccountRepository;
    protected $roleRepository;
    protected $moduleRepository;

    public function __construct(TypeAccountRepository $typeAccountRepository, RoleRepository $roleRepository, ModuleRepository $moduleRepository)
    {
        $this->typeAccountRepository = $typeAccountRepository;
        $this->roleRepository = $roleRepository;
        $this->moduleRepository = $moduleRepository;
    }

    public function index()
    {
        $typeAccounts = $this->typeAccountRepository->all();

        return view('admin.type-account.index', compact('typeAccounts'));
    }

    public function create()
    {
        $modules = $this->moduleRepository->lists('name', 'id');

        return view('admin.type-account.create', compact('modules'));
    }

    public function store(StoreTypeAccountRequest $request)
    {
        $data = [
            'name' => $request->name,
            'label' => $request->label,
            'enable' => $request->enable,
            'module_id' => $request->module
        ];

        $typeAccount = $this->typeAccountRepository->create($data);

        if (!$typeAccount) {
            $request->session()->flash('error','Erro ao tentar salvar.');

            return redirect()->back()->withInput($request->all());
        }

        $request->session()->flash('success','Perfil criado com sucesso.');

        return redirect()->route('admin.type.account.index');
    }

    public function destroy(Request $request)
    {
        $id = $request->input('id');

        if ($this->typeAccountRepository->delete($id)) {
            $request->session()->flash('success','Perfil excluído com sucesso.');
        } else {
            $request->session()->flash('error','Erro ao tentar excluir a perfil');
        }

        return redirect()->back();
    }
}
