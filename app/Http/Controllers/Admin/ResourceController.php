<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreResourceRequest;
use App\Src\Repositories\CategoryResourceRepository;
use App\Src\Repositories\ModuleRepository;
use App\Src\Repositories\ResourceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResourceController extends Controller
{
    protected $resourceRepository;
    protected $moduleRepository;
    protected $categoryResourceRepository;

    public function __construct(ResourceRepository $resourceRepository, ModuleRepository $moduleRepository, CategoryResourceRepository $categoryResourceRepository)
    {
        $this->resourceRepository = $resourceRepository;
        $this->moduleRepository = $moduleRepository;
        $this->categoryResourceRepository = $categoryResourceRepository;
    }

    public function index()
    {
        $resources = $this->resourceRepository->getAll();

        return view('admin.resource.index', compact('resources'));
    }

    public function create()
    {
        $modules = $this->moduleRepository->lists('name', 'id');
        $categories = $this->categoryResourceRepository->lists('name', 'id');

        return view('admin.resource.create', compact('modules', 'categories'));
    }

    public function store(StoreResourceRequest $request)
    {
        $data = [
            'module_id' => $request->module,
            'category_resource_id' => $request->category,
            'name' => $request->name,
            'label' => $request->label,
            'icon' => $request->icon,
            'enable' => $request->enable,
            'order' => $request->order,
        ];

        $resource = $this->resourceRepository->create($data);

        if (!$resource) {
            $request->session()->flash('error','Erro ao tentar salvar.');

            return redirect()->back()->withInput($request->all());
        }

        $request->session()->flash('success','Recurso criado com sucesso.');

        return redirect()->route('admin.resource.index');
    }

    public function destroy(Request $request)
    {
        $id = $request->input('id');

        if ($this->resourceRepository->delete($id)) {
            $request->session()->flash('success','Recurso excluída com sucesso.');
        } else {
            $request->session()->flash('error','Erro ao tentar excluir o recurso');
        }

        return redirect()->back();
    }
}
