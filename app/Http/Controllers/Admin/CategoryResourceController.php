<?php
namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\StoreCategoryResourceRequest;
use App\Src\Repositories\CategoryResourceRepository;
use App\Src\Model\CategoryResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryResourceController extends Controller
{
    protected $categoryResourceRepository;

    public function __construct(CategoryResourceRepository $categoryResourceRepository)
    {
        $this->categoryResourceRepository = $categoryResourceRepository;
    }

    public function index()
    {
        $categoryResources = $this->categoryResourceRepository->allOrderBy('order');

        return view('admin.category-resource.index', compact('categoryResources'));
    }

    public function create()
    {
        return view('admin.category-resource.create');
    }

    public function store(StoreCategoryResourceRequest $request)
    {
        $data = [
            'name' => $request->name,
            'label' => $request->label,
            'icon' => $request->icon,
            'order' => $request->order,
            'enable' => $request->enable,
        ];

        $categoria = $this->categoryResourceRepository->create($data);

        if (!$categoria) {
            $request->session()->flash('error','Erro ao tentar salvar.');

            return redirect()->back()->withInput($request->all());
        }

        $request->session()->flash('success','Categoria criada com sucesso.');

        return redirect()->route('admin.category.resource.index');
    }

    public function destroy(Request $request)
    {
        $id = $request->input('id');

        if ($this->categoryResourceRepository->delete($id)) {
            $request->session()->flash('success','Categoria excluída com sucesso.');
        } else {
            $request->session()->flash('error','Erro ao tentar excluir a categoria');
        }

        return redirect()->back();
    }
}
