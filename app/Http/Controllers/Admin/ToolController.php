<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ToolController extends Controller
{
    public function index()
    {
        return view('admin.tool.index')->with('titlePage', 'Ferramentas');
    }
}