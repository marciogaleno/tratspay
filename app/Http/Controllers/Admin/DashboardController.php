<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(Request $request)
    {
        $data = [
            'totalSalesToday'    => 0, // Vendas Totais Hoje
            'totalSalesMonth'    => 0, // Vendas Totais do Mês
            'winningsTratsToday' => 0, // Ganhos da Trats hoje
            'winningsTratsMonth' => 0, // Ganhos da Trats no mês
            'withdrawalToday'    => 0, // Saques realizados hoje
            'withdrawalMonth'    => 0, // Saques realizados no mês
            'pendingWithdrawRequest'  => 0, // Solicitações de Saques Pendentes
            'productAwaitingApproval' => 0, // Produtos Aguardando Aprovação
            'salesMadeToday'  => 0, // Vendas realizadas hoje
            'salesMadeMonth'  => 0, // Vendas realizadas no mês
            'userRegisteredPlatform' => 0 // Usuários Cadastrados na Plataforma
        ];

        return view('admin.dashboard.index', $data);
    }
}