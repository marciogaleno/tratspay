<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Plataforma\Client;
use App\Models\Plataforma\BankAccount;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Categorie;
use App\Src\Model\ConfigAccount;
use App\Traits\Upload;
use App\Models\Admin\AdminSystemConfiguration;

class SystemConfigurationController extends Controller
{
    use Upload;

    public function index(Categorie $categories, BankAccount $bankAccount, Request $request)
    {
        if ($request->has('q')) {
            $categories = $categories->where('desc','ilike', sprintf('%s%%', $request->get('q')));
        }

        $categories = $categories->orderBy('desc')->paginate();
        $config = AdminSystemConfiguration::pluck('value', 'key')->toArray();
        
        return view('admin.configuration.index', compact('categories', 'config'))->with('titlePage', 'Dados Cadastrais');
    }

    public function update(Request $request)
    {
        $data = $request->only('rate_physical_product', 'rate_virtual_product');

        if ($request->has('background_login')) {
            $filename = $this->background($request);
            if (!is_null($filename)) $data['background_login'] = $filename;
        }

        try {
            foreach ($data as $key => $value) {
                if (!AdminSystemConfiguration::where('key', $key)->update(['value' => $value])) {
                    AdminSystemConfiguration::insert(['key'=> $key, 'value' => $value]);
                }
            }

            session()->flash("success", "Informações atualizadas com sucesso.");
            return redirect()->route('admin.client.index');
        } catch(\Exception $e) {
            session()->flash("error", "Não foi possivel atualizar as informações.");
        }

        return redirect()->back();
    }

    private function background(Request $request)
    {
        $file = $request->file('background_login');

        try {
            return $this->saveImage($file, ConfigAccount::PATH_BACKGROUND_LOGIN, 'public');
        } catch(\Exception $e) {
            session()->flash("error", "Não foi possivel salvar imagem.");
        }

        return null;
    }

    public function category(Request $request, $category)
    {
        //@TODO CRIAR REQUEST VALIDATE
        try {
            $category = Categorie::find($category);

            if ($request->has('del')) {
                if (!$category->delete()) {
                    session()->flash("error", "Não foi possivel excluir.");
                    return redirect()->back();
                }
            }

            if ($request->has('name')) {
                $category->desc = $request->get('name');
                if (!$category->save()) {
                    session()->flash("error", "Não foi possivel salvar.");
                    return redirect()->back();
                }
            }
        } catch (\Exception $e) {
            session()->flash("error", "Não foi possivel salvar.");
            return redirect()->back();
        }

        session()->flash("success", "Informações atualizadas com sucesso.");
        return redirect()->back();
    }
}