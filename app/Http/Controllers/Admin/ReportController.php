<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function sales()
    {
        return view('admin.report.sales')->with('titlePage', 'Todas as Vendas');
    }

    public function extract()
    {
        return view('admin.report.extract')->with('titlePage', 'Extrato Financeiros');
    }
}