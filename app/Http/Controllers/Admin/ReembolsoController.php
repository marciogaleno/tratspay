<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Repository\ReembolsoRepository;

class ReembolsoController extends Controller
{
    public function __construct(ReembolsoRepository $reembolsoRepository)
    {
        $this->reembolsoRepository = $reembolsoRepository;
    }

    public function index(Request $request)
    {
        $reembolsos = $this->reembolsoRepository->getFiltered($request->all());

        return view('admin.reembolsos.index', compact('reembolsos'));
    }

    public function view($id)
    {
        $reembolso = $this->reembolsoRepository->find($id);

        if(!$reembolso) {
            session()->flash("error", "Reembolso não existe.");
            return redirect()->back();
        }

        return view('admin.reembolsos.view', compact('reembolso'));
    }

    public function edit($id, $status)
    {
        $reembolso = $this->reembolsoRepository->find($id);

        if(!$reembolso) {
            session()->flash("error", "Reembolso não existe.");
            return redirect()->back();
        }

        if($status != "A" && $status!= "R") {
            session()->flash("error", "Status não existe.");
            return redirect()->back();
        }

        $reembolso->status = $status;

        if(!$reembolso->save()){
            session()->flash("error", "Erro ao realizar reembolso.");
            return redirect()->back();
        } else {
            session()->flash("success", "Status reembolso atualizado com sucesso");
            return redirect()->back();
        }

        return view('admin.reembolsos.view', compact('reembolso'));
    }

    public function create(Request $request)
    {
        $reembolso = $this->reembolsoRepository->create($request->all());

        if(!$reembolso)
            return Response(['msg'=> 'Erro ao solicitar reembolso'], 500);

        return Response(200)->withHeaders(['Content-Type: application/json']);
    }
}
