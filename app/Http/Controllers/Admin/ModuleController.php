<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreModuleRequest;
use App\Src\Repositories\ModuleRepository;
use Illuminate\Http\Request;

class ModuleController extends Controller
{
    protected $moduleRepository;

    public function __construct(ModuleRepository $module)
    {
        $this->moduleRepository = $module;
    }

    public function index()
    {
        $modules = $this->moduleRepository->all();

        return view('admin.module.index', compact('modules'));
    }

    public function create()
    {
        return view('admin.module.create');
    }

    public function store(StoreModuleRequest $request)
    {
        $data = [
            "name" => $request->name,
            "icon" => $request->icon,
            "label" => $request->label,
            "enable" => $request->enable
        ];

        $module = $this->moduleRepository->create($data);

        if (!$module) {
            $request->session()->flash('error', 'Erro ao tentar salvar.');
            
            return redirect()->back()->withInput($request->all());
        }

        $request->session()->flash('success', 'Módulo criado com sucesso.');

        return redirect()->route('admin.module.index');
    }

    public function destroy(Request $request)
    {
        $id = $request->input('id');

        if ($this->moduleRepository->delete($id)) {
            $request->session()->flash('success', 'Módulo excluído com sucesso.');
        } else {
            $request->session()->flash('error', 'Erro ao tentar excluir o módulo.');
        }

        return redirect()->back();
    }
}
