<?php

namespace App\Http\Controllers\Admin;

use App\Src\Repositories\TypeAccountRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;

class TypeAccountRoleController extends Controller
{
    protected $typeAccountRepository;

    public function __construct(TypeAccountRepository $typeAccountRepository)
    {
        $this->typeAccountRepository = $typeAccountRepository;
    }

    public function index()
    {
        $typeAccounts = $this->typeAccountRepository->getAllTypeAccountsWithModules();

        return view('admin.type-account-role.index', compact('typeAccounts'));
    }

    public function sync(Request $request, $id)
    {
        try {
            $id = decrypt($id);
        } catch (DecryptException $e) {
            $request->session()->flash('error','Woop! Ocorreu um erro.');
            return redirect()->back();
        }
        
        $typeAccount = $this->typeAccountRepository->getTypeAccountWithModule($id);

        $roles = $this->typeAccountRepository->getTreeOfRolesByTypeAccountAndModule($typeAccount->id, $typeAccount->module_id);

        return view('admin.type-account-role.sync', compact('typeAccount', 'roles'));
    }

    public function syncRole(Request $request)
    {
        $typeAccountId = $request->type_account;

        if ($request->input('roles') == '') {
            $request->session()->flash('error','Não existem permissões cadastradas para o módulo no qual esse perfil faz parte.');

            return redirect()->route('admin.type.account.roles.sync',['id'=> encrypt($typeAccountId)]);
        }

        $roles = explode(',', $request->input('roles'));

        $this->typeAccountRepository->syncRoles($typeAccountId, $roles);

        $request->session()->flash('success','Permissões atribuidas com sucesso.');

        return redirect()->route('admin.type.account.roles.sync',['id'=> encrypt($typeAccountId)]);
    }
}
