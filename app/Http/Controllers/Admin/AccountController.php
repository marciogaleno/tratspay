<?php
namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Plataforma\BankAccount;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function index(User $user)
    {
        $bankAccounts = BankAccount::with('user')->where('status', BankAccount::STATUS_WAIT)->get();

        return view('admin.account.approve', compact('bankAccounts'))->with('titlePage', 'Aprovação de Contas');
    }
}