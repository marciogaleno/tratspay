<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\TransactionAccount;
use App\Models\Plataforma\Cashout;
use Illuminate\Support\Facades\DB;

class CashoutController extends Controller
{
    public function index()
    {
        $sql = "SELECT A.ID cashout_id, C.NAME AS bank_name, A.value, A.created_at date_solicitation, 
                    A.status status_cashout, B.*, D.NAME name_user, D.email, E.cpf_cnpj
                    FROM CASHOUTS A, BANK_ACCOUNT B, BANKS C, USERS D, CLIENTS E
                    WHERE A.BANK_ACCOUNT_ID = B.ID 
                    AND B.BANK = C.CODE
                    AND A.USER_ID = D.ID
                    AND E.USER_ID = A.USER_ID
                    ORDER BY A.ID desc";

        $cashouts = DB::select($sql);

        return View('admin.cashouts.index', compact('cashouts'))->with('titlePage', 'Solicitações de Saque');
    }

    public function process(Request $reques, $id)
    {
        try {
            $cashout = Cashout::where('id', $id)->withoutGlobalScopes()->first();

            $update = $cashout->update([
                'status' => 'finished'
            ]);

            if ($update) {
                session()->flash("success", "Saque processado com sucesso");
                return redirect()->back();
            }

        } catch (\Throwable $th) {
            throw $th;
        }

        session()->flash("error", "Dados atualizados com sucesso.");
            return redirect()->back();
    }
}
