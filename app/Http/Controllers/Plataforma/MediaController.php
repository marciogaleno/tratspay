<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Media;
use App\Http\Requests\Plataforma\MediaFormRequest;

class MediaController extends Controller
{
    private $numberTab = 8
    ;

    public function add()
    {
        $medias = Media::where('product_id', app('request')->product->id)->get();

        return View('plataforma.media.add', compact('medias'))->with('numberTab', $this->numberTab);
    }

    public function create(MediaFormRequest $request)
    {
        $data = $request->except(['_token']);
        $data['product_id'] = app('request')->product->id;

        if (Media::create($data)) {
            session()->flash("success", "Criado com sucesso.");
            return redirect()->route("product.media.add", [app('request')->product->id]);
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function delete($product_id, $id)
    {
        $media = Media::find($id);
        
        if ($media->delete()) {
            session()->flash("success", "Mídia deletada com sucesso!");
            return redirect()->back();
        }
        
        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();   
    }
}
