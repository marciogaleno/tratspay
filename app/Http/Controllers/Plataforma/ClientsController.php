<?php

namespace App\Http\Controllers\Plataforma;

use App\Http\Requests\Plataforma\ClientBankFormRequest;
use App\User;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Mail\SendEmailUpdate;
use App\Models\Plataforma\Client;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Plataforma\BankAccount;
use Illuminate\Support\Facades\Storage;
use App\Helpers;


class ClientsController extends Controller
{

    public function edit(BankAccount $bankAccount)
    {


        $user = Client::with(['user','bankAccounts'])->first();
       //dd(auth()->user()->image);
        if(in_array(1, $user->bankAccounts->toArray()) && in_array(2, $user->bankAccounts->toArray())){
            $exists = "true";
        }else{
            $exists = "false";
        }

        // dd(Helper::ListCountries($user->country));
        return view('plataforma.clients.edit', compact('user','bankAccount','exists'))->with('titlePage', 'Editar Perfil de '.$user->user->name);
    }

    public function update(Request $request)
    {

        DB::beginTransaction();

        $data = $request->all();
        if (!isset($data['doacao'])) {
            $data['doacao'] = 0;
        }

        $data['postal_code'] = str_replace(['-','.'],'',$data['postal_code']);

        $name = $data['name'];
        $user = User::find(Auth()->id(Auth::user()->name));


        $update = $user->update(['name' => $name]);

        // Remover pois não existe name na tabela clients
        unset($data['name']);

        if($user->email != $data['email']){
            Mail::to($user->email)
            ->send(new SendEmailUpdate('Confirme a atualizaçao de email!', $user, $data['email']));

            unset($data['email']);
        }

        $update = $user->client->update($data);


        if ($update) {
            DB::commit();
            if(!isset($data['email'])){
                session()->flash("success", "Para finalizar sua atualizaçao de dados, confira seu email.");
                return redirect()->back();
            }
            session()->flash("success", "Atualizado com sucesso.");
            return redirect()->back();
        }

        DB::rollBack();
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function addBankAccount(ClientBankFormRequest $request)
    {
        DB::beginTransaction();

        $user_id = auth()->id();
        $data = $request->all();

        $data['user_id'] = $user_id;
        $data['status'] = 0;

        $account = BankAccount::create($data);

        if ($account) {

            DB::commit();
            session()->flash("success", "Conta adicionada com sucesso.");
            return redirect()->route("client.edit");
        }

        DB::rollBack();
        session()->flash("danger", "Verifique seus dados, inclusão não concluída!");
        return redirect()->back()->withInput();
    }

    public function removeBankAccount(Request $request)
    {
        $account = BankAccount::find($request->id);
        if(!$account) {
            session()->flash("danger", "Conta não encontrada.");
            return redirect()->back();
        }
        if(!$account->delete()){
            session()->flash("danger", "Erro ao excluir a  conta bancária");
            return redirect()->back();
        }

        // session()->flash("success", "Conta removida com sucesso.");
        // return redirect()->route("client.edit");
    }

    public function changeImage(Request $request)
    {

        $user_id = auth()->id();
        $data = $request->all();
        $data['user_id'] = $user_id;
        $upload = null;
        $client = User::find(auth()->id());

        $request->validate([
            'image_profile'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ],[
            'image_profile.required'     =>  'O campo IMAGEM DE PERFIL é obrigatório.',
            'image_profile.mimes'  =>  'O campo IMAGEM DE PERFIL deve conter um arquivo do tipo: jpeg, bmp, png, gif, svg, pdf.',
            'image_profile.image'  =>  'O campo IMAGEM DE PERFIL deve conter uma imagem.',
            'image_profile.max'  =>  'O campo IMAGEM DE PERFIL deve conter um tamanho máximo de 2MB'
        ]);

        if($request->hasFile('image_profile') && Storage::disk('images')->exists($client->image)){
            Storage::disk('images')->delete($client->image);
        }

        if ($request->hasFile('image_profile') && $request->file('image_profile')->isValid()) {
            $file = Storage::disk('images')->put($client->id . '/profile',  $data['image_profile']);
            $data['image'] = $file;
        }

        $update = $client->update($data);

        if ($update) {
            session()->flash("success", "Atualizado com sucesso.");
            return redirect()->back()->withInput();
        }

        session()->flash("danger", "Verifique seus dados e anexos, inclusão não concluída!");
        return redirect()->back()->withInput();
    }

    public function updateEmail(Request $request)
    {
        DB::beginTransaction();

        $data = $request->all();

        $email = $data['email'];
        $user = User::find($data['id']);

        $update = $user->update(['email' => $email]);

        if ($update) {
            DB::commit();
            session()->flash("success", "Email atualizado com sucesso.");
            return redirect('client/edit');
        }

        DB::rollBack();
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect('client/edit');
    }

}
