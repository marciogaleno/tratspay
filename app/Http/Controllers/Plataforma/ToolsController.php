<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Doctrine\DBAL\Schema\View;

class ToolsController extends Controller
{
    private $titlePage = 'Ferramentas Inovadoras';

    public function index()
    {
        return View('plataforma.tools.index')->with('titlePage', $this->titlePage);
    }

    public function linkManager()
    {
        return View('plataforma.tools.linkManager');
    }

    public function leadAutomator()
    {
        return View('plataforma.tools.leadAutomator');
    }
}
