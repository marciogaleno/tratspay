<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Categorie;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Repository\MarketRepository;


class MarketController extends Controller
{
     public function __construct(MarketRepository $marketRepository)
     {
         $this->marketRepository = $marketRepository;
     }

    function index(Request $request)
    {
        $categories = Categorie::pluck('desc', 'id');

        $products = $this->marketRepository->getFiltered($request->all());

        $productsLast = $this->marketRepository->getFilteredLast($request->all());

        return View('plataforma.market.list-all', compact('products', 'productsLast','categories'));
    }
}
