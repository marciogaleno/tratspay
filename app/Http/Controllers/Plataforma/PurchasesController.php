<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Purchase;
use Illuminate\Support\Facades\Auth;
use \Carbon\Carbon;
use App\Services\FileService;

class PurchasesController extends Controller
{
    public function index()
    {
        $purchases = Purchase::where('user_id', Auth::user()->id)->with('product')->get();
        $now = Carbon::now();

        foreach ($purchases as $purchase) {
            $carbon = Carbon::createFromFormat('Y-m-d H:i:s',$purchase->created_at)->addDays($purchase->product->warranty);
            $purchase->product->warranty = $now->diffInDays($carbon) <= 0 ? "Garantia Expirada" : $now->diffInDays($carbon). " Dias restantes";
        }

        return view('plataforma.purchases.list', compact('purchases'))->with('titlePage', 'Minhas Compras');
    }

    public function download(Request $request)
    {
        $data = $request->except('_token');

        $filePath = $data['file_path'];
        $fileName = $data['file_name'];

        return FileService::download($filePath, $fileName, 'products');
    }
}
