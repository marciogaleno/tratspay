<?php

namespace App\Http\Controllers\Plataforma;

use App\Repository\Plataforma\IndicatedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Services\IndicationService;

class IndicationController extends Controller
{
    public function __construct( IndicatedRepository $indicatedRepository)
    {
        $this->indicatedRepository = $indicatedRepository;
    }

    public function index()
    {
        $userId = Auth::user()->id;

        $indication = DB::select("select count(*) total from users a where a.user_indication = {$userId}");

        $total = $indication[0]->total;

        return View('plataforma.indication.index', compact('total'));
    }

    public function report(Request $request)
    {
        $commissions  = $this->indicatedRepository->getFiltered($request->all());
        $user = User::find(Auth::user()->id);
        $products = $user->products()->where('relation_type', 'producer')->pluck('products.name', 'products.id');

        $coaffiliates = DB::table('co_affiliates')
            ->join('products', 'co_affiliates.product_id', '=', 'products.id')
            ->join('producer_affiliate', 'producer_affiliate.code', '=', 'co_affiliates.code_affiliate')
            ->join('users', 'producer_affiliate.user_id', '=', 'users.id')
            ->where('co_affiliates.user_id', Auth::user()->id)
            ->pluck('users.name', 'co_affiliates.id');


        $paymentForms = ['credit_card' => 'Cartão', 'ticket' => 'Boleto'];

        return View('plataforma.indicated.index', compact('commissions', 'products', 'paymentForms','coaffiliates'))->with('titlePage', 'Todas as vendas de indicados');
    }

    public function tracking(Request $request, $indication_code)
    {
        $indicatedService = new IndicationService;
  
        return $indicatedService->tracking($request, $indication_code);
    }
    

}
