<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Coupon;
use App\Models\Plataforma\Plan;
use App\Models\Plataforma\Product;
use App\Repository\Plataforma\CouponRepository;
use App\Models\Plataforma\Checkout;

class CouponsController extends Controller
{
    private $numberTab = 10;
    private $couponRepository;

    public function __construct(CouponRepository $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    public function index($product_id)
    {
        $coupons = Coupon::where('product_id', $product_id)->get();
 
        return View('plataforma.coupons.index', compact('coupons'))
                     ->with('numberTab', $this->numberTab);
    }

    public function add($product_id)
    {
        $plans = Plan::where('product_id', $product_id)->pluck('desc', 'id');
        $affiliates = app('request')->product->affiliates->pluck('name', 'id');

        return View('plataforma.coupons.add', compact('plans', 'affiliates'))
                     ->with('numberTab', $this->numberTab);        
    }

    public function edit($product_id, $coupon_id)
    {
        $plans = Plan::where('product_id', $product_id)->pluck('desc', 'id');
        $affiliates = app('request')->product->affiliates->pluck('name', 'id');
        $coupon = Coupon::where('id', $coupon_id)->first();

        return View('plataforma.coupons.add', compact('plans', 'affiliates', 'coupon'))
                     ->with('numberTab', $this->numberTab);        
    }

    public function urls($product_id, $coupon_id)
    {
        $coupon = Coupon::where('id', $coupon_id)->with('plan')->first();
        $codeCoupon = $coupon->code;
        $checkouts = $coupon->plan->checkouts;

        $urls = [];

        foreach ($checkouts as $checkout) {
            $urls[] = [
                'desc' => $checkout->desc,
                'url' =>  url('/c') . '/' . $checkout->code . '?cupom=' . $coupon->code
            ];
        }

        return $urls;      
    }

    public function create(Request $request, $product_id)
    {
        $data = $request->except('_token');
        $data['product_id'] = $product_id;

        if ($this->couponRepository->create($data)) {
            session()->flash("success", "Salvo com sucesso!");
            return redirect()->route('product.coupon.index', $product_id);
        }

        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function update(Request $request, $product_id, $coupon_id)
    {
        $data = $request->except('_token');

        if ($this->couponRepository->update($coupon_id, $data)) {
            session()->flash("success", "Salvo com sucesso!");
            return redirect()->route('product.coupon.index', $product_id);
        }

        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function delete($product_id, $coupon_id)
    {
        if ($this->couponRepository->delete($coupon_id)) {
            session()->flash("success", "Cupom deletado com sucesso!");
            return redirect()->route('product.coupon.index', $product_id);
        }

        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
}
