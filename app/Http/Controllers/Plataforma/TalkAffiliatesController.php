<?php

namespace App\Http\Controllers\Plataforma;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Models\Plataforma\Messages;
use App\Models\Plataforma\Categorie;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Repository\TalkAffiliatesRepository;


class TalkAffiliatesController extends Controller
{
    public function __construct(TalkAffiliatesRepository $talkAffiliatesRepository)
    {
        $this->talkAffiliatesRepository = $talkAffiliatesRepository;
    }

    function index(Request $request)
    {
        return View('plataforma.talktoaffiliates.index');
    }

    function allMessages(Request $request)
    {
        $messages = $this->talkAffiliatesRepository->getMessages($request);

        return View('plataforma.talktoaffiliates.allmessages', compact('messages'));
    }

    function getCreate(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $productsFilter = $user->products()->where('relation_type', 'producer')->pluck('products.name', 'products.id');

        return View('plataforma.talktoaffiliates.send', compact('productsFilter'));
    }

    function getView($idMessage)
    {
        $message = Messages::find($idMessage);
        $user = User::find(Auth::user()->id);

        if (!$message) {
            session()->flash("error", "Mensagem não existe");
            return redirect()->back()->withInput();
        }

        $message->read = 1;

        if(!$message->save()) {
            session()->flash("error", "Erro ao visualizar mensagem");
            return redirect()->back()->withInput();
        }

        return View('plataforma.talktoaffiliates.view', compact('message', 'user'));
    }

    function send(Request $request)
    {
        if (!$request->get('content') || !$request->get('product_id')) {
            session()->flash("error", "Conteudo da mensagem vazio. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }

        $existsAffiliate = $this->talkAffiliatesRepository->existsAffiliate($request->all());
        if (!$existsAffiliate) {
            session()->flash("error", "Produtos selecionados não possuem affiliados");
            return redirect()->back()->withInput();
        }

        $sended = $this->talkAffiliatesRepository->sendMessages($existsAffiliate, $request['content']);

        if (!$sended) {
            session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }

        session()->flash("success", "Mensagem enviada com sucesso!");
        return redirect('/talktoaffiliates');
    }

    function updateMessage(Request $request)
    {
        unset($request['_token']);
        $message = Messages::whereIn('id', $request->get('ids'))
            ->where('id_affiliate', Auth::user()->id)
            ->update(['trash' => 1]);

        if (!$message) {
            return Response(['error' => 'Mensagem não excluída'], 500);
        }

        return Response(['success' => 'excluido com sucesso', 200]);
    }

    function getMessagesApi()
    {
        $messages = $this->talkAffiliatesRepository->getMessagesApi();

        return Response($messages, 200);
    }
}
