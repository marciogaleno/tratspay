<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\ProducerAffiliate;
use App\Models\Plataforma\Checkout;
use App\Models\Plataforma\UrlAlternative;
use App\Http\Requests\Plataforma\UrlAlternativeFormRequest;

class UrlsController extends Controller
{
    private $numberTab = 3;

    public function index($product_id)
    {
        $producer = ProducerAffiliate::where('product_id', $product_id)
            ->where('relation_type', 'producer')
            ->with('product')
            ->first();

        $checkouts = Checkout::whereHas('plan', function($q) use ($product_id) {
            $q->where('product_id', $product_id);
        })->get();

        $urlAlternatives = UrlAlternative::where('product_id', $product_id)->get();

        return view('plataforma.urls.index', compact('producer', 'checkouts', 'urlAlternatives'))
                ->with('numberTab', $this->numberTab);
    }

    public function createUrlAlternative(UrlAlternativeFormRequest $request, $product_id)
    {
        $data = $request->except('_token');
        $data['product_id'] = $product_id;

        if (isset($data['private']) && $data['private']) {
            $data['private'] = 1;
        } else {
            $data['private'] = 0;
        }
        if (UrlAlternative::create($data)) {
            session()->flash("success", "Criado com sucesso.");
            return redirect()->back();
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
}
