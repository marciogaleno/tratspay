<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\File;
use App\Services\FileService;
use Illuminate\Support\Facades\DB;

class FilesController extends Controller
{
    public function index($product_id)
    {   
        $product = Product::find($product_id);

        $files = [];
        $productsFiles = File::where('product_id', $product_id)->orderBy('created_at', 'desc')->get();
    
            foreach ($productsFiles as $file) {
                $infoParts = pathinfo($file['name']);
               
                if (!array_key_exists($infoParts['extension'], $this->filesIcons)) {
                    $iconExtension = $this->filesIcons['default'];
                } else {
                    $iconExtension = $this->filesIcons[$infoParts['extension']];
                }

                $files[] = [
                    'name' => $file['name'],
                    'file_path' => $file['file_path'],
                    'name_original' => $file['name_original'],
                    'iconExtension' => $iconExtension,
                    'size'   =>  $this->formatSizeUnits($file['size'])
                ];
        }
        //$request->fileProduct->store('products', 's3');
        
        return view('plataforma.files.index', compact('files', 'product'))
                ->with('numberTab', 4);
    }

    public function upload(Request $request)
    {
        $params = $request->all();

        $file = FileService::uploadFileProduct(app('request')->product->id,  app('request')->product->uuid);

        if (!$file) {
            return json_encode([
                'status' => 'error',
                'message' => 'Erro ao fazer o upload'
            ]);
        }

        try {

            DB::beginTransaction();
            File::create($file);
            DB::commit();

        } catch (Throwable $th) {
            DB::rollBack();
            Storage::disk('products')->delete($file['file_path']);
            
            return json_encode([
                'status' => 'error',
                'message' => 'Erro ao fazer o upload'
            ]);
        }

        return json_encode([
            'status' => 'ok',
            'message' => 'Upload realizado com suceso',
            'data' => $file
        ]);


    }

    public function delete(Request $request, $product_id)
    {
        $data = $request->except('_token');
        $filePath = $data['file_path'];

        if (FileService::delete($filePath)) {
            File::where('product_id', $product_id)->where('file_path', $filePath)->delete();
            
            session()->flash("success", "Arquivo excluído com sucesso!");
            return redirect()->back();
        }
        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
    
    public function download(Request $request, $product_id)
    {

        $data = $request->except('_token');

        $filePath = $data['file_path'];
        $fileName = $data['file_name'];

        return FileService::download($filePath, $fileName);
           
    }

}
