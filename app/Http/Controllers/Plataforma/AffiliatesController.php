<?php

namespace App\Http\Controllers\Plataforma;

use App\Helpers\Helper;
use App\Models\Plataforma\Categorie;
use App\Models\Produto;
use App\User;
use Illuminate\Http\Request;
use App\Models\Plataforma\Product;
use App\Scopes\Tenant\TenantScope;
use Illuminate\Support\Facades\DB;
use App\Models\Plataforma\Checkout;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Plataforma\ProducerAffiliate;

class AffiliatesController extends Controller
{
    public function list(Request $request)
    {
        $user = User::find(Auth::user()->id);

//        $products = Product::join('producer_affiliate', function ($join){
//                $join->on('product_id', '=', 'products.id')
//                    ->where('relation_type', '=', "affiliate");
//            })
//            ->leftJoin('transaction_mp', function ($join){
//                $join->on('transaction_mp.product_id', '=', 'products.id')
//                    ->where('transaction_mp.code', '=', "producer_affiliate.code");
//            })
//            ->leftJoin('commissions', 'commissions.transaction_id', '=', 'transaction_mp.id')
//            ->where('producer_affiliate.user_id', $user->id)
//            ->select('products.*');

        $products = $user->products()->where('relation_type', 'affiliate');

        if($request['product_name']) {
            $products->whereIn('products.id', $request['product_name']);
        }

        $products =  $products->get();
        $productsFilter = $user->products()->where('relation_type', 'affiliate')->pluck('products.name', 'products.id');
        return view('plataforma.affiliates.list', compact('products', 'productsFilter'));
    }

    public function detailsProduct($product_id)
    {
        $product = Product::where('id', $product_id)->withoutGlobalScopes()->first();

        $category = Categorie::find($product->category_id);

        $valueComission = Helper::getValueCommissionByType($product);

        $producer = $product->producer[0];

        $salePage =  $product->salepage()->withoutGlobalScopes()->first();

        $checkouts = Checkout::whereHas('plan', function($q) use ($product_id) {
            $q->where('product_id', $product_id);
        })->get();


        return view('plataforma.affiliates.details-product', compact('product', 'valueComission', 'producer', 'salePage', 'category', 'checkouts'));
    }

    public function urls($product_id)
    {
        $product = Product::where('id', $product_id)->withoutGlobalScopes()->first();

        $checkouts = Checkout::whereHas('planChk', function($q) use ($product) {
            $q->where('product_id', $product->id);
        })->withoutGlobalScopes()->get();

        return view('plataforma.affiliates.urls', compact('product', 'checkouts'));
    }

    public function solicitacao()
    {
        $user = User::find(Auth::user()->id);

        $products = $user->products()->where('affiliate')->withoutGlobalScopes()->get();

        return view('plataforma.affiliates-solicitacao.list', compact('products'));
    }

    public function myRequest(ProducerAffiliate $product_id = null)
    {
        $data = ['user, producers, product_id'];

        $user = User::find(Auth::user()->id);

        $producers = ProducerAffiliate::where('product_id', $product_id)->where('relation_type', 'affiliate')->withoutGlobalScopes()->get();

        return view('plataforma.affiliates-solicitacao.list-request', compact('producers'));
    }

    public function cancel(Request $request)
    {
        $affiliate = ProducerAffiliate::where('product_id', $request->product_id)
                    ->where('user_id', auth()->user()->id)
                    ->where(function($q) {
                        $q->orWhere('relation_type', 'affiliate');
                    })
                    ->first();
        $cancel = $affiliate->update(['status' => 'I']);

        if($cancel){
            return "true";
        }

        // ESTO NAO VAI POR QUE É COM JAVASCRIPT NAO PHP
        // O ALERT VAI DEFINIDO NA RESPOSTA DO SCRIPT
        // if ($cancel) {
        //     session()->flash("success", "Afiliação cancelada com succeso.");
        //     return redirect()->back();
        // }

        // session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        // return redirect()->back()->withInput();
    }

    public function listMyAffiliates(Request $request)
    {
        $product = Product::find($request->id);

        $user_id = $product->affiliates->first();
        if($user_id != NULL){
            $product_affiliates = $product->affiliates;
            $user_id = $user_id->pivot->user_id;

            $comision = DB::table('commissions')
                ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
                ->join('users', 'commissions.user_id', '=', 'users.id')
                ->where('users.id',$user_id)
                ->whereIn('transaction_mp.status', ['finished', 'completed'])
                ->select('commissions.*', 'transaction_mp.phone', 'users.id','users.name')
                ->groupBy(['users.id', 'users.name'])
                ->count();
        }else{
            $product_affiliates = "";
            $comision = "";
        }

         $vendas = DB::table('transaction_mp')
            ->join('products', 'transaction_mp.product_id', '=', 'products.id')
            ->where('transaction_mp.status','finished')
            ->count();

            return ['comision' => $comision, 'vendas' => $vendas, 'product' => $product_affiliates];
    }

    public function statusAffiliate(Request $request)
    {
        $affiliate = ProducerAffiliate::where('product_id', $request->product_id)
                    ->where('user_id', $request->affiliate_id)
                    ->where(function($q) {
                        $q->orWhere('relation_type', 'affiliate');
                    })
                    ->first();

        $affiliate->update(['status' => $request->estatus]);
    }
}
