<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\TransactionMp;
use Illuminate\Support\Facades\Auth;
use App\Repository\ReembolsoRepository;
use Illuminate\Support\Facades\DB;

class ReembolsoController extends Controller
{
    public function __construct(ReembolsoRepository $reembolsoRepository)
    {
        $this->reembolsoRepository = $reembolsoRepository;
    }

    public function index(Request $request)
    {
        $reembolsos = $this->reembolsoRepository->getFiltered($request->all());

        return view('plataforma.reembolsos.index', compact('reembolsos'));
    }

    public function view($id)
    {
        $reembolso = $this->reembolsoRepository->find($id);

        if(!$reembolso) {
            session()->flash("error", "Reembolso não existe.");
            return redirect()->back();
        }

        return view('plataforma.reembolsos.view', compact('reembolso'));
    }

    public function edit($id, $status)
    {
        $reembolso = $this->reembolsoRepository->find($id);

        if(!$reembolso) {
            session()->flash("error", "Reembolso não existe.");
            return redirect()->back();
        }

        if($status != "A" && $status!= "R") {
            session()->flash("error", "Status não existe.");
            return redirect()->back();
        } 

        try {

            DB::beginTransaction();

            $reembolso->status = $status;

            if($reembolso->save()){
                DB::commit();
                session()->flash("success", "Status reembolso atualizado com suceso!");
                return redirect()->back();               
            }
            
            DB::commit();

        } catch (\Throwable $th) {
            throw $th;
            DB::rollBack();
        }

        session()->flash("error", "Erro ao realizar reembolso.");
        return redirect()->back();
  
        return view('plataforma.reembolsos.view', compact('reembolso'));
    }

    public function create(Request $request)
    {
        $reembolso = $this->reembolsoRepository->create($request->all());

        if(!$reembolso)
            return Response(['msg'=> 'Erro ao solicitar reembolso'], 500);

        return Response(200)->withHeaders(['Content-Type: application/json']);
    }
}
