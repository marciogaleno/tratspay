<?php

namespace App\Http\Controllers\Plataforma;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Categorie;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Repository\ReportRepository;


class ReportController extends Controller
{
     public function __construct(ReportRepository $reportRepository)
     {
         $this->reportRepository = $reportRepository;
     }

    function salesCoProducer(Request $request)
    {
        $page = ['title' => 'co-produtores', 'route' => 'report.salescoproducer'];
        $user = User::find(Auth::user()->id);

        $productsFilter = $user->products()->where('relation_type', 'producer')->pluck('products.name', 'products.id');

        $report =  empty($request->all()) ? [] : $this->reportRepository->getSalesCoproducer($request);

        return View('plataforma.report.base', compact('page','report', 'productsFilter'));
    }

    function salesAffiliates(Request $request)
    {
        $page = ['title' => 'Afiliados', 'route' => 'report.salesaffiliate'];
        $user = User::find(Auth::user()->id);

        $productsFilter = $user->products()->where('relation_type', 'producer')->pluck('products.name', 'products.id');

        $report =  empty($request->all()) ? [] : $this->reportRepository->getSalesAffiliate($request);

        return View('plataforma.report.base', compact('page','report', 'productsFilter'));
    }

    public function salesCoAffiliates(Request $request)
    {
        $page = ['title' => 'Co-Afiliados', 'route' => 'report.salescoaffiliate'];
        $user = User::find(Auth::user()->id);

        $productsFilter = $user->products()->where('relation_type', 'coaffiliate')->pluck('products.name', 'products.id');

        $report =  empty($request->all()) ? [] : $this->reportRepository->getSalesCoAffiliate($request);

        return View('plataforma.report.base', compact('page','report', 'productsFilter'));
    }

    function salesManager(Request $request)
    {
        $page = ['title' => 'Gerentes', 'route' => 'report.salesmanager'];
        $user = User::find(Auth::user()->id);

        $productsFilter = $user->products()->where('relation_type', 'producer')->pluck('products.name', 'products.id');

        $report =  empty($request->all()) ? [] : $this->reportRepository->getSalesManager($request);

        return View('plataforma.report.base', compact('page','report', 'productsFilter'));
    }

    function salesIndication(Request $request) {
        $page = ['title' => 'Indicações', 'route' => 'report.salesIndication'];

        $indicados = User::where('user_indication', Auth::user()->id)->pluck('name', 'id');

        $totalComissions  = 0; //implement this
        $totalTransactions  = 0; //implement this
        $user = User::find(Auth::user()->id);
        $products = $user->products()->pluck('products.name', 'products.id');

        $report =  empty($request->all()) ? [] : $this->reportRepository->getSalesIndication($request);

        return View('plataforma.report.salesIndication', compact('page','report', 'indicados', 'totalComissions', 'totalTransactions', 'products'))->with('titlePage', 'Todas as Vendas de indicados');
    }

}
