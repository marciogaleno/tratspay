<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Plan;
use App\Services\PlanService;
use App\Http\Requests\Plataforma\PlanFormRequest;

class PlansController extends Controller
{
    private $plan;
    private $planService;
    private $numberTab = 1;

    function __construct(Plan $plan, PlanService $planService )
    {
        $this->plan = $plan;
        $this->planService = $planService;
    }

    public function view($product_id) 
    {
        $product = Product::where("id", $product_id)->with('plans')->first();

        return view('plataforma.plans.view', compact('product'))
                ->with('numberTab', $this->numberTab);
    }

    public function add() 
    {        
        return view('plataforma.plans.add')
                ->with('numberTab', $this->numberTab);   
    }

    public function edit($product_id, $codePlan) 
    {
        $plan = Plan::where('code', $codePlan)->first();
        
        if ($plan) {
            return view('plataforma.plans.edit', compact('plan'))
                    ->with('numberTab', $this->numberTab);
        }
        
        return redirect('/');
    }

    public function create(PlanFormRequest $request) 
    {
        $data = $request->except(['_token']);

        if ($this->planService->create($data, app('request')->product->id)) {
            session()->flash("success", "Criado com sucesso.");
            return redirect()->route("product.plan.view", [app('request')->product->id]);
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function update(PlanFormRequest $request, $product_id, $codePlan) {
        $data = $request->except(['_token']);
        
        $plan = $this->planService->update($data, $codePlan);

        if ($plan) {
            session()->flash("success", "Salvo com sucesso!");
            return redirect()->route("product.plan.view", [$product_id]);
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function delete($product_id = null, $codePlan = null) 
    {   
        $plan = Plan::where('code', $codePlan)->first();

        if ($plan->default) {
            session()->flash("error", "O plano padrão não pode ser excluído");
            return redirect()->back();
        }
        $plan = $this->planService->delete($codePlan, $product_id);
        
        if ($plan) {
            session()->flash("success", "Removido com sucesso!");
            return redirect()->route("product.plan.view", [$product_id]);
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
}
