<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;
use App\Models\Plataforma\Product;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\Models\Plataforma\Postback;
use App\Http\Requests\Plataforma\PostbackFormRequest;
use Illuminate\Support\Carbon;
use App\Jobs\SendPostbackTestJob;

class PostbackController extends Controller
{
    private $titlePage = 'Postback';

    public function index()
    {
        $products = Product::whereHas('users', function($q){
            $q->where('users.id', Auth::user()->id);
        })->pluck('name', 'id');

        $types = [
            1 => 'Postback',
            2 => 'Iframe ou Pixel - Executado em Página de Obrigado'
        ];

        $postbacks = Postback::all();

        return View('plataforma.postback.index', compact('products', 'types', 'postbacks'))->with('titlePage', $this->titlePage);
    }

    public function edit($id)
    {
        $products = Product::whereHas('users', function($q){
            $q->where('users.id', Auth::user()->id);
        })->pluck('name', 'id');

        $types = [
            1 => 'Postback',
            2 => 'Iframe ou Pixel - Executado em Página de Obrigado'
        ];

        $postback = Postback::where('id', $id)
                    ->first();

        $postbacks = Postback::with('product')
        ->get();

        return View('plataforma.postback.index', compact('postback', 'types', 'products', 'postbacks'))
                ->with('titlePage', $this->titlePage);
    }

    public function create(PostbackFormRequest $request)
    {
        $data = $request->except('_token');

        if ($data['action'] == 'test') {
            $codeResp = $this->test($data, $data['url']);

            if ( $codeResp['code'] == 200) {
                session()->flash("success", "Postback com evento \"{$codeResp['event']}\" enviado com Sucesso! Código: " . $codeResp['code']);
            } else {
                session()->flash("error", "Error. Resposta: " . $codeResp);
            }

            return redirect()->back()->withInput();
        } else {
            if (!isset($data['pending'])) {
                $data['pending'] = 0;
            }
            if (!isset($data['refund'])) {
                $data['refund'] = 0;
            }
            if (!isset($data['abandon_checkout'])) {
                $data['abandon_checkout'] = 0;
            }
            if (!isset($data['finished'])) {
                $data['finished'] = 0;
            }
            if (!isset($data['blocked'])) {
                $data['blocked'] = 0;
            }
            if (!isset($data['canceled'])) {
                $data['canceled'] = 0;
            }
            if (!isset($data['completed'])) {
                $data['completed'] = 0;
            }

            $data['user_id'] = Auth::user()->id;

            if (Postback::create($data)) {
                session()->flash("success", "Postback criado com sucesso.");
                return redirect()->back();
            }

            session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }


    }

    public function update(PostbackFormRequest $request, $id)
    {
        $postback = Postback::find($id);
        $data = $request->except('_token');

        if (!isset($data['pending'])) {
            $data['pending'] = 0;
        }
        if (!isset($data['refund'])) {
            $data['refund'] = 0;
        }
        if (!isset($data['abandon_checkout'])) {
            $data['abandon_checkout'] = 0;
        }
        if (!isset($data['finished'])) {
            $data['finished'] = 0;
        }
        if (!isset($data['blocked'])) {
            $data['blocked'] = 0;
        }
        if (!isset($data['canceled'])) {
            $data['canceled'] = 0;
        }
        if (!isset($data['completed'])) {
            $data['completed'] = 0;
        }


        if ($postback->update($data)) {
            session()->flash("success", "Postback editado com sucesso.");
            return redirect()->back();
        }

        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function delete($id)
    {
        $postback = Postback::find($id);

        if ($postback->delete()) {
            session()->flash("success", "Postback deletado com sucesso.");
            return redirect()->back();
        }

        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function test(Request $request)
    {
        $datarequest = $request->except("_token");

        $allEvents = [
            'pending' => 'Pendente',
            'refund' => 'Devolvida',
            'abandon_checkout' => 'Abandono Checkout',
            'finished' => 'Finalizada',
            'blocked' => 'Bloqueada',
            'canceled' => 'Cancelada',
            'completed' => 'Completa'
        ];

        $data = [];

        foreach ($allEvents as $key => $event) {
            if (array_key_exists($key, $datarequest)) {
                $data['chave_unica'] = Auth::user()->token;

                $data['produto']['codigo'] = 1;
                $data['produto']['chave'] = '1fb9f46d-1377-454c-aaa7-6bf37fa80f63';
                $data['produto']['nome'] = 'Teste ColinaPay Postback';

                $data['tipoPostback']['codigo'] = 2;
                $data['tipoPostback']['descricao'] = 'Produtor';

                $data['venda'] = [
                    'codigo'            =>  '1',
                    'plano'             =>  '482F3D4BB9',
                    'cupom'             =>  null,
                    'dataInicio'        =>  date('Y-m-d H:m:s'),
                    'dataFinalizada'    =>  date('Y-m-d H:m:s', strtotime('+5 days')),
                    'meioPagamento'     =>  'mercadopago',
                    'formaPagamento'    =>  'credit_card',
                    'garantiaRestante'  =>  15,
                    'status'            =>  $event,
                    'valor'             =>  97.00,
                    'quantidade'        =>  1,
                    'valorRecebido'     =>  86.40,
                    'tipo_frete'        =>  999999,
                    'descr_tipo_frete'  =>  'Valor Fixo',
                    'frete'             =>  0.0,
                    'onebuyclick'       =>  '',
                    'venda_upsell'      =>  '',
                    'src'               =>  "",
                    'utm_source'        =>  "",
                    'utm_medium'        => "",
                    'utm_content'       =>  "",
                    'utm_campaign'      =>   "",
                    'linha_digitavel'   =>  ''
                ];

                $data['comissoes'][0] = [
                    "nome" => "Nome Afiliado",
                    "tipo_comissao" => "Último clique",
                    "valor" => 12.96,
                    "porcentagem" => 15.00,
                    "email" => "email@doAfiliado.com.br"
                ];

                $data['comprador']['nome'] = 'Comprador Teste ColinaPay';
                $data['comprador']['email'] = 'comprador@colinapay.com.br';
                $data['comprador']['cnpj_cpf'] = '999.999.999-99';
                $data['comprador']['telefone'] = '(11) 9999-9999';
                $data['comprador']['cep'] = '99999-999';
                $data['comprador']['endereco'] = 'Rua 15';
                $data['comprador']['numero'] = '17';
                $data['comprador']['complemento'] = 'Condominio Residencial ColinaPay';
                $data['comprador']['bairro'] = 'Distrito 12';
                $data['comprador']['cidade'] = 'Chapadinha';
                $data['comprador']['estado'] = 'SP';
                $data['comprador']['pais'] = 'Brasil';

                $data['json'] = json_encode($data);

                try {
                    $client = new \GuzzleHttp\Client();
                    $res = $client->post($datarequest['url'], ['form_params' =>  $data]);

                    return $res->getBody();
                } catch (\GuzzleHttp\Exception\ConnectException $th) {
                    return $th->getResponse();
                }
            }
        }

    }
}
