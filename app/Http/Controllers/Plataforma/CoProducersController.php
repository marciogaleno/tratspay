<?php

namespace App\Http\Controllers\Plataforma;

use App\Models\Plataforma\CoproducerPlan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Product;
use App\Http\Requests\Plataforma\CoProducerFormRequest;
use App\User;
use App\Models\Plataforma\CoProducer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CoProducersController extends Controller
{
    private $numberTab = 6;

    public function list($product_id)
    {
        $coproducers = CoProducer::with('product')->where('product_id', $product_id)->with('user')->get();

        return view('plataforma.coproducers.list', compact('coproducers'))
                ->with('numberTab', $this->numberTab);
    }

    public function add($product_id)
    {
        $product = Product::find($product_id);

        if(!$product) {
            session()->flash("danger", "Produto não existe. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }

        $tipo = ['coproducer' => 'Co-produtor', 'manager' => 'Gerente'];
        $formatCommission = ['percent' => 'Porcentagem (%)', 'valuefixed' => 'Valor fixo (R$)'];

        return view('plataforma.coproducers.add', compact('tipo', 'formatCommission', 'product'))
                ->with('numberTab', $this->numberTab);
    }

    public function create(CoProducerFormRequest $request, $productUuid)
    {
        $product = Product::find($productUuid);
        $data = $request->except(['_token']);

        $user = User::where('email', $data['email'])->first();
        $data['user_id']  = $user->id;
        $data['status'] = 'I';
        $data['product_id'] = app('request')->product->id;

        try {
            \DB::beginTransaction();

             $coproducer = CoProducer::create($data);

            if ($coproducer) {

                foreach ($product->plans as $plan) {
                    $item = json_decode($request[str_ireplace(' ','_', $plan->name)]);
                    if(!$item)
                        throw new \PDOException('Problemas ao salvar dados do plano');

                    CoproducerPlan::create([
                        'coproducer_id' => $coproducer->id,
                        'product_id' => $productUuid,
                        'plan_id' => $plan->id,
                        'value' => $item->value,
                        'type' => $item->type
                    ]);
                }

                \DB::commit();

                session()->flash("success", "Solicitação enviada com sucesso!");
                return redirect()->route("product.coproducer.list", $productUuid);
            }
        } catch (\PDOException $exception) {
            \DB::rollBack();
            session()->flash("danger", $exception->getMessage());
            return redirect()->back()->withInput();
        }

    }

    public function edit($product_id, $id)
    {
        $tipo = ['coproducer' => 'Co-produtor', 'manager' => 'Gerente'];
        $formatCommission = ['percent' => 'Porcentagem (%)', 'valuefixed' => 'Valor fixo (R$)'];
        $coproducer = CoProducer::find($id);

        return view('plataforma.coproducers.edit', compact('tipo', 'formatCommission', 'coproducer'))
                ->with('numberTab', $this->numberTab);
    }
    public function update(CoProducerFormRequest $request, $productUuid, $id)
    {
        $data = $request->except(['_token', '_method', 'productUuid']);

        $coproducer = CoProducer::find($id);

        if(!$coproducer) {
            session()->flash("danger", "Coprodutor não existe. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }

        $product = Product::find($productUuid);

        if(!$product) {
            session()->flash("danger", "Produto não existe. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }

        try {
            \DB::beginTransaction();

            $saved = $coproducer->update($data);
            if ($saved) {

                foreach ($product->plans as $plan) {
                    $item = json_decode($request[str_ireplace(' ','_', $plan->name)]);

                    if(!$item)
                        throw new \PDOException('Problemas ao salvar dados do plano');

                    $coproducerPlan = CoproducerPlan::where('coproducer_id', $coproducer->id)->where('plan_id', $plan->id)->where('product_id', $productUuid)->first();

                    $coproducerPlan->update([ 'value' => $item->value, 'type' => $item->type ]);
                }

                \DB::commit();

                session()->flash("success", "Salvo com sucesso!");
                return redirect()->back();
            }
        } catch (\PDOException $exception) {
            \DB::rollBack();
            session()->flash("danger", $exception->getMessage());
            return redirect()->back()->withInput();
        }

    }

    public function received()
    {
        $coproducers = DB::table('co_producers')
            ->join('products', 'co_producers.product_id', '=', 'products.id')
            ->join('producer_affiliate', 'producer_affiliate.product_id', '=', 'co_producers.product_id')
            ->join('users', 'producer_affiliate.user_id', '=', 'users.id')
            ->where('co_producers.user_id', Auth::user()->id)
            ->where('producer_affiliate.relation_type', 'producer')
            ->select(
                'products.name as name_product',
                'users.name as name_producer',
                'users.email as email_producer',
                'co_producers.type',
                'co_producers.value_commission',
                'co_producers.format_commission',
                'co_producers.status',
                'co_producers.id'
            )
            ->get();

        return view('plataforma.coproducers.received', compact('coproducers'));
    }

    public function approve(Request $request, $coproducerId)
    {
        if ($coproducerId) {
            $coproducer = CoProducer::where('id', $coproducerId)->withoutGlobalScopes()->update(['status' => 'A']);;

            if ($coproducer) {
                session()->flash("success", "Co-Produção aprovada com sucesso!");
                return redirect()->back();
            }
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
    public function cancel(Request $request, $coproducerId)
    {
        if ($coproducerId) {
            $coproducer = CoProducer::where('id', $coproducerId)->withoutGlobalScopes()->update(['status' => 'I']);;

            if ($coproducer) {
                session()->flash("success", "Co-Produção cencelada com sucesso!");
                return redirect()->back();
            }
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function delete($productUuid, $coproducerId)
    {
        $coproducer = CoProducer::find($coproducerId);

        if ($coproducer->delete()) {
            session()->flash("success", "Co-Produção deletada com sucesso!");
            return redirect()->back();
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
}
