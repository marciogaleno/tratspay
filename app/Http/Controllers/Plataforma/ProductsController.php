<?php

namespace App\Http\Controllers\Plataforma;

use Auth;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Plataforma\ProductFormRequest;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\File as FileProduct;
use App\Models\Plataforma\TypeDeliverie;
use App\Models\Plataforma\Checkout;
use App\Models\Plataforma\Plan;
use App\Services\ProductService;
use App\Models\Plataforma\Categorie;
use App\Models\Plataforma\ProductsComplaint;
use App\User;
use App\Scopes\Tenant\TenantScope;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\Input;
use SebastianBergmann\Timer\RuntimeException;
use App\Services\FileService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App\Models\Plataforma\ProducerAffiliate;
use function GuzzleHttp\json_encode;

class ProductsController extends Controller
{      
    private $product;
    private $plan;
    private $productService;
    private $checkout;
    private $numberTab = 0;
    
    public function __construct(Product $product, Plan $plan, Checkout $checkout, ProductService $productService)
    {
        $this->product = $product;
        $this->plan = $plan;
        $this->checkout = $checkout;
        $this->productService = $productService;
    }   

    public function view($product_id)
    {
        $product = Product::where('id', $product_id)->withoutGlobalScope(TenantScope::class)->with('configAffiliation')->first();
        $category = Categorie::find($product->category_id);

        $isAffiliateUserloggedThisProduct =  $product->usersWithoutGlobalScope()->where('relation_type', 'affiliate')->where('user_id', Auth::user()->id)->first();
        $isAffiliateUserloggedThisProduct = $isAffiliateUserloggedThisProduct ? $isAffiliateUserloggedThisProduct->pivot->status : false;

        $valueComission = Helper::getValueCommissionByType($product);

        $producer = User::whereHas('productsWithoutScopes', function($q) use ($product) {
            $q->where('product_id', $product->id);
            $q->where('relation_type', 'producer');
        })->first();

        $checkouts = Checkout::whereHas('plan', function($q) use ($product_id) {
            $q->where('product_id', $product_id);
        })->get();

        $salePage =  $product->salepage()->withoutGlobalScopes()->first();


        return view('plataforma.products.view', compact('product', 'valueComission', 'salePage', 'producer', 'isAffiliateUserloggedThisProduct', 'checkouts','category'));
    }

    public function list() 
    {
        $user = User::find(Auth::user()->id);
        $products = $user->products()->where('relation_type', 'producer')->get();

        return view('plataforma.products.list', compact('products'));
    }

    public function add() 
    {
        $categories = Categorie::pluck('desc', 'id');
        $deliverieTypes = TypeDeliverie::pluck('desc', 'id');
    
        return view('plataforma.products.add', compact('categories', 'deliverieTypes'));
    }

    public function store(ProductFormRequest $request) 
    {   


        $product = $this->productService->create($request);

        $response = [];

        if ($product) {
            $response['product_id'] = $product->id;
            $response = json_encode($response);
            return response($response, 200);
        }
        
        dd($response);
        $response = json_encode($response);
        return response($response, 500);
    }

    public function edit($id)
    {
        $product = $this->product->where("id", $id)->with('salepage')->first();
        $categories = Categorie::pluck('desc', 'id');
        $severalPurchases = [1 => 'Sim', 0 => 'Não'];
    
        return view('plataforma.products.edit', compact('product', 'categories', 'severalPurchases'))
                    ->with('numberTab', $this->numberTab);
    }

    public function update(ProductFormRequest $request, $id)
    {   
        // Important because editing can not change the payment type.
        $data = $request->except(['payment_type']);

        if ($this->productService->updateProduct($data, $id, $request)) {
            session()->flash("success", "Dados atualizados com sucesso.");
            return redirect()->back();
            
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput()->withErrors($request->validator);

    }

    public function complaints (Request $request)
    {
        $input = $request->except('_token');
        $user_affiliate =  Auth::user()->id;
        $product_id = $request->product_id;

        $complaint = ProductsComplaint::where('product_id', $product_id)
                    ->where('user_id', $user_affiliate)
                    ->where(function($q) {
                        $q->orWhere('relation_type', 'affiliate');
                        $q->orWhere('relation_type', 'producer');
                    })
                    ->first();

        $complaint = [
            'user_id'       => $user_affiliate,
            'product_id'    => $product_id,
            'relation_type' => 'affiliate',
            'motivo'        => 'motivo',
            'desc'          => 'desc'
        ];

        $create = ProductsComplaint::create($complaint);

        if ($create) {
            session()->flash("success", "Denúncia realizada com sucesso!");
            return redirect()->back();   
    }
}
    
    public function affiliate(Request $request)
    {      
        $input = $request->except('_token');
        $user_affiliate =  Auth::user()->id;
        $product_id = $request->product_id;
        $uuid = $request->uuid;

        $isAfiliate = ProducerAffiliate::where('product_id', $product_id)
                    ->where('user_id', $user_affiliate)
                    ->where(function($q) {
                        $q->orWhere('relation_type', 'affiliate');
                    })
                    ->first();

        if ($isAfiliate){
            session()->flash("error", "Você já é afiliado deste produto.");
            return redirect()->back();            
        }

        $isProducer = ProducerAffiliate::where('product_id', $product_id)
                    ->where('user_id', $user_affiliate)
                    ->where(function($q) {
                        $q->orWhere('relation_type', 'producer');
                    })
                    ->first();

        if ($isProducer){
            session()->flash("error", "Você não pode se afiliar so seu próprio produto.");
            return redirect()->back();            
        }

        $afiliate = [
            'user_id'       => $user_affiliate,
            'product_id'    => $product_id,
            'relation_type' => 'affiliate',
            'request_date'  => date('Y-m-d h:i:s'),
            'approval_date' => date('Y-m-d h:i:s'),
            'status'        => 'A'
        ];

        $create = ProducerAffiliate::create($afiliate);

        if ($create) {
            session()->flash("success", "Afiliação realizada com sucesso!");
            return redirect()->route("afilliate.detailsProduct", [$product_id]);   
        }
       
    }

    public function tabs($uuid)
    {
        $product = $this->product->where("uuid", $uuid)->with('salepages')->first();

        return view('plataforma.products.tabs', compact('product'));
    }

    public function memberArea($product_id)
    {
        $product = app('request')->product;

        return View('plataforma.products.memberArea', compact('product'))->with('numberTab', 5);
    }

    public function updateMemberArea(Request $request, $product_id)
    {
        $data = $request->except('_token');

        $product = Product::find($product_id);

        if ($product->update($data)) {
            session()->flash("success", "Área de membros atualizada com sucesso!");
            return redirect()->back();
            
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput()->withErrors($request->validator);
    }

    public function sales($product_id)
    {
        $product = app('request')->product;

        return View('plataforma.products.sales', compact('product'))->with('numberTab', 11);
    }
}
