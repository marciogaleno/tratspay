<?php

namespace App\Http\Controllers\Plataforma;

use App\Helpers\Helper;
use App\Repository\SalesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\TransactionMp;
use Illuminate\Support\Facades\Auth;
use App\Models\Plataforma\Commission;
use App\User;
use App\Models\Plataforma\ProducerAffiliate;
use App\Models\Plataforma\Product;
use App\Events\ReprocessingCommissionsEvent;
use App\Services\CommissionService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailBuyer;
use App\Models\Plataforma\Reembolso;

class SalesController extends Controller
{
    public function __construct( SalesRepository $salesRepository)
    {
        $this->salesRepository = $salesRepository;
    }

    public function list(Request $request)
    {
        $commissions  = empty($request->all()) ? [] : $this->salesRepository->getFiltered($request->all());
        $totalComissions  = empty($request->all()) ? 0 : $this->salesRepository->getFilteredSumTotal($request->all(), 'value');
        $totalTransactions  = empty($request->all()) ? 0 : $this->salesRepository->getFilteredSumTotal($request->all(), 'value_transaction');
        $user = User::find(Auth::user()->id);
        $clients = "";

        $products = $user->products()->pluck('products.name', 'products.id');
        $productsProducer = $user->products()->where('relation_type', 'producer')->pluck('products.id')->toArray();

        $affiliates = DB::table('users')
            ->join('producer_affiliate', 'producer_affiliate.user_id', '=', 'users.id')
            ->whereIn('producer_affiliate.product_id', $productsProducer)
            ->where('producer_affiliate.relation_type', '=', 'affiliate')
            ->where('producer_affiliate.status', '=', 'A')
            ->distinct()
            ->pluck('users.name', 'users.id');

        $status = [
            'pending' =>'Aguardando Pagamento',
            'finished' => 'Finalizada',
            'canceled' => 'Cancelada',
            'refund' => 'Devolvida',
            'blocked' => 'Bloqueada',
            'completed' => 'Completa'
        ];

        $statusTransactions = ['approved' => 'Aprovado', 'awaiting_payment' => 'Aguardando Pagamento', 'refunded' => 'Reembolsado', 'unsolved' => 'Reclamação em Aberto'];

        $paymentForms = ['credit_card' => 'Cartão', 'ticket' => 'Boleto'];

        return View('plataforma.sales.list'
            , compact('commissions'
                , 'products'
                , 'status'
                , 'paymentForms'
                , 'affiliates'
                , 'totalComissions'
                , 'totalTransactions'
                , 'clients'
                , 'statusTransactions'
            )
        )->with('titlePage', 'Todas as Vendas');
    }


    public function details($transaction_id)
    {
        $user_id = Auth::user()->id;
        $commissionUser = Commission::where('transaction_id', $transaction_id)
                            ->where('user_id', $user_id)
                            ->first();


        $allCommissions =  Commission::where('product_id', $commissionUser->product_id)
                                ->where('transaction_id', $commissionUser->transaction_id)
                                ->get();

        $hasCommissionAffiliate = false;

        foreach ($allCommissions as $commissions) {
            if ($commissions->commission_type == 'affiliate') {
                $hasCommissionAffiliate = true;
            }
        }

        $reembolso = Reembolso::where('transaction_id', $transaction_id)->first();

        return View('plataforma.sales.details', compact('commissionUser', 'reembolso', 'allCommissions', 'hasCommissionAffiliate'));
    }

    public function detailRelatorio(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $products = $user->products()->where('relation_type', 'producer')->pluck('products.name', 'products.id');

        $sales = $this->salesRepository->getDataFiltered($request->all());
        $data = [
            'qtdVenda' => $sales->where('status', 'finished')->count(),
            'lucroTotal' => $sales->sum('value'),
            //pega o valor total recebido nas vendas e divide pelo total de vendas realizadas.
            'ticketMedio' => $sales->where('status', 'finished')->count() != 0 ? $sales->sum('value')/$sales->where('status', 'finished')->count(): 0
        ];

        $pieChart = $this->salesRepository->getDataChart($request->all());

        return View('plataforma.sales.detalhamentovendas', compact('products', 'data', 'pieChart'));
    }

    public function affiliatesByProduct($product_id)
    {
        $product = DB::select(
            "SELECT B.* FROM PRODUCER_AFFILIATE A, USERS B
                WHERE 
                A.USER_ID = B.ID
                AND A.RELATION_TYPE = 'affiliate'
                AND A.PRODUCT_ID = {$product_id}"
        );

        return $product;
    }

    public function reprocessCommission(Request $request)
    {
        $data = $request->except('_token');

        if (!isset($data['affiliate_id']) || empty($data['affiliate_id'])) {
            session()->flash("error", "Por favor, selecione um afiliado");
            return redirect()->back()->withInput();
        }

        $transaction = TransactionMp::where('code', $data['transaction_code'])->first();

        $commissionService = new CommissionService($transaction);

        if ($commissionService->splitCommissions( $data['affiliate_id'])) {
            session()->flash("success", "Comissão enviada com sucesso!");
            return redirect()->back();
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function editEmailUser(Request $request)
    {
        $data = $request->except('_token');

        $user = User::where('email', $data['old_email'])->first();
        $transaction = TransactionMp::where('code', $data['transaction_code'])->first();

        if (!$transaction) {
            session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
            return redirect()->back()->withInput();
        }

        try {
            DB::beginTransaction();

            if ($user) {
                $user->update([
                    'email' =>  $data['email']
                ]);
            }

            $transaction->update([
                'email' => $data['email']
            ]);
            DB::commit();

            session()->flash("success", "Email alterado com sucesso!");
            return redirect()->back();

        } catch (\Illuminate\Database\QueryExceptio $th) {
            DB::rollback();
            throw $th;
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();

    }

    public function resendDataAffiliate(Request $request)
    {
        $data = $request->except('_token');

        $user = User::where('email', $data['email'])->first();
        $transaction = TransactionMp::where('code', $data['transaction_code'])->first();

        $newPassword = $this->generateRandomString();


        $updated = $user->update([
            'password' => $newPassword
        ]);


        if ($updated) {
            Mail::to($user->email)
            ->send(new SendMailBuyer('Compra Aprovada!', true, true, $newPassword, $transaction));

            session()->flash("success", "Dados de acesso enviado com sucesso!");
            return redirect()->back();
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();

    }

    public function resendTicket(Request $request)
    {
        $data = $request->except('_token');
        $transaction = TransactionMp::where('code', $data['transaction_code'])->first();


        if ($transaction->payment_type_id == 'ticket') {
            Mail::to($transaction->email)
            ->send(new SendMailBuyer('Imprima Seu Email', false, false, null, $transaction));

            session()->flash("success", "Boleto  reenviado com sucesso!");
            return redirect()->back();
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function tracking(Request $request)
    {
        return View('plataforma.sales.tracking');
    }
}
