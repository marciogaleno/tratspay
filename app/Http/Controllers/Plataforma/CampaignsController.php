<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Checkout;
use App\Models\Plataforma\UrlAlternative;
use App\Models\Plataforma\Campaign;
use App\Http\Requests\Plataforma\CampaignsFormRequest;

class CampaignsController extends Controller
{
    private $numberTab = 9;

    function index($product_id)
    {
        $campaings = Campaign::where('product_id', $product_id)->with('product')->get();

        return View('plataforma.campaigns.index', compact('campaings'))
                ->with('numberTab', $this->numberTab);
    }

    function add($product_id)
    {
        $checkouts = Checkout::whereHas('plan', function($q) use ($product_id) {
            $q->where('product_id', $product_id);
        })->pluck('desc', 'id');

        $urlAlternatives = UrlAlternative::where('product_id', $product_id)->pluck('desc', 'id');

        return View('plataforma.campaigns.add', compact('checkouts', 'urlAlternatives'))
            ->with('numberTab', $this->numberTab);
    }

    function edit($product_id, $code)
    {
        $checkouts = Checkout::whereHas('plan', function($q) use ($product_id) {
            $q->where('product_id', $product_id);
        })->pluck('desc', 'id');

        $urlAlternatives = UrlAlternative::where('product_id', $product_id)->pluck('desc', 'id');

        $campaing = Campaign::where('code',$code)->first();

        return View('plataforma.campaigns.edit', compact('checkouts', 'urlAlternatives', 'campaing'))
            ->with('numberTab', $this->numberTab);
    }

    function create(CampaignsFormRequest $request, $product_id)
    {
        $data = $request->except('_token');
        $data['product_id'] = $product_id;
        
        if (Campaign::create($data)) {
            session()->flash("success", "Campanha cadatrada com sucesso!");
            return redirect()->route('campaign.index', $product_id);
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    function update(CampaignsFormRequest $request, $product_id, $code)
    {
        $data = $request->except('_token');

        $campaign = Campaign::where('code', $code)->first();
        $salved = $campaign->update($data);

        if ($salved) {
            session()->flash("success", "Campanha atualizada com sucesso!");
            return redirect()->route('campaign.index', $product_id);
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    function delete(Request $request, $product_id, $code)
    {
        $campaign = Campaign::where('code', $code)->first();
        $deleted = $campaign->delete();

        if ($deleted) {
            session()->flash("success", "Campanha deletada com sucesso!");
            return redirect()->route('campaign.index', $product_id);
        }
        
        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
}
