<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use App\Models\Plataforma\Commission;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Plataforma\Click;
use App\Models\Plataforma\ClickP;
use App\User;

class DashboardController extends Controller
{
    public function index(Request $request) {

        $start = Carbon::now()->startOfMonth()->toDateString();
        $end = Carbon::now()->endOfMonth()->toDateString();

        $today = Carbon::now()->format('Y-m-d');

        $salesToday = DB::table('commissions')
                    ->join('transaction_mp', 'transaction_mp.id', 'commissions.transaction_id')
                    ->whereIn('transaction_mp.status', ['finished'])
                    ->where(DB::raw('DATE(transaction_mp.updated_at)'), $today)
                    ->where('commissions.user_id', Auth::user()->id)
                    ->sum('commissions.value');
        
        $salesMonth = DB::table('commissions')
                    ->join('transaction_mp', 'transaction_mp.id', 'commissions.transaction_id')
                    ->whereIn('transaction_mp.status', ['finished', 'completed'])
                    ->whereBetween(DB::raw('DATE(transaction_mp.updated_at)'), [$start, $end])
                    ->where('commissions.user_id', Auth::user()->id)
                    ->sum('commissions.value');

        $totalPending = DB::table('commissions')
                        ->join('transaction_mp', 'transaction_mp.id', 'commissions.transaction_id')
                        ->whereIn('transaction_mp.status', ['finished'])
                        ->where('commissions.user_id', Auth::user()->id)
                        ->sum('commissions.value');

        $totalAvailable = DB::table('accounts')
                            ->where('user_id', Auth::user()->id)
                            ->first();

        $salesNextWeek = DB::table('commissions')
            ->join('transaction_mp', 'transaction_mp.id', 'commissions.transaction_id')
            ->whereIn('transaction_mp.status', ['finished'])
            ->where('commissions.user_id', Auth::user()->id)
            ->whereRaw(" NOW()  <= transaction_mp.date_approved + INTERVAL '7 day'")
            ->selectRaw("CAST(transaction_mp.date_approved AS DATE) AS datePayment, CAST(transaction_mp.date_approved + interval '7 day' AS DATE) as date, sum(commissions.value) as value")
            ->groupBy(db::raw("CAST(transaction_mp.date_approved + interval '7 day' AS DATE), datePayment"))
            ->get();

        $widgetFiltered = $this->getValuesWidgetFiltered($request->all());

        $user_id = Auth::user()->id;
        $start2 = Carbon::now()->subMonth()->toDateString();
        $end2 = Carbon::now()->toDateString();

        $salesBySource = DB::select("
                WITH clicks_tmp(id, user_id, product_id, reference_code, tracking_code, src, created_at) as ( 
                    SELECT id, user_id, product_id, reference_code, tracking_code, source->>'src' src, created_at from clicks 
                ) 
                SELECT 
                a.user_id, a.src, COUNT(*) clicks, 
                (SELECT COUNT(*) FROM commissions c where a.src = c.source->>'src' and c.user_id = a.user_id) total_sales
                FROM clicks_tmp a
                WHERE a.user_id = {$user_id}
                AND DATE(a.created_at) BETWEEN '{$start2}' and '{$end2}'
                GROUP BY a.user_id, a.src;
            ");

        $salesLastSevenDays = json_encode($this->salesLastSevenDays());

        $totalSales = $this->sumTotalSales();

        $bestSellersProducts = DB::select("
                    SELECT B.ID, B.name,  b.image, count(*) quant, SUM(A.value) total FROM COMMISSIONS A,
                    PRODUCTS B,
                    TRANSACTION_MP C
                    WHERE 
                    A.product_id = B.id
                    AND A.USER_ID = {$user_id}
                    AND A.transaction_id = C.ID
                    AND C.STATUS IN ('finished', 'completed')
                    GROUP BY B.ID, B.name
                    ORDER BY TOTAL DESC
                    FETCH FIRST 5 ROWS ONLY
        ");
        /*$clicksBySrc = $this->getClicksBySrc();

        

        foreach ($clicksBySrc as $clicks) {
            $source['src'] = $clicks->_id->src;
            $source['clicks'] = $clicks->clicks;

            //Ultimos 30 dias
            $start = Carbon::now()->subMonth()->toDateString();
            $end = Carbon::now()->toDateString();
    

            $value = DB::table('commissions')
            ->join('transaction_mp', 'transaction_mp.transaction_id', 'commissions.transaction_id')
            ->whereIn('transaction_mp.status', ['finished'])
            ->whereBetween(DB::raw('DATE(transaction_mp.created_at)'), [$start, $end])
            ->where('commissions.user_id', Auth::user()->id)
            ->where('commissions.src', $clicks->_id->src)
            ->count('commissions.id');


            $source['total'] = $value;
            $source['roi'] = number_format(($value * 100) / $clicks->clicks, 2, ',', '.');
            $salesSources[] = $source;

        }*/

        //$salesByRegion = $this->getSalesByRegion($salesMonth);
        $salesByRegion = [];

        $totalSales = 0.0;
        $totalSales = $totalAvailable->balance + $totalPending;

        $salesToday = number_format($salesToday, 2, ',', '.');
        $salesMonth = number_format($salesMonth, 2, ',', '.');
        $totalPending = number_format($totalPending, 2, ',', '.');
        $totalAvailable = $totalAvailable ? number_format($totalAvailable->balance, 2, ',', '.') : "0,00";

        return view('plataforma.dashboard.index', compact('salesToday', 'salesMonth', 'totalPending', 
            'totalAvailable', 'salesBySource', 'salesByRegion', 'salesLastSevenDays',
            'totalSales', 'totalSales', 'salesNextWeek', 'bestSellersProducts','widgetFiltered'));
    }

    function getClicksBySrc()
    {


        $startDate = new \MongoDB\BSON\UTCDateTime( strtotime("-1 month") * 1000);
        $startDate = $startDate->toDateTime()->format(DATE_ISO8601);

        $endDate = new \MongoDB\BSON\UTCDateTime(strtotime("now") * 1000);
        $endDate = $endDate->toDateTime()->format(DATE_ISO8601);

        $userId = Auth::user()->id;

        $results = Click::raw(function($collection) use ($userId, $startDate, $endDate)
        {
            return $collection->aggregate([
                [ '$match' => [
                    'userId' => $userId,
                ]],

                ['$unwind' => '$dClicks'],
                [ '$match' => [
                    'dClicks.date' => ['$gte' => $startDate, '$lt' => $endDate]
                ]],

                ['$group' => [
                    "_id" => ['src' => '$dClicks.src'],
                    'clicks' => ['$sum' => 1]
                ]],
                [ '$match' => [
                    '_id.src' => ['$exists' => true],
                ]],

                ['$project' => [
                    "_id.userId" => 1,
                    "_id.src" => 1,
                     "clicks" => 1
                ] ]
                ]
            );
        });

        return $results;

    }

    function getValuesWidgetFiltered($filtros)
    {
        $result = ['totalReclamacoes' => 0, 'totalBoletos' => 0, 'totalCliques' => 0, 'totalVendas' => 0];
        $user_id =  Auth::user()->id;

        if(!isset($filtros['date']) || !$filtros['date']) {
            return $result ;
        }
        switch ($filtros['date']) {
            case 'last15days':
                $sqlFilter = "transaction_mp.created_at BETWEEN CURRENT_DATE - '15 days'::interval AND CURRENT_DATE";
                break;
            case 'vendasmes':
                $sqlFilter = 'extract(month from transaction_mp.created_at) ='.Carbon::now()->format('m');
                break;
            case 'last7days':
                $sqlFilter =  "transaction_mp.created_at BETWEEN CURRENT_DATE - '7 days'::interval AND CURRENT_DATE";
                break;
        }

        $data = Commission::join('products', 'product_id', '=', 'products.id')
            ->join('transaction_mp', 'commissions.transaction_id', '=', 'transaction_mp.id')
            ->where('commissions.user_id', Auth::user()->id)
            ->whereRaw($sqlFilter)
            ->select('transaction_mp.id as code_transaction', 'transaction_mp.*', 'commissions.*', 'products.*');

        $boletos = clone $data;
        $sqlFilterClick = str_replace('transaction_mp', 'a', $sqlFilter);

        $result['totalBoletos'] = $boletos->where('transaction_mp.payment_type_id', 'ticket')->count();
        $result['totalVendas'] = $data->whereIn('transaction_mp.status',['finished','completed'])->count();

        $result['totalCliques'] = head(DB::select("
                WITH clicks_tmp(id, user_id, product_id, reference_code, tracking_code, src, created_at) as ( 
                    SELECT id, user_id, product_id, reference_code, tracking_code, source->>'src' src, created_at from clicks 
                ) 
                SELECT 
                a.user_id, a.src, COUNT(*) clicks 
                FROM clicks_tmp a
                WHERE a.user_id = {$user_id}
                AND {$sqlFilterClick}
                GROUP BY a.user_id, a.src;
            "))->clicks;

        return $result;
    }

    function getSalesByRegion($totalSalesMonth)
    {
        $start = Carbon::now()->subMonth()->toDateString();
        $end = Carbon::now()->toDateString();

        $salesByRegion = DB::table('commissions')
            ->join('transaction_mp', 'transaction_mp.id', 'commissions.transaction_id')
            ->whereIn('transaction_mp.status', ['finished'])
            ->where('commissions.user_id', Auth::user()->id)
            ->whereBetween(DB::raw('DATE(transaction_mp.created_at)'), [$start, $end])
            ->select(DB::raw('SUBSTRING(transaction_mp.cpf_cnpj, 9, 1) as cpf_region, SUM(commissions.value) as total'))
            ->groupBy('cpf_region')
            ->orderBy('cpf_region')
            ->get();


        $regions[0]['total'] = 0;
        $regions[0]['region'] = 'RS';

        $regions[1]['total'] = 0;
        $regions[1]['region'] = 'DF, GO, MT, MS, To';

        $regions[2]['total'] = 0;
        $regions[2]['region'] = 'AM, PA, RO, AP, AC, RO';

        $regions[3]['total'] = 0;
        $regions[3]['region'] = 'CE, MA, PI';

        $regions[4]['total'] = 0;
        $regions[4]['region'] = 'PB, PE, AL, RN';

        $regions[5]['total'] = 0;
        $regions[5]['region'] = 'BA, SE';

        $regions[6]['total'] = 0;
        $regions[6]['region'] = 'MG';

        $regions[7]['total'] = 0;
        $regions[7]['region'] = 'RJ, ES';

        $regions[8]['total'] = 0;
        $regions[8]['region'] = 'SP';

        $regions[8]['total'] = 0;
        $regions[8]['region'] = 'PR, SC';

        foreach ($salesByRegion as $value) {
            if ($value->total > 0 && $totalSalesMonth > 0) {
                $regions[$value->cpf_region]['total'] = ($value->total * 100) / $totalSalesMonth;
            }
            
        }
        
        return $regions;
    }

    public function salesLastSevenDays()
    {
        
        $user_id = Auth::user()->id;
        $sales = [];

        $sales = DB::select(
                "SELECT to_char(A.CREATED_AT, 'YYYY-MM-DD') t, SUM(A.VALUE) y FROM commissions A
                INNER JOIN transaction_mp B ON B.ID = A.transaction_id
                WHERE A.USER_ID = {$user_id}
                AND B.status IN ('finished', 'completed')
                AND A.CREATED_AT BETWEEN NOW() - INTERVAL '7 day' AND NOW() 
                GROUP BY to_char(A.CREATED_AT, 'YYYY-MM-DD')
                ORDER BY 1");

        if (count($sales) > 0) {
            $databegin = $sales[0];
            $dateTwoDayLast = date('Y-m-d', strtotime("-2 days",strtotime($databegin->t)));
    
            $dataEnd = end($sales);
            $dateTwoDayAfter = date('Y-m-d', strtotime("+2 days",strtotime($dataEnd->t)));
    
            $data1 = (object) ['t' => $dateTwoDayLast, 'y' => 0];
            $data2 =  (object) ['t' => $dateTwoDayAfter, 'y' => 0];
    
            array_unshift($sales,  $data1);
            array_push($sales,  $data2);

        }

        return $sales;
    }

    public function sumSalesLastSevenDays()
    {
        $user_id = Auth::user()->id;

        $sales = DB::select(
                "SELECT SUM(A.VALUE) total FROM commissions A
                INNER JOIN transaction_mp B ON B.ID = A.transaction_id
                WHERE A.USER_ID = {$user_id}
                AND B.status IN ('finished', 'completed')
                AND A.CREATED_AT BETWEEN NOW() - INTERVAL '7 day' AND NOW()");

        return $sales;
    }

    public function sumTotalSales()
    {
        $user_id = Auth::user()->id;
        
        $total = DB::select(
                "SELECT SUM(A.VALUE) total FROM commissions A
                INNER JOIN transaction_mp B ON B.ID = A.transaction_id
                WHERE A.USER_ID = {$user_id}
                AND B.status IN ('finished', 'completed')");

        return $total;
    }

}
