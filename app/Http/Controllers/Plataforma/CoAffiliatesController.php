<?php

namespace App\Http\Controllers\Plataforma;

use Auth;
use App\User;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Plataforma\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\CoAffiliate;
use App\Models\Plataforma\ProducerAffiliate;
use App\Http\Requests\Plataforma\CoAffiliateFormRequest;

class CoAffiliatesController extends Controller
{
    public function add($product_id)
    {
        $product = Product::whereHas('users', function($q) {
            $q->where('user_id', Auth::user()->id);
            $q->where('relation_type', 'affiliate');
        })
        ->where('id', $product_id)
        ->withoutGlobalScopes()
        ->first();

        $formatCommission = ['percent' => 'Porcentagem (%)', 'valuefixed' => 'Valor fixo (R$)'];

        return view('plataforma.coaffiliates.add', compact('product',  'formatCommission'));
    }

    public function create(CoAffiliateFormRequest $request, $product_id)
    {
        $data = $request->except(['_token']);

        $user = User::where('email', $data['email'])->first();

        $data['user_name'] = $user->name;
        $data['user_id']  = $user->id;
        $data['status'] = 'I';
        $data['product_id'] = $product_id;

        $affiliate = ProducerAffiliate::where('user_id', Auth::user()->id)
            ->where('product_id', $product_id)
            ->where('relation_type', 'affiliate')
            ->first();

        $data['code_affiliate'] = $affiliate->code;

        //$dateFormated = \App\Helpers\Helper::dateToAmerican($data['deadline']);

        if (CoAffiliate::create($data)) {
            session()->flash("success", "Solicitação enviada com sucesso!");
            return redirect()->route("coaffiliate.list", $product_id);
        }

    }

    public function list($product_id)
    {
        $product = Product::where('id', $product_id)->withoutGlobalScopes()->first();
        
        $affiliate = ProducerAffiliate::where('user_id', Auth::user()->id)
                ->where('product_id', $product_id)
                ->where('relation_type', 'affiliate')
                ->first();

        $coaffiliates = [];

        $coaffiliates = CoAffiliate::where('code_affiliate', $affiliate->code)->get();
    
        return view('plataforma.coaffiliates.list', compact('coaffiliates', 'product'));
    }

    public function approve(Request $request, $coaffiliateid)
    {
        if ($coaffiliateid) {
            $coaffiliate = CoAffiliate::where('id', $coaffiliateid)->withoutGlobalScopes()->update(['status' => 'A']);;

            if ($coaffiliate) {
                session()->flash("success", "Co-Afiliação aprovada com sucesso!");
                
                return redirect()->back();
            }
        }
        
        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }
    public function cancel(Request $request, $coaffiliateid)
    {
        if ($coaffiliateid) {
            $coaffiliate = CoAffiliate::where('id', $coaffiliateid)->withoutGlobalScopes()->update(['status' => 'I']);;

            if ($coaffiliate) {
                session()->flash("success", "Co-Afiliação cancelada com sucesso!");
                return redirect()->back();
            }
        }
        
        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function delete(Request $request)
    {
 
        $coaffiliate = CoAffiliate::find($request->id);
        
       if ($coaffiliate->delete()) {
           return "true";
       }
        
        // if ($coaffiliate->delete()) {
        //     session()->flash("success", "Co-Afiliação deletada com sucesso!");
        //     return redirect()->back();
        // }
        
        // session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        // return redirect()->back()->withInput();   
    }

    public function received()
    {
        $coaffiliates = DB::table('co_affiliates')
            ->join('products', 'co_affiliates.product_id', '=', 'products.id')
            ->join('producer_affiliate', 'producer_affiliate.code', '=', 'co_affiliates.code_affiliate')
            ->join('users', 'producer_affiliate.user_id', '=', 'users.id')
            ->where('co_affiliates.user_id', Auth::user()->id)
            ->select(
                'products.name as name_product', 
                'users.name as name_affiliate',
                'users.email as email_affiliate',
                'co_affiliates.value_commission',
                'co_affiliates.format_commission',
                'co_affiliates.status',
                'co_affiliates.id'
            )
            ->get();

        return view('plataforma.coaffiliates.received', compact('coaffiliates'))->with('titlePage', 'Co-Affiliações recebidas');
    }

}
