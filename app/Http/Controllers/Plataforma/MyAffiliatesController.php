<?php

namespace App\Http\Controllers\Plataforma;

use App\Repository\MyAffiliatesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Categorie;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\User;


class MyAffiliatesController extends Controller
{
    public function __construct(MyAffiliatesRepository $myAffiliatesRepository)
    {
        $this->myAffiliatesRepository = $myAffiliatesRepository;
    }

    function index(Request $request)
    {      
        $user = Auth::user();
        $user = User::findOrFail($user->id);
        $products = $user->productsIsProducer->pluck('name', 'id');

        $affiliates = $this->myAffiliatesRepository->getFiltered($request->all());

        $totalSales =  array_sum(array_column($affiliates, 'total'));

        return View('plataforma.myaffiliates.list',compact('products', 'affiliates', 'totalSales'));
    }
}
