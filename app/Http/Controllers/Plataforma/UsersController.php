<?php

namespace App\Http\Controllers\Plataforma;

use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\Plataforma\UserStoreRequest;
use App\Http\Requests\Plataforma\UserUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Plataforma\Product;
use App\Services\UploadService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Plataforma\BankAccount;

use \Validator;

class UsersController extends Controller
{   

    private $product;
    
    public function __construct(Product $product)
    {
        $this->product = $product;
    }   

    /**
     * @param $id
     * @return $this
     */
    public function perfil()
    {

        $user = Auth::user();
        try {
            $user = User::findOrFail($user->id);

            return view("plataforma.users.perfil", compact("user"))->with('titlePage', 'Perfil do Usuário');

        } catch (ModelNotFoundException $exception) {

            session()->flash("error", "Registro não encontrado");
            return redirect()->route("dashboard");
        }

    }

    public function userPerfil($id = null)
    {

        $data = ['producer', 'products'];
        $user = Auth::user();

        try {
            $user = User::findOrFail($user->id);

            $products = $user->productsIsProducer;

            $producer = Auth::user()->client;
            
            return view('plataforma.users.perfil', compact($data));

        } catch (ModelNotFoundException $exception) {

            session()->flash("error", "Registro não encontrado");
            return redirect()->route("dashboard");
        }

    
    }

    public function updatePassword(\Illuminate\Http\Request $request)
    {

        $input = $request->all();
        $user = Auth::user();

        if (! Hash::check($input['password_old'], $user->password)){
            return Response(['error'=>'Senha atual está incorreta'], 500);
        }

        $validator = Validator::make($request->all(), [
            'password_old' => 'required',
            'password'   => ["required"],
            'password_confirmation' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            return Response(['error'=>'Erro ao alterar senha'], 500);
        }

        $input['password'] = bcrypt($input['password']);//criptografa password

        $user->update($input);

        return Response(['success'=>'Salvo com sucesso'], 200);
    }

}
