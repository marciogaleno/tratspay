<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\ConfigAffiliation;
use App\Models\Plataforma\Checkout;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ConfigAffiliationController extends Controller
{
    private $numberTab = 7;

    public function edit($product_id)
    {
        $assignment_type = ['1' => 'Primeiro clique', '2' => 'Último Clique', '3' => 'Primeiro e Último clique'];
        $configaffiliation = ConfigAffiliation::where('product_id', $product_id)->first();

        $price = (float) app('request')->product->plans[0]->price;

        $taxa = app('request')->product->product_type == "F" ? 1+($price / 100 * 6.9): 1+($price / 100 * 9.9);

        $valorReceber = $price - ($taxa);

        $comissao = (float) $configaffiliation->value_commission;

        if($configaffiliation->format_commission == "P") {
            $comissao =  $valorReceber / 100 * (float) $configaffiliation->value_commission;
        }

        $valorReceber  = $valorReceber - $comissao ;

        $valorReceber = \App\Helpers\Helper::moneyBR($valorReceber);
        $taxa = \App\Helpers\Helper::moneyBR($taxa);
        $comissao = \App\Helpers\Helper::moneyBR($comissao);

        return View('plataforma.configaffiliation.edit', compact('assignment_type', 'configaffiliation', 'valorReceber', 'comissao', 'taxa'))
                ->with('numberTab', $this->numberTab);
    }

    public function update(Request $request, $product_id, $id)
    {
        $data = $request->except(['_token', '_method']);
        $data['producer_refund'] = !isset($data['producer_refund']) ? 0 : 1;
        $data['hide_data_customer'] = !isset($data['hide_data_customer']) ? 0 : 1;
        $data['afil_link_chk'] = !isset($data['afil_link_chk']) ? 0 : 1;
        $data['coupon'] = !isset($data['coupon']) ? 0 : 1;
        
        $configAffiliation = ConfigAffiliation::find($id);

        $saved = $configAffiliation->update($data);

        if ($saved) {
            session()->flash("success", "Salvo com sucesso!");
            return redirect()->back();
        }

        session()->flash("danger", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function recruitAffiliate($recruit_code) {

        $product = Product::whereHas('configAffiliation', function($q) use ($recruit_code) {
                        $q->where('recruit_code', $recruit_code);
                    })->first();

        $valueComission = 0.0;

        if ($product->payment_type == 'SINGPRICE' && 
            $product->configAffiliationWithoutGlobalScope->format_commission == 'P') {
            $valueComission = (float) $product->plansWithoutGlobalScope[0]->price * ((float) $product->configAffiliationWithoutGlobalScope->value_commission / 100);
            $valueComission = number_format($valueComission, 2, ',', '.');
        }

        if ($product->payment_type == 'SINGPRICE' && 
            $product->configAffiliationWithoutGlobalScope->format_commission == 'v') {
            $valueComission = (float) $product->configAffiliationWithoutGlobalScope->value_commission;
            $valueComission = number_format($valueComission, 2, ',', '.');
        }

        $producer = $product->producer[0];

        $salePage =  $product->salepage()->withoutGlobalScopes()->first();

        $checkouts = Checkout::whereHas('plan', function($q) use ($product) {
                        $q->where('product_id', $product->id);
                    })->get();
                    
        $isAffiliateUserloggedThisProduct = null;

        if (Auth::user()) {
            $isAffiliateUserloggedThisProduct =  $product->usersWithoutGlobalScope()->where('relation_type', 'affiliate')->where('user_id', Auth::user()->id)->first();
        }
       

        return  View('plataforma.configaffiliation.recruit-affiliate', 
                compact('product', 'valueComission', 'producer', 'salePage', 
                'checkouts', 'isAffiliateUserloggedThisProduct', 'recruit_code'));
    }

    function login($code)
    {
        Session::put('recruit_code', $code);
        
        return redirect('/login');
    }
}
