<?php

namespace App\Http\Controllers\Plataforma;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class ResolveUrlController extends Controller 
{
    public function resolve(Request $request, $promotionCode)
    {
        $resolveUrlService = app()->make('ResolveUrlService');
        $params = $request->query();
        if (!empty($request->c)) {
            return $resolveUrlService->redirectCheckout($request, $promotionCode,  $params, $request->ip());
        }

        return $resolveUrlService->redirectSalePage($request, $promotionCode,  $params,  $request->ip());

    }
}