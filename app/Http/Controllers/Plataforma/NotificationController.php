<?php

namespace App\Http\Controllers\Plataforma;

use App\Models\Plataforma\Notification;
use App\Repository\NotificationRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use App\Models\Plataforma\Messages;
use App\Models\Plataforma\Categorie;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use App\Repository\TalkAffiliatesRepository;


class NotificationController extends Controller
{
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }


    function index(Request $request)
    {
        $notifications = $this->notificationRepository->getNotifications($request);

        return View('plataforma.notification.index', compact('notifications'))->with(['title' => "Pagina de Notificações"]);
    }

    function getView($id)
    {
        $notification = Notification::find($id);
        $user = User::find(Auth::user()->id);

        if (!$notification) {
            session()->flash("error", "Notificação não existe");
            return redirect()->back()->withInput();
        }

        $notification->read = 1;

        if(!$notification->save()) {
            session()->flash("error", "Erro ao visualizar Notificação");
            return redirect()->back()->withInput();
        }

        return View('plataforma.notification.view', compact('notification', 'user'));
    }

    function getNotificationsApi()
    {
        $notifications = $this->notificationRepository->getNotificationsApi();

        return Response($notifications, 200);
    }
}
