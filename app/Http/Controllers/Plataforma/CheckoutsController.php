<?php

namespace App\Http\Controllers\Plataforma;

use App\Http\Requests\Plataforma\CheckoutPaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Checkout;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Plan;
use App\Services\CheckoutService;
use App\Http\Requests\Plataforma\CheckoutFormRequest;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use App\Models\Plataforma\Tracking;
use App\Models\Plataforma\TransactionMp;
use App\Jobs\TrackingJob;
use MercadoPago;
use Illuminate\Support\Facades\DB;
use App\Helpers\Helper;
use App\Providers\App\Events\ProductSold;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
use mysql_xdevapi\Exception;

class CheckoutsController extends Controller
{
    public $checkoutService;
    private $numberTab = 2;

    const PROD_CLIENT_ID = 'Client_Id_f0171d7154bf2112d56460a8a92d1d3d6dfd235e';
    const PROD_CLIENT_SECRET = 'Client_Secret_da97152281b2ce6fec7b5a78ddb526c591cbea84';

    const DEV_CLIENT_ID = 'Client_Id_5f4456fd54b7637c854cc95eb215692d5e66b8bc';
    const DEV_CLIENT_SECRET = 'Client_Secret_3562ef57a8f91fa31b4f9a747c3238544b948b70';


    public function __construct(CheckoutService $checkoutService)
    {
        $this->checkoutService = $checkoutService;
    }


    public function view($product_id, $planId = null)
    {
        $product = Product::where('id', $product_id)->first();
        $plans = Plan::where('product_id', $product->id)->pluck('name', 'id');

        if ($product->payment_type == 'SINGPRICE') {
            $checkouts = Checkout::where('plan_id', $product->plans[0]->id)->get();
            $view = 'plataforma.checkouts.view-type-price-unique';
        } else {
            $checkouts = Checkout::where('plan_id', $planId)->get();
            $view = 'plataforma.checkouts.view-type-plan';
        }

        return view($view, compact('product', 'checkouts', 'plans', 'planId'))
            ->with('numberTab', $this->numberTab);
    }

    public function add($codeProduct)
    {
        $plans = Plan::where('product_id', app('request')->product->id)->pluck('name', 'id');

        return view('plataforma.checkouts.add', compact('plans'))
            ->with('numberTab', $this->numberTab);;
    }

    public function create(CheckoutFormRequest $request, $productUuid)
    {
        $checkout = $this->checkoutService->create($request->all());

        if ($checkout) {
            session()->flash("success", "Checkout criado com sucesso.");
            return redirect()->route('product.checkout.view', $productUuid);
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function search($productUuid, $planId)
    {
        $checkouts = Checkout::where('plan_id', $planId)->get();

        return $checkouts;
    }

    public function edit($productUuid, $codCheckout)
    {
        $checkout = Checkout::where('code', $codCheckout)->first();

        return view('plataforma.checkouts.edit', compact('checkout'))
            ->with('numberTab', $this->numberTab);
    }

    public function update(CheckoutFormRequest $request, $productUuid, $codCheckout)
    {
        $data = $request->all();

        if (!isset($data['pay_card'])) {
            $data['pay_card'] = 0;
        }

        if (!isset($data['pay_ticket'])) {
            $data['pay_ticket'] = 0;
        }

        $checkout = Checkout::where('code', $codCheckout)->first();

        if (isset($data['banner_1']) || isset($data['banner_2'])) {
            $data = $this->saveBanners($checkout, $data);
        }

        $checkout = $this->checkoutService->update($data, $codCheckout);

        if ($checkout) {
            session()->flash("success", "Checkout atualizado com sucesso.");
            return redirect()->back();
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }

    public function saveBanners($checkout, $data)
    {
        if (isset($data['banner_1']) && Storage::disk('images')->exists($checkout->banner_1)) {
            Storage::disk('images')->delete($checkout->banner_1);
        }

        if (isset($data['banner_2']) && Storage::disk('images')->exists($checkout->banner_2)) {
            Storage::disk('images')->delete($checkout->banner_2);
        }

        $pathFile = Auth::user()->id . '/banners';

        if (isset($data['banner_1'])) {
            $file = Storage::disk('images')->put($pathFile, $data['banner_1']);
            $data['banner_1'] = $file;
        }

        if (isset($data['banner_2'])) {
            $file = Storage::disk('images')->put($pathFile, $data['banner_2']);
            $data['banner_2'] = $file;
        }

        return $data;

    }

    public function delete($productUuid, $codCheckout)
    {
        $checkout = Checkout::where('code', $codCheckout)->first();

        if ($checkout && $checkout->default) {
            session()->flash("error", "Checkout padrão não pode ser excluído.");
            return redirect()->back();
        }

        if ($this->checkoutService->delete($codCheckout)) {
            session()->flash("success", "Checkout deltado com sucesso.");
            return redirect()->to('product/' . $productUuid . '/checkouts/v/' . $checkout->plan->id);
        }

        session()->flash("error", "Ocorreu um erro. Por favor, tente novamente");
        return redirect()->back()->withInput();
    }


    public function front(Request $request, $checkoutCode)
    {
        $params = $request->query();

        // Caso o acesso a esta rota seja direto então realizar tracking para o produtor
        if (!isset($params['ref']) || empty($params['ref'])) {
            $resolveUrlService = app()->make('ResolveUrlService');
            $resolveUrlService->trackingDirectCheckout($request, $params, $checkoutCode);
        }

        $states = Helper::ListStates();

        $checkout = Checkout::withoutGlobalScopes()->where('code', $checkoutCode)->with('planChk')->first();

        if (!$checkout) {
            return 'Not Found';
        }

        $price = $checkout->planChk->getOriginal('price');
        $parcelas = [];

        if ($price > 10) {
            $parcelas = Helper::parcelas($checkout->planChk->getOriginal('price'));
        } else {
            $parcelas[1] = $price;
        }

        $trats_id = $request->cookie('trats_id');

        $selectView = "plataforma.checkouts.template2.payment";
        if ($checkout->template == 2) {
            $selectView = "plataforma.checkouts.template2.payment";
        }

        try {
            $valor = str_replace("." , "" , $price );
            $valor = str_replace("," , "" , $valor);

            if($valor < 500) {
                return "Valor mínimo do produto é R$ 5,00";
            }

            $repasses = $this->getInfoRepasses();
            $item = [
                'name' => $checkout->planChk->productChk->name,
                'amount' => 1,
                'value' => (int) $valor,
                'marketplace'=>array('repasses' => $repasses)
            ];

            $charge = $this->getPaymentToken($item);
            $chargeId = $charge['data']["charge_id"];
        } catch (GerencianetException $e) {
            return  Redirect::back()->withInput()->withErrors([$e->getMessage()]);
        } catch (Exception $e) {
            return Redirect::back()->withInput()->withErrors([$e->getMessage()]);
        }

        return view($selectView, compact('checkout', 'states', 'parcelas', 'trats_id', 'chargeId'));
    }

    private function getPaymentToken($item)
    {
        $options = $this->getDataAcessGerenciaNet();

        $metadata = array('notification_url'=> url('/api/webhook/gerencianet'));

        $body = [
            'items' => [ $item ],
            'metadata' => $metadata
        ];

        try {
            $api = new Gerencianet($options);
            return $api->createCharge([], $body);

        } catch (GerencianetException $e) {
            throw new \Exception($e->getMessage());
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
    /**
     * @param Request $request
     * @param $codeCheckout
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function payment(CheckoutPaymentRequest $request, $codeCheckout)
    {
        $data = $request->all();
        $checkout = Checkout::withoutGlobalScopes()->where('code', $codeCheckout)->with('planChk')->first();
        $product = $checkout->planChk->productChk;
        $plan = $checkout->planChk;

        $fieldsValidator = [
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];

        if($data['paymentMethodId']  == 'bolbradesco')
            $fieldsValidator['docCliBol'] = 'required';
        else{
            $fieldsValidator['payment_token'] = 'required';
        }

        if ($data['paymentMethodId'] != 'bolbradesco') {
            $fieldsValidator['docNumber'] = 'required';

            if(strlen($request ['docNumber'])  < 11 ) {
                return Redirect::back()->withInput()->withErrors(['o campo CPF deve conter no mínimo 14 caracteres.']);
            }

        } else if(!$checkout->request_cpf) {
            $fieldsValidator['docCliBol'] = 'required';
            if(strlen($request['docCliBol'])  < 14 ) {
                return Redirect::back()->withInput()->withErrors(['o campo CPF deve conter no mínimo 14 caracteres.']);
            }
        }
        $validator = Validator::make($request->all(), $fieldsValidator);

        if ($validator->fails()) {
            return redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $url = "https://api.mercadopago.com/v1/payments?access_token=" . config('mercadopago.credencial.access_token');

        $installments = (isset($data['installmentsOption'])) ? (integer)$data['installmentsOption'] : null;

        $transaction_amount = null;

        if ($data['paymentMethodId'] == 'bolbradesco' && $checkout->pay_ticket) {

            $transaction_amount = $checkout->planChk->getOriginal('price');


        } else if ($data['paymentMethodId'] == 'bolbradesco' && !$checkout->pay_ticket) {
            return Redirect::back()->withInput()->withErrors(['pagamento não permitido']);
        } else if ($data['paymentMethodId'] != 'bolbradesco' && !$checkout->pay_card) {
            return Redirect::back()->withInput()->withErrors(['pagamento não permitido']);
        } else if ($data['paymentMethodId'] != 'bolbradesco') {

            $val_parcela = Helper::jurosComposto($checkout->planChk->getOriginal('price'), $installments);
            $transaction_amount = $val_parcela * $installments;
        }


        //ENDEREÇO PADRÃO PARA BOLETOS
        $zip_code_default = '88058300';
        $street_name_default = 'Rua Principal';
        $street_number_default = '1';
        $neighborhood_default = 'Centro';
        $city_default = 'Florianópolis';
        $federal_unit_default = 'SC';

        $billingAddress = [
            'street' => $street_name_default,
            'number' => $street_number_default,
            'neighborhood' => $neighborhood_default,
            'zipcode' => $zip_code_default,
            'city' => $city_default,
            'state' => $federal_unit_default,
        ];

        $docTypeCli = 'cpf';
        if($data['paymentMethodId']  == 'bolbradesco') {
            $doc_cliente_processamento = str_replace(['.', ',', '-', '/'], '', $data["docCliBol"]);
            if (strlen($doc_cliente_processamento) == 11) {
                $docTypeCli = "cpf";
            } else if (strlen($doc_cliente_processamento) == 14) {
                $docTypeCli = "cnpj";
            } else {
                return Redirect::back()->withInput()->withErrors(['Verifique documento digitado']);
            }
        }


        if (isset($data['phone'])) {
            $phone = str_replace(['(', ')', '-', ' '], '', $data['phone']);
            $phone_area_code = substr($phone, 0, 2);
            $phone_number = substr($phone, 2);
        }

        $name = explode(' ', $data['name']);
        $t_nome = count($name);
        if ($t_nome == 1) {
            return Redirect::back()->withInput()->withErrors(['Digite nome e Sobrenome']);
        } else if ($t_nome == 2) {
            $first_name = $name[0];
            $last_name = $name[1];
        } else {
            $first_name = $name[0];
            $last_name = $name[$t_nome - 1];
        }

        $post = [
            "transaction_amount" => (float)$transaction_amount,
            "token" => $data['token'] ?? null,
            "installments" => $installments,
            "description" => $checkout->planChk->productChk->name,
            "payment_method_id" => $data['paymentMethodId'],
            "external_reference" => mt_rand(10000000, 99999999),
            "additional_info" => [
                "ip_address" => $request->ip(),
                "items" => [
                    [
                        "id" => $checkout->planChk->productChk->id,
                        "title" => $checkout->planChk->productChk->name,
                        "description" => $checkout->planChk->productChk->desc,
                        "picture_url" => url('storage/products/' . $checkout->planChk->productChk->image),
                        "category_id" => "others",
                        "quantity" => 1,
                        "unit_price" => (float)$transaction_amount
                    ],
                ],

                "payer" => [
                    "first_name" => $first_name,
                    "last_name" => $last_name,
                    "address" => [
                        "zip_code" => $data['zipcode'] ?? $zip_code_default,
                        "street_name" => $data['address'] ?? $street_name_default,
                        "street_number" => $data['address_number'] ?? $street_number_default,
                    ],
                    "phone" => [
                        "area_code" => $phone_area_code ?? null,
                        "number" => $phone_number ?? null,
                    ],
                ],
            ],
            "payer" => [
                "email" => $data['email'],
                "first_name" => $first_name,
                "last_name" => $last_name,
                "identification" => [
                    "type" => $docTypeCli,
                    "number" => isset($doc_cliente_processamento)? $doc_cliente_processamento : $data['docNumber']
                ],
                "address" => [
                    "zip_code" => $data['zipcode'] ?? $zip_code_default,
                    "street_name" => $data['address'] ?? $street_name_default,
                    "street_number" => $data['address_number'] ?? $street_number_default,
                    "neighborhood" => $data['address_district'] ?? $neighborhood_default,
                    "city" => $data['address_city'] ?? $city_default,
                    "federal_unit" => $data['address_state'] ?? $federal_unit_default,
                ],
            ]
        ];

        try {
            $res = $this->gerenciaNet($post, $request['payment_token'], $billingAddress, $request['charge_id'], $request['installmentsOption'],  $phone);
        } catch (\Exception $e) {
            return Redirect::back()->withInput()->withErrors([$e->getMessage()]);
        }

        $transaction = [
            'code' => $res['data']['charge_id'], //$res->external_reference,
            'transaction_id' => $res['data']['charge_id'], // $res->id,
            'gateway' => 'gerencianet',
            'date_created' => \Carbon\Carbon::now()->format('Y-m-d h:i:s'),
            'date_approved' => null, //$res->date_approved ? date('Y-m-d H:i:s', strtotime($res->date_approved)) : NULL,
            'operation_type' => '', //$res->operation_type,
            'payment_method_id' => $res['data']['payment'],
            'payment_type_id' => $res['data']['payment'] == "banking_billet" ? 'ticket': 'credit_card',// $res->payment_type_id,
            'barcode' => $res['data']['barcode'] ?? null,
            'last_four_digits' => $res['card']['last_four_digits '] ?? 0,
            'expiration_month' => $res['card']['expiration_month '] ?? 0,
            'expiration_year' => $res['card']['expiration_year '] ?? 0,
            'status_gateway' => $res['data']['status'],
            'status_detail_gateway' => \App\Helpers\Helper::getStatusDetailGerenciaNetTratspay($res['data']['status']), //$res->status_detail,
            'product_description' => $post['additional_info']['items'][0]['title'],//$res->description,
            'captured' => null, //$res->captured,
            'installments' => $request['installmentsOption'] ? $request['installmentsOption'] : 1,
            'shipping_amount' => '', //$res->shipping_amount,
            'transaction_amount' => floatval($post['transaction_amount']),// valor cheio
            'external_resource_url' => $res['data']['pdf']['charge'] ?? "",//$res->transaction_details->external_resource_url, 'first_name' =>  $post['payer']['first_name'] ?? $first_name,
            'first_name' => $post['payer']['first_name'] ?? $first_name,
            'last_name' => $post['payer']['last_name'] ?? $last_name,
            'email' => $post['payer']['email'],
            'cpf_cnpj' => $doc_cliente_processamento ?? null,
            'phone' =>  null,
            'zipcode' => $data['zipcode'] ?? $zip_code_default,
            'address' => $data['address'] ?? $street_name_default,
            'address_number' => $data['address_number'] ?? $street_number_default,
            'address_complement' => $data['address_complement'] ?? null,
            'address_district' => $data['address_district'] ?? $neighborhood_default,
            'address_city' => $data['address_city'] ?? $city_default,
            'address_state' => $data['address_state'] ?? $federal_unit_default,
            'address_country' => $data['address_country'] ?? null,
            'product_id' => $product->id,
            'product_uuid' => $product->uuid,
            'product_type' => $product->product_type,
            'product_name' => $product->name,
            'email_support' => $product->email_support,
            'checkout_id' => $checkout->id,
            'checkout_code' => $checkout->code,
            'plan_id' => $checkout->planChk->id,
            'plan_code' => $checkout->planChk->code,
            'plan_name' => $checkout->planChk->name,
            'plan_quantity' => $checkout->planChk->quantity ?? null,
            'plan_price' => $checkout->planChk->getOriginal('price'),
            'plan_frequency' => $checkout->planChk->frequency,
            'warranty' => $product->warranty,
            'end_warranty' => null,
            'rate_percent' => null,//$this->getRateByProductType($product, $product->product_type),
            'fixed_rate' => 1.0,
            'rate_total' => $this->calculeRateTratsPay($product, $transaction_amount, $product->product_type), //taxa de serviço
            'status' => \App\Helpers\Helper::getStatusPaymentGerenciaNetTratspay($res['data']['status']),
            'traking_code' => $request->cookie('trats_id') ?? null
        ];

        $sale = TransactionMp::create($transaction);
        //event(new ProductSold($transaction));

        return view('plataforma.checkouts.finalized', compact('sale', 'checkout'));
    }


    public function webhookGerenciaNet(Request $request)
    {
        $options = $this->getDataAcessGerenciaNet();

        $token = $request->get("notification");
        $params = [
            'token' => $token
        ];
        try {
            $api = new Gerencianet($options);
            $chargeNotification = $api->getNotification($params, []);

            // Conta o tamanho do array data (que armazena o resultado)
            $i = count($chargeNotification["data"]);
            // Pega o último Object chargeStatus
            $ultimoStatus = $chargeNotification["data"][$i-1];
            // Acessando o array Status
            $status = $ultimoStatus["status"];
            // Obtendo o ID da transação
            $charge_id = $ultimoStatus["identifiers"]["charge_id"];
            // Obtendo a String do status atual
            $statusAtual = $status["current"];

            $sale = TransactionMp::where('transaction_id', $charge_id)->first();

            if ($sale) {

                $to_update = [
                    'status_gateway' => $statusAtual,
                    'status_detail_gateway' => \App\Helpers\Helper::getStatusDetailGerenciaNetTratspay($statusAtual)
                ];

                if ($statusAtual == 'paid') {
                    $to_update['date_approved'] = \Carbon\Carbon::now()->format('Y-m-d');
                }

                $sale->update($to_update);
                $response['status'] = 'ok';
                $response['message'] = 'Status pagamento atualizado com sucesso';
                return response()->json($response, 200);
            }

            return response()->json('Transação não encontrada na colinapay', 500);
        } catch (GerencianetException $e) {
            return response()->json($e->error, 500);
        } catch (Exception $e) {
            return response()->json($e->error, 500);
        }
    }

    public function webhookMp(Request $request)
    {
        $response = [
            'status' => 'error',
            'message' => 'Erro ao atualizar status pagamento'
        ];

        $data = $request->all();

        $transaction_id = $data['data']['id'];
        $date_created = $date = $data['date_created'];

        MercadoPago\SDK::setAccessToken(config('mercadopago.credencial.access_token'));

        $payment = MercadoPago\Payment::find_by_id($transaction_id);

        if (!$payment->id) {
            return response()->json($response, 500);
        } else {
            $sale = TransactionMp::where('transaction_id', $transaction_id)->first();
            if ($sale) {

                $to_update = [
                    'status_gateway' => $payment->status,
                    'status_detail_gateway' => $payment->status_detail
                ];

                if ($payment->status == 'approved' && $payment->status_detail == 'accredited') {
                    $date_created = date("Y-m-d H:i:s", strtotime($date_created));
                    $to_update['date_approved'] = $date_created;
                }

                $sale->update($to_update);
            }
        }

        $response['status'] = 'ok';
        $response['message'] = 'Status pagamento atualizado com sucesso';
        return response()->json($response, 200);

    }

    public function ticket($transaction_id)
    {
        $transaction = TransactionMp::where('id', $transaction_id)->first();
        $linkTicket = $transaction->external_resource_url;

        return View('plataforma.checkouts.ticket', compact('linkTicket'));
    }

    private function getDataAcessGerenciaNet()
    {
        if(env('GERENCIANET_PRODUCAO'))
            return [ 'client_id' => self::PROD_CLIENT_ID, 'client_secret' => self::PROD_CLIENT_SECRET, 'sandbox' => false ];

        return [ 'client_id' => self::DEV_CLIENT_ID, 'client_secret' => self::DEV_CLIENT_SECRET, 'sandbox' => true ];
    }
    private function getInfoRepasses()
    {
        $repass_1 = [
            'payee_code' => "e6d15656a2f077ae2d5cb74819659300",
            'percentage' => 100 // 1% Junior
        ];
        $repass_2 = [
            'payee_code' => "9687193b54089718fea15ad8fa6ee0b6",
            'percentage' => 100 // 1% Jackson
        ];
        $repass_3 = [
            'payee_code' => "db32e6c50b95cd623f3800c459a41748",
            'percentage' => 100 // 1% Jhonsef
        ];
        $repass_4 = [
            'payee_code' => "009efb7a8306a3940f675854429877b1",
            'percentage' => 100 // 1% Andre
        ];
        $repass_5 = [
            'payee_code' => "8baf60a6b3ab67baea89202f60c8fc16",
            'percentage' => 100 // 1% Ademir
        ];
        $repass_6 = [
            'payee_code' => "5b51f50354cedbc4621705c82eb172c3",
            'percentage' => 100 // 1% laize
        ];

        return [$repass_1, $repass_2, $repass_3, $repass_4, $repass_5, $repass_6];
    }


    private function gerenciaNet($data, $paymentToken, $billingAddress, $charge_id, $installment, $phone)
    {
        $repasses = $this->getInfoRepasses();
        $options = $this->getDataAcessGerenciaNet();

        $valor = str_replace("." , "" , $data['transaction_amount'] ); // Primeiro tira os pontos
        $valor = str_replace("," , "" , $valor); // Depois tira a vírgula

        $item = [
            'name' => $data['additional_info']['items'][0]['title'],
            'amount' => $data['additional_info']['items'][0]['quantity'],
            'value' => $valor,
            'marketplace'=>array('repasses' => $repasses)
        ];

        $customer = [
            'name' => $data['payer']['first_name']." ".$data['payer']['last_name'],
            'cpf' =>  $data['payer']['identification']['number'],
            'phone_number' => $phone,
            'email' => $data['payer']['email'], //credit card
            'birth' => '1977-01-15'
        ];
        $params = [
            'id' => $charge_id
        ];
        if($data['payment_method_id'] == "bolbradesco") {
            $payment = [
                'banking_billet' => [
                    'expire_at' => \Carbon\Carbon::now()->addDays(3)->format('Y-m-d'), //$_POST["vencimento"],
                    'customer' => $customer
                ]
            ];
        } else {
            $params = [
                'id' => $charge_id
            ];

            $creditCard = [
                'installments' => (int) $installment,
                'billing_address' => $billingAddress,
                'payment_token' => $paymentToken, // vem do front
                'customer' => $customer
            ];
            $payment = [
                'credit_card' => $creditCard
            ];
        }
        $body = ['payment' => $payment];
        try {
            $api = new Gerencianet($options);
            return $api->payCharge($params, $body);
        } catch (GerencianetException $e) {
            throw new \Exception($e->getMessage());
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
