<?php

namespace App\Http\Controllers\Plataforma;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Plataforma\Account;
use Illuminate\Support\Facades\Auth;
use App\Models\Plataforma\TransactionAccount;
use App\Models\Plataforma\BankAccount;
use App\Models\Plataforma\Cashout;
use Illuminate\Support\Facades\DB;

class CashoutController extends Controller
{
    public function index()
    {

        $account = Account::where('user_id', Auth::user()->id)->first();
        $bankAccounts = BankAccount::where('user_id', Auth::user()->id)->with('bank')->get();
        $totalCashouts = Cashout::where('user_id', Auth::user()->id)->sum('value');

        $user_id = Auth::user()->id;
        $sql = "SELECT
                    C.NAME AS bank_name
                    , A.value
                    , A.created_at date_solicitation
                    , A.status status_cashout
                    , B.*
                FROM CASHOUTS A, BANK_ACCOUNT B, BANKS C
                WHERE A.BANK_ACCOUNT_ID = B.ID
                    AND B.BANK = C.CODE
                    AND A.USER_ID = {$user_id}
                ORDER BY A.created_at, date_solicitation DESC";

        $cashouts = DB::select($sql);

        $formatedBankAccounts = [];

        foreach ($bankAccounts as $key => $bankAccount) {
            $text = $bankAccount->Bank->name . ' ';
            $text .= 'Ag: ' .   $bankAccount->bank_branch . '-'. $bankAccount->bank_branch_digit . ' | ';
            $text .= 'Conta ' . $bankAccount->bank_account . '-'. $bankAccount->bank_account_digit;

            if ($bankAccount->variation) {
                $text .= '| Operacao: ' . $bankAccount->variation;
            }

            $formatedBankAccounts[$bankAccount->id] = $text;
        }

        return View('plataforma.cashouts.index', compact('account', 'cashouts', 'formatedBankAccounts', 'totalCashouts'))
                ->with('titlePage', 'Saques');
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $valueTotal = 0.0;

        $value = str_replace('.', '',  $data['value']);
        $value = str_replace(',', '.',  $value);

        $valueTotal = $value + 4.90;
        $data['value_total'] = $valueTotal;

        $data['rate'] = 4.90;
        $data['user_id'] = Auth::user()->id;
        $data['status'] = "processing";

        $usetHasThisAccount = BankAccount::where('user_id', Auth::user()->id)->first();

        if (!$usetHasThisAccount) {
            session()->flash("error", "Esta conta não pertece ao seu usuário.");
            return redirect()->back()->withInput();
        }

        $account = Account::where('user_id', Auth::user()->id)->first();

        if ( $valueTotal > $account->balance) {
            session()->flash("error", "Valor do saque não pode ser maior do que o saldo disponível.");
            return redirect()->back()->withInput();
        }

        try {
            DB::beginTransaction();
            $account->balance = $account->balance - $valueTotal;
            $account->update();
            Cashout::create($data);

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            session()->flash("error", "Erro a realizar a solicitação. Por favor, tentar novamente!");
            return redirect()->back()->withInput();
        }

        session()->flash("success", "Solicitação de saque enviada com sucesso!");
        return redirect()->route('cashout.index');

    }
}
