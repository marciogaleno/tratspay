<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Plataforma\Product;
use App\Models\Admin\AdminSystemConfiguration;

class Controller extends BaseController
{
    protected $filesIcons = [
        'default'   =>  'fa fa-file-text-o',
        'pdf'   =>  'fa fa-file-pdf-o',
        'xls' =>  'fa fa-file-excel-o',
        'xlsx' =>  'fa fa-file-excel-o',
        'epub'  =>  'fa fa-leanpub',
        'csv'   =>  'fa fa-file-text-o',
        'jpg' =>  'fa fa-file-image-o',
        'jpeg' =>  'fa fa-file-image-o',
    ];

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function guid() {
        return strtoupper(bin2hex(openssl_random_pseudo_bytes(3)));
    }

    public function formatSizeUnits($bytes) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }

    protected function calculeRateTratsPay(Product $product, $transaction_amount, $productType)
    {
//        $rateVirtualProduct = $this->getRateByProductType($product, 'V');
//        $rateProductPhysical = $this->getRateByProductType($product, 'F');
//        $fixedRate = $this->getRateFixed();
//
//        $transaction_amount -= ($transaction_amount * 0.05);
//        $transaction_amount -= ($transaction_amount * 0.05);

//        $rateTotal = 0.0;
//
//        switch ($productType) {
//            case 'V':
//                $rateTotal = (($transaction_amount *  $rateVirtualProduct) / 100 ) + $fixedRate;
//                break;
//            case 'F':
//                $rateTotal = (($transaction_amount * $rateProductPhysical) / 100 ) * $fixedRate;
//                break;
//            default:
//                break;
//        }

        //5% da gerencianet 5% dos aplit
        return $transaction_amount * 0.10;

    }

    protected function getRateByProductType(Product $product, $productType)
    {
        if ($product->rate && $product->rate > 0) {
            return floatval($product->getOriginal('rate'));
        }

        $config = AdminSystemConfiguration::pluck('value', 'key')->toArray();

        switch ($productType) {
            case 'V':
                return floatval($config['rate_virtual_product']);
                break;
            case 'F':
            return floatval($config['rate_physical_product']);
                break;
            default:
                break;
        }

        return 0.0;

    }

    protected function getRateFixed()
    {
        $fixedRate = 1.0;
        return $fixedRate;
    }

    function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
    }
}
