<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Post $post)
    {   
        //$this->authorize('view_post');
        //if( Gate::denies('view_post') ) 
        //    return 'Não autorizado';

        ///$posts = $post->where('user_id', auth()->user()->id)->get();
        //$posts = $post::all();
        return view('home');
    }
}
