<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;
use  App\User;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\CoProducer;
use App\Models\Plataforma\CoAffiliate;

class CoAffiliateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {     
        $validations = [];

        $validations = [
            'email'                 => 'required',
            'format_commission'     => 'required',
            'value_commission'       => 'required',
        ];

        if ($this->deadline) {
            $validations['deadline'] = 'data';
        }
        
        
        return $validations;

    }

    public function messages()
    {
        return [
            'email.unique' => 'Co-Afiliado já cadastrado '
        ];
    }
    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
  
            $user = User::where('email', $this->email)->first();

            if (!$user) {
                $validator->errors()->add('email_not_exists', 'E-mail informado não encontrado na Plataforma');
            }

            $coaffiliate = CoAffiliate::where('email', $this->email)->where('product_id', $this->product_id)->first();
         
            if ($coaffiliate) {
                $validator->errors()->add('coaffiliate_exists', 'Co-Afiliado já cadastrado para esse produto');
            }
        });
    }

}
