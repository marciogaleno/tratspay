<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
        ];

//        if ($data['paymentMethodId'] != 'bolbradesco') {
//            $fieldsValidator['docNumber'] = 'required';
//        } else if(!$checkout->request_cpf) {
//            $fieldsValidator['docCliBol'] = 'required';
//        }

//        if ($checkout->request_phone) {
//
//            $fieldsValidator['phone'] = 'required';
//        }

//        if ($checkout->request_address) {
//
//            $fieldsValidator['zipcode'] = 'required';
//            $fieldsValidator['address'] = 'required';
//            $fieldsValidator['address_number'] = 'required';
//            $fieldsValidator['address_district'] = 'required';
//            $fieldsValidator['address_city'] = 'required';
//            $fieldsValidator['address_state'] = 'required';
//            $fieldsValidator['address_country'] = 'required';
//        }

        return $rules;
    }
}
