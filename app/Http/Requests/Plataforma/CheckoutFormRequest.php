<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            //'desc'                => 'required',
            'invoice des'         => 'max:12',
            'pay_card'            => 'numeric',
            'pay_ticket'          => 'numeric',
            'request_address'     => 'numeric',
            'days_expir_ticket'   => 'between:1,5',
            'request_cpf'         => 'numeric',
            'request_phone'       => 'numeric',
            'banner_1'            => 'file|max:5120|mimes:jpeg,jpg,png',
            'banner_2'            => 'file|max:5120|mimes:jpeg,jpg,png'
        ];

        return $validations;
    }

    public function messages()
    {
      return [            
        'banner_1.max' => "O tamanho da imagem não pode ser maior do que 5MB",
        'banner_1.mimes' => "A imagem deve ser um arquivo do tipo: jpg, jpeg ou png.",
        'banner_2.max' => "O tamanho da imagem não pode ser maior do que 5MB",
        'banner_2.mimes' => "A imagem deve ser um arquivo do tipo: jpg, jpeg ou png."
      ];
    }
}
