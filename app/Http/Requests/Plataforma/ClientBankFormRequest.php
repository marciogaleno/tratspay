<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class ClientBankFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validations = [
            'cpf_cnpj_banco'        => 'required'
            , 'titular_conta'       => 'required'
            , 'bank'                => 'required'
            , 'type'                => 'required'
            , 'bank_branch'         => 'required'
            , 'bank_branch_digit'   => 'required'
            , 'bank_account'        => 'required'
            , 'bank_account_digit'  => 'required'
        ];

        return $validations;
    }

    public function messages()
    {
        return [
            'bank_branch.required'                      => 'Necessário informar a agência!'
            , 'bank_branch_digit.required'              => 'Necessário informar o dígito da agência!'
            , 'bank_account.required'                   => 'Necessário informar a conta!'
            , 'bank_account_digit.required'   => 'Necessário informar o dígito da conta!'
        ];
    }
}
