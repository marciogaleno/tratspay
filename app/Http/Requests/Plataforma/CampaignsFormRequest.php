<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class CampaignsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'name'            => 'required',
            'type_url'        => 'required',
            'pixel'           => 'required',
            'pixel_id'        => 'required',
        ];

        if ($this->type_url == 2) {
            $validations['checkout_code'] = 'required';
        }

        if ($this->type_url == 3) {
            $validations['url_alternative_code'] = 'required';
        }

        return $validations;
    }

}
