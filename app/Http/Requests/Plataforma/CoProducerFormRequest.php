<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;
use  App\User;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\CoProducer;

class CoProducerFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [];

        $validations = [
            'email'                 => 'required',
            'type'                  => 'required'
        ];

        return $validations;

    }

    public function messages()
    {
        return [
            'email.unique' => 'Co-produtor já cadastrado '
        ];
    }
    public function withValidator($validator)
    {
        if ($this->coproducerId) {
            return;
        }

        $validator->after(function ($validator) {

            $user = User::where('email', $this->email)->first();

            if (!$user) {
                $validator->errors()->add('email_not_exists', 'E-mail informado não está cadastrado Plataforma');
            }

            $coproducer = CoProducer::where('email', $this->email)->where('product_id', app('request')->product->id)->first();

            if ($coproducer) {
                $validator->errors()->add('coproducer_exists', 'Co-produtor já cadastrado para esse produto');
            }
        });
    }

}
