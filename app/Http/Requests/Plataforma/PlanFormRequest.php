<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;

class PlanFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'name'            => 'required',
            'different_value' => 'numeric',
            'price'           => 'numeric',
        ];

        // Se plano grátis não precisa validar os outros campos
        if ($this->free) {
            return $validations;
        }

        if (!$this->free) {
            $validations['price'] = 'required';
            $validations['frequency'] = 'required';
        }

        // Sepois de salvo o plano não pode mais editar mais o tipo de frequencia de pagamentoo 
       /* if ($this->route()->getActionMethod() == 'update') {
            return $validations;
        }*/

        if ($this->frequency != 'UNI') {
            $validations['first_installment'] = 'required';
        }

        if ($this->first_installment == 3) {
            $validations['different_value'] = 'required';
        }

        if ($this->first_installment == 4) {
            $validations['free_days'] = 'required';
        }

        return $validations;
    }

}
