<?php

namespace App\Http\Requests\Plataforma;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Route;

class ProductFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {     
        $routeName = $this->route()->getName();

        $validations = [];

        if ($routeName == "product.store") {

            $validations = [
                'product_type'          => 'required',
                'name'                  => 'required|max:100',
                'email_support'         => 'required:max:100',
                'category_id'          => 'required',
                'type_deliverie_id'       => 'required:',
                'local_page_sale'       => 'required:url',
                'several_purchases'   => 'required',
                'warranty'              => 'required|integer|between:1,30',
                'payment_type'        => 'required',
                'accept_affiliation'      => 'required',
                'image'                     => 'file|max:5120|mimes:jpeg,jpg,png',
            ];
            
            if (isset($this->url_page_sale) && !empty($this->url_page_sale)) {
                $validations['url_page_sale'] = 'url';
            }

            if (!empty($this->url_page_thank)) {
                $validations['url_page_thank'] = 'url';
            }
    
            if (!empty($this->url_page_print_ticket)) {
                $validations['url_page_print_ticket'] = 'url';
            }
    
            if ($this->payment_type == 'PLAN') {
                $validations['name_plan'] = 'required';
            }
    
            if (!empty($this->free)) {
                $validations['price'] = 'required';
                $validations['frequency'] = 'required';
            }

            if ($this->type_deliverie_id == 1) {
                $validations['type_member_area'] = 'required|between:1,2';
            } 

            if ($this->type_member_area == 1) {
                $validations['url_member_area'] = 'required|url';
            } 
        } 

        if ($routeName == 'product.update') {

            $validations = [
                'name'                  => 'required|max:100',
                'email_support'         => 'required:max:100',
                'category_id'           => 'required',
                'several_purchases'     => 'required',
                'url_page_sale'         => 'required|url',
                'image'                     => 'mimes:jpeg,jpg,png'
            ];

        }
        
        return $validations;

    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            /*if ($this->tipo_entrega_id != 1 && $this->tipo_entrega_id != 2 && $this->tipo_pagamento <> 'PRECOUNICO') {
                $validator->errors()->add('tipo_pagamento', 'O tipo de pagamento dever ser preço unico para o tipo de entrega selecionado.');
            }*/

            if ($this->local_page_sale == 'PE' && 
                    (!isset($this->url_page_sale) || empty($this->url_page_sale))
            ) {
                $validator->errors()->add('url_page_sale', 'O campo endereço de página de vendas é obrigatório.');
            }

            if ($this->accept_affiliation && 
                    (!isset($this->value_commission) || empty($this->value_commission))
            ) {
                $validator->errors()->add('value_commission', 'O campo valor comissão é obrigatório');
            }

            if ($this->accept_affiliation && 
                    (!isset($this->expiration_coockie) || empty($this->expiration_coockie))
            ) {
                $validator->errors()->add('expiration_coockie', 'O campo validade coockie é obrigatório');
            }

        });
    }

    public function messages()
    {
      return [            
        'image.max' => "O tamanho da imagem não pode ser maior do que 5MB",
        'image.mimes' => "A imagem deve ser um arquivo do tipo: jpg, jpeg ou png.",
      ];
    }

}
