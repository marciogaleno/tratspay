<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StoreResourceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module' => 'required',
            'category' => 'required',
            'name' => 'required|min:3',
            'label' => 'required|min:5',
            'icon' => 'required|min:3',
            'enable' => 'required',
            'order' => 'required',
        ];
    }
}
