<?php

namespace App\Listeners;

use App\Events\TrackingEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ClickListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TrackingEvent  $event
     * @return void
     */
    public function handle(TrackingEvent $event)
    {
        $data = $event->getData();
        $params = $event->getParams();
        $trackingCode = $event->getTrackingCode();
        $ip = $event->getIp();
        $typeUrl = $event->getTypeUrl();
        $codeUrl = $event->getCodeUrl();
   
        $clickService = app()->make('ClickService');
        $clickService->count($data, $params,  $trackingCode,  $typeUrl, $codeUrl);
        
    }
}
