<?php

namespace App\Listeners;

use App\Events\ProductSold;
use App\Services\UnlockProductService;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailBuyer;
use App\User;
use App\Mail\SendMail;
use App\Models\Plataforma\Commission;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SendEmailsProductSold implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $transaction;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductSold  $event
     * @return void
     */
    public function handle(ProductSold $event)
    {
        $this->transaction = $event->getTransaction();
        $this->sendEmailBuyer();
        $this->sendEmailsCommissioneds();
        
    }

    private function sendEmailBuyer()
    {   
        if ($this->transaction->payment_type_id == 'ticket') {
            Mail::to($this->transaction->email)
            ->send(new SendMailBuyer('Imprima Seu Boleto', false, false, null, $this->transaction));
            
        }

        if ($this->transaction->payment_type_id == 'ticket' 
            && $this->transaction->status_gateway == 'approved'
            && $this->transaction->status_detail_gateway == 'accredited') 
        {
            /*if ($user) {
                Mail::to($this->transaction->email)
                ->send(new SendMailBuyer('Compra Aprovada!', true, true, null, $this->transaction));  
            } else {
                Mail::to($this->transaction->email)
                ->send(new SendMailBuyer('Compra Aprovada!', false, true, null, $this->transaction));  
            }*/

        }

      
    }

    private function sendEmailsCommissioneds()
    {
        $commissions = Commission::where('transaction_id', $this->transaction->id)
                    ->where('commission_type', '<>', 'system')
                    ->get();

        foreach ($commissions as $commission) {
            Mail::to($commission->user->email)
                ->send(new SendMail($commission, $this->transaction));
        }
    }





}
