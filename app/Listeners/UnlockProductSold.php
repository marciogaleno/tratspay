<?php

namespace App\Listeners;

use App\Events\ProductSold;
use App\Services\UnlockProductService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UnlockProductSold implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $transaction;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductSold  $event
     * @return void
     */
    public function handle(ProductSold $event)
    {
        $this->transaction = $event->getTransaction();
        $unlockProdutoService = new UnlockProductService($this->transaction);

        if ($this->transaction->status_gateway == 'paid') { //approved
            $unlockProdutoService->unLock();
        }
    }

}
