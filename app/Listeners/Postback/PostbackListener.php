<?php

namespace App\Listeners\Postback;

use App\Events\PostbackEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Plataforma\Postback;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Services\PostbackMonetizzeService;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class PostbackListener implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostbackEvent  $event
     * @return void
     */
    public function handle(PostbackEvent $event)
    {
        $transaction = $event->getTransaction();

        $postback = new PostbackMonetizzeService($transaction);
        $postback->execute();
    }
 
}
