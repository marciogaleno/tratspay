<?php

namespace App\Listeners;

use App\Events\ProductSold;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\CommissionService;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SeparateCommissionsProductSold implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $transaction;
    private $commissionService;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductSold  $event
     * @return void
     */
    public function handle(ProductSold $event)
    {
        $this->transaction = $event->getTransaction();
        $this->commissionService = new CommissionService($this->transaction);
        $this->commissionService->splitCommissions();

    }

    private function paymentByCreditCard(){
        switch ($this->transaction->status_detail_gateway) {
            case 'Pagamento confirmado':
                $this->commissionService->splitCommissions();
                break;
            case 'Aguardando a confirmação do pagamento':
                $this->commissionService->splitCommissions();
                break;
            default:
                # code...
                break;
        }
    }

    private function paymentByTicket(){
        switch ($this->transaction->status_detail_gateway) {
            case 'Aguardando a confirmação do pagamento':
                $this->commissionService->splitCommissions();
                break;
            default:
                # code...
                break;
        }
    }

}
