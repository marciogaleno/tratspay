<?php

namespace App\Services;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cookie;
use App\Jobs\TrackingJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Events\TrackingEvent;
use App\Models\Plataforma\Checkout;

class ResolveUrlService {

    function generateUuidVisitor() 
    {
        $trakingCode = (string) Str::uuid();
        //C//ookie::queue(Cookie::make('trats_id',  $trakingCode, 60 * 24 * 365 * 200));

        return $trakingCode;
    }


    function redirectSalePage(Request $request, $referenceCode, $params, $ip)
    {   
        $data = DB::table('producer_affiliate')
                    ->join('products', 'producer_affiliate.product_id', '=', 'products.id')
                    ->join('sale_pages', 'products.id', '=', 'sale_pages.product_id')
                    ->join('users', 'users.id', '=', 'producer_affiliate.user_id')
                    ->select(
                            'producer_affiliate.code as referenceCode', 
                            'producer_affiliate.relation_type as relationType', 
                            'products.id as productId',
                            'products.uuid as productUuid',
                            'products.name as productName',
                            'sale_pages.local_page_sale',
                            'sale_pages.url_page_sale',
                            'users.id as userId',
                            'users.name as userName',
                            'users.image as userImage'
                    )
                    ->where('producer_affiliate.code', $referenceCode)
                    ->first();

        if (isset($data->local_page_sale) && $data->local_page_sale == 'PE') {
            $response = Response(null, 301);
            $urlRedirect = $this->createUrlRedirect($data->url_page_sale . '?ref=' . $referenceCode, $params);
            $response->header( 'Location', $urlRedirect);
            
            $trats_id = $request->cookie('trats_id');
            
            if (!$trats_id) {
                $trats_id = $this->generateUuidVisitor();
            }      
    
            $response->cookie('trats_id', $trats_id, 60 * 24 * 365 * 200);
            $response->header('Content-Type', 'text/html');
            $response->header('Content-Length', 0);
            
            event(new TrackingEvent($data, $params, $trats_id, $ip, 'salePage', null));
    
            return $response;
        }

    }

    public function redirectCheckout(Request $request, $referenceCode, $params, $ip)
    {
        $data = DB::table('producer_affiliate')
            ->join('products', 'producer_affiliate.product_id', '=', 'products.id')
            ->join('plans', 'plans.product_id', '=', 'products.id')
            ->join('checkouts', 'checkouts.plan_id', '=', 'plans.id')
            ->join('users', 'users.id', '=', 'producer_affiliate.user_id')
            ->select(
                'producer_affiliate.code as referenceCode', 
                'producer_affiliate.relation_type as relationType',
                'products.id as productId',
                'products.uuid as productUuid',
                'products.name as productName',
                'checkouts.code as codeCheckout',
                'users.id as userId',
                'users.name as userName',
                'users.image as userImage'
             )
            ->where('checkouts.code', $params['c'])
            ->where('producer_affiliate.code', $referenceCode)
            ->first();
        
        $response = Response(null, 301);
        $urlRedirect = $this->createUrlRedirect(url('chk/' . $data->codeCheckout . '/?ref=' . $data->referenceCode), $params);
        $response->header( 'Location', $urlRedirect);
        
        $trats_id = $request->cookie('trats_id');
        
        if (!$trats_id) {
            $trats_id = $this->generateUuidVisitor();
        }      

        $response->cookie('trats_id', $trats_id, 60 * 24 * 365 * 200);
        $response->header('Content-Type', 'text/html');
        $response->header('Content-Length', 0);


        event(new TrackingEvent($data, $params, $trats_id, $ip, 'checkout',  $data->codeCheckout));

        return $response;

    }

    // realizar tracking para o caso de acessar o checkout direto sem passar pelo redirecionamento normal dos links de divulgação
    function trackingDirectCheckout(Request $request, $params, $checkoutCode)
    {
        $trats_id = $request->cookie('trats_id');
       
        Checkout::where('code', $checkoutCode);


        $data = DB::table('producer_affiliate')
            ->join('products', 'producer_affiliate.product_id', '=', 'products.id')
            ->join('plans', 'plans.product_id', '=', 'products.id')
            ->join('checkouts', 'checkouts.plan_id', '=', 'plans.id')
            ->join('users', 'users.id', '=', 'producer_affiliate.user_id')
            ->select(
                'producer_affiliate.code as referenceCode', 
                'producer_affiliate.relation_type as relationType',
                'products.id as productId',
                'products.uuid as productUuid',
                'products.name as productName',
                'checkouts.code as codeCheckout',
                'users.id as userId',
                'users.name as userName',
                'users.image as userImage'
             )
            ->where('checkouts.code', $checkoutCode)
            ->where('producer_affiliate.relation_type', 'producer')
            ->first();
 
        if (!$trats_id) {
            $trats_id = $this->generateUuidVisitor();
            Cookie::queue('trats_id', $trats_id, 60 * 24 * 365 * 200);
        }      
 
        event(new TrackingEvent($data, $params, $trats_id, null,'checkout',  $data->codeCheckout));
    }

    function createUrlRedirect($url, $params)
    {
        if ((isset($params['src']) && !empty($params['src']))) {
            $url = $url . '&src=' . $params['src'];
        } 

        if ((isset($params['utm_source']) && !empty($params['utm_source']))) {
            $url = $url . '&utm_source=' . $params['utm_source'];
        }

        if ((isset($params['utm_campaing']) && !empty($params['utm_campaing']))) {
            $url = $url . '&utm_campaing=' . $params['utm_campaing'];
        }

        if ((isset($params['utm_medium']) && !empty($params['utm_medium']))) {
            $url = $url . '&utm_medium=' . $params['utm_medium'];
        } 
        
        if ((isset($params['utm_content']) && !empty($params['utm_content']))) {
            $url = $url . '&utm_content=' . $params['utm_content'];
        }

        return $url;
    }

}