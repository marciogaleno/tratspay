<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Models\Plataforma\TrackingIndication;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class IndicationService
{
   private function generateUuidVisitor() 
   {
       $trakingCode = (string) Str::uuid();
       return $trakingCode;
   }
   
   public function tracking(Request $request, $indication_code)
   {
      $track_indication = $request->cookie('track_indication');

      
      $response = Response(null, 301);
      
      $urlRedirect = 'http://tratspay.com.br/?ref=' .  $indication_code;

      if (!$track_indication) {
         $track_indication = $this->generateUuidVisitor();

         TrackingIndication::create([
            'tracking_code' => $track_indication,
            'indication_code' => $indication_code
         ]);

         $response->cookie('track_indication', $track_indication, 60 * 24 * 365 * 200);
      }

      $response->header( 'Location', $urlRedirect);
      $response->header('Content-Type', 'text/html');
      $response->header('Content-Length', 0);
      
      return $response;
   }
}