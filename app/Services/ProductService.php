<?php

namespace App\Services;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Plataforma\Checkout;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\ConfigAffiliation;
use App\Models\Plataforma\Plan;
use Illuminate\Support\Facades\Storage;
use App\Models\Plataforma\SalePage;
use Carbon\Carbon;
use App\User;

class ProductService 
{   
    private $product;
    private $plan;
    private $configAffiliation;
    private $checkout;
    private $salePage;

    function __construct(Product $product, Plan $plan, ConfigAffiliation $configAffiliation, 
            Checkout $checkout, SalePage $salePage)
    {
        $this->product = $product;
        $this->plan = $plan;
        $this->configAffiliation = $configAffiliation;
        $this->checkout = $checkout;
        $this->salePage = $salePage;
    }

    public function create($data) 
    {
        $product = $this->extractDataProduct($data);
        $plans = $this->extractDataPlan($data);
        $configAffiliate = $this->extractConfigffiliation($data);
        $salePage = $this->extractDataSalePage($data);

        try {
            DB::beginTransaction();
            
            $user = User::find(Auth::user()->id);
            
            $file = $this->saveImage($data->image);
            
            if (!$file) {
                return false;
            }

            $product['image'] = $file;
            
            $this->product->fill($product);
            $this->product->save();
            
            $user->products()->save($this->product, ['relation_type' => 'producer', 'status' => 'A']);
            
            if ($this->product->id) {
                $this->configAffiliation->fill($configAffiliate);
                $this->configAffiliation->product_id =  $this->product->id;
                $this->configAffiliation->save();
            }

            
            if ($this->product->id) {
                $this->salePage->fill($salePage);
                $this->salePage->product_id =  $this->product->id;
                $this->salePage->save();
            }
            
            if ($product['payment_type'] == 'SINGPRICE') {
                $this->plan->fill($plans);
                $this->plan->name = 'Plano Padrao';
                $this->plan->product_id = $this->product->id;
                $this->plan->default = 1;
                $this->plan->save();

                $this->checkout->url_page_thank = $data['url_page_thank'];
                $this->checkout->url_page_print_ticket = $data['url_page_print_ticket'];
                $this->checkout->default = 1;
                $this->checkout->plan_id = $this->plan->id;
                $this->checkout->template = 2;               
                $this->checkout->save();
            }
            
            if ($product['payment_type'] == 'PLAN') {
                unset($plans['price']); // Tem que arrumar no frontend pra não vim esse valor quando for plano
                foreach ($plans as $plan) {
                    $newplan = new plan();
                    $newplan->fill($plan);
                    $newplan->default = 1;
                    $newplan->product_id = $this->product->id;
                    $newplan->save();
    
                    $checkout = new Checkout();
                    $checkout->url_page_thank = $data['url_page_thank'];
                    $checkout->url_page_print_ticket = $data['url_page_print_ticket'];
                    $checkout->default = 1;
                    $checkout->plan_id = $newplan->id;
                    $checkout->desc = $newplan->name;
                    $checkout->template = 2;
                    $checkout->save();
                }
            }

            DB::commit();
            return $this->product;
        }catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            if ($this->product->image) {
                $this->deleteImage($this->product->image);
            }
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            if ($this->product->image) {
                $this->deleteImage($this->product->image);
            }
            throw $th;
        }

        return false;

    }

    public function updateProduct($data, $id, $request) {
        try {
            DB::beginTransaction();
            
            $this->product = $this->product->where('id', $id)->first();

            $plan = $this->product->plans->first();

            if ($request->image) {
                $image = $this->saveImage($request->image);
                $data['image'] = $image;
            }

            $this->product->fill($data);
            $this->product->save();

            $this->salePage = SalePage::where('product_id', $this->product->id)->first();
            $this->salePage->url_page_sale = $data['url_page_sale'];
            $this->salePage->save();

            if(isset($data['price'])) {
                $plan->price = $data['price'];
                $plan->save();
            }

            if ( isset($request['image']) 
                && !empty($request['image']) 
                && !$this->saveImage($request)) 
            {
                DB::rollback();
                return false;
            };
            
            DB::commit();
            return true;
        }catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            if ($this->product->image) {
                $this->deleteImage($this->product->image);
            }
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            if ($this->product->image) {
                $this->deleteImage($this->product->image);
            }
            
            throw $th;
        }

        return false;
    }

    public function saveImage($file)
    {    
        $pathFile = Auth::user()->id . '/products';

        if (isset($file) && $file) {
            $pathSaved = Storage::disk('images')->put($pathFile, $file);
           if ($pathSaved) {
               return $pathSaved;
           }
        }
        return null;
    }

    public function deleteImage($pathFile)
    {    
        return Storage::disk('images')->delete($pathFile);     
    }

    /**
     * Essa função faz dois updates em tabelas difentes, 
     * pois a precificação é um conjunto de dados de product com plan.
     * 
     * Mesmo um product com preço, internamente esse product terá um plan padrão.
     * Dessa forma, quando se falar em fazer um update de precificação está 
     * incluso tipo_pagamento e garantia do product mais dados do plan.
     * 
     * $data request daya
     * $uuidproduct uuid do product
     * $planid Id do plan
     * @return void
     */
    public function updatePrecificacaoPrecoUnico($data, $uuidproduct) 
    {
        try {
            $product = $this->product->where('uuid', $uuidproduct)->first();
            $product->fill($data);
            $product->plans[0]->valor = $data['valor'];
            $product->push();
            return true;
        } catch(\Illuminate\Database\QueryException $ex){ 
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            throw $th;
        }
        
        return false;
    }

    private function extractDataProduct($data) {
        $product = [];
        
        if (isset($data['product_type'])) {
            $product['product_type'] = $data['product_type'];
        }
        if (isset($data['payment_type'])) {
            $product['payment_type'] = $data['payment_type'];
        }

        if (isset($data['warranty'])) {
            $product['warranty'] = $data['warranty'];
        }
        if (isset($data['name'])) {
            $product['name'] = $data['name'];
        }
        if (isset($data['desc'])) {
            $product['desc'] = $data['desc'];
        }
        if (isset($data['email_support'])) {
            $product['email_support'] = $data['email_support'];
        }
        
        if (isset($data['several_purchases'])) {
            $product['several_purchases'] = $data['several_purchases'];
        }
        if (isset($data['max_item_by_order'])) {
            $product['max_item_by_order'] = $data['max_item_by_order'];
        }

        if (isset($data['category_id'])) {
            $product['category_id'] = $data['category_id'];
        }

        if (isset($data['type_deliverie_id'])) {
            $product['type_deliverie_id'] = $data['type_deliverie_id'];
        }

        if (isset($data['url_member_area'])) {
            $product['url_member_area'] = $data['url_member_area'];
        }

        if (isset($data['type_member_area'])) {
            $product['type_member_area'] = $data['type_member_area'];
        }

        return $product;
    }

    private function extractDataPlan($data)
    {
        $plans = [];
 
        if (isset($data['price_unique']) && !empty($data['price_unique'])) {
            $plans['price'] =  $data['price_unique'];
        }

        if ( isset( $data['payment_type']) && !empty( $data['payment_type']) &&
                $data['payment_type'] == 'PLAN') {
            
            if (isset( $data['name_plan']) && !empty( $data['name_plan'])) {
                foreach ($data['name_plan'] as $key => $value) {
                    $plans[$key]['name'] = $value;
                }
    
            }

            if (isset( $data['desc_plan']) && !empty( $data['desc_plan'])) {
                foreach ($data['desc_plan'] as $key => $value) {
                    $plans[$key]['desc'] = $value;
                }
    
            }
            if (isset( $data['free_plan']) && !empty( $data['free_plan'])) {
                foreach ($data['free_plan'] as $key => $value) {
                    $plans[$key]['free_plan'] = $value;
                }
            }

            if (isset( $data['price']) && !empty( $data['price'])) {
                foreach ($data['price'] as $key => $value) {
                    $plans[$key]['price'] = $value;
                }
            }

            if (isset( $data['frequency']) && !empty( $data['frequency'])) {
                foreach ($data['frequency'] as $key => $value) {
                    $plans[$key]['frequency'] = $value;
                }
            }

            if (isset( $data['frequency']) && !empty( $data['frequency'])) {
                foreach ($data['frequency'] as $key => $value) {
                    $plans[$key]['frequency'] = $value;
                }
            }
    
            if (isset($data['amount_recurrence']) && !empty( $data['amount_recurrence'])) {
                foreach ($data['amount_recurrence'] as $key => $value) {
                    $plans[$key]['amount_recurrence'] = $value;
                }
            }
    
            if (isset($data['first_installment']) && !empty( $data['first_installment'])) {
                foreach ($data['first_installment'] as $key => $value) {
                    $plans[$key]['first_installment'] = $value;
                }
            }
    
            if (isset($data['free_days']) && !empty( $data['free_days'])) {
                foreach ($data['free_days'] as $key => $value) {
                    $plans[$key]['free_days'] = $value;
                }
            }
    
            if (isset($data['different_value']) && !empty( $data['different_value'])) {
                foreach ($data['different_value'] as $key => $value) {
                    $plans[$key]['different_value'] = $value;
                }
            }
        
        }

        return $plans;
    }

    private function extractConfigffiliation($data) {
        $affiliation = [];
        $afiliacao['accept_affiliation'] = $data['accept_affiliation'];
       
        if ($data['accept_affiliation']) {
            
            if (isset($data['format_commission'])) {
                $afiliacao['format_commission'] = $data['format_commission'];
            }
            if (isset($data['assignment_type'])) {
                $afiliacao['assignment_type'] = $data['assignment_type'];
            }
            if (isset($data['value_commission'])) {
                $afiliacao['value_commission'] = $data['value_commission'];
            }
            if (isset($data['expiration_coockie'])) {
                $afiliacao['expiration_coockie'] = $data['expiration_coockie'];
            }
            if (isset($data['desc_affiliate'])) {
                $afiliacao['desc_affiliate'] = $data['desc_affiliate'];
            }
            if (isset($data['producer_refund']) &&  $data['producer_refund']) {
                $afiliacao['producer_refund'] = 1;
            }
            if (isset($data['hide_data_customer']) &&  $data['hide_data_customer']) {
                $afiliacao['hide_data_customer'] = 1;
            }
            if (isset($data['afil_link_chk']) &&  $data['afil_link_chk']) {
                $afiliacao['afil_link_chk'] = 1;
            }
            if (isset($data['coupon']) &&  $data['coupon']) {
                $afiliacao['coupon'] = 1;
            }
            if (isset($data['product_hide']) &&  $data['product_hide']) {
                $afiliacao['product_hide'] = 1;
            }
        } else {
            if (isset($data['accept_affiliation'])) {
                $afiliacao['accept_affiliation'] = 0;
            }            
        }

        return $afiliacao;
    }

    private function extractDataSalePage($data)
    {
        $salePage = [];

        $salePage['title'] = 'Pagina de Vendas';

        if (isset($data['local_page_sale'])) {
            $salePage['local_page_sale'] = $data['local_page_sale'];
        }
        if (isset($data['url_page_sale'])) {
            $salePage['url_page_sale'] = $data['url_page_sale'];
        }
        if (isset($data['url_page_thank'])) {
            $salePage['url_page_thank'] = $data['url_page_thank'];
        }
        if (isset($data['url_page_print_ticket'])) {
            $salePage['url_page_print_ticket'] = $data['url_page_print_ticket'];
        }

        return $salePage;
        
    }
}