<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Tenant;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Models\Plataforma\Client;

class UserService
{
    public static function create($data)
    {
        try {
            DB::beginTransaction();

            $tenant = Tenant::create([
                'email' => $data['email']
            ]);
    
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'accept_terms' => 0,
                'password' => Hash::make($data['password']),
                'tenant_id' => $tenant->id
            ]);
            
            Client::create(['user_id'=> $user->id]);

            DB::commit();
            return $user;
        } catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
}