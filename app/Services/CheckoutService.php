<?php
namespace App\Services;

use App\Models\Plataforma\Checkout;
use App\Models\Plataforma\Plan;


class CheckoutService
{
    public function create($data)
    {
        try {
            if (!isset($data['plan_id'])) {
                return false;
            }

            $plan = Plan::find($data['plan_id']);

            if (!$plan) {
                return false;
            }

            $checkout = $plan->checkouts()->create($data);
            if (!$checkout) {
                return false;
            }

            return $checkout;
        } catch(\Illuminate\Database\QueryException $ex){ 
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        } catch (\Throwable $th) {
            throw $th;
        }

        return false;
    }
    
    public function update($data, $codCheckout)
    {
        try {

            $checkout = Checkout::where('code', $codCheckout)->first();

            if (!$checkout) {
                return false;
            }

            $checkout->fill($data);
            $checkout->save();

            return $checkout;
        } catch(\Illuminate\Database\QueryException $ex){ 
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        } catch (\Throwable $th) {
            throw $th;
        }

        return false;
    }

    public function delete($codCheckout)
    {
        try {
            $checkout = Checkout::where('code', $codCheckout)->first();

            if (!$checkout) {
                return false;
            } 

            $deleted = $checkout->delete();
    
            if (!$deleted) {
                return false;
            }
            return $deleted;
        }catch(\Illuminate\Database\QueryException $ex){ 
            throw $ex;
        }
        catch (\Throwable $th) {
            throw $th;
        }
        
        return false;
    }
}

