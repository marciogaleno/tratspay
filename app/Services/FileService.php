<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Storage;
use MongoDB\Exception\RuntimeException;
use Illuminate\Support\Facades\Response;
use App\Models\Plataforma\Product;
use Illuminate\Support\Facades\Auth;

class FileService
{
    public static function uploadFileProduct($product_id, $uuid)
    {

        try {
            if (
                !isset($_FILES['file']['error']) ||
                is_array($_FILES['file']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            switch ($_FILES['file']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            $originalFileName = $_FILES['file']['name'];
            $fileName = uniqid() . '_' . $_FILES['file']['name'];
            $filePath = sprintf('%s/products/%s/%s', Auth::user()->id, $product_id, $fileName);
            $size = $_FILES['file']['size'];

            $upload = Storage::disk('products')->put($filePath,  file_get_contents($_FILES['file']['tmp_name']));
            
            if (!$upload) {
                throw new RuntimeException('Failed to move uploaded file.');
            }

            $file['file_path'] = $filePath;
            $file['name_original'] = $originalFileName;
            $file['name'] = $fileName;
            $file['product_uuid'] = $uuid;
            $file['product_id'] =  $product_id;
            $file['size'] =  $size;

            if ($upload) {
                return $file;
            }
            // All good, send the response
            
        } catch (RuntimeException $e) {
            // Something went wrong, send the err message as JSON
            http_response_code(400);

            return false;
        }
    }

    /*public static function upload($bucket, $file, $pathfile)
    {
        try {
            
            if ($file && Storage::disk($bucket)->exists($file)) {
                Storage::disk('images')->delete($file);
            }
    
            $pathFile = Auth::user()->id . '/banners';
    
            if (isset($request['banner_1'])) {
                $file = Storage::disk('images')->put($pathFile, $request['banner_1']);
                $data['banner_1'] = $file;
            }
    
            if (isset($request['banner_2'])) {
                $file = Storage::disk('images')->put($pathFile, $request['banner_2']);
                $data['banner_2'] = $file;
            }
            
            
            
        } catch (RuntimeException $e) {
            // Something went wrong, send the err message as JSON
            http_response_code(400);

            return false;
        }
    }*/

    public static function delete($pathfile)
    {
        $deleted = Storage::disk('products')->delete($pathfile);

        if (!$deleted)
            return false;

        return true;
    }

    public static function download($pathFile, $fileName)
    {
        try {

            $mime = Storage::disk('products')->getDriver()->getMimetype($pathFile);
            $size = Storage::disk('products')->getDriver()->getSize($pathFile);

            $response =  [
                'Content-Type' => $mime,
                'Content-Length' => $size,
                'Content-Description' => 'File Transfer',
                'Content-Disposition' => "attachment; filename={$fileName}",
                'Content-Transfer-Encoding' => 'binary',
            ];

            return Response::make(Storage::disk('products')->get($pathFile), 200, $response);
        } catch (Exception $e) {
            report($e);
            return false;
        }
    }
}
