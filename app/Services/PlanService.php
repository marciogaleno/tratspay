<?php
namespace App\Services;

use App\Models\Plataforma\Plan;
use Illuminate\Support\Facades\DB;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Checkout;


class PlanService
{
    public $plan;

    function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }

    public function create($data, $product_id)
    {
        try {
            DB::beginTransaction();

            $product = Product::where('id', $product_id)->first();
        
            if ($product) {
                $this->plan->fill($data);
                $this->plan->product_id = $product->id;
                $this->plan->save();
                
                $checkout = new Checkout();
                $checkout->default = 1;
                $checkout->desc = $data['name'];
                $checkout->pay_card = 1;
                $checkout->pay_ticket = 1;
                $checkout->plan_id = $this->plan->id;
                $checkout->save();
            }

            DB::commit();
            return true;
        }catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
        
        return false;

    }

    public function update($data, $codePlan)
    {
        try {
            DB::beginTransaction();

            $plan = Plan::where('code', $codePlan)->first();
            
            if (isset($data['free']) && !empty($data['free'])) {
                $data['price'] = 0.0;
                $data['frequency'] = NULL;
                $data['amount_recurrence'] = NULL;
                $data['first_installment'] = NULL;
                $data['free_days'] = NULL;
                $data['different_value'] = 0.0;
            }

            if ($plan) {
                $plan->update($data); 
            }

            DB::commit();
            return $plan;
        }catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
        
        return false;

    }

    public function delete($codePlan, $product_id) 
    {
        try {
            DB::beginTransaction();
            $plan = Plan::where('code', $codePlan)->first();

            if ($plan && $plan->product->id == $product_id) {
                $plan->delete();
            }else {
                DB::rollback();
                return false;
            }

            DB::commit();
            return $plan;
        }catch(\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
        
        return false;
    }
}