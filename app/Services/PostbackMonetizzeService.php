<?php

namespace App\Services;

use App\Models\Plataforma\TransactionMp;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Client;
use App\Models\Plataforma\Postback;
use \GuzzleHttp\Client as ClientHttp;

class PostbackMonetizzeService
{
    private $transaction;
    private $clienHttp;

    function __construct(TransactionMp $transaction)
    {
        $this->transaction = $transaction;
        $this->clienHttp = new ClientHttp();
    }

    public function execute()
    {
        $commissions = DB::select("
                select 
                a.*,
                d.token token_user, d.name name_affiliate, d.email email_affiliate
                from commissions a 
                    inner join transaction_mp b on b.id = a.transaction_id
                    inner join users d on d.id = a.user_id
                where
                    a.transaction_id = {$this->transaction->id}
                    and a.commission_type <> 'system'
                ");

        $statusTransaction = $this->transaction->status;

        $datas = [];
        
        foreach ($commissions as $commission) {

            $postbacks = DB::select("select * from postbacks where user_id = {$commission->user_id} and (product_id = {$commission->product_id } or product_id is null)");

            foreach ($postbacks as $postback) {
                //dd(isset($commission->{$statusTransaction}));
                if(isset($postback->{$statusTransaction}) && $postback->{$statusTransaction})  {
                    $data = [];

                    $data['chave_unica'] = $commission->token_user;
                    
                    $product = DB::table('products')->select('id as codigo', 'uuid as chave', 'name as nome')->first();

                    // Produto
                    $data['produto']['codigo'] = $product->codigo;
                    $data['produto']['chave'] = $product->chave;
                    $data['produto']['nome'] = $product->nome;

                    // Tipo Postback
                    $data['tipoPostback'] = $this->getTypeCommission($commission);

                    // Venda
                    $data['venda'] = $this->extractTransactionData($commission);
                    
                    $data['url_recuperacao'] = '';

                    // Plano
                    $data['plano']['codigo'] = $this->transaction->plan_id;
                    $data['plano']['referencia'] = $this->transaction->plan_code;
                    $data['plano']['nome'] = $this->transaction->plan_name;
                    $data['plano']['quantidade'] = $this->transaction->plan_quantity;

                    if ($this->transaction->plan_frequency) {
                        $data['plano']['periodicidade'] = \App\Helpers\Helper::frequency($this->transaction->plan_frequency);
                    } else {
                        $data['plano']['periodicidade'] = null;
                    }
                    
                    
                    // Assinatura
                    $data['assinatura']['codigo'] = null;
                    $data['assinatura']['status'] = null;
                    $data['assinatura']['data_assinatura'] = null;
                    $data['assinatura']['parcela'] = null;

                    // Comissões
                    $data['comissoes'] = $this->extractAllCommissionsData($commissions, $commission);

                    // Comprador
                    $data['comprador'] = $this->extractBuyerData($commission);

                    // Produtor
                    $data['produtor'] = $this->extractProducerData($commission);
                    
                    $data['json'] = json_encode($data);

                    $this->sendPostback($postback->url, $data);                  
                }
            }
         
        }

       

    }

    private function getTypeCommission($commission)
    {
        $codigo = null;
        $descricao = null;
        
        switch($commission->commission_type) {
            case 'producer':
                $codigo = 2;
                $descricao = 'Produtor';
                break;
            case 'coproducer':
                $codigo = 3;
                $descricao = 'Co-Produtor';
                break;
            case 'affiliate':
                $codigo = 4;
                $descricao = 'Afiliado';
                break;
            case 'affiliates_premium': // Ainda não temos
                $codigo = 5;
                $descricao = 'Afiliado Premium';
                break;
            case 'manager':
                $codigo = 6;
                $descricao = 'Gerente de Afiliado';
                break;
            case 'coaffiliate':
                $codigo = 7;
                $descricao = 'Co-Afiliado';
                break;
            default:
                $codigo = 1;
                $descricao = 'Sistema';            
        }

        return [
                'codigo' => $codigo,
                'descricao' => $descricao
        ];
    }

    private function extractTransactionData($commission)
    {
        $end_warranty = new Carbon($this->transaction->end_warranty);
        $now = Carbon::now();
        
        $source = $commission->source;
        $source = json_decode($source);

        $data = [
            'codigo'            =>  $this->transaction->id,
            'plano'             =>  $this->transaction->plan_code,
            'cupom'             =>  null,
            'dataInicio'        =>  $this->transaction->date_created,
            'dataFinalizada'    =>  $this->transaction->date_approved,
            'meioPagamento'     =>  $this->transaction->gateway,
            'formaPagamento'    =>  $this->transaction->payment_type_id == 'credit_card' ? 'Cartão de crédito': 'Boleto',
            'garantiaRestante'  =>  $end_warranty->diff($now)->days,
            'status'            =>  \App\Helpers\Helper::getStatusPayment($this->transaction->status),
            'valor'             =>  $this->transaction->transaction_amount,
            'quantidade'        =>  $this->transaction->quantity ?? 1,
            'valorRecebido'     =>  ($this->transaction->transaction_amount - $this->transaction->rate_total),
            'tipo_frete'        =>  999999,
            'descr_tipo_frete'  =>  'Valor Fixo',
            'frete'             =>  0.0,
            'onebuyclick'       =>  '',
            'venda_upsell'      =>  '',
            'src'               =>  $source->src ?? "",
            'utm_source'        =>  $source->utm_source ?? "",
            'utm_medium'        =>  $source->utm_medium ?? "",
            'utm_content'       =>  $source->utm_content ?? "",
            'utm_campaign'      =>  $source->utm_campaing ?? "",
            'linha_digitavel'   =>  ''
        ];

        if ($commission->commission_type == 'producer' || 
            $commission->commission_type == 'coproducer' ||
            $commission->commission_type == 'manager' ) 
        {
                $data['linkBoleto' ] =  route('checkout.payment.ticket', $this->transaction->id);
        }
        
        return $data;

    }

    function extractAllCommissionsData($commissions, $commission)
    {

        if ($commission->commission_type == 'affiliate' || $commission->commission_type == 'coaffiliate') {
            return;
        }
            

        $dataComissions = [];

        foreach ($commissions as $commission) {
            $data['refAfiliado']      =    $commission->reference_code;
            $data['nome']             =    $commission->name_affiliate;
            $data['tipo_comissao']    =    \App\Helpers\Helper::assignmentType($commission->assignment_type);
            $data['valor']            =    $commission->value;
            $data['comissao']         =    $commission->percent;
            $data['email']            =    $commission->email_affiliate;
            $dataComissions[] = $data;
        }

        return $dataComissions;
    }

    private function extractBuyerData($commission)
    {
        if ($commission->commission_type == 'affiliate' || $commission->commission_type == 'coaffiliate') {
            return;
        }

        $data = [];

        $data['nome'] = $this->transaction->first_name . ' ' . $this->transaction->last_name;
        $data['email'] = $this->transaction->email;
        //$data['data_nascimento'] = $this->transaction->email;
        $data['cnpj_cpf'] = $this->transaction->cnpj_cpf;
        $data['telefone'] = $this->transaction->phone;
        $data['cep'] = $this->transaction->zipcode;
        $data['endereco'] = $this->transaction->address;
        $data['numero'] = $this->transaction->address_number;
        $data['complemento'] = $this->transaction->address_complement;
        $data['bairro'] = $this->transaction->address_district;
        $data['cidade'] = $this->transaction->address_city;
        $data['estado'] = $this->transaction->address_state;
        $data['pais'] = $this->transaction->address_country;

        return $data;
    }

    public function extractProducerData($commission)
    {
        if ($commission->commission_type == 'affiliate' || $commission->commission_type == 'coaffiliate') {
            return;
        }

        $data = [];

        $product = Product::where('id', $this->transaction->product_id)
                        ->with('producer')
                        ->first();

        $data['nome'] =  $product->producer[0]->client->fantasia ?? $product->producer[0]->name;
        $data['email'] = $product->producer[0]->email ?? null;
        $data['cpf_cnpj'] = $product->producer[0]->client->cpf_cnpj ?? null;
    
        return $data;
    }

    function sendPostback($url, $data)
    {
        try {
            $res = $this->clienHttp->post($url, ['form_params' =>  $data]);
            return $res->getStatusCode();
        } catch (\GuzzleHttp\Exception\ConnectException $th) {
            return $th->getResponse();
        } catch (\GuzzleHttp\Exception\RequestException $th) {
            return $th->getResponse();
        }
    }
}