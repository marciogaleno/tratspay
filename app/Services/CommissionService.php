<?php

namespace App\Services;

use App\Helpers\Helper;
use App\Models\Plataforma\Notification;
use App\Models\Plataforma\TransactionMp;
use App\Models\Plataforma\ConfigAffiliation;
use App\Models\Plataforma\Tracking;
use App\User;
use App\Models\Plataforma\CoProducer;
use Illuminate\Support\Facades\DB;
use App\Models\Plataforma\Commission;
use App\Models\Plataforma\ProducerAffiliate;
use Illuminate\Support\Str;
use App\Models\Plataforma\Product;

class CommissionService
{
    private $transaction;
    private $totalToSplit = 0.0; // Total dividir
    private $totalAffiliates = 0.0; // Total dividir com Affiliates
    private $remainder = 0.0;
    private $affiliateId = null;
    private $affiliates = [];
    private $commissions = [
        'manager' => [],
        'coproducer' => [],
        'producer' => [],
        'system'   => [],
        'indication' => []
    ];

    private $commissionsAffiliates = [];

    function __construct(TransactionMp $transaction)
    {
        $this->transaction = $transaction;
        $this->totalToSplit = floatval($transaction->transaction_amount);
        $this->remainder = $this->totalToSplit;

    }

    public function splitCommissions($affiliateId = null)
    {
        $this->affiliateId = $affiliateId;

        $this->addCommissionSystem();
        $this->splitCommissionAffiliates();
        $this->splitCommissionManagers();
        $this->splitCommissionCoProducers();
        $this->addComissionProducer();
        $this->splitCommissionIndications();
        return $this->save();
    }

    private function addCommissionSystem()
    {
        $commission_system = $this->transaction->rate_total;
        $this->totalToSplit = floatval($this->totalToSplit) - floatval($commission_system);

        $this->addCommission(null,  $commission_system, 'system');
    }

    public function splitCommissionAffiliates()
    {
        if ($this->affiliateId) {
            $this->affiliates = $this->getAffiliateById($this->affiliateId);
        } else {
            $this->affiliates = $this->getAllAffiliatesByTracking($this->transaction->traking_code);
        }


        if ($this->affiliates->count() <= 0) {
            return;
        }



        $configAffiliate = ConfigAffiliation::where('product_id', $this->transaction->product_id)->withoutGlobalScopes()->first();
        $valueCommission = floatval($configAffiliate->getOriginal('value_commission'));
        // Se a comissão for por porcentagem
        if ($configAffiliate->format_commission == 'P') {
            $this->totalAffiliates = ($this->totalToSplit * $valueCommission) / 100;
        }

        // Se a comissão for por valor fixo
        if ($configAffiliate->format_commission == 'V') {
            $this->totalAffiliates = $valueCommission;
        }

        $this->totalAffiliates =  floatval(number_format($this->totalAffiliates, 2));
        $this->remainder =  $this->totalToSplit - $this->totalAffiliates;

        $commissionAffiliateService = new CommissionAffiliateService(
                                            $this->totalAffiliates,
                                            $this->transaction,
                                            $this->affiliates);

        // Realizado a comissão pra cada prdutor conforme o tipo de atribuição (primeiro click, último click ou primeito e último)
        $this->commissionsAffiliates = $commissionAffiliateService->split($configAffiliate->assignment_type);


    }


    private function splitCommissionManagers()
    {

        $managers = CoProducer::where('product_id', $this->transaction->product_id)
                        ->where('status', 'A')
                        ->where('type', 'manager')
                        ->where('deadline', '>=', date('Y-m-d'))
                        ->whereNull('deleted_at')// Importnate pra não pegar os deletados
                        ->with('user')
                        ->withoutGlobalScopes()
                        ->orderBy('created_at', 'asc')
                        ->get();

        $valueCommissionManager = 0.0;

        foreach($managers as $manager) {
            if ( $this->remainder <= 0 ) {
                return;
            }

            $valueComission = floatval($manager->getOriginal('value_commission'));
            if ($manager->format_commission == 'percent') {
                $valueCommissionManager = ( $valueComission * $this->totalToSplit) / 100;
            }

            if ($manager->format_commission == 'valuefixed' && $manager->value_commission <=  $this->totalToSplit) {
                $valueCommissionManager = $valueComission;
            }

            $this->addCommission($manager->user_id,  $valueCommissionManager, 'manager');
        }
    }

    private function splitCommissionCoProducers()
    {

        $coproducers = CoProducer::where('product_id', $this->transaction->product_id)
                        ->where('status', 'A')
                        ->where('type', 'coproducer')
                        ->whereRaw('(deadline >= CURRENT_DATE OR deadline IS NULL)')
                        ->whereNull('deleted_at')// Importnate pra não pegar os deletados
                        ->with('user')
                        ->withoutGlobalScopes()
                        ->orderBy('created_at', 'asc')
                        ->get();

        $valueCommissionCoProducer = 0.0;

        foreach($coproducers as $coproducer) {
            if ( $this->remainder <= 0 ) {
                return;
            }

            $valueCommission = floatval($coproducer->getOriginal('value_commission'));

            if ($coproducer->format_commission == 'percent') {
                //$valueCommissionCoProducer = bcdiv(( $valueCommission * $this->remainder), 100, 2);
                $valueCommissionCoProducer = ($valueCommission * $this->remainder) / 100;
            }

            if ($coproducer->format_commission == 'valuefixed' && $coproducer->value_commission <= $this->remainder) {
                $valueCommissionCoProducer = $valueCommission;

            }

            $this->addCommission($coproducer->user_id,  $valueCommissionCoProducer, 'coproducer');
        }
    }

    public function addComissionProducer()
    {
        $product_id = $this->transaction->product_id;

        $producerTracking = $this->getProducerByTracking($this->transaction->traking_code);

        if ($producerTracking) {
            $this->addCommission($producerTracking->indicated_user,  $this->remainder, 'producer');
        } else {
            $user = User::whereHas('productsWithoutScopes', function($q) use($product_id) {
                $q->where('product_id', $product_id);
                $q->where('relation_type', 'producer');
            })->first();


            if (!$user) {
                return false;
            }

            $this->addCommission($user->id, $this->remainder, 'producer');
        }

        return true;
    }


    private function splitCommissionIndications()
    {
        // Ids de de quem indicou afiliados e produtores dessa transação. Se tiver algum, será serparado 1%
        // do valor destino a plataforma para esses usuários
        $IdsUsersWhoIndicated = [];
        $valueToSplitIndications = ($this->transaction->transaction_amount * 1) / 100;

        foreach ($this->affiliates as $affiliate) {
            $user = User::where('id', $affiliate->indicated_user)->first();
            if ($user->user_indication) {
                $IdsUsersWhoIndicated[] = $user->user_indication;
            }
        }

        $userIndicatedProducer = $this->getUserIndicatedProducer();
        if ($userIndicatedProducer) {
            $IdsUsersWhoIndicated[] = $userIndicatedProducer->id;
        }

        $quantUsersIndicate  = count($IdsUsersWhoIndicated);
        $valueForEachUsers = 0.0;
        if ( $quantUsersIndicate > 0) {
            $valueForEachUsers =  $valueToSplitIndications/$quantUsersIndicate;
        }

        foreach ($IdsUsersWhoIndicated as $idUser) {
            $valueCommissionSystem = $this->commissions['system'][0]['value'];
            $this->commissions['system'][0]['value'] =  $valueCommissionSystem - $valueForEachUsers;

            $this->addCommission($idUser, $valueForEachUsers, 'indication');
        }

    }

    private function addCommission($user_id, $value, $commission_type)
    {
        $this->remainder = $this->remainder - $value;

        $commission  = [
            'value'   => $value,
            'commission_type' =>  $commission_type,
            'product_id' => $this->transaction->product_id,
            'transaction_id' => $this->transaction->id,
            'value_transaction' => $this->transaction->transaction_amount,
            'user_id' => $user_id
        ];

        $this->commissions[$commission_type][] = $commission;
    }



    private function save()
    {
        try {
            DB::beginTransaction();
            $this->commissions = array_merge($this->commissions, $this->commissionsAffiliates);

            if ($this->affiliateId) {
                $this->transaction->commissions()->delete();
            }

            foreach($this->commissions as $commision) {
                foreach ($commision as $com) {
                    if ($com['commission_type'] != 'system') {
                        $value_commission = $com['value'];
                        //$com['percent'] = bcdiv(($value_commission * 100), $this->transaction->transaction_amount, 2);
                        $com['percent'] = ($value_commission * 100) / $this->transaction->transaction_amount;
                    } else {
                        $com['percent'] = $this->transaction->rate_percent;
                        $com['fixed_rate'] = $this->transaction->fixed_rate;
                    }

                    Commission::create($com);
                }
            }

            $commissionProducer = Commission::where('transaction_id', $this->transaction->id)
                ->where('commission_type', '=', 'producer')
                ->first();

            $dataNotification = [
                'content' => 'Sua comissão foi R$: '.Helper::moneyBR($commissionProducer->value),
                'user_id' => $this->transaction->productAffiliate->user_id,
                'product_id'=> $this->transaction->product_id,
                'type' => 'sale',
                'first_name' => $this->transaction->first_name,
                'last_name' => $this->transaction->last_name,
                'email' => $this->transaction->email,
                'cpf_cnpj' => $this->transaction->cpf_cnpj,
                'phone' => $this->transaction->phone
            ];

            Notification::create($dataNotification);
            DB::commit();
            return true;

        } catch(\Illuminate\Database\QueryException $ex){
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

        return false;
    }

    function getAllAffiliatesByTracking($traking_code)
    {
        // Código que verifica se o comprador é um dos afiliados. Se for, não atribuir comissão.
        $buyer_email = $this->transaction->email;
        $buyer_affilite = User::where('email', $buyer_email)->first();

        if ($buyer_affilite) {
            return Tracking::where('tracking_code', $traking_code)
                        ->where('relation_type', 'affiliate')
                        ->where('indicated_user', '<>', $buyer_affilite->id)
                        ->where('product_id', $this->transaction->product_id)
                        ->orderBy('created_at', 'asc')->get();
        }

        return Tracking::where('tracking_code', $traking_code)
                ->where('relation_type', 'affiliate')
                ->where('product_id', $this->transaction->product_id)
                ->orderBy('created_at', 'asc')->get();

    }

    function getAffiliateById($affiliateId)
    {
        $source = [
            'type_url' => '',
            'code_url' => '',
            'src' => '',
            'utm_source' =>'',
            'utm_campaing' => '',
            'utm_medium' => '',
            'utm_content' => '',
            'manually_commission'  => true

        ];

        $source = json_encode($source);

        $affiliate = ProducerAffiliate::where('user_id', $affiliateId)
                    ->where('product_id', $this->transaction->product_id)
                    ->where('relation_type', 'affiliate')
                    ->first();

        $dataToSave = [
            'tracking_code' => (string) Str::uuid(),
            'product_id' =>  $this->transaction->product_id,
            'reference_code' => $affiliate->code,
            'indicated_user' => $affiliateId,
            'relation_type' => 'affiliate',
            'source' => $source
        ];

        return collect([(object) $dataToSave]);
    }

   /* function getAllAffiliatesByTracking($traking_code)
    {

        $affiliatesTracking = Tracking::raw(function ($collection) use ($traking_code) {
                return $collection->aggregate(
                    [
                        ['$unwind' => ['path' => '$track']],
                        ///['$group' => [
                        //   "_id" => ['track'],
                        //   'soma' => ['$sum' => 1]
                        //]]
                        ['$match' => ['trakingCode' => $traking_code]],
                        ['$match' => ['track.relationType' => 'affiliate']],
                        ['$sort' => ['track.date' => 1]] // crescente
                    ]
                );
            });

        return  $affiliatesTracking;
    }*/


    function getUserIndicatedProducer()
    {
        $product_id = $this->transaction->product_id;

        $user = null;
        $user = User::whereHas('productsWithoutScopes', function($q) use ($product_id) {
            $q->where('product_id', $product_id);
            $q->where('relation_type', 'producer');
        })
        ->whereNotNull('user_indication')
        ->where('indication_end', '<=', date('Y-m-d H:i:s'))
        ->first();

        return $user;
    }

    function getProducerByTracking($traking_code)
    {
        $product = Product::where('id', $this->transaction->product_id)->first();

        return Tracking::where('tracking_code', $traking_code)
                ->where('relation_type', 'producer')
                ->where('product_id', $product->id)
                ->where('indicated_user', $product->producer[0]->id)
                ->orderBy('created_at', 'asc')->first();
    }

    /*function getProducerByTracking($traking_code)
    {

        $producerTracking = Tracking::raw(function ($collection) use ($traking_code) {
                return $collection->aggregate(
                    [
                        ['$match' => ['trakingCode' => $traking_code]],
                        ['$unwind' => ['path' => '$track']],
                        ///['$group' => [
                        //   "_id" => ['track'],
                        //   'soma' => ['$sum' => 1]
                        //]]
                        ['$match' => ['track.relationType' => 'producer']],
                        ['$sort' => ['track.date' => 1]] // crescente
                    ]
                );
            });

        return  $producerTracking;
    }*/

}
