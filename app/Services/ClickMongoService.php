<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use App\Models\Plataforma\Click;
use Illuminate\Support\Facades\DB;

class ClickMongoService 
{
    public static function count($data, $params, $trackingCode, $typeUrl, $codeUrl)
    {

        $clickData = [];
   
   

      

       //$mongo = DB::connection('mongodb');

        //dd($mongo->collection('clicks'));

        /*$mongoConn = new \MongoDB\Driver\Manager("mongodb://localhost:27017");
        $query = new \MongoDB\Driver\Query([]);

        $client =  new \MongoDB\Client("mongodb://localhost:27017");
        
        $c = $client->tratspay->clicks;

        $re = $c->aggregate(array(
            array('$unwind' => '$dClicks'),
            array(
                '$group' => array(
                '_id' => array('utm_source' => '$dClicks.   ', 'src' => '$dClicks.src'),
                'total' => array('$sum' => 1)
                ),
            ),
            array(
                '$project' => array(
                    '$dClicks' => 1,
                    'total' => 1,
                )
            ),
        ));

        foreach ($re as $key => $value) {
            var_dump($value);
        }
        
        die();*/

            $clicks = [         
                'trackingCode' => $trackingCode,       
                'date' =>  Carbon::now()->toJSON(), 
                'clicks'     => 1,
                'typeUrl'     => $typeUrl,
                'codeUrl'     => $codeUrl,
                'src' => (isset($params['src']) && !empty($params['src'])) ? $params['src'] : null,
                'utm_source' => (isset($params['utm_source']) && !empty($params['utm_source'])) ? trim($params['utm_source']) : null, 
                'utm_campaing' => (isset($params['utm_campaing']) && !empty($params['utm_campaing'])) ? $params['utm_campaing'] : null, 
                'utm_medium' => (isset($params['utm_medium']) && !empty($params['utm_medium'])) ? $params['utm_medium'] : null, 
                'utm_content' => (isset($params['utm_content']) && !empty($params['utm_content'])) ? $params['utm_content'] : null, 
            ];

            $trackClicks = Click::whereRaw(['productUuid' => array('$eq' => $data->productUuid)])
                                ->where('promotionCode',  $data->promotionCode)
                                ->first();
                   
            if (!$trackClicks) {
                $clickData = [
                    'product_id' => $data->productId,
                    'userId' => $data->userId,
                    'promotionCode' =>  $data->promotionCode,
                    'productUuid' => $data->productUuid,
                    'productName' => $data->productName,
                    'dClicks' => [$clicks]
                ];
                
                return Click::create($clickData);
            }
            
            $trackClicks = Click::
                raw(function($collection) use ($data)
                {
                    return $collection->aggregate([
                        ['$unwind' => ['path' => '$dClicks']],
                        ///['$group' => [
                        //   "_id" => ['track'],
                        //   'soma' => ['$sum' => 1]
                        //]]
                        [ '$match' => ['productUuid' => $data->productUuid] ],
                        [ '$match' => ['promotionCode' => $data->promotionCode] ],
                        //[ '$match' => ['dClicks.date' =>   Carbon::now()->subHours(24)->format('Y-m-d')]],                        
                        //[ '$match' => ['dClicks.originParam' => $param] ],
                        //[ '$match' => ['dClicks.originType' => $valueParam] ],
                        //[ '$match' => ['dClicks.ip' => $ip] ],
                        ['$sort' => ['dClicks.date' => 1]]
                        ]
                    );

            })->first();
            
            if ($trackClicks) {
                return Click::whereRaw(['productUuid' => array('$eq' => $data->productUuid)])
                            ->where('promotionCode',  $data->promotionCode)
                            ->push('dClicks', $clicks);
            } else {
                //$totalClicks = $trackClicks->dClicks->clicks = $trackClicks->dClicks->clicks + 1;
                //Self::save($totalClicks, $data, $param, $valueParam, $ip);
                
            }


       return false;
    }

    private static function save($totalClicks, $data, $param, $valueParam, $ip)
    {
        $clicks = [ 
            'dClicks.$.clicks'     => $totalClicks,
            'dClicks.$.ip'     => '192.168.1.2'
        ];

        $value = Click::whereRaw(['productUuid' => array('$eq' => $data->productUuid)])
        ->where(
            'promotionCode', $data->promotionCode
        )
        ->where(
            'dClicks.date',   Carbon::now()->toJson()
        )
        ->where(
            'dClicks.originParam',  $param
        )
        ->where(
            'dClicks.originType',  $valueParam
        )->update($clicks);
    
    }

}