<?php

namespace App\Services;
use App\User;
use App\Mail\SendMailBuyer;
use Illuminate\Support\Facades\DB;
use App\Models\Plataforma\Purchase;
use Illuminate\Support\Facades\Mail;

class UnlockProductService
{
    private $transaction;

    function __construct($transaction)
    {
        $this->transaction = $transaction;
    }

    public function unLock() {
        $user = User::where('email', $this->transaction->email)->first();
  
        try {
            DB::beginTransaction();
            if (!$user) {
                $password = $this->generateRandomString(10);
                $data['name'] = $this->transaction->first_name . ' ' . $this->transaction->last_name;
                $data['email'] = $this->transaction->email;
                $data['password'] = $password;
    
                $newUser = UserService::create($data);
                $purchase = $this->createPurchase($newUser);

                if ($purchase) {
                    Mail::to($this->transaction->email)
                    ->send(new SendMailBuyer('Compra Aprovada!', true, true, $password, $this->transaction));  
                }

            } else {
                $this->createPurchase($user);
                Mail::to($this->transaction->email)
                ->send(new SendMailBuyer('Compra Aprovada!', false, true, null, $this->transaction));  
            }
            DB::commit();
        } catch (\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
        catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }

    }


    private function createPurchase($user) {
        try {
            $data['transaction_id'] =  $this->transaction->id;
            $data['product_id'] =  $this->transaction->product_id;
            $data['user_id'] =  $user->id;
            $data['date'] = $this->transaction->date_approved;
            $data['value'] = $this->transaction->plan_price;
    
            return Purchase::create($data);
        } catch (\Illuminate\Database\QueryException $ex){ 
            DB::rollback();
            throw $ex;
            // Note any method of class PDOException can be called on $ex.
        }
       
    }

    function generateRandomString($length = 10) {
        return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
    }
}