<?php
namespace App\Services;

use App\Models\Plataforma\CoAffiliate;
use App\Models\Plataforma\Tracking;
use App\Models\Plataforma\ConfigAffiliation;

class CommissionAffiliateService
{
    private $totalToSplit = 0.0;
    private $remainder = 0.0;
    private $transaction;
    private $assignment_type = null;
    private $commissions = [];
    private $affiliates = [];

    function __construct($totalToSplit, $transaction, $affiliates)
    {
        $this->totalToSplit = $totalToSplit;
        $this->remainder = $totalToSplit;
        $this->transaction = $transaction;
        $this->affiliates = $affiliates;
    }

    public function split($assignment_type)
    {
        $this->assignment_type = $assignment_type;

        if ($this->affiliates) {
            switch ($assignment_type) {
                case '1': // Ultimo click
                    $this->splitComissionByFisrtClick($this->affiliates);
                    break;
                case '2':
                    $this->splitComissionByLastClick($this->affiliates);
                    break;
                case '3':
                    $this->splitComissionByFirstAndLastClick($this->affiliates);
                    break;
                case '100':
                    $this->splitComissionByFirstAndLastClick($this->affiliates);
                    break;
                default:
                    $this->splitComissionByLastClick($this->affiliates);
                    break;
            }
        }
        
        return $this->commissions;
    }

    /**
     * Divi as comissões por porcentagem
     *
     * @return void
     */
    private function splitComissionByFisrtClick($affiliates)
    {
        $valueCommissionAffiliate = $this->totalToSplit;

        $coaffilites = CoAffiliate::where('code_affiliate', $affiliates[0]->reference_code)
            ->where('status', 'A')
            ->whereRaw('(deadline >= CURRENT_DATE OR deadline IS NULL)')
            ->whereNull('deleted_at')
            ->withoutGlobalScopes()
            ->orderBy('created_at', 'asc')
            ->get();

        if ($coaffilites->count() > 0) {
            $valueCommissionAffiliate = $this->splitCommissionCoAffiliates($coaffilites,  $this->totalToSplit);
        }
        
        $this->addCommission($affiliates[0], $valueCommissionAffiliate, 'affiliate');
    }

    private function splitComissionByLastClick($affiliates)
    {
        $lastClickAffiliate = end($affiliates);
        $lastClickAffiliate = end($lastClickAffiliate);

        $valueCommissionAffiliate = $this->totalToSplit;
       
        $coaffilites = CoAffiliate::where('code_affiliate', $lastClickAffiliate->reference_code)
            ->where('status', 'A')
            ->whereRaw('(deadline >= CURRENT_DATE OR deadline IS NULL)')
            ->whereNull('deleted_at')
            ->withoutGlobalScopes()
            ->orderBy('created_at', 'asc')
            ->get();

        if ($coaffilites->count() > 0) {
            $valueCommissionAffiliate = $this->splitCommissionCoAffiliates($coaffilites, $valueCommissionAffiliate);
        }
        
        $this->addCommission($lastClickAffiliate, $valueCommissionAffiliate, 'affiliate');
        
    }

    private function splitComissionByFirstAndLastClick($affiliates)
    {   
        $affilitesToCommission = [];

        $firstAffiliateClick = reset($affiliates);
        $firstAffiliateClick = reset($firstAffiliateClick);
        $affilitesToCommission[] = $firstAffiliateClick;

        $endAffiliateClick = end($affiliates);
        $endAffiliateClick = end($endAffiliateClick);
        $affilitesToCommission[] = $endAffiliateClick;

        foreach ($affilitesToCommission as $affiliate) {

            $valueCommissionAffiliate = 0.0;

            // Como são dois afiliados para receber a comissão, cada um recebe 50%.
            if (count($affilitesToCommission) == 2) {
                $valueCommissionAffiliate = $this->totalToSplit * 0.50;
                $valueCommissionAffiliate = floatval(number_format($valueCommissionAffiliate, 2));
            }  
            // Quando tiver apenas um afiliados ele recebe o valor total da comissão.
            if (count($affilitesToCommission) == 1) {
                $valueCommissionAffiliate = $this->totalToSplit;
            }  

            $coaffilites = CoAffiliate::where('code_affiliate', $affiliate->reference_code)
                ->where('status', 'A')
                ->whereRaw('(deadline >= CURRENT_DATE OR deadline IS NULL)')
                ->whereNull('deleted_at')
                ->withoutGlobalScopes()
                ->orderBy('created_at', 'asc')
                ->get();

            if ($coaffilites->count() > 0) {
                $valueCommissionAffiliate = $this->splitCommissionCoAffiliates($coaffilites, $valueCommissionAffiliate);
            }

            $this->addCommission($affiliate, $valueCommissionAffiliate, 'affiliate');
        }
    }

    private function splitCommissionCoAffiliates($coaffiliates, $valueToSplit)
    {
        $remainder = $valueToSplit;
        
        foreach ($coaffiliates as $coaffiliate) {

            // Se o total de commissões para dividir para os afiliados acabou para o loop
            if ($remainder <= 0) {
                return;
            }

            $commissionCoAffiliate = 0.0;
            $commisionValue = floatval($coaffiliate->getOriginal('value_commission'));

            if ($coaffiliate->format_commission == 'percent') {
                //$commissionCoAffiliate = bcdiv(( $commisionValue * $valueToSplit), 100, 2);
                $commissionCoAffiliate = ($commisionValue * $valueToSplit) / 100;
            }

            if ($coaffiliate->format_commission == 'valuefixed' && $commisionValue <= $valueToSplit) {
                $commissionCoAffiliate = $commisionValue;
            }
            
            $this->addCommission($coaffiliate, $commissionCoAffiliate, 'coaffiliate');

            $remainder = $remainder - $commissionCoAffiliate;

        }

        return  $remainder;
    }

    private function addCommission($affiliate, $value, $commission_type)
    {
        $this->remainder = $this->remainder - $value;
        
        $commission  = [
            'value'   => $value,
            'commission_type' =>  $commission_type,
            'assignment_type' =>  $this->assignment_type,
            'product_id' => $this->transaction->product_id,
            'transaction_id' => $this->transaction->id,
            'value_transaction' => $this->transaction->transaction_amount,
            'source' => $affiliate->source
        ];

        if ($commission_type == 'affiliate') {
            $commission['user_id'] = $affiliate->indicated_user;
            $commission['reference_code'] = $affiliate->reference_code;
        }

        if ($commission_type == 'coaffiliate') {
            $commission['user_id'] = $affiliate->user_id;
        }


        $this->commissions['affiliate'][] = $commission;
    }

    /*function getAllAffiliatesByTracking($traking_code)
    {

        $affiliatesTracking = Tracking::raw(function ($collection) use ($traking_code) {
                return $collection->aggregate(
                    [
                        ['$unwind' => ['path' => '$track']],
                        ///['$group' => [
                        //   "_id" => ['track'],
                        //   'soma' => ['$sum' => 1]
                        //]]
                        ['$match' => ['trakingCode' => $traking_code]],
                        ['$match' => ['track.relationType' => 'affiliate']],
                        ['$sort' => ['track.date' => 1]] // crescente
                    ]
                );
            });

        return  $affiliatesTracking;
    }*/


}
