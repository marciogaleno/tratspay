<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use App\Models\Plataforma\Click;
use function GuzzleHttp\json_encode;

class ClickService 
{
    public static function count($data, $params, $trackingCode, $typeUrl, $codeUrl)
    {
        $source = [
            'type_url' => $typeUrl,
            'code_url' => $codeUrl,
            'src' => (isset($params['src']) && !empty($params['src'])) ? $params['src'] : null,
            'utm_source' => (isset($params['utm_source']) && !empty($params['utm_source'])) ? trim($params['utm_source']) : null, 
            'utm_campaing' => (isset($params['utm_campaing']) && !empty($params['utm_campaing'])) ? $params['utm_campaing'] : null, 
            'utm_medium' => (isset($params['utm_medium']) && !empty($params['utm_medium'])) ? $params['utm_medium'] : null, 
            'utm_content' => (isset($params['utm_content']) && !empty($params['utm_content'])) ? $params['utm_content'] : null, 
            'cupom' => (isset($params['cupom']) && !empty($params['cupom'])) ? $params['cupom'] : null, 
        ];

        $source = json_encode($source);

        $dataToSave = [
            'product_id' => $data->productId,
            'user_id' => $data->userId,
            'reference_code' => $data->referenceCode,
            'tracking_code' => $trackingCode,
            'source' =>  $source
        ];

        Click::create($dataToSave);
    }

}