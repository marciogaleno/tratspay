<?php

namespace App\Services;

use App\Models\Plataforma\Tracking;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\json_encode;

class TrackingService 
{

    function track($data = null, $params = null, $trakingCode = null, $ip = null, $typeUrl = null, $codeUrl = null) 
    {   
        $tracking = Tracking::where('tracking_code', $trakingCode)
                ->where('product_id', $data->productId)
                ->where('reference_code', $data->referenceCode)
                ->where('indicated_user', $data->userId)
                ->where('relation_type', $data->relationType)
                ->first();

        if ($tracking) {

            $source = json_decode($tracking->source, true);

            if ( (isset($params['src']) && !empty($params['src']))) {
                $source['src'] = $params['src'];
            }
            if ( (isset($params['utm_source']) && !empty($params['utm_source']))) {
                $source['utm_source'] = $params['utm_source'];
            }
            if ( (isset($params['utm_campaing']) && !empty($params['utm_campaing']))) {
                $source['utm_campaing'] = $params['utm_campaing'];
            }
            if ( (isset($params['utm_medium']) && !empty($params['utm_medium']))) {
                $source['utm_medium'] = $params['utm_medium'];
            }
            if ( (isset($params['utm_content']) && !empty($params['utm_content']))) {
                $source['utm_content'] = $params['utm_content'];
            }

            $data = [];
            $data['source'] = json_encode($source);
  
            $tracking->update($data);

        } else {
            $source = [
                'type_url' => $typeUrl,
                'code_url' => $codeUrl,
                'src' => (isset($params['src']) && !empty($params['src'])) ? $params['src'] : '',
                'utm_source' => (isset($params['utm_source']) && !empty($params['utm_source'])) ? $params['utm_source'] : '', 
                'utm_campaing' => (isset($params['utm_campaing']) && !empty($params['utm_campaing'])) ? $params['utm_campaing'] : '', 
                'utm_medium' => (isset($params['utm_medium']) && !empty($params['utm_medium'])) ? $params['utm_medium'] : '', 
                'utm_content' => (isset($params['utm_content']) && !empty($params['utm_content'])) ? $params['utm_content'] : '', 
                
            ];
    
            $source = json_encode($source);
    
            $dataToSave = [
                'tracking_code' => $trakingCode,
                'product_id' =>  $data->productId, 
                'reference_code' => $data->referenceCode,
                'indicated_user' => $data->userId, 
                'relation_type' => $data->relationType,
                'source' => $source
            ];

            Tracking::create($dataToSave);
        }
        

         
    }

}
