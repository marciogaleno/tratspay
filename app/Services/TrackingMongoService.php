<?php

namespace App\Services;

use App\Models\Plataforma\Tracking;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class TrackinMongogService 
{

    function track($data = null, $params = null, $trakingCode = null, $ip = null, $typeUrl = null, $codeUrl = null) 
    {   
        $ok = true;

        /*$results = Tracking::
                raw(function($collection) use ($visitorId)
                {
                    return $collection->aggregate([
                        ['$unwind' => ['path' => '$track']],
                        ///['$group' => [
                         //   "_id" => ['track'],
                         //   'soma' => ['$sum' => 1]
                        //]]
                        [ '$match' => ['visitorId' => $visitorId] ],
                        ['$sort' => ['track.data' => 1]]
                        ]
                    );
            });
                    
        dd($results);*/
                
        $tracking = Tracking::whereRaw(['trakingCode' => array('$eq' => $trakingCode)])->first();
        
        if (!$tracking) {

            $dataToSave = [
                'trakingCode' => $trakingCode,
                'date' => Carbon::now()->toJSON(),
                'track' => [
                    [ 
                        'productId' =>  $data->productId, 
                        'productUUid' =>  $data->productUuid, 
                        'productName' =>  $data->productName, 
                        'promotionCode' => $data->promotionCode, 
                        'relationType' => $data->relationType, 
                        'typeUrl' => $typeUrl,
                        'codeUrl' => $codeUrl,
                        'userId' => $data->userId, 
                        'userName' => $data->userName, 
                        'userImage' => $data->userImage, 
                        'date' => Carbon::now()->toJSON(), 
                        'src' => (isset($params['src']) && !empty($params['src'])) ? $params['src'] : '',
                        'utm_source' => (isset($params['utm_source']) && !empty($params['utm_source'])) ? $params['utm_source'] : '', 
                        'utm_campaing' => (isset($params['utm_campaing']) && !empty($params['utm_campaing'])) ? $params['utm_campaing'] : '', 
                        'utm_medium' => (isset($params['utm_medium']) && !empty($params['utm_medium'])) ? $params['utm_medium'] : '', 
                        'utm_content' => (isset($params['utm_content']) && !empty($params['utm_content'])) ? $params['utm_content'] : '', 
                    ]
                ]
            ];

            $ok = Tracking::create($dataToSave);

        } 

        if ($tracking) {

            $track = Tracking::whereRaw(['trakingCode' => array('$eq' => $trakingCode)])
                            ->where(
                                'track.productUUid', $data->productUuid
                            )
                            ->where(
                                'track.promotionCode',  $data->promotionCode
                            )
                            ->project(array( 'track.$' => 1 ) )->get();
           
            if (empty($track->toArray())) {
                $dataTrack = [ 
                    'productId' =>  $data->productId, 
                    'productUUid' =>  $data->productUuid, 
                    'productName' =>  $data->productName, 
                    'promotionCode' => $data->promotionCode, 
                    'relationType' => $data->relationType, 
                    'typeUrl' => $typeUrl,
                    'codeUrl' => $codeUrl,
                    'userId' => $data->userId, 
                    'userName' => $data->userName, 
                    'userImage' => $data->userImage, 
                    'date' =>  Carbon::now()->toJSON(), 
                    'src' => (isset($params['src']) && !empty($params['src'])) ? $params['src'] : '',
                    'utm_source' => (isset($params['utm_source']) && !empty($params['utm_source'])) ? $params['utm_source'] : '', 
                    'utm_campaing' => (isset($params['utm_campaing']) && !empty($params['utm_campaing'])) ? $params['utm_campaing'] : '', 
                    'utm_medium' => (isset($params['utm_medium']) && !empty($params['utm_medium'])) ? $params['utm_medium'] : '', 
                    'utm_content' => (isset($params['utm_content']) && !empty($params['utm_content'])) ? $params['utm_content'] : '', 
                ];
    
                $ok = Tracking::whereRaw(['trakingCode' => array('$eq' => $trakingCode)])->push('track', [$dataTrack]);

            } else {
               $dataTrack = [
                    /*'track.$.productId' =>  $data->productId, 
                    'track.$.productUUid' =>  $data->productUuid, 
                    'track.$.productName' =>  $data->productName, 
                    'track.$.promotionCode' => $data->promotionCode, 
                    'track.$.relationType' => $data->relationType, 
                    'track.$.userId' => $data->userId, 
                    'track.$.userName' => $data->userName, 
                    'track.$.userImage' => $data->userImage, */
                ];

                if ( (isset($params['src']) && !empty($params['src']))) {
                    $dataTrack['track.$.src'] = $params['src'];
                }
                if ( (isset($params['utm_source']) && !empty($params['utm_source']))) {
                    $dataTrack['track.$.utm_source'] = $params['utm_source'];
                }
                if ( (isset($params['utm_campaing']) && !empty($params['utm_campaing']))) {
                    $dataTrack['track.$.utm_campaing'] = $params['utm_campaing'];
                }
                if ( (isset($params['utm_medium']) && !empty($params['utm_medium']))) {
                    $dataTrack['track.$.utm_medium'] = $params['utm_medium'];
                }
                if ( (isset($params['utm_content']) && !empty($params['utm_content']))) {
                    $dataTrack['track.$.utm_content'] = $params['utm_content'];
                }

                if (count($dataTrack) > 0) {
                    $dataTrack['track.$.date'] = Carbon::now()->toJSON();
                }

                
                //dd($dataTrack);
                $ok = Tracking::whereRaw(['trakingCode' => array('$eq' => $trakingCode)])
                        ->where(
                            'track.productUUid', $data->productUuid
                        )
                        ->where(
                            'track.promotionCode',  $data->promotionCode
                        )
                        ->update($dataTrack);               
            }
           

        }
        //dd($ok);
        return $ok;
    }

}
