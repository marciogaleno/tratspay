<?php

namespace App\Console\Commands;

use App\Tenant;
use App\User;
use App\Models\Plataforma\Client;
use App\Src\Model\Module;
use App\Src\Model\CategoryResource;
use App\Src\Model\TypeAccount;
use App\Src\Model\Resource;
use App\Src\Model\Roles;
use App\Src\Model\TypeAccountRole;
use App\Src\Model\TypeAccountUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Console\Command;

class BuildAdminSecurity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tratspay:admin:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build Panel Admin and create Super Admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \DB::beginTransaction();
            
            $this->info('Iniciando build do Admin');

            $this->info('Criando Tenant');
            $tenant = Tenant::create([
                'email' => 'admin@tratspay.com'
            ]);

            $this->info('Criando Usuario');
            /** Cria um Usuario*/
            $user = User::create([
                'name' => 'Administrador',
                'email' => 'admin@tratspay.com',
                'password' => Hash::make('t#4tSp4y'),
                'tenant_id' => $tenant->id,
                'email_verified_at' => date('Y-m-d h:i:s'),
                'accept_terms' => 1
            ]);

            Client::create(['user_id'=>$user->id]);

            $this->info('Criando Modulo');
            /** Cria um Modulo*/
            $moduleAdmin = Module::create([
                'name' => 'origin',
                'label' => 'Painel Administrativo do Sistema',
                'icon' => 'fa fa-cog',
                'enable' => true
            ]);

            $this->info('Criando Categoria para Recursos');
            /** Cria uma Categora do Recurso*/
            $categoryConfiguracoes = CategoryResource::create([
                'name' => 'Configurações',
                'icon' => 'fa fa-cog',
                'label' => 'Area para configurar permissoes de acesso',
                'order' => 10,
                'enable' => true
            ]);
            $categoryComeceAqui = CategoryResource::create([
                'name' => 'Comece Aqui',
                'icon' => 'icon-home',
                'label' => 'Area inicial do painel',
                'order' => 20,
                'enable' => true
            ]);
            $categoryUsuarios = CategoryResource::create([
                'name' => 'Usuários',
                'icon' => 'fa fa-user',
                'label' => 'Area para gerenciar usuarios',
                'order' => 30,
                'enable' => true
            ]);
            $categoryProdutos = CategoryResource::create([
                'name' => 'Produtos',
                'icon' => 'fa fa-cubes',
                'label' => 'Area para gerenciar produtos',
                'order' => 40,
                'enable' => true
            ]);
            $categoryRelatorios = CategoryResource::create([
                'name' => 'Relatórios',
                'icon' => 'icon-pie-chart',
                'label' => 'Area para gerenciar relatórios',
                'order' => 50,
                'enable' => true
            ]);

            $this->info('Criando Perfil');
            /** Cria um Perfil*/
            $typeAccountAdmin = TypeAccount::create([
                'module_id' => $moduleAdmin->id,
                'name' => 'Super Admin',
                'label' => 'Perfil Administrador do painel'
            ]);

            $this->info('Criando Recursos');
            /** Cria Recursos do Sistema - Administrador*/
            $recursoHome = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryComeceAqui->id,
                'name' => 'index',
                'label' => 'Home',
                'icon' => 'fa fa-home',
                'order' => 10,
                'enable' => true
            ]);
            $resourceModulo = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'module',
                'label' => 'Modulos',
                'icon' => 'fa fa-cog',
                'order' => 10,
                'enable' => true
            ]);
            $resourceCategoria = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'category',
                'label' => 'Categoria dos Recursos',
                'icon' => 'fa fa-cog',
                'order' => 20,
                'enable' => true
            ]);
            $resourceRecurso = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'resource',
                'label' => 'Recursos',
                'icon' => 'fa fa-cog',
                'order' => 30,
                'enable' => true
            ]);
            $resourcePermissao = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'roles',
                'label' => 'Permissões',
                'icon' => 'fa fa-cog',
                'order' => 40,
                'enable' => true
            ]);
            $resourcePerfil = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'type-account',
                'label' => 'Perfis',
                'icon' => 'fa fa-cog',
                'order' => 50,
                'enable' => true
            ]);
            $resourcePerfilPermissao = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'type-account-roles',
                'label' => 'Perfil Permissões',
                'icon' => 'fa fa-cog',
                'order' => 60,
                'enable' => true
            ]);
            $resourcePerfilUsuario = Resource::create([
                'module_id' => $moduleAdmin->id,
                'category_resource_id' => $categoryConfiguracoes->id,
                'name' => 'type-account-user',
                'label' => 'Perfis do Usuário',
                'icon' => 'fa fa-cog',
                'order' => 70,
                'enable' => true
            ]);

            $this->info('Criando Permissões do Recurso');
            /**Cria Permissões do Recurso - Administrador*/
            //Home Painel
            $permissaoHomeIndex = Roles::create([
                'resource_id' => $recursoHome->id,
                'name' => 'index',
                'label' => 'Dashboard da Home'
            ]);

            //Modulo
            $permissaoModuloIndex = Roles::create([
                'resource_id' => $resourceModulo->id,
                'name' => 'index',
                'label' => 'Listagem do Recurso Modulo'
            ]);
            $permissaoModuloCreate = Roles::create([
                'resource_id' => $resourceModulo->id,
                'name' => 'create',
                'label' => 'Cria um novo Modulo'
            ]);
            $permissaoModuloDelete = Roles::create([
                'resource_id' => $resourceModulo->id,
                'name' => 'delete',
                'label' => 'Apaga um Modulo'
            ]);

            //Categoria Recurso
            $permissaoCategoriaIndex = Roles::create([
                'resource_id' => $resourceCategoria->id,
                'name' => 'index',
                'label' => 'Listagem do Recurso Categoria'
            ]);
            $permissaoCategoriaCreate = Roles::create([
                'resource_id' => $resourceCategoria->id,
                'name' => 'create',
                'label' => 'Cria um novo Categoria'
            ]);
            $permissaoCategoriaDelete = Roles::create([
                'resource_id' => $resourceCategoria->id,
                'name' => 'delete',
                'label' => 'Apaga um Categoria'
            ]);

            //Recurso
            $permissaoRecursoIndex = Roles::create([
                'resource_id' => $resourceRecurso->id,
                'name' => 'index',
                'label' => 'Listagem do Recurso Recurso'
            ]);
            $permissaoRecursoCreate = Roles::create([
                'resource_id' => $resourceRecurso->id,
                'name' => 'create',
                'label' => 'Cria um novo Recurso'
            ]);
            $permissaoRecursoDelete = Roles::create([
                'resource_id' => $resourceRecurso->id,
                'name' => 'delete',
                'label' => 'Apaga um Recurso'
            ]);
            //Permissao
            $permissaoPermissaoIndex = Roles::create([
                'resource_id' => $resourcePermissao->id,
                'name' => 'index',
                'label' => 'Listagem do Recurso Permissao'
            ]);
            $permissaoPermissaoCreate = Roles::create([
                'resource_id' => $resourcePermissao->id,
                'name' => 'create',
                'label' => 'Cria um novo Permissao'
            ]);
            $permissaoPermissaoDelete = Roles::create([
                'resource_id' => $resourcePermissao->id,
                'name' => 'delete',
                'label' => 'Apaga um Permissao'
            ]);

            //Perfil
            $permissaoPerfilIndex = Roles::create([
                'resource_id' => $resourcePerfil->id,
                'name' => 'index',
                'label' => 'Listagem do Recurso Perfil'
            ]);
            $permissaoPerfilCreate = Roles::create([
                'resource_id' => $resourcePerfil->id,
                'name' => 'create',
                'label' => 'Cria um novo Perfil'
            ]);
            $permissaoPerfilDelete = Roles::create([
                'resource_id' => $resourcePerfil->id,
                'name' => 'delete',
                'label' => 'Apaga um Perfil'
            ]);

            //Perfil Permissão
            $permissaoPerfilPermissaoIndex = Roles::create([
                'resource_id' => $resourcePerfilPermissao->id,
                'name' => 'index',
                'label' => 'Listagem do Recuso Perfil Permissão'
            ]);
            $permissaoPerfilPermissaoAtribuirpermissoes = Roles::create([
                'resource_id' => $resourcePerfilPermissao->id,
                'name' => 'sync',
                'label' => 'Atribuir permissoes do Recurso Perfil Permissão'
            ]);

            //Perfil Usuário
            $permissaoPerfilUsuarioIndex = Roles::create([
                'resource_id' => $resourcePerfilUsuario->id,
                'name' => 'index',
                'label' => 'Listagem do Recurso Perfil Usuário'
            ]);
            $permissaoPerfilUsuarioAtribuirperfis = Roles::create([
                'resource_id' => $resourcePerfilUsuario->id,
                'name' => 'assign',
                'label' => 'Atribuir perfis do Recurso Perfil Usuário'
            ]);
            $permissaoPerfilUsuarioPerfis = Roles::create([
                'resource_id' => $resourcePerfilUsuario->id,
                'name' => 'type-accounts',
                'label' => 'Perfis do Recurso Perfil Usuário'
            ]);
            $permissaoPerfilUsuarioDesvincularperfil = Roles::create([
                'resource_id' => $resourcePerfilUsuario->id,
                'name' => 'detach',
                'label' => 'Desvincular perfil do Recurso Perfil Usuário'
            ]);
            $permissaoPerfilUsuarioAtribuir = Roles::create([
                'resource_id' => $resourcePerfilUsuario->id,
                'name' => 'attach',
                'label' => 'Atribuir do Recurso Perfil Usuário'
            ]);

            $this->info('Vinculando Permissões ao Perfil');
            /** Vincular Permissões ao Perfil - Administrador*/
            //Vinculando ao Perfil ao Home
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoHomeIndex->id
            ]);
            //Vinculando ao Perfil ao Modulo
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoModuloIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoModuloCreate->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoModuloDelete->id
            ]);
            //Vinculando ao Perfil ao Categoria
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoCategoriaIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoCategoriaCreate->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoCategoriaDelete->id
            ]);
            //Vinculando ao Perfil ao Recurso
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoRecursoIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoRecursoCreate->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoRecursoDelete->id
            ]);
            //Vinculando ao Perfil ao Permissao
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPermissaoIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPermissaoCreate->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPermissaoDelete->id
            ]);
            //Vinculando ao Perfil ao Perfil
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilCreate->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilDelete->id
            ]);
            //Vinculando ao perfil ao Perfil Permissao
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilPermissaoIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilPermissaoAtribuirpermissoes->id
            ]);
            //Vinculando ao Perfil ao Perfil Usuário
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilUsuarioIndex->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilUsuarioAtribuirperfis->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilUsuarioAtribuir->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilUsuarioDesvincularperfil->id
            ]);
            TypeAccountRole::create([
                'type_account_id' => $typeAccountAdmin->id,
                'role_id' => $permissaoPerfilUsuarioPerfis->id
            ]);

            $this->info('Vinculando o Usuário a um Perfil');
            /** Vinculando o Usuário a um Perfil*/
            TypeAccountUser::create([
                'type_account_id' => $typeAccountAdmin->id,
                'user_id' => $user->id,
            ]);

            \DB::commit();
            
            $this->info('Finalizado build!');
            $this->info('Seu Super Admin: admin@tratspay.com');
            $this->info('Sua Senha: t#4tSp4y');
            
        } catch (\PDOException $exception) {
            echo $exception->getMessage();
            \DB::rollBack();
        }
    }
}
