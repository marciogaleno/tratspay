<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    protected $table = 'clicks';
    
    protected $fillable = [
        'product_id', 'user_id', 'reference_code', 'tracking_code', 'source'
    ];
}