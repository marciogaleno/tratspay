<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\User;

class CoproducerPlan extends Model
{
    protected $table = 'coproducers_plans';

    protected $primaryKey = 'id';
    protected $fillable = [
        'coproducer_id',
        'plan_id',
        'product_id',
        'value',
        'type'
    ];


    function product()
    {
        return $this->hasOne(Product::class, "id", 'product_id');
    }

    function coproducer()
    {
        return $this->hasOne(CoProducer::class, "id", "coproducer_id");
    }

    function plan()
    {
        return $this->hasOne(Plan::class, "id", "plan_id");
    }
}
