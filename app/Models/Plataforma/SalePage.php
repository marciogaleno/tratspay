<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CodigoUnico;
use App\Traits\TenantTrait;

class SalePage extends Model
{
    use CodigoUnico;

    protected $fillable = [
        'local_page_sale', 'url_page_sale', 'url_page_thank', 'url_page_print_ticket',
        'title', 'desc', 'tenant_id'
    ];

}
