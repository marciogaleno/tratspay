<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoAffiliate extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'email', 
        'format_commission', 
        'value_commission', 
        'deadline',
        'user_id',
        'product_id',
        'code_affiliate',
        'status',
        'user_name'
    ];

    function product()
    {
        return $this->belongsTo(Product::class)->withoutGlobalScopes();
    }

    public function setDeadlineAttribute($value)
    {   
        if ($value) {
            $dateFormated = \App\Helpers\Helper::dateToAmerican($value);
            $this->attributes['deadline'] = $dateFormated;
        } else {
            $this->attributes['deadline'] = null;
        }

    }

    public function setValueCommissionAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['value_commission'] = $valorFormatdo;
    }

    public function getValueCommissionAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }


}
