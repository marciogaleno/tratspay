<?php

namespace App\Models\Plataforma;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ClickMongo extends Eloquent
{
    protected $connection = 'mongodb';

    protected $collection = 'clicks';

    protected $fillable = [
        'productName', 'product_id', 'productUuid', 'dClicks', 'promotionCode', 'userId'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

}
