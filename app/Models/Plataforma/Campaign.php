<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CodigoUnico;

class Campaign extends Model
{
    use CodigoUnico;

    protected $fillable = [
        'code', 'name', 'type_url', 'checkout_code', 'url_alternative_code', 'pixel', 
        'pixel_id', 'label', 'exec_ticket', 'exec_checkout', 'product_id', 'value'
    ];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function setValueAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['value'] = $valorFormatdo;
    }

    public function getValueAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }
}
