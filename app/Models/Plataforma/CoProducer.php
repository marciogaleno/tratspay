<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;


class CoProducer extends Model
{   
    use SoftDeletes;

    protected $fillable = [
        'email', 
        'type', 
        'format_commission', 
        'value_commission', 
        'deadline',
        'user_id',
        'product_id',
        'status'
    ];

    public function setValueCommissionAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['value_commission'] = $valorFormatdo;
    }

    public function getValueCommissionAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withoutGlobalScopes();
    }
    
    public function setDeadlineAttribute($value)
    {
        if ($value == "-") {
            return;
        }

        $this->attributes['deadline'] = \App\Helpers\Helper::dateToEn($value, false);
    }

    public function getDeadlineAttribute($value)
    {
        return $value;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
