<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Commission extends Model
{
    protected $primaryKey ='id';

    protected $fillable = [
        'value',
        'assignment_type',
        'commission_type',
        'percent',
        'fixed_rate',
        'value_transaction',
        'relation_type',
        'user_id',
        'product_id',
        'reference_code',
        'transaction_id' ,
        'source'
    ];

    function transaction()
    {
        return $this->belongsTo(TransactionMp::class);
    }

    function producer()
    {
        return $this->hasMany(TransactionMp::class, 'transaction_id');
    }

    function product()
    {
        return $this->belongsTo(Product::class);
    }

    function user()
    {
        return $this->belongsTo(User::class);
    }
}
