<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;

class ConfigAffiliation extends Model
{    
    protected $fillable = [
        'accept_affiliation', 'format_commission', 'value_commission', 'expiration_coockie',
        'desc_affiliate', 'producer_refund', 'hide_data_customer', 'afil_link_chk',
        'coupon', 'tenant_id', 'assignment_type', 'product_hide', 'recruit_code'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->recruit_code = strtoupper(bin2hex(openssl_random_pseudo_bytes(5)));
        });
    }

    public function setValueCommissionAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['value_commission'] = $valorFormatdo;
    }

    public function getValueCommissionAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }

}
