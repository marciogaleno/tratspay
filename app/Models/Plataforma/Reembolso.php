<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Reembolso extends Model
{
    protected $primaryKey = 'id';

    protected $fillable = [
        'user_id',
        'product_id',
        'status',
        'prazo',
        "motivo",
        'observacao',
        'transaction_id'
    ];


    function product()
    {
        return $this->hasOne(Product::class, "id", 'product_id');
    }

    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }
}
