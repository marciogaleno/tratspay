<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ProductsComplaint extends Model
{
    protected $fillable = ['product_id', 'desc', 'motivo'];

}
