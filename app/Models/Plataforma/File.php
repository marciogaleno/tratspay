<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;

class File extends Model
{
    protected $fillable = [
        'name', 'name_original', 'product_uuid', 'product_id', 'file_path', 'size'
    ];
    
}
