<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CodigoUnico;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;

class Checkout extends Model
{   
    use SoftDeletes;
    use CodigoUnico;

    protected $fillable = [
        'desc', 'invoice_desc', 'pay_card', 'pay_ticket','url_page_thank','url_page_print_ticket', 'request_address', 'days_expir_ticket',
        'request_cpf', 'request_phone', 'plan_id', 'tenant_id', 'template', 'banner_1', 'banner_2'
    ];

    public function plan() 
    {
        return $this->belongsTo(Plan::class);
    }

    public function planChk()
    {
        return $this->belongsTo(Plan::class,'plan_id','id')->withoutGlobalScopes();
    }
}
