<?php

namespace App\Models\Plataforma;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Traits\TenantTrait;

class BankAccount extends Model
{
    use TenantTrait;
    use SoftDeletes;

    const STATUS_APPROVED = 1; //Aprovada

    const STATUS_WAIT = 0; //Em Avaliação

    const STATUS_DISAPPROVED = 2;//Reprovado

    protected $table = 'bank_account';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'type',
        'bank',
        'bank_branch',
        'bank_branch_digit',
        'bank_account',
        'bank_account_digit',
        'variation',
        'status',
        'type_people',
        'tenant_id'
    ];

    public function bank()
    {
        return $this->belongsTo(Bank::class, 'bank', 'code');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
