<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class TypeDeliverie extends Model
{
    public function products() {
        return $this->hasMany(Product::class);
    }
}
