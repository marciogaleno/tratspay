<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Traits\CodigoUnico;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use App\Scopes\PlanScope;
use App\Traits\TenantTrait;

class Plan extends Model
{
    use CodigoUnico;
    use SoftDeletes;

    protected $fillable = [
        'code', 'name', 'price', 'desc','default', 'frequency',  'first_installment', 'free_days',
        'different_value', 'amount_recurrence', 'free', 'active', 'tenant_id', 'quantity'
    ];

    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productChk()
    {
        return $this->belongsTo(Product::class,'product_id','id')->withoutGlobalScopes();
    }

    public function checkouts()
    {
        return $this->hasMany(Checkout::class);
    }

    public function setFreeAttribute($value)
    {
        $free = 0;

        if ($value == 'on')  {
            $free = 1;
        }

        $this->attributes['free'] = $free;
    }

    public function setPriceAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['price'] = $valorFormatdo;
    }

    public function getPriceAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }

    public function setDifferentValueAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['different_value'] = $valorFormatdo;
    }

    public function getDifferentValueAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }

}
