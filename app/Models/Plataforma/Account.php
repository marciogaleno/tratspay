<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = [
        'balance', 'user_id'
    ];
}
