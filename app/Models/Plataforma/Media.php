<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;

class Media extends Model
{    
    protected $fillable = [
        'type', 'url', 'product_id'
    ];
    
}
