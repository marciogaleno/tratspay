<?php

namespace App\Models\Plataforma;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'content',
        'read',
        'user_id',
        'product_id',
        'type', //['sale', 'refund']
        'first_name',
        'last_name',
        'email',
        'cpf_cnpj',
        'phone'
    ];

    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    function product()
    {
        return $this->hasOne(Product::class, "id", "product_id");
    }

}
