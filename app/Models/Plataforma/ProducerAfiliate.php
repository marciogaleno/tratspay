<?php

namespace App\Models\Plataforma;

use App\User;
use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Traits\CodigoUnico;
use Carbon\Carbon;
use App\Traits\TenantTrait;

class ProducerAffiliate extends Pivot
{
    use CodigoUnico;
    protected $primaryKey ='id';

    protected $table = 'producer_affiliate';
    
    protected $fillable = [
        'relation_type', 'request_date', 'approval_date', 'status', 'user_id', 'product_id', 'code'
    ];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($model) {
            $model->request_date = Carbon::now();
        });
    }

    function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class)->withoutGlobalScopes();
    }

}
