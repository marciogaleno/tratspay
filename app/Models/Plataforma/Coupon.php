<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'code', 'coupon', 'price', 'active', 'free', 'desc', 'url_thank_you', 'url_ticket', 'validity',
        'max_purchase', 'email_buyer', 'counter', 'text_counter', 'background_counter',
        'color_text_counter', 'time_counter', 'take_interest', 'plan_id', 'product_id'
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->code = strtoupper(bin2hex(openssl_random_pseudo_bytes(3)));
        });
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function setPriceAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['price'] = $valorFormatdo;
    }

    public function getPriceAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }

    public function setValidityAttribute($value)
    {
        $this->attributes['validity'] = \App\Helpers\Helper::datetimeToEn($value);
    }

    public function getValidityAttribute($value)
    {
        return \App\Helpers\Helper::dateToBr($value, true);
    }
}
