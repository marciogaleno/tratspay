<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $fillable = [
        'tracking_code', 'product_id', 'reference_code', 'relation_type', 'indicated_user',
        'source',
    ];

}
