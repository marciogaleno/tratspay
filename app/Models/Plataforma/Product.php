<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\TenantTrait;
use App\User;
use App\Traits\GeneralTrait;
use App\Scopes\Tenant\TenantScope;
use App\Traits\GenerateId;
use Illuminate\Support\Facades\Auth;
use App\Src\Model\ConfigAccount;

class Product extends Model
{   
    use SoftDeletes;
    use Uuid;

    const ACTIVE = true;
    const APPROVED = true;
    const DISAPPROVED = false;

    protected $fillable = [
        'product_type', 'name', 'email_support', 'several_purchases', 'max_item_by_order',
        'category_id', 'type_deliverie_id', 'desc', 'payment_type', 'warranty', 
        'tipo_entrega_id', 'image', 'tenant_id', 'type_member_area', 'url_member_area',
        'login_member_area', 'password_member_area', 'rate'
    ];

    protected $hidden = [

    ];

    public function plans() 
    {
        return $this->hasMany(Plan::class);
    }

    public function plansWithoutGlobalScope() 
    {
        return $this->hasMany(Plan::class)->WithoutGlobalScopes();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status']); 
    }

    public function usersWithoutGlobalScope()
    {
        return $this->belongsToMany(User::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status']); 
    }

    public function categorie() 
    {
        return $this->belongsTo(Categorie::class,'category_id','id');
    }

    public function categorieChk()
    {
        return $this->belongsTo(Categorie::class,'category_id','id')->withoutGlobalScopes();
    }

    public function typeDeliverie() 
    {
        return $this->belongsTo(TypeDeliverie::class);
    }

    public function configAffiliation() 
    {
        return $this->hasOne(ConfigAffiliation::class);
    }

    public function configAffiliationWithoutGlobalScope()
    {
        return $this->hasOne(ConfigAffiliation::class)->withoutGlobalScopes();
    }

    public function salepage()
    {
        return $this->hasOne(SalePage::class);
    }

    public function coproducers()
    {
        return $this->hasMany(CoProducer::class);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function producer()
    {
        return $this->belongsToMany(User::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->wherePivot('relation_type', 'producer')
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status']); 
    }

    public function affiliates()
    {
        return $this->belongsToMany(User::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->wherePivot('relation_type', 'affiliate')
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status']); 
    }

    public function affiliatelogged()
    {
        return $this->belongsToMany(User::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->wherePivot('relation_type', 'affiliate')
                    ->wherePivot('user_id', Auth::user()->id)
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status'])
                    ->withoutGlobalScopes();
    }

    public function getImageAttribute($value)
    {
        if (!$value) {
            return 'image_product_default.jpg';
        }

        return $value;
    }

    public function reembolso()
    {
        return $this->hasOne(Reembolso::class, "product_id", "id")->where('user_id', Auth::user()->id);
    }

    public static function taxaFisico()
    {
        return ConfigAccount::getKey('keyname');
    }

    public static function taxaVirtual()
    {
        return ConfigAccount::getKey('keyname');;
    }

    public function setRateAttribute($value)
    {
        if (!$value) {
            return $this->attributes['rate'] = null;
        }
        
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['rate'] = $valorFormatdo;
    }

    public function getRateAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }
}
