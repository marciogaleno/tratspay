<?php

namespace App\Models\Plataforma;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TrackingMongo extends Eloquent
{
    protected $connection = 'mongodb';

    protected $collection = 'tracking';

    protected $fillable = [
        'trakingCode','track', 'date'
    ];
}
