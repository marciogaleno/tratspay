<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;

class Postback extends Model
{
    use TenantTrait;
    
    protected $fillable = [
        'product_id',
        'user_id',
        'type',
        'url',
        'pending',
        'refund',
        'abandon_checkout',
        'finished',
        'blocked',
        'canceled',
        'completed'
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
}
