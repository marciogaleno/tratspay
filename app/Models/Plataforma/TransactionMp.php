<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Observers\TransactionObserver;

class TransactionMp extends Model
{    
    protected $table = 'transaction_mp';

    protected $fillable = [
        'code',
        'transaction_id',
        'date_created',
        'date_approved',
        'operation_type',
        'client_id',
        'payment_method_id',
        'payment_type_id',
        'barcode',
        'last_four_digits',
        'expiration_month',
        'expiration_year',
        'status_gateway',
        'status_detail_gateway',
        'product_description',
        'captured',
        'installments',
        'shipping_amount',
        'transaction_amount',
        'external_resource_url',
        'first_name',
        'last_name',
        'email',
        'cpf_cnpj',
        'phone',
        'zipcode',
        'address',
        'address_number',
        'address_complement',
        'address_district',
        'address_city',
        'address_state',
        'address_country',
        'traking_code',
        'checkout_id',
        'checkout_code',
        'plan_id',
        'plan_code',
        'plan_name',
        'plan_price',
        'plan_frequency',
        'plan_quantity',
        'product_type',
        'rate_percent',
        'rate_total',
        'fixed_rate',
        'product_id',
        'product_uuid',
        'product_name',
        'warranty',
        'end_warranty',
        'status',
        'email_support',
        'gateway',
        'quantity ',
        'last_status'
    ];

    public function commissions()
    {
        return $this->hasMany(Commission::class, 'transaction_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function productAffiliate()
    {
        return $this->hasOne(ProducerAffiliate::class, 'product_id', 'product_id')->where('relation_type', '=', 'producer');
    }
    

    public function setCpfCnpjAttribute($value)
    {
        $value = str_replace(' ', '-', $value); // Replaces all spaces with hyphens.
        $value = preg_replace('/[^A-Za-z0-9\-]/', '', $value); // Removes special chars.
        
        $this->attributes['cpf_cnpj'] = $value;
    }

}
