<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CodigoUnico;

class UrlAlternative extends Model
{
    use CodigoUnico;
    
    protected $fillable = [
       'code', 'desc', 'url', 'private', 'product_id',
    ];
}
