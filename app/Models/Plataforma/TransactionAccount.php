<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class TransactionAccount extends Model
{
    protected $table="transaction_accounts";

    protected $fillable = [
        'type', 'type_transaction', 'value', 'source_account', 'commission_id', 'transaction_id', 
        'chashout_id', 'destination_account', 'source_account'
    ];
}
