<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class TrackingIndication extends Model
{
    protected $fillable = [
        'tracking_code', 'indication_code', 'updated_at'
    ];
}
