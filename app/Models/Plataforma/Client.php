<?php

namespace App\Models\Plataforma;

use App\Helpers\Helper;
use App\Models\Plataforma\BankAccount;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'external_reference_id',
        'person',
        'name',
        'cpf_cnpj',
        'rg_ie',
        'fantasia',
        'birth',
        'social_facebook',
        'social_twitter',
        'social_linkedin',
        'social_site',
        'phone',
        'cellphone',
        'email',
        'street',
        'number',
        'complement',
        'district',
        'postal_code',
        'city',
        'state',
        'country',
        'obs',
        'doacao'
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope('user_id', function (Builder $builder) {
            $builder->where('user_id', '=',auth()->id());
        });
    }

    public function getBirthAttribute($value)
    {
        return Helper::dateToBr($value);
    }

    public function setBirthAttribute($value)
    {
        $this->attributes['birth'] = Helper::dateToEn($value);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function bankAccounts() {
        return $this->hasMany(BankAccount::class,'user_id','user_id');
    }

}
