<?php

namespace App\Models\Plataforma;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    
    protected $fillable = [
        'content',
        'read',
        'favorite',
        'trash',
        'user_id',
        'id_affiliate'
    ];

    function user()
    {
        return $this->hasOne(User::class, "id", "user_id");
    }

    function affiliate()
    {
        return $this->hasOne(User::class, "id", "id_affiliate");
    }

}
