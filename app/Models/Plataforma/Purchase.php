<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    
    protected $fillable = [
        'transaction_id', 
        'product_id', 
        'user_id', 
        'date', 
        'value'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class)->withoutGlobalScopes();
    }

    public function transaction()
    {
        return $this->belongsTo(TransactionMp::class);
    }
}
