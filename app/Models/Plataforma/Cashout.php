<?php

namespace App\Models\Plataforma;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TenantTrait;

class Cashout extends Model
{
    use TenantTrait;

    protected $fillable = [
        'user_id', 'bank_account_id', 'value', 'rate', 'value_total', 'status'
    ];

    public function bankaccount()
    {
        return $this->belongsTo(BankAccount::class);
    }

    public function setValueAttribute($value)
    {
        $valorFormatdo = str_replace('.', '',  $value);
        $valorFormatdo = str_replace(',', '.',  $valorFormatdo);

        $this->attributes['value'] = $valorFormatdo;
    }

    public function getValueAttribute($value)
    {
        return number_format($value, 2, ',', '');
    }
}
