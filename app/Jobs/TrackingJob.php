<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Plataforma\Tracking;
use App\Models\Plataforma\Product;
use App\Services\TrackingService;

class TrackingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;
    private $params;
    private $trackingCod;
    private $ip;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $params, $trackingCod, $ip)
    {
        $this->data = $data;
        $this->params = $params;
        $this->trackingCod = $trackingCod;
        $this->ip = $ip;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tranking = [
            'trackingId' =>  '4565465',
            'trancking' => [
                ['afliate' => 1265, 'count' => 1]
            ]
        ];

        
        $TrakingService = app()->make('TrakingService');
        $ok = $TrakingService->track($this->data, $this->params, $this->trackingCod, $this->ip);
   
        if ($ok) {
            
            $clickService = app()->make('ClickService');
      
            $clickService->count($this->data, $this->params,  $this->ip);

        }

        //Tracking::create($tranking);
        //dd(Tracking::whereRaw(['trackingId' => array('$eq' => '4565465'), 'trancking' => ['afliate' =>['$eq' => 1265]] ])->get());
     
        //Tracking::where('trackingId', '4565465')->whereRaw(['trancking' => ['$eq' => ]])
    }
}
