<?php

namespace App;

use App\Models\Plataforma\Client;
use App\Src\Model\TypeAccount;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\ProducerAffiliate;
use App\Models\Plataforma\Purchase;
use App\Src\Security\Traits\HasPerfilTrait;
use App\Models\Plataforma\BankAccount;
use App\Traits\Token;
use App\Traits\IndicationCode;
use App\Notifications\ResetPassword;

class User extends Authenticatable 
//implements MustVerifyEmail
{
    use Notifiable;
    use HasPerfilTrait;
    use Token;
    use IndicationCode;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'accept_terms', 'image', 'tenant_id', 'token', 'user_indication', 'indication_end'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tenant()
    {
        return $this->belongsTo(Tenant::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->hasOne(Client::class, 'user_id', 'id');
    }

    public function bankAccounts() {
        return $this->hasMany(BankAccount::class,'user_id','id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status']); 
    }

    public function productsWithoutScopes()
    {
        return $this->belongsToMany(Product::class, 'producer_affiliate')
                    ->using(ProducerAffiliate::class)
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status'])
                    ->withoutGlobalScopes(); 
    }

    public function productsIsProducer()
    {
        return $this->belongsToMany(Product::class, 'producer_affiliate')
                    ->withPivot(['code','relation_type', 'request_date', 'approval_date', 'status'])
                    ->wherePivot('relation_type', 'producer');
    }


    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    public function hasPermission(Permission $permission) {
        return $this->hasAnyRoles($permission->roles);
    }

    public function hasAnyRoles($roles) {
        if (is_array($roles) || is_object($roles)) {
            return !! $roles->intersect($this->roles)->count();
        }
        return $this->roles->contains('name', $roles);
    }

    public function sendPasswordResetNotification($token)
    {
        // Não esquece: use App\Notifications\ResetPassword;
        $this->notify(new ResetPassword($token));
    }

    public function TypeAccountadmin()
    {
        return $this->hasOne('App\Src\Model\TypeAccountUser','user_id','id')->where('type_account_id', '=', 1);//admin
    }
}
