<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Services\TrackingService;
use App\Models\Plataforma\Categorie;
use App\Services\ResolveUrlService;
use App\Services\ClickMongoService;
use App\Models\Plataforma\TransactionMp;
use App\Observers\TransactionObserver;
use App\Services\ClickService;
use App\Models\Plataforma\Commission;
use App\Models\Plataforma\Reembolso;
use App\Observers\CommissionObserver;
use App\Observers\PostbackObserver;
use App\Observers\ReembolsoObserver;
use App\User;
use App\Observers\UserObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer(
            'plataforma.Market.*',
            function ($view) {
                $view->with('categories', Categorie::pluck('desc', 'id'));
            }
        );

        \App\Models\Plataforma\BankAccount::creating(function($model){
            $model->user_id = auth()->id();
        });

        $this->app->singleton('TrakingService', function($app)
        {
            return new TrackingService();
        });

        $this->app->singleton('ResolveUrlService', function($app)
        {
            return new ResolveUrlService();
        });

        $this->app->singleton('ClickMongoService', function($app)
        {
            return new ClickMongoService();
        });

        $this->app->singleton('ClickService', function($app)
        {
            return new ClickService();
        });

        TransactionMp::observe(TransactionObserver::class);
        Commission::observe(CommissionObserver::class);
        TransactionMp::observe(PostbackObserver::class);
        User::observe(UserObserver::class);
        Reembolso::observe(ReembolsoObserver::class);
    }
}
