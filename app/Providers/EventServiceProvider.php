<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ProductSold' => [
            'App\Listeners\SeparateCommissionsProductSold',
            'App\Listeners\UnlockProductSold',
//            'App\Listeners\SendEmailsProductSold'
        ],
        'App\Events\ReprocessingCommissionsEvent' => [
            'App\Listeners\reprocessCommissions\SeparateCommissionsHandler',
            'App\Listeners\reprocessCommissions\SendEmailsHandler'
        ],
        'App\Events\TrackingEvent' => [
            'App\Listeners\TrackingListener',
            'App\Listeners\ClickListener',
        ],
        'App\Events\PostbackEvent' => [
            'App\Listeners\Postback\PostbackListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
