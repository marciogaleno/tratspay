<?php

namespace App\Providers\App\Listeners;

use App\Providers\App\Events\ProductSold;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SeparateCommissionsProductSold
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductSold  $event
     * @return void
     */
    public function handle(ProductSold $event)
    {
        //
    }
}
