<?php

namespace App\Src\Menu;

use DB;

class Menu
{
    protected $request;
    protected $auth;

    public function __construct($request, $auth)
    {
        $this->request = $request;
        $this->auth = $auth;
    }

    public function render()
    {
        $path = preg_split('/\//', $this->request->path());
        $modulo = current($path);
        $controller = next($path);

        $userId = $this->auth->user()->getAuthIdentifier();

        $permissoes = $this->getPermissoes($modulo, $userId);

        $navigation = $this->getNavigationArray($permissoes, $controller);

        $html = "<ul class='sidebar-menu do-nicescrol'>";

        foreach ($navigation as $nav) {
            $active = '';
            if (array_key_exists('activeMenu', $nav)) {
                $active = "class='{$nav['activeMenu']}'";
            }
            $html .= "<li {$active}>";
            $html .= "<a href='{$nav['uri']}' class='waves-effect'><i class='{$nav['icon']}'></i><span>{$nav['label']}</span><i class='fa fa-angle-left pull-right'></i></a>";
            $html .= "<ul class='sidebar-submenu'>";
            foreach ($nav['pages'] as $page) {
                $isActive = $page['active'] == true ? "class='active'" : '';
                $html .= "<li {$isActive}><a title='{$page['label']}' href='{$page['uri']}'>{$page['label']}</a></li>";
            }
            $html .= '</ul>';
            $html .= '</li>';
        }

        $permissoes = $this->getPermissoesOff($modulo, $userId);
        $navigation = $this->getNavigationOffArray($permissoes, $controller);

        foreach ($navigation as $rcs => $nav) {
            $active = '';
            if (array_key_exists('activeMenu', $nav)) {
                $active = "class='{$nav['activeMenu']}'";
            }
            $html .= "<li {$active}>";
            $html .= "<a href='{$nav['uri']}' class='waves-effect'><i class='{$nav['icon']}'></i><span>{$nav['label']}</span></a>";
            $html .= '</li>';
        }

        $html .= '</ul>';

        echo $html;
    }

    private function getPermissoes($modulo, $usuarioId)
    {
        $sql = "SELECT
                    DISTINCT m.name as mod_nome, cr.name as ctr_nome, cr.icon as ctr_icone, cr.order as ctr_ordem, r.label as rcs_descricao, r.name as rcs_nome, r.order as rcs_ordem
                FROM
                    users AS u
                    INNER JOIN type_account_user AS pu ON pu.user_id = u.id
                    INNER JOIN type_account AS p ON p.id = pu.type_account_id
                    INNER JOIN module AS m ON m.id = p.module_id
                    INNER JOIN type_account_role AS pp ON pp.type_account_id = p.id
                    INNER JOIN roles AS pm ON pm.id = pp.role_id
                    INNER JOIN resource AS r ON r.id = pm.resource_id
                    INNER JOIN category_resource AS cr ON cr.id = r.category_resource_id
                WHERE
                    m.name = :mod_nome AND pm.name ilike '%index%' AND u.id = :usuario_id AND cr.enable = true
                ORDER BY
                    cr.order, r.order";

        return DB::select($sql, ['mod_nome' => $modulo, 'usuario_id' => $usuarioId]);
    }

    private function getPermissoesOff($modulo, $usuarioId)
    {
        $sql = "SELECT
                    m.name as mod_nome, cr.name as ctr_nome, r.name as rcs_nome, cr.icon as ctr_icone, r.order as rcs_ordem, r.label as rcs_descricao
                FROM
                    users AS u
                    INNER JOIN type_account_user AS pu ON pu.user_id = u.id
                    INNER JOIN type_account AS p ON p.id = pu.type_account_id
                    INNER JOIN module AS m ON m.id = p.module_id
                    INNER JOIN type_account_role AS pp ON pp.type_account_id = p.id
                    INNER JOIN roles AS pm ON pm.id = pp.role_id
                    INNER JOIN resource AS r ON r.id = pm.resource_id
                    INNER JOIN category_resource AS cr ON cr.id = r.category_resource_id
                WHERE
                    m.name = :mod_nome AND pm.name ilike '%index%' AND u.id = :usuario_id AND cr.enable = false
                ORDER BY
                    cr.order, r.order";

        return DB::select($sql, ['mod_nome' => $modulo, 'usuario_id' => $usuarioId]);
    }

    private function getNavigationArray($permissoes, $activeController = null)
    {
        $navigation = [];
        if (!empty($permissoes)) {
            foreach ($permissoes as $key => $permissao) {
                $navigation[$permissao->ctr_nome]['label'] = $permissao->ctr_nome;
                $navigation[$permissao->ctr_nome]['uri'] = 'javaScript:void();';
                $navigation[$permissao->ctr_nome]['icon'] = $permissao->ctr_icone;

                if ($activeController == strtolower($permissao->rcs_nome)) {
                    $navigation[$permissao->ctr_nome]['activeMenu'] = 'active';
                }

                $navigation[$permissao->ctr_nome]['pages'][$key] = [
                    'label' => $permissao->rcs_descricao,
                    'uri' => url('/'.strtolower($permissao->mod_nome).'/'.strtolower($permissao->rcs_nome).'/index'),
                    'controller' => strtolower($permissao->rcs_nome),
                    'action' => 'index',
                    'active' => (strtolower($permissao->rcs_nome) == $activeController),
                ];
            }
        }

        return $navigation;
    }

    private function getNavigationOffArray($permissoes, $activeController = null)
    {
        $navigation = [];
        if (!empty($permissoes)) {
            foreach ($permissoes as $permissao) {
                $navigation[$permissao->rcs_nome] = [
                    'label' => $permissao->ctr_nome,
                    'uri' => url('/'.strtolower($permissao->mod_nome).'/'.strtolower($permissao->rcs_nome).'/index'),
                    'controller' => strtolower($permissao->rcs_nome),
                    'action' => 'index',
                    'active' => (strtolower($permissao->rcs_nome) == $activeController),
                    'icon' => $permissao->ctr_icone
                ];

                if ($activeController == strtolower($permissao->rcs_nome)) {
                    $navigation[$permissao->rcs_nome]['activeMenu'] = 'active';
                }
            }
        }

        return $navigation;
    }
}
