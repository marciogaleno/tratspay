<?php
namespace App\Src\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;

class TypeAccountRepository extends Repository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Src\Model\TypeAccount';
    }

    public function getAllTypeAccountsWithModules()
    {
        $sql = 'SELECT
                    p.id,
                    p.enable,
                    p.name,
                    p.label,
                    m.name as mod_name
                FROM type_account p
                INNER JOIN module m ON m.id = p.module_id
                WHERE p.deleted_at is null
                ORDER BY m.id';

        return DB::select($sql);
    }

    public function getTypeAccountWithModule($id)
    {
        $sql = 'SELECT
                    p.id,
                    p.name,
                    p.label,
                    p.module_id,
                    m.name as mod_name
                FROM type_account p
                INNER JOIN module m ON m.id = p.module_id
                WHERE p.id = :id';

        return DB::selectOne($sql, ['id' => $id]);
    }

    public function getTreeOfRolesByTypeAccountAndModule($typeAccountId, $moduleId)
    {
        $sql = 'SELECT
                  recurso_id, nome, permissao_id, prm_nome, (CASE WHEN bol=1 THEN 1 ELSE 0 END) AS habilitado
                FROM (
                    SELECT r.id as recurso_id, r.label as nome, p.id as permissao_id, p.name as prm_nome FROM roles p
                    LEFT JOIN resource r ON r.id = p.resource_id
                    WHERE r.module_id = :moduleid 
                ) AS all_role
                LEFT JOIN (
                    SELECT p.id as temp, 1 as bol FROM roles p
                    LEFT JOIN resource r ON r.id = p.resource_id
                    LEFT JOIN type_account_role pp ON pp.role_id = p.id
                    WHERE pp.type_account_id = :typeAccountId
                ) less_role ON all_role.permissao_id = less_role.temp';

        $permissoes = DB::select($sql, ['moduleid' => $moduleId, 'typeAccountId' => $typeAccountId]);

        $retorno = [];
        if (count($permissoes)) {
            foreach ($permissoes as $key => $perm) {
                if (isset($retorno[$perm->recurso_id])) {
                    $retorno[$perm->recurso_id]['permissoes'][$perm->permissao_id] = array('permissao_id' => $perm->permissao_id, 'prm_nome' => $perm->prm_nome, 'habilitado' => $perm->habilitado);
                } else {
                    $retorno[$perm->recurso_id]['recurso_id'] = $perm->recurso_id;
                    $retorno[$perm->recurso_id]['nome'] = $perm->nome;
                    $retorno[$perm->recurso_id]['permissoes'][$perm->permissao_id] = array('permissao_id' => $perm->permissao_id, 'prm_nome' => $perm->prm_nome, 'habilitado' => $perm->habilitado);
                }
            }
        }

        return $retorno;
    }

    public function getTypeAccountWithModuleByUserId($userId)
    {
        $result = DB::select('SELECT
                    p.id,
                    p.name,
                    p.label,
                    p.module_id,
                    m.name as mod_nome
                FROM type_account p
                INNER JOIN type_account_user pu ON p.id = pu.type_account_id
                INNER JOIN module m ON m.id = p.module_id
                WHERE pu.user_id = :userId', ['userId' => $userId]);

        return $result;
    }

    /**
     * Sincroniza as permissões do perfil na base.
     *
     * @param $typeAccountId
     * @param array $roles
     */
    public function syncRoles($typeAccountId, array $roles)
    {
        return $this->model->find($typeAccountId)->roles()->sync($roles);
    }

    public function getModulesByUserId($userId)
    {
        $result = DB::select('SELECT
                    p.module_id
                FROM type_account p
                INNER JOIN type_account_user pu ON p.id = pu.type_account_id
                WHERE pu.user_id = :userId', ['userId' => $userId]);

        return $result;
    }

    public function getPerfis($moduleId)
    {
        $modulosNaoVinculados = DB::table('type_account')->select('id', 'name')
                    ->where('module_id', $moduleId)->get();

        return $modulosNaoVinculados;
    }

    public function getPerfisNaoAtribuidos($moduleId)
    {
        $perfisNaoAtribuidos = DB::table('type_account')->select('id', 'name')
                    ->where('module_id', $moduleId)->whereRaw("deleted_at is null")->get();

        return $perfisNaoAtribuidos;
    }
}
