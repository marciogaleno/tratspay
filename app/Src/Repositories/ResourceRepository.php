<?php
namespace App\Src\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;

class ResourceRepository extends Repository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Src\Model\Resource';
    }

    public function getAll()
    {
        $sql = 'SELECT cr.name as category_resource, cr.icon as cr_icon, m.name as module, r.id, r.name, r.enable, r.icon, r.label
                FROM resource r
                INNER JOIN category_resource cr ON cr.id = r.category_resource_id
                INNER JOIN module m ON m.id = r.module_id
                WHERE r.deleted_at IS NULL
                ORDER BY cr.order, r.order';

        return DB::select($sql);
    }

    public function lists($value, $key = null)
    {
        $collection = $this->model->join('module','module.id','=','module_id')
                                 ->selectRaw("concat(module.name,'/', resource.{$value}) as {$value}, resource.{$key}")
                                 ->get();
        
        return $collection->pluck($value,$key);


    }
}
