<?php
namespace App\Src\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;

class ModuleRepository extends Repository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Src\Model\Module';
    }

    public function getModulesNoLinked($modulesLinked)
    {
        $modulesNoLinked = DB::table('module')->select('id', 'name')
                    ->whereNotIn('id', $modulesLinked)
                    ->where('deleted_at', null)
                    ->get();

        return $modulesNoLinked;
    }

    public function lists($value, $key = null)
    {
        return $this->model->pluck($value,$key);
    }
}
