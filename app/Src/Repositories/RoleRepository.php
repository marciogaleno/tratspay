<?php
namespace App\Src\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;

class RoleRepository extends Repository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Src\Model\Roles';
    }

    public function getAll()
    {
        $sql = 'SELECT m.name as module, r.name as resource, p.id, p.name, p.label
                FROM roles p
                INNER JOIN resource r ON r.id = p.resource_id
                INNER JOIN module m ON m.id = r.module_id
                ORDER BY m.name, r.name';

        return DB::select($sql);
    }
}
