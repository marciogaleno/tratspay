<?php
namespace App\Src\Repositories;

use \DB;

class UserRepository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\User';
    }

    public function usuariosModulo($modulo)
    {
        $sql = "select p.nome,u.id, u.name from apps.modulo m
inner join apps.perfil p on p.fk_perfil_modulo_id = m.modulo_id
inner join apps.perfil_usuario pu on pu.fk_perfil_usuario_perfil = p.pk_perfil
inner join apps.users u on u.id = pu.fk_perfil_usuario_user
where m.nome = '{$modulo}'";

        return collect(DB::select($sql));
    }

    public function usuariosModuloComPerfil($modulo, $perfil)
    {
        $sql = "select p.nome,u.id, u.name from apps.modulo m
inner join apps.perfil p on p.fk_perfil_modulo_id = m.modulo_id and p.nome = '{$perfil}'
inner join apps.perfil_usuario pu on pu.fk_perfil_usuario_perfil = p.pk_perfil
inner join apps.users u on u.id = pu.fk_perfil_usuario_user
where m.nome = '{$modulo}'";

        return collect(DB::select($sql));
    }
}
