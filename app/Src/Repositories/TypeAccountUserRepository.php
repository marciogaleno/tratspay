<?php
namespace App\Src\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;
use Carbon\Carbon;

class TypeAccountUserRepository extends Repository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Src\Model\TypeAccountUser';
    }

    public function detachTypeAccount($userId, $typeAccountId)
    {
        $sql = 'DELETE
                FROM type_account_user
                WHERE user_id = :userid AND type_account_id = :typeAccountid';

        return DB::delete($sql, ['userid' => $userId, 'typeAccountid' => $typeAccountId]);
    }

    public function attachTypeAccount($userId, $typeAccountId)
    {
        $sql = "INSERT INTO type_account_user (user_id, type_account_id, created_at, updated_at)
				VALUES (:userid, :typeAccountid, '".Carbon::now()."', '".Carbon::now()."')";

        return DB::insert($sql, ['userid' => $userId, 'typeAccountid' => $typeAccountId]);
    }
}
