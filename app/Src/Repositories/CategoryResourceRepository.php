<?php
namespace App\Src\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class CategoryResourceRepository extends Repository
{
    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return 'App\Src\Model\CategoryResource';
    }

    public function lists($value, $key = null)
    {
        return $this->model->pluck($value,$key);
    }

    public function allOrderBy($field, $orientation = 'asc')
    {
        $model = new $this->model();
        return $model->orderBy($field,$orientation)->get();
    }
}
