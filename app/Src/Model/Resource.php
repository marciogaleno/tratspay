<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Resource extends Model
{
    use SoftDeletes;

    protected $table = 'resource';

    protected $primaryKey = 'id';

    protected $fillable = ['module_id','category_resource_id','name','label','icon','enable','order'];
}