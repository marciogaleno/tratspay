<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';

    protected $primaryKey = 'id';

    protected $fillable = ['name', 'label', 'resource_id'];
}