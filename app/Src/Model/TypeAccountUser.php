<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeAccountUser extends Model
{
    use SoftDeletes;

    protected $table = 'type_account_user';

    protected $fillable = ['type_account_id','user_id'];
}