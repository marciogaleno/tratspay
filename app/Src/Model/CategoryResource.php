<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryResource extends Model
{
    use SoftDeletes;

    protected $table = 'category_resource';

    protected $primaryKey = 'id';

    protected $fillable = ['name','label','icon','order','enable'];
}