<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class ConfigAccount extends Model
{
    const STATUS_ENABLED = 1;

    const STATUS_DISENABLED = 0;

    const PATH_BACKGROUND_LOGIN = 'background/login';

    protected $table = 'config_account';

    protected $fillable = ['key', 'value', 'status'];

    public static function getConfig($keyname)
    {
        return self::where('key', $keyname)->where('status', self::STATUS_ENABLED)->first();
    }

    public static function val($keyname)
    {
        $config = self::getConfig($keyname);
        return !is_null($config) ? $config->value : null;
    }

    public static function BGLogin($key = 'background_login')
    {
        $image = self::val($key);
        if ($image && Storage::disk('public')->exists('/'.$image)) {
            return url('storage/' . $image);
        } else {
            return url('assets/images/tratspay.jpg');
        }
    }
}