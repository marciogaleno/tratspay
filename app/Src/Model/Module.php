<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;

    protected $table = 'module';

    protected $primaryKey = 'id';

    protected $fillable = ["name","icon","label","enable"];

    public function getTypeAccounts()
    {
        return $this->hasMany('App\Src\Model\TypeAccount', 'modulo_id','id');
    }
}