<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeAccount extends Model
{
    use SoftDeletes;

    protected $table = 'type_account';

    protected $primaryKey = 'id';

    protected $fillable = ['name','label','module_id'];

    /**
     * Relacionamento NxN com a tabela de permissoes.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany('App\Src\Model\Roles', 'type_account_role', 'type_account_id', 'role_id');
    }

    /**
     * Relacionamento 1xN com a tabela de módulos.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function module()
    {
        return $this->belongsTo('App\Src\Model\Module', 'module_id', 'id');
    }

    public function vincular()
    {
        return $this->belongsToMany('App\User','type_account_user','type_account_id','user_id');
    }
}