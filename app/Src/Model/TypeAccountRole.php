<?php
namespace App\Src\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeAccountRole extends Model
{
    use SoftDeletes;

    protected $table = 'type_account_role';

    protected $fillable = ['type_account_id','role_id'];
}