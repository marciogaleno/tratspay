<?php

namespace App\Src\Security;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\Compilers\BladeCompiler;

class SecurityServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->singleton('security', function ($app) {
            return new Security($app);
        });

        $this->registerBladeExtensions();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['security'];
    }

    /**
     * Register new blade extensions.
     */
    protected function registerBladeExtensions()
    {
        $this->app->afterResolving('blade.compiler', function (BladeCompiler $bladeCompiler) {
            /*
             * add @haspermission and @endhaspermission to blade compiler
             */
            $bladeCompiler->directive('haspermission', function ($expression) {
                return "<?php if(app('security')->haspermission($expression)): ?>";
            });

            $bladeCompiler->directive('endhaspermission', function () {
                return '<?php endif; ?>';
            });

            /*
             * add @hasrole and @endhasrole to blade compiler
             */
            $bladeCompiler->directive('hasrole', function ($expression) {
                return "<?php if(app('security')->hasrole($expression)): ?>";
            });

            $bladeCompiler->directive('endhasrole', function () {
                return '<?php endif; ?>';
            });
        });
    }
}
