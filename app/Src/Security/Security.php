<?php

namespace App\Src\Security;

use Illuminate\Contracts\Foundation\Application;
use App\Src\Security\Contracts\Security as SecurityContract;
use App\Src\Security\Exceptions\ForbiddenException;
use DB;

class Security implements SecurityContract
{
    /**
     * The Laravel Application.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * @param \Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Retorna o usuário logado na aplicação.
     */
    public function getUser()
    {
        return $this->app['auth']->user();
    }

    /**
     * Verifica se o usuário tem acesso ao recurso.
     *
     * @param string|array $roles
     *
     * @return bool
     *
     * @throws ForbiddenException
     */
    public function haspermission($path)
    {
        list($module, $recurso, $roles) = $this->extractPathResources($path);

        // O usuario nao esta logado, porem a rota eh liberada para usuarios guest.
        if (is_null($this->getUser())) {
            if($module == 'password' && $recurso == 'reset') {
                return true;
            }

            if ($this->isPreLoginOpenActions($module, $recurso, $roles)) {
                return true;
            }

            return false;
        }

       if($module == 'notification') {
            return true;
        }

        // Verifica se a rota eh liberada pas usuarios logados.
        if ($this->isPostLoginOpenActions($module, $recurso, $roles)) {
            return true;
        }

        if ($this->hasrole($module.":".env('PROFILE_ADMIN'))) {
            return true;
        }

        // Verifica na base de dados se o perfil do usuario tem acesso ao recurso
        $hasPermission = $this->checkPermission($this->getUser()->getAuthIdentifier(), $module, $recurso, $roles);

        if ($hasPermission) {
            return true;
        }

        return false;
    }

    /**
     * Verifica se o usuário tem o perfil indicado (module:perfil)
     *
     * @param $expression
     * @return mixed
     */
    public function hasrole($expression)
    {
        $data = explode(':', $expression);

        $module = current($data);
        $perfil = next($data);

        return $this->getUser()->hasPerfil($module, $perfil);
    }

    /**
     * Verifica se a rota eh liberada para usuarios que nao estao logados no sistema.
     *
     * @param $module
     * @param $recurso
     * @param $roles
     *
     * @return bool
     */
    private function isPreLoginOpenActions($module, $recurso, $roles)
    {
        $fullRoute = $module.'/'.$recurso.'/'.$roles;

        $openActions = $this->app['config']->get('security.prelogin_openactions', []);

        return in_array($fullRoute, $openActions);
    }

    /**
     * Verifica se a rota eh liberada para usuarios que estao logados no sistema.
     *
     * @param $module
     * @param $recurso
     * @param $roles
     *
     * @return bool
     */
    private function isPostLoginOpenActions($module, $recurso, $roles)
    {
        $fullRoute = $module.'/'.$recurso.'/'.$roles;

        $openActions = $this->app['config']->get('security.postlogin_openactions', []);

        return in_array($fullRoute, $openActions);
    }

    /**
     * Verifica se o usuario tem acesso ao recurso.
     *
     * @param int    $usr_id
     * @param stirng $mod_name
     * @param string $rcs_name
     * @param string $prm_name
     *
     * @return mixed
     */
    private function checkPermission($usr_id, $mod_name, $rcs_name = 'index', $prm_name = 'index')
    {
        $sql = 'SELECT
                  r.id, r.name
                FROM roles r
                  INNER JOIN type_account_role tar ON tar.role_id = r.id
                  INNER JOIN resource re ON re.id = r.resource_id
                  INNER JOIN module m ON m.id = re.module_id
                WHERE m.name = :mod_name
                  AND re.name = :rcs_name
                  AND r.name = :prm_name
                  AND tar.type_account_id = (
                    SELECT ta.id
                    FROM type_account ta
                    INNER JOIN module mm ON mm.id = ta.module_id
                    INNER JOIN type_account_user tau ON tau.type_account_id = ta.id
                    WHERE tau.user_id = :usr_id AND mm.name = :modl_name
                  )';

        return DB::select(DB::raw($sql), [
            'mod_name' => $mod_name,
            'rcs_name' => $rcs_name,
            'prm_name' => $prm_name,
            'usr_id' => $usr_id,
            'modl_name' => $mod_name,
        ]);
    }

    /**
     * Gera um array com as partes da url -> module / recurso / roles.
     *
     * @param $fullPath
     *
     * @return array
     */
    private function extractPathResources($fullPath)
    {
        if (is_string($fullPath)) {
            $fullPath = explode('/', $fullPath);
        }

        $pathArray[0] = isset($fullPath[0]) ? $fullPath[0] : 'index';
        $pathArray[1] = isset($fullPath[1]) ? $fullPath[1] : 'index';
        $pathArray[2] = isset($fullPath[2]) ? $fullPath[2] : 'index';

        return $pathArray;
    }
}
