<?php

namespace App\Src\Security\Traits;

use DB;

trait HasPerfilTrait
{
    /**
     * Verifica se o usuário tem o perfil de um determinado módulo.
     *
     * @param $modulo
     * @param $perfil
     *
     * @return bool
     */
    public function hasPerfil($modulo, $perfil)
    {
        $sql = 'SELECT true FROM type_account p
                INNER JOIN type_account_user pu ON (p.id = pu.type_account_id AND pu.user_id = :usuarioid)
                INNER JOIN module m ON m.id = p.module_id AND LOWER(m.name) = LOWER(:modulo)
                WHERE LOWER(p.name) = LOWER(:perfil)';
        $data = ['usuarioid' => $this->getAuthIdentifier(), 'modulo' => $modulo, 'perfil' => $perfil];

        $hasPerfil = current(DB::select($sql, $data));

        if (!empty($hasPerfil)) {
            return true;
        }

        return false;
    }
    /**
     * Verifica se o usuário tem um determinado módulo.
     *
     * @param $modulo
     *
     * @return bool
     */
    public function hasModule($modulo)
    {
        $sql = 'SELECT true FROM type_account p
                INNER JOIN type_account_user pu ON (p.id = pu.type_account_id AND pu.user_id = :usuarioid)
                INNER JOIN module m ON m.id = p.module_id AND LOWER(m.name) = LOWER(:modulo)';
        $data = ['usuarioid' => $this->getAuthIdentifier(), 'modulo' => $modulo];

        $hasPerfil = current(DB::select($sql, $data));

        if (!empty($hasPerfil)) {
            return true;
        }

        return false;
    }

    /**
     * Relação nxm com os perfis do usuário.
     *
     * @return mixed
     */
    public function typeAccounts()
    {
        return $this->belongsToMany('App\Src\Model\TypeAccount', 'type_account_user', 'user_id', 'type_account_id');
    }
}
