<?php

namespace App\Observers;

use App\Models\Plataforma\TransactionMp;
use App\Events\PostbackEvent;

class PostbackObserver
{
    public function created(TransactionMp $transaction)
    {   
        event(new PostbackEvent($transaction));
    }


    public function updated(TransactionMp $transaction)
    {
        $original = $transaction->getOriginal();
        if ( $original['status'] == $transaction['status'] ) {
            return;
        }

        event(new PostbackEvent($transaction));
        
    }
}