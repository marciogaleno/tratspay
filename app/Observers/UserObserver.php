<?php

namespace App\Observers;

use App\User;
use App\Models\Plataforma\Client;
use App\Models\Plataforma\Account;

class UserObserver
{
    public function created(User $user)
    {   
        Client::create(['user_id' => $user->id]);

        Account::create([
            'balance' => 0,
            'user_id'=> $user->id
        ]);
    }
}