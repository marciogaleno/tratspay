<?php

namespace App\Observers;

use App\Mail\PedidoReembolsoMail;
use App\Models\Plataforma\Account;
use App\Models\Plataforma\Commission;
use App\Models\Plataforma\Notification;
use App\Models\Plataforma\Reembolso;
use App\Models\Plataforma\TransactionAccount;
use App\Models\Plataforma\TransactionMp;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ReembolsoObserver
{
    public function created(Reembolso $reembolso) 
    {
        $transaction = TransactionMp::find($reembolso->transaction_id);

        if(!$transaction) {
            return false;
        }

        $dataNotification = [
            'content' => $reembolso->observacao,
            'user_id' => $transaction->productAffiliate->user_id,
            'product_id'=> $transaction->product_id,
            'type' => 'refund',
            'first_name' => $transaction->first_name,
            'last_name' => $transaction->last_name,
            'email' => $transaction->email,
            'cpf_cnpj' => $transaction->cpf_cnpj,
            'phone' => $transaction->phone
        ];

        Notification::create($dataNotification);
        Mail::to($reembolso->user->email)
        ->send(new PedidoReembolsoMail($reembolso, 'client', 0.0 ));

        $producerAndAffiliates = DB::select("SELECT B.EMAIL, B.NAME, C.VALUE  FROM producer_affiliate A, USERS B, COMMISSIONS C
                                                WHERE 
                                                A.USER_ID = B.ID
                                                AND A.USER_ID = C.USER_ID
                                                AND C.transaction_id = {$reembolso->transaction_id}
                                                AND A.PRODUCT_ID = {$reembolso->product_id}");

        foreach ($producerAndAffiliates as $user) {
            Mail::to($user->email)
                ->send(new PedidoReembolsoMail($reembolso, null, $user->value));
        }

        $transaction = TransactionMp::where('id', $reembolso->transaction_id)->first();
        $transaction->last_status = $transaction->status;
        $transaction->status = "claimed";
        $transaction->update();


    }

    public function updated(Reembolso $reembolso)
    {   
        if ($reembolso['status'] == 'R')  {
            $transaction = TransactionMp::where('id', $reembolso['transaction_id'])->first();
            $transaction->status = $transaction->last_status;
            $transaction->update();
        }
        

        try {
            if ($reembolso['status'] == 'A') {
                DB::beginTransaction();

                $transactionAcoounts = TransactionAccount::where('transaction_id', $reembolso['transaction_id'])->get()->toArray();
                
                if (!empty($transactionAcoounts)) {
                    foreach ($transactionAcoounts as $transactionAccount) {
                        $destination_account = $transactionAccount['destination_account'];
                        $transactionAccount['type'] = 'refund';
                        $transactionAccount['type_transaction'] = 'OUT';
                        unset($transactionAccount['created_at']);
                        unset($transactionAccount['updated_at']);

                        TransactionAccount::create($transactionAccount);

                        $account = Account::where('id', $destination_account)->first();
                        $account->balance = $account->balance - $transactionAccount['value'];
                        $account->update();
                    }
                }

                $transaction = TransactionMp::where('id', $reembolso['transaction_id'])->first();
                $transaction->status = 'refunded';
                $transaction->update();

                DB::commit();
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            DB::rollBack();
            throw $ex;
        }

    }
}