<?php

namespace App\Observers;

use App\Models\Plataforma\Notification;
use App\Models\Plataforma\TransactionMp;
use App\Events\ProductSold;
use Symfony\Component\EventDispatcher\Event;
use App\Services\UnlockProductService;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\Models\Plataforma\Commission;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;

class TransactionObserver
{
    public function creating(TransactionMp $transaction)
    {
        // caso o pagamento seja aprovado cria uma da data fim da garantia
        if ($transaction->status_gateway == 'approved') {
            $transaction->end_warranty  =  date('Y-m-d H:m:s', strtotime('+' . $transaction->warranty . ' days'));
        }
       
    }
    public function created(TransactionMp $transaction)
    {
//        $commissionProducer = Commission::where('transaction_id', $transaction->id)
//            ->where('commission_type', '=', 'producer')
//            ->first();
//
//        dd($commissionProducer);
//        $dataNotification = [
//            'content' => 'Sua comissão foi '.$commissionProducer->value,
//            'user_id' => $transaction->productAffiliate->user_id,
//            'product_id'=> $transaction->product_id,
//            'type' => 'sale',
//            'first_name' => $transaction->first_name,
//            'last_name' => $transaction->last_name,
//            'email' => $transaction->email,
//            'cpf_cnpj' => $transaction->cpf_cnpj,
//            'phone' => $transaction->phone
//        ];
//
//        Notification::create($dataNotification);
        event(new ProductSold($transaction));
    }

    public function updating(TransactionMp $transaction)
    {
        $original = $transaction->getOriginal();
        if ( !($original['status_gateway'] != $transaction['status_gateway']) && !($original['status_detail_gateway'] != $transaction['status_detail_gateway']) ) {
            return;
        }

        if ($transaction['status_gateway'] == 'approved' && $transaction['status_detail_gateway'] == 'accredited' ) {       
            $transaction['end_warranty'] =  date('Y-m-d H:m:s', strtotime('+' . $transaction['warranty'] . ' days'));
            $transaction['status'] =  'finished';
        }

    }

    public function updated(TransactionMp $transaction)
    {
        $original = $transaction->getOriginal();
        if ( ($original['status_gateway'] == $transaction['status_gateway']) && ($original['status_detail_gateway'] == $transaction['status_detail_gateway']) ) {
            return;
        }

        if ($transaction['status_gateway'] == 'approved' && $transaction['status_detail_gateway'] == 'accredited' ) {
            
            $unlockProdutoService = new UnlockProductService($transaction);
            $unlockProdutoService->unlock();
            
            $commissions = Commission::where('transaction_id', $transaction->id)
                            ->where('commission_type', '<>', 'system')
                            ->get();

            foreach ($commissions as $commission) {
                $user = User::find($commission->user_id);
                $typePyament = $transaction->payment_type_id;
        
                $subject = 'Venda Realizada! - ' .  $transaction->product_name;

                Mail::to($user->email)
                    ->send(new SendMail($commission, $transaction));
            }
        }
 
    }
}