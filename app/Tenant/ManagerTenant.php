<?php

namespace App\Tenant;
use Illuminate\Support\Facades\Auth;

class ManagerTenant
{
    public function getTenantIdentify($guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return auth()->user()->tenant->id;
        }
    }
}