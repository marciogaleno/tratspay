<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\Models\Plataforma\Product;
use App\Models\Plataforma\Commission;
use App\Models\Plataforma\TransactionMp;

class SendMail extends Mailable 
//implements ShouldQueue  
{
    //use Queueable, SerializesModels;

    private $user;
    private $commission;
    private $transaction;
    private $subjectText;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Commission $commission, TransactionMp $transaction)
    {
        $this->commission = $commission;
        $this->transaction = $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $commissionsAffiliates = Commission::where('transaction_id',  $this->transaction->id)
                            ->where('commission_type', 'affiliate')
                            ->get();

        $source =  json_decode($this->commission->source);
        $view = 'sale';

        $typePyament = $this->transaction->payment_type_id;
        $subject = 'Venda Realizada! - ' .  $this->transaction->product_name;

        if ($this->transaction->status_gateway == 'in_process') {
            return;
        }

        if ($typePyament == 'credit_card' && $this->transaction->status_gateway == 'approved'
            && $this->transaction->status_detail_gateway == 'accredited') {
            $subject = 'Venda realizada - ' . $this->transaction->product_name;
            $view = 'sale';
        }

        if ($typePyament == 'ticket' && $this->transaction->status_gateway == 'pending'
            && $this->transaction->status_detail_gateway == 'pending_waiting_payment') {
            $subject = 'Boleto gerado - ' . $this->transaction->product_name;
            $view = 'ticket';
        }

        
        return $this->view('emails.' . $view)
                ->subject($subject)
                ->with([
                    'commission' =>  $this->commission,
                    'transaction' => $this->transaction,
                    'commissionsAffiliates' => $commissionsAffiliates,
                    'source' =>  $source
                ]);
    }
}
