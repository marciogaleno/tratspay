<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Plataforma\Reembolso;
use App\Models\Plataforma\TransactionMp;

class PedidoReembolsoMail extends Mailable implements ShouldQueue  
{
    //use Queueable, SerializesModels;

    private $reembolso;

    private $type = null;

    private $value = 0.0;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Reembolso $reembolso, $type = null, $value = 0.0)
    {
        $this->reembolso = $reembolso;
        $this->type = $type;
        $this->value = $value;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $subject = "Solicitação de pedido de reembolso";

        return $this->view('emails.reembolso')
                ->subject($subject)
                ->with([
                    'type' => $this->type,
                    'value' => $this->value,
                    'reembolso' => $this->reembolso
                ]);
    }
}
