<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailUpdate extends Mailable
{
    // use Queueable, SerializesModels;

    private $subjectText;
    private $user;
    private $email = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subjectText, User $user, $email)
    {
        $this->subjectText = $subjectText;
        $this->user = $user;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = 'emails.update-email';

        return $this->view($view)
                ->subject($this->subjectText)
                ->with([
                    'user' => $this->user,
                    'email' => $this->email
                ]);
    }
}
