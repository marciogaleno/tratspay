<?php

namespace App\Mail;

use App\Models\Plataforma\Client;
use App\Models\Plataforma\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Plataforma\TransactionMp;
use Illuminate\Support\Str;

class SendMailBuyer extends Mailable 
//implements ShouldQueue
{
    //use Queueable, SerializesModels;

    private $transaction;
    private $subjectText;
    private $isUserNew = false;
    private $buyApproved = false;
    private $password = null;
    private $producer = '';
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subjectText, $isUserNew, $buyApproved, $password, TransactionMp $transaction)
    {
        $this->transaction = $transaction;
        $this->subjectText = $subjectText;
        $this->isUserNew = $isUserNew;
        $this->buyApproved = $buyApproved;
        $this->password = $password;

        $client = Client::where('user_id', $transaction->product->producer[0]->id)->withoutGlobalScopes()->first();
        if ($client && !empty($client->fantasia)) {
            $this->producer = $client->fantasia;
        } else {
            $this->producer =  $transaction->product->producer[0]->name;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {   
        $view = 'emails.buy-ticket';

        if ($this->buyApproved) {
            $view = 'emails.buy-approved';
        }

        return $this->view($view)
                ->subject($this->subjectText)
                ->with([
                    'transaction' => $this->transaction,
                    'isUserNew' => $this->isUserNew,
                    'password' => $this->password,
                    'producer' => $this->producer
                ]);
    }
}
