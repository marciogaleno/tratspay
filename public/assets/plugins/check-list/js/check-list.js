const EL = '.check-list'
const ACTION = '.check-list-action';
const ITEM = '.check-list-item';
const ALL = '.check-list-all';

class CheckList {
    constructor(options = {}) {
        this.el = options.el || EL
        this.obj = document.querySelector(this.el)

        this.actions = this.actions(options.action)
        this.items = this.items(options.item)

        this.checkall = this.obj.querySelector(options.checkall || ALL)

        this.methods || {};
    }

    build() {
        this.actions.forEach(action => {
            action.addEventListener('click', this.onAction.bind(this));
        });

        this.items.forEach(item => {
            item.addEventListener('click', this.onCheckItem.bind(this));
        });

        this.checkall.addEventListener('click', this.onCheckAll.bind(this));
    }

    actions(action = null) {
        return action ? this.obj.querySelectorAll(action) : this.obj.querySelectorAll(ACTION)
    }

    items(item = null) {
        return item ? this.obj.querySelectorAll(item) : this.obj.querySelectorAll(ITEM)
    }

    onAction(event) {
        var method = event.target.getAttribute('chk-on');
        this.methods[method](event.target, this._itemChecked(), this.obj);

        event.preventDefault();
    }

    onCheckAll(event) {
        if (event.target.checked) {
            this.items.forEach( item => {
                item.checked = event.target.checked
            })
        } else {
            this.items.forEach(item => {
                item.checked = false
            })
        }
    }

    onCheckItem(event) {
        if (!event.target.checked) {
            this.checkall.checked = false
        }
    }

    _itemChecked() {
        var checkeds = [];
        this.items.forEach(item => {
            if (item.checked) {
                checkeds.push(item)
            }
        });

        return checkeds;
    }
    
}