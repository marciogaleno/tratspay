var inputImage = $("#mage");
var inputProductType = $("#product_type");
var inputName = $("#name");
var inputCategoryId = $("#category_id");
var inputDesc = $("#desc");
var inputEmailSupport = $("#email_support");
var inputTypeDeliverieId = $("#type_deliverie_id");
var inputLocalPageSale = $("#local_page_sale");
var inputUrlPageSale = $("#url_page_sale");
var inputUrlPageThank = $("#url_page_thank");
var inputUrlPagePrintTicket = $("#url_page_print_ticket");
var inputSeveralPurchases = $("#several_purchases");
var inputPaymentType = $("#payment_type");
var inputWarranty = $("#warranty");
var inputPriceUniquePlan = $("#price_unique");
var inputAcceptAffiliation = $("#accept_affiliation");
var inputValueCommission = $("#value_commission");
var inputExpirationCoockie = $("#expiration_coockie");
var inputFormatCommission = $('#format_commission');

$('#buttonNext01, #tabSales').click( function() {
   var valid = validationAba01();
   
   if (valid) {
      $(function () {
         $('#tabSales').tab('show')
      })
   }

   $("html, body").animate({ scrollTop: 0 }, "slow");
});

$('#buttonNext02, #tabAffiliation').click( function() {
   var valid = validationAba02();
   
   if (valid) {
      $(function () {
         $('#tabAffiliation').tab('show')
      })
   }

   $("html, body").animate({ scrollTop: 0 }, "slow");
});

$('#formAddProduct').submit( function() {

   var valid = validationAba03();
   
   if (valid) {
      return true;
   }

   $("html, body").animate({ scrollTop: 0 }, "slow");

   return false;
});



cleanValidationFormGroup = function (idInput) {
    idInput.closest('.form-group').removeClass('has-error');
    var label = "#" + 'label_' + idInput.attr('id');
    $(label).text("");
}

validationAba01 = function () {
    valid = true;

    /*image = $('input[type=file]').val().split('\\').pop();
       if (!image) {
        $("#label_imagem").text("É obrigatório uma imagem do produto.");
        valid = false;
      }*/

       if (!inputProductType.val()) {
        inputProductType.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_product_type").text("Este campo é obrigatório.");
        valid = false;
      }

      if (!inputName.val()) {
        inputName.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_name").text("Este campo é obrigatório.");
        valid = false;
      }
      if (!inputCategoryId.val()) {
        inputCategoryId.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_category_id").text("Este campo é obrigatório.");
        valid = false;
      }
      if (!inputDesc.val()) {
        inputDesc.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_desc").text("Este campo é obrigatório.");
        valid = false;
      }
      if (!inputEmailSupport.val()) {
        inputEmailSupport.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_email_support").text("Este campo é obrigatório.");
        valid = false;
      }
      if (!inputTypeDeliverieId.val()) {
        inputTypeDeliverieId.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_type_deliverie_id").text("Este Este campo é obrigatório.");
        valid = false;
      }
      if (!inputLocalPageSale.val()) {
        inputLocalPageSale.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_local_page_sale").text("Este Este campo é obrigatório.");
        valid = false;
      }

      if (inputLocalPageSale.val() === 'PE' && !inputUrlPageSale.val()) {
        inputUrlPageSale.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_url_page_sale").text("Este Este campo é obrigatório.");
        valid = false;
      }

      if (!inputSeveralPurchases.val()) {
        inputSeveralPurchases.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_several_purchases").text("Este Este campo é obrigatório.");
        valid = false;
      }

      return valid;
}

validationAba02 = function () {
    valid = true;
    
    if (!inputPaymentType.val()) {
        inputPaymentType.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_payment_type").text("Este campo é obrigatório.");
        valid = false;
    }

    if (!inputWarranty.val()) {
        inputWarranty.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_warranty").text("Este campo é obrigatório.");
        valid = false;
    }
    
    if (inputPaymentType.val() == 'SINGPRICE' &&  !inputPriceUniquePlan.val()) {
        console.log('aqui');
        inputPriceUniquePlan.closest('.form-group').removeClass('has-success').addClass('has-error');
        $("#label_price_unique").text("Este campo é obrigatório.");
        valid = false;        
    }
      
    return valid;
}

validationAba03 = function () {
    valid = true;

    if (inputAcceptAffiliation.val() == 1) {

        if (!inputFormatCommission.val()) {
            inputFormatCommission.closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#label_format_commission").text("Este campo é obrigatório.");
            valid = false;
        }

        if (!inputValueCommission.val()) {
            inputValueCommission.closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#label_value_commission").text("Este campo é obrigatório.");
            valid = false;
        }

        if (!inputExpirationCoockie.val()) {
            inputExpirationCoockie.closest('.form-group').removeClass('has-success').addClass('has-error');
            $("#label_expiration_coockie").text("Este campo é obrigatório.");
            valid = false;
        }
        
    }

 
      
    return valid;
}


inputProductType.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputProductType);
  } 
});

inputName.keyup(function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputName);
  }
});

inputCategoryId.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputCategoryId);
  }
});

inputDesc.keyup(function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputDesc);
  }
});

inputEmailSupport.keyup(function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputEmailSupport);
  }
});

inputTypeDeliverieId.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputTypeDeliverieId);
  }
});

inputSeveralPurchases.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputSeveralPurchases);
  }
});

inputUrlPageSale.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputUrlPageSale);
  }
});




/* ######################################## SEGUNDA ABA ########################### */


inputPaymentType.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputPaymentType);
  } 
});

inputWarranty.on('change', function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputWarranty);
  }
});


inputPriceUniquePlan.keyup(function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputPriceUniquePlan);
  } 
});

/* ############################################## TERCEIRA ABA ##########################*/

inputFormatCommission.on('change', function() {
  if ($(this).val()) {
    cleanValidationFormGroup(inputFormatCommission);
  }
});

inputExpirationCoockie.on('change', function() {
  if ($(this).val()) {
    cleanValidationFormGroup(inputExpirationCoockie);
  }
});

inputValueCommission.keyup(function() {
  if ($(this).val()) {
     cleanValidationFormGroup(inputValueCommission);
  } 
});


