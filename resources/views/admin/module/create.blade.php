@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Módulos</h6>
    </div>
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header bprder-0">
                    Novo Módulo
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'admin.module.create']) !!}
                    <div class="row">
                        <div class="col-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="Nome">Nome</label>
                                {!! Form::text('name', null, ['class' => 'form-control sty1', 'placeholder' => 'Nome do módulo', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="Icone">Icone <small><a href="http://codervent.com/fobia-admin-v2/demo/color-admin/icons-font-awesome.html" target="_blank" title="Abrir lista de icones" data-toggle="tooltip"><i class="fa fa-share-square"></i></a></small></label>
                                {!! Form::text('icon', null, ['class' => 'form-control sty1', 'placeholder' => 'Ex.: fa fa-home', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="Descrição">Descrição</label>
                                {!! Form::text('label', null, ['class' => 'form-control sty1', 'placeholder' => 'Descrição do módulo', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="Status">Status</label>
                                {!! Form::select('enable', ['1' => 'Ativo', '0' => 'Inativo'], [], ['class' => 'custom-select form-control', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::button('Criar Módulo', ['type'=> 'submit','class' => 'btn btn-primary waves-effect waves-light']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection