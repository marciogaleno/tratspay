@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Módulos</h6>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header bprder-0">
            <a href="{{ route('admin.module.create') }}" class="btn btn-success waves-effect waves-light m-1">Novo módulo</a>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Nome</th>
                  <th>Descrição</th>
                  <th class="text-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($modules as $module)
                <tr>
                  <td>{{ ($module->enable) ? 'Ativo' : 'Inativo' }}</td>
                  <td>{{ $module->name }}</td>
                  <td>{{ $module->label }}</td>
                  <td class="col-1 text-center">
                      <form action="{{ route('admin.module.delete')  }}" method="POST">
                          @csrf()
                          <input type="hidden" name="id" value="{{ $module->id}}" />
                          <button type="submit" title="Excluir registro" data-toggle="tooltip" data-placement="left" class="btn btn-danger waves-effect waves-light"><i class="fa fa-remove"></i></button>
                      </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection