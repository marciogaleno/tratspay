@extends('admin.templates.template')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body"> 
        <div class="row">
          <div class="col-md-12">
              <!-- Menu Tab panes -->
              <ul class="nav nav-tabs nav-tabs-warning nav-justified top-icon">
                <li class="nav-item">
                  <a class="nav-link py-4 active" data-toggle="tab" href="#tabe-produtor">
                    <i class="icon-diamond"></i>
                    <span class="hidden-xs">Produtor</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link py-4" data-toggle="tab" href="#tabe-afiliado">
                    <i class="icon-share"></i>
                    <span class="hidden-xs">Afiliado</span>
                  </a>
                </li>
              </ul>
              <!-- Tab panes -->
              <div class="tab-content">
                <!-- Produtor -->
                <div id="tabe-produtor" class="container tab-pane active">
                  <div class="row">
                    <div class="col-lg-12">
                      @if($produtores->count())
                        <div class="card check-list">
                          <div class="card-header d-flex align-items-center">
                            <div class="icheck-material-primary m-5" data-toggle="tooltip" data-placement="top" title="Marcar todos" data-original-title="Marcar todos">
                              <input type="checkbox" class="check-list-all" id="account" name="account" value="all">
                              <label for="account">&nbsp;</label>
                            </div>
                            <div class="btn-group m-1" role="group">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Ações
                                </button>
                                <div class="dropdown-menu">
                                  <a href="#" class="dropdown-item check-list-action" chk-on="confirm">Confirmar todos</a>
                                  <a href="#" class="dropdown-item check-list-action" chk-on="cancel">Recusar todos</a>
                                </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="">
                                    <div class="">
                                      <table class="table">
                                        <thead>
                                          <tr>
                                            <th scope="col">&nbsp;</th>
                                            <th scope="col">Nome</th>
                                            <th scope="col">Banco</th>
                                            <th scope="col">Agência</th>
                                            <th scope="col">Conta</th>
                                            <th scope="col-1">&nbsp;</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($produtores as $user)
                                          <tr>
                                            <td class="col-1 text-center">
                                              <div class="icheck-material-primary">
                                                <input type="checkbox" class="check-list-item" id="account-{{ $user->id }}" value="{{ $user->id }}">
                                                <label for="account-{{ $user->id }}">&nbsp;</label>
                                              </div>
                                            </td>
                                            <td>{{ $user->user->name }}</td>
                                            <td>{{ \App\Helpers\Helper::ListBank($user->type) }}</td>
                                            <td>{{ sprintf("%s-%s", $user->bank_branch, $user->bank_branch_digit) }}</td>
                                            <td>{{ sprintf("%s-%s", $user->bank_account, $user->bank_account_digit) }}</td>
                                            <td class="text-right">
                                              <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-inverse-dark waves-effect waves-light" data-toggle="dropdown" id="dropdownMenu{{ $user->id }}">
                                                  <span class="fa fa-ellipsis-v" aria-hidden="true"></span>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenu{{ $user->id }}">
                                                  <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-check"></i> Confirmar</a>
                                                  <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-close"></i> Recusar</a>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      @else
                        <p>Sem produtores</p>
                      @endif
                    </div>
                  </div>
                </div>
                <!-- Produtor Fim -->
                <!-- Afiliado -->
                <div id="tabe-afiliado" class="container tab-pane">
                  <div class="row">
                    <div class="col-lg-12">
                      @if($afiliados->count())
                        <div class="card check-list">
                          <div class="card-header d-flex align-items-center">
                            <div class="icheck-material-primary m-5" data-toggle="tooltip" data-placement="top" title="Marcar todos" data-original-title="Marcar todos">
                              <input type="checkbox" class="check-list-all" id="account" name="account" value="all">
                              <label for="account">&nbsp;</label>
                            </div>
                            <div class="btn-group m-1" role="group">
                                <button type="button" class="btn btn-outline-secondary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Ações
                                </button>
                                <div class="dropdown-menu">
                                  <a href="#" class="dropdown-item check-list-action" chk-on="confirm">Confirmar todos</a>
                                  <a href="#" class="dropdown-item check-list-action" chk-on="cancel">Recusar todos</a>
                                </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <div class="col-lg-12">
                                <div class="">
                                    <div class="">
                                      <table class="table">
                                        <thead>
                                          <tr>
                                            <th scope="col">&nbsp;</th>
                                            <th scope="col">Nome</th>
                                            <th scope="col">Banco</th>
                                            <th scope="col">Agência</th>
                                            <th scope="col">Conta</th>
                                            <th scope="col-1">&nbsp;</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @foreach($afiliados as $user)
                                          <tr>
                                            <td class="col-1 text-center">
                                              <div class="icheck-material-primary">
                                                <input type="checkbox" class="check-list-item" id="account-{{ $user->id }}" value="{{ $user->id }}">
                                                <label for="account-{{ $user->id }}">&nbsp;</label>
                                              </div>
                                            </td>
                                            <td>{{ $user->user->name }}</td>
                                            <td>{{ \App\Helpers\Helper::ListBank($user->type) }}</td>
                                            <td>{{ sprintf("%s-%s", $user->bank_branch, $user->bank_branch_digit) }}</td>
                                            <td>{{ sprintf("%s-%s", $user->bank_account, $user->bank_account_digit) }}</td>
                                            <td class="text-right">
                                              <div class="btn-group dropleft">
                                                <button type="button" class="btn btn-inverse-dark waves-effect waves-light" data-toggle="dropdown" id="dropdownMenu{{ $user->id }}">
                                                  <span class="fa fa-ellipsis-v" aria-hidden="true"></span>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenu{{ $user->id }}">
                                                  <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-check"></i> Confirmar</a>
                                                  <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-close"></i> Recusar</a>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          @endforeach
                                        </tbody>
                                      </table>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      @else
                        <p>Sem afiliados</p>
                      @endif
                    </div>
                  </div>
                </div>
                <!-- Afiliado Fim-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection