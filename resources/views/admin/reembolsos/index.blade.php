@extends('plataforma.templates.template')
@section('content')

    <div class="card" style="">
        <div class="card-header">Lista de Reembolsos</div>
        <div class="card-body">
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="table-responsive" style="">
                        @if(count($reembolsos))
                            <table class="table table-sm">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Transação</th>
                                    <th scope="col">Status</th>
                                    <th class="text-center">AÇÃO</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reembolsos as $reembolso)
                                    <tr>
                                        <td>{{$reembolso->id}}</td>
                                        <td style="width: 30%">{{$reembolso->product->name}}</td>
                                        <td style="width: 30%">{{$reembolso->user->name}}</td>                                      
                                        <td style="width: 30%">{{$reembolso->transaction_id}}</td>
                                        <td>
                                            @if($reembolso->status == "A")
                                                <p><i class="fa fa-circle text-info"></i> APROVADO</p>
                                            @elseif($reembolso->status == "R")
                                                <p><i class="fa fa-circle text-danger"></i> RECUSADO</p>
                                            @else
                                                <p><i class="fa fa-circle text-warning"></i> PENDENTE</p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{route('admin.reembolso.view', $reembolso->id)}}" class="btn btn-primary btn-sm"> Visualizar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="col-md-12">
                                <h6 class="text-center">Nenhum registro encontrado</h6>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
