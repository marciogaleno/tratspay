@extends('admin.templates.template')

@section('content')
<div class="row">
  <div class="col-lg-12">
  @if($products->count())
    <div class="card check-list">
      <div class="card-header d-flex align-items-center">
        <div class="icheck-material-primary m-5" data-toggle="tooltip" data-placement="top" title="Marcar todos" data-original-title="Marcar todos">
          <input type="checkbox" class="check-list-all" id="account" name="account" value="all">
          <label for="account">&nbsp;</label>
        </div>
        <div class="btn-group m-1" role="group">
            <button type="button" class="btn btn-outline-secondary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Ações
            </button>
            <div class="dropdown-menu">
              <a href="#" class="dropdown-item check-list-action" chk-on="confirm">Confirmar todos</a>
              <a href="#" class="dropdown-item check-list-action" chk-on="cancel">Recusar todos</a>
            </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="">
                <div class="">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">&nbsp;</th>
                        <th scope="col">Produto</th>
                        <th scope="col">Nome</th>
                        <th scope="col" class="text-center">Tipo</th>
                        <th scope="col" class="text-center">Categoria</th>
                        <th scope="col">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($products as $product)
                      <tr>
                        <td class="col-1 text-center">
                          <div class="icheck-material-primary">
						                <input type="checkbox" class="check-list-item" id="account-{{ $product->id }}" value="{{ $product->id }}">
						                <label for="account-{{ $product->id }}">&nbsp;</label>
					                </div>
                        </td>
                        <td class="col-1">
                          <figure class="figure">
                            <img class="figure-img img-fluid rounded" src="{{ \App\Helpers\Helper::imageProduct($product) }}" alt="{{ $product->name }}" title="{{ $product->name }}">
                          </figure>
                        </td>
                        <td class="col-3" style="white-space: normal;">
                          <div>
                            <p class="text-uppercase font-weight-bold">{{ $product->name }}</p>
                            <p>{{ str_limit($product->desc) }}</p>
                          </div>
                          <div><small>Criado: {{ date('d/m/Y', strtotime($product->created_at)) }}</small> - <small>Atualizado: {{ date('d/m/Y', strtotime($product->updated_at)) }}</small></div>
                        </td>
                        <td class="text-center align-middle">{{ \App\Helpers\Helper::productTypes($product->product_type) }}</td>
                        <td class="text-center align-middle">{{ $product->categorie->desc }}</td>
                        <td class="text-right">
                          <div class="btn-group dropleft">
                            <button type="button" class="btn btn-inverse-dark waves-effect waves-light" data-toggle="dropdown" id="dropdownMenu{{ $product->id }}">
                              <span class="fa fa-ellipsis-v" aria-hidden="true"></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu{{ $product->id }}">
                              <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-check"></i> Confirmar</a>
                              <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-close"></i> Recusar</a>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @else
    <p>Sem produtos</p>
  @endif
  </div>
</div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/plugins/check-list/js/check-list.js') }}"></script>
  <script>

    var checklist = new CheckList({
      el: '.check-list',
      action: '.check-list-action',
      item: '.check-list-item',
      checkall: '.check-list-all' 
    })

    checklist.methods = {
      'confirm': (target, items, el) => {
        console.log(items);
      },
      'cancel': (target, items, el) => {
        console.log(el);
      }
    }

    checklist.build()

  </script>
@endsection