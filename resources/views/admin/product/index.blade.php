@extends('admin.templates.template')

@section('content')
<div class="row">
  <div class="col-lg-12">
  @if($products->count())
    <div class="card check-list">
      <div class="card-header d-flex align-items-center">

        <div class="icheck-material-primary" data-toggle="tooltip" data-placement="top" title="Marcar todos" data-original-title="Marcar todos">
          <input type="checkbox" class="check-list-all" id="account" name="account" value="all">
          <label for="account">&nbsp;</label>
        </div>

        <div class="ml-3" role="group">
            <a href="" class="text-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="fa-2x fa fa-ellipsis-v" aria-hidden="true"></span>
            </a>
            <div class="dropdown-menu">
              <a href="#" class="dropdown-item check-list-action" chk-on="confirm">Confirmar todos</a>
              <a href="#" class="dropdown-item check-list-action" chk-on="cancel">Recusar todos</a>
            </div>
        </div>

        <div class="ml-4" role="group">
            <a href="" class="text-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="top" title="Classificar por" data-tooltip="true">
              <i class="fa-2x fa fa-tags"></i>
            </a>
            <div class="dropdown-menu">
              <a href="#" class="dropdown-item check-list-action" chk-on="best-sellers">Mais vendidos</a>
              <a href="#" class="dropdown-item check-list-action" chk-on="less-sold">Menos vendidos</a>
            </div>
        </div>

        <div class="ml-4 dropdown" role="group">
            <a href="" class="text-secondary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="top" title="Filtrar por categoria" data-tooltip="true">
              <i aria-hidden="true" class="fa-2x fa fa-filter"></i>
            </a>
            <div class="dropdown-menu pt-0" style="max-height: 400px;">
              <ul class="navbar-nav mr-auto align-items-center">
                <li class="nav-item">
                  <form class="search-bar ml-0">
                    <input type="text" class="form-control input-filter" placeholder="Filtrar por categoria">
                    <a href="javascript:void();"><i class="fa fa-filter"></i></a>
                  </form>
                </li>
              </ul>
              <div class="content-filter" style="overflow-y: auto;max-height: 362px;">
              @foreach($categories as $categorie)
              <a href="#" class="dropdown-item line">{{ $categorie->desc }}</a>
              @endforeach
              </div>
            </div>
        </div>

        <ul class="navbar-nav mr-auto align-items-center">
          <li class="nav-item">
            <form class="search-bar">
              <input type="text" class="form-control" placeholder="ID de Produto [Press Enter]">
              <a href="javascript:void();"><i class="icon-magnifier"></i></a>
            </form>
          </li>
        </ul>

        <div class="d-flex">
            <span class="ml-3 text-secondary">1-{{ $products->perPage() }} de {{ $products->total() }}</span>
            <a href="{{ $products->previousPageUrl() }}" class="text-dark ml-3 {{ !$products->onFirstPage() ?: 'disabled' }}" data-toggle="tooltip" data-placement="bottom" title="Anterior"><i class="fa fa-angle-left"></i></a>
            <a href="{{ $products->nextPageUrl() }}" class="text-dark ml-4 {{ $products->hasMorePages() ?: 'disabled' }}" data-toggle="tooltip" data-placement="bottom" title="Próximo"><i class="fa fa-angle-right"></i></a>
        </div>

      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="">
                <div class="">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">&nbsp;</th>
                        <th scope="col">ID</th>
                        <th scope="col">Imagem</th>
                        <th scope="col">Nome do Produto</th>
                        <th scope="col">Produtor</th>
                        <th scope="col">Vendas</th>
                        <th scope="col">Reembolsos</th>
                        <th scope="col">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($products as $product)
                      <tr>
                        <td class="col-1 align-middle">
                          <div class="icheck-material-primary">
						                <input type="checkbox" class="check-list-item" id="account-{{ $product->id }}" value="{{ $product->id }}">
						                <label for="account-{{ $product->id }}">&nbsp;</label>
					                </div>
                        </td>
                        <td class="col-1 align-middle">{{ $product->id }}</td>
                        <td class="col-1 align-middle">
                          <figure class="figure">
                            <img class="figure-img img-fluid rounded" src="{{ \App\Helpers\Helper::imageProduct($product) }}" alt="{{ $product->name }}" title="{{ $product->name }}">
                          </figure>
                        </td>
                        <td>{{ $product->name }}</td>
                        <td class="col-2 align-middle">dsdsd</td>
                        <td class="col-1 align-middle">dsdsd</td>
                        <td class="col-1 align-middle">dsdsd</td>
                        <td class="text-right">
                          <div class="dropleft">
                            <a href="" class="text-dark" data-toggle="dropdown" id="dropdownMenu{{ $product->id }}">
                              <span class="fa fa-2x fa-ellipsis-v" aria-hidden="true"></span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu{{ $product->id }}">
                              <a href="{{route('admin.product.edit',$product->id)}}" class="dropdown-item"><i class="fa fa-pencil"></i> Editar</a>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @else
    <p>Sem produtos</p>
  @endif
  </div>
</div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/plugins/check-list/js/check-list.js') }}"></script>
  <script>

    var checklist = new CheckList({
      el: '.check-list',
      action: '.check-list-action',
      item: '.check-list-item',
      checkall: '.check-list-all' 
    })

    checklist.methods = {
      'confirm': (target, items, el) => {
        console.log(items);
      },
      'cancel': (target, items, el) => {
        console.log(el);
      }
    }

    checklist.build()
  </script>
@endsection