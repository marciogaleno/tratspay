@extends('plataforma.templates.template')

@section('styles')
<link rel="stylesheet" href="{{asset('dist/css/tabs.css')}}">
@endsection

@section('content')

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header text-uppercase">Solicitações</div>
        <div class="card-body">
          <ul class="list-unstyled">
            <div class="col-lg-12" style="">
              <div class="table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th scope="col">ID</th>
                      <th scope="col">Favorecido</th>
                      <th scope="col">CPF/CNPJ</th>
                      <th scope="col">E-mail</th>
                      <th scope="col">Valor</th>
                      <th scope="col">Banco</th>
                      <th scope="col">Agência</th>
                      <th scope="col">Conta Bancária</th>
                      <th scope="col">Data</th>
                      <th scope="col">status</th>
                      <th scope="col">Ações</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($cashouts as $cashout)
                          <tr>
                            
                            <td scope="row">{{$cashout->cashout_id}}</th>
                            <td>{{$cashout->name_user}}</th>
                            <td>{{$cashout->cpf_cnpj}}</th>
                            <td>{{$cashout->email}}</th>
                            <td>R$ {{$cashout->value}}</th>
                            <td>{{$cashout->bank_name}}</td>
                            <td>
                              {{$cashout->bank_branch . '-' . $cashout->bank_branch_digit}}
                            </td>
                            <td>
                              {{$cashout->bank_account . '-' . $cashout->bank_account_digit}}
                            </td>
                            <td>{{\App\Helpers\Helper::dateToBr($cashout->date_solicitation, true)}}</td>
                            <td>{{\App\Helpers\Helper::statusCashout($cashout->status_cashout)}}</td>
                            <td>
                              <td>
                                {!! Form::open(['route' => ['admin.cashout.process', $cashout->cashout_id], 'method' => 'PUT', 'onSubmit' => "return validate(this)"]) !!}
                                        <button class="btn btn-success btn-sm"  onclick="return confirm('Tem certeza disso?')">Processar</button>
                                {!! Form::close() !!}
                            </td>

                            </td>
                            
      
                          </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </ul>
        </div>
      </div>
    </div>
  </div>

  @endsection