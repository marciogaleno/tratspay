<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ColinaPay - Painel </title>
    <!-- Responsivo -->
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
    <!-- notifications css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/notifications/css/lobibox.min.css')}}"/>
    <!-- Vector CSS -->
    <link href="{{asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
    <!-- simplebar CSS-->
    <link href="{{asset('assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
    <!-- Bootstrap core CSS-->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="{{asset('assets/css/sidebar-menu.css')}}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{asset('assets/css/app-style.css')}}" rel="stylesheet"/>

@yield('css-dashboard')
@yield('css-cadastro-produto')
@yield('styles')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>



    <![endif]-->
    <style>
        .loader {
            border: 10px solid #f3f3f3; /* Light grey */
            border-top: 10px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 100px;
            height: 100px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .table, .small, small {
            font-size: 90%;
            font-weight: 400;
        }

        .table thead th {
            font-size: 0.7rem;
        }

        .card .table td, .card .table th {
            padding-right: 0.5rem;
            padding-left: 0.5rem;
        }

        .btn-sm {
            padding: 4px 10px;
        }

        a.disabled, a.disabled:hover {
            cursor: default;
            color: #ccc !important;
        }
    </style>

</head>

<body>
<!-- Start wrapper-->
<div id="wrapper">
    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
        <div class="brand-logo">
            <a href="{{ route('admin.index') }}">
                <p> <center> <img src="{{asset('assets/images/logo-trats-mini.png')}}" >
            </a>
        </div>
        <div class="user-details">
            <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
                <div class="avatar"><img class="mr-3 side-user-img" src="{!! \App\Helpers\Helper::imageProfile() !!}" alt="user avatar"></div>
                <div class="media-body">
                    <h6 class="side-user-name">{{App\Helpers\Helper::splitname(Auth::user()->name)}}</h6>
                </div>
            </div>
            <div id="user-dropdown" class="collapse">
                <ul class="user-setting-menu">
                    <li><a href="{{url('logout')}}"><i class="icon-power"></i> Sair</a></li>
                </ul>
            </div>
        </div>
        <!-- Renderiza Menu Dinâmico-->
        @if(Auth::user()->accept_terms)
            {{-- {{ Menu::render() }} --}}
            <ul class="sidebar-menu do-nicescrol">
                @hasrole('origin:'.env('PROFILE_ADMIN'))
                <li>
                    <a href="javaScript:void();" class="waves-effect"><i class="fa fa-cog"></i><span>Configurações</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="sidebar-submenu">
                        @haspermission('origin/module/index')
                        <li><a title="Modulos" href="{{ route('admin.module.index') }}">Modulos</a></li>
                        @endhaspermission
                        @haspermission('origin/category/index')
                        <li><a title="Categoria dos Recursos" href="{{ route('admin.category.resource.index') }}">Categoria dos Recursos</a></li>
                        @endhaspermission
                        @haspermission('origin/resource/index')
                        <li><a title="Recursos" href="{{ route('admin.resource.index') }}">Recursos</a></li>
                        @endhaspermission
                        @haspermission('origin/roles/index')
                        <li><a title="Permissões" href="{{ route('admin.role.index') }}">Permissões</a></li>
                        @endhaspermission
                        @haspermission('origin/type-account/index')
                        <li><a title="Perfis" href="{{ route('admin.type.account.index') }}">Perfis</a></li>
                        @endhaspermission
                        @haspermission('origin/type-account-roles/index')
                        <li><a title="Perfil Permissões" href="{{ route('admin.type.account.roles.index') }}">Perfil Permissões</a></li>
                        @endhaspermission
                        @haspermission('origin/type-account-user/index')
                        <li><a title="Perfis do Usuário" href="{{ route('admin.type.account.user.index') }}/">Perfis do Usuário</a></li>
                        @endhaspermission
                    </ul>
                </li>
                @endhasrole
                <li>
                    <a href="javaScript:void();" class="waves-effect"><i class="icon-home"></i><span>Comece Aqui</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="sidebar-submenu">
                        @haspermission('origin/index/index')
                        <li><a title="Home" href="{{ route('admin.index') }}">Home</a></li>
                        @endhaspermission
                        @haspermission('origin/configuration/index')
                        <li><a title="Dados Cadastrais" href="{{ route('admin.configuration.index') }}">Configurações</a></li>
                        @endhaspermission
                        @haspermission('origin/cashouts/index')
                        <li><a title="Solicitações de Saque" href="{{ route('admin.cashout.index') }}">Solicitações de Saque</a></li>
                        @endhaspermission
                        @haspermission('origin/cashouts/index')
                        <li><a title="Solicitações de Reembolso" href="{{ route('admin.reembolso.index') }}">Solicitações de Reembolso</a></li>
                        @endhaspermission
                        @haspermission('origin/user-admin/index')
                        <li><a title="Usuários Administrativos" href="{{ route('admin.user.admin.index') }}">Usuários Administrativos</a></li>
                        @endhaspermission
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect"><i class="fa fa-user"></i><span>Usuários</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="sidebar-submenu">
                        @haspermission('origin/users/index')
                        <li><a title="Todos os usuários" href="{{ route('admin.users.index') }}">Todos os usuários</a></li>
                        @endhaspermission
                        @haspermission('origin/approve-account/index')
                        <li><a title="Aprovação de Contas" href="{{ route('admin.account.approve.index') }}">Aprovação de Contas</a></li>
                        @endhaspermission
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect"><i class="fa fa-cubes"></i><span>Produtos</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="sidebar-submenu">
                        @haspermission('origin/products/index')
                        <li><a title="Todos os Produtos" href="{{ route('admin.product.index') }}">Todos os Produtos</a></li>
                        @endhaspermission
                        @haspermission('origin/product-pendent/index')
                        <li><a title="Produtos Pendentes" href="{{ route('admin.product.pendent') }}">Produtos Pendentes</a></li>
                        @endhaspermission
                        @haspermission('origin/product-disapprove/index')
                        <li><a title="Produtos Reprovados" href="{{ route('admin.product.disapprove') }}">Produtos Reprovados</a></li>
                        @endhaspermission
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect"><i class="icon-pie-chart"></i><span>Relatórios</span><i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="sidebar-submenu">
                        @haspermission('origin/report-sales/index')
                        <li><a title="Todas as Vendas" href="{{ route('admin.report.sales') }}">Todas as Vendas</a></li>
                        @endhaspermission
                        @haspermission('origin/report-extract/index')
                        <li><a title="Extrato Financeiros" href="{{ route('admin.report.extract') }}">Extrato Financeiros</a></li>
                        @endhaspermission
                    </ul>
                </li>
                @haspermission('origin/tool/index')
                <li>
                    @haspermission('origin/tool/index')
                    <a href="{{ route('admin.tool.index') }}" class="waves-effect" title="Ferramentas"><i class="icon-magnet"></i><span>Ferramentas</span></a>
                    @endhaspermission
                </li>
                @endhaspermission
            </ul>
        @endif
        </ul>
    </div>


    <!--Fim de Menu da sidebar-->

    <!--Start topbar header-->
    <header class="topbar-nav">
        <nav class="navbar navbar-expand fixed-top bg-dark">
            <ul class="navbar-nav mr-auto align-items-center">
                <li class="nav-item">
                    <a class="nav-link toggle-menu" href="javascript:void();">
                        <i class="icon-menu menu-icon"></i>
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center right-nav-link">
                <li class="nav-item">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                        <span class="user-profile"><img src="{!! \App\Helpers\Helper::imageProfile() !!}" class="img-circle" alt="user avatar"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <li class="dropdown-item user-details">
                            <a href="javaScript:void();">
                                <div class="media">
                                    <div class="avatar"><img class="align-self-start mr-3" src="{!! \App\Helpers\Helper::imageProfile() !!}" alt="user avatar"></div>
                                    <div class="media-body">
                                        <h6 class="mt-2 user-title">{{App\Helpers\Helper::splitname(Auth::user()->name)}}</h6>
                                        <p class="user-subtitle">{{Auth::user()->email}}</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-item"><i class="icon-power mr-2"></i>
                            <a href="{{url('logout')}}">Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="container-fluid">

                @if ($errors->any())
                    <div class="alert alert-light-danger alert-dismissible" role="alert">
                        <div class="alert-icon">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span>Ocorreu um erro ao Salvar. Por favor, verificar os dados informados.</span>
                        </div>
                    </div>

                    <div class="alert alert-icon-danger alert-dismissible" role="alert">
                        <div class="alert-icon icon-part-danger">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                         <span>
                           <ul>
                            @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                         </span>
                        </div>
                    </div>

                @endif

                @if(Session::has('success'))
                    <div class="alert alert-light-success alert-dismissible" role="alert">
                        <div class="alert-icon contrast-alert">
                            <i class="icon-check"></i>
                        </div>
                        <div class="alert-message">
                            <span>{{Session::get('success')}}</span>
                        </div>
                    </div>
                @endif
                @if(Session::has('error'))

                    <div class="alert alert-light-danger alert-dismissible" role="alert">
                        <div class="alert-icon">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span> {{Session::get('error')}}</span>
                        </div>
                    </div>

                @endif

                <div class="row pt-2 pb-2">
                    <div class="col-sm-9">
                        <h4 class="page-title">{{isset($titlePage) && !empty($titlePage) ? $titlePage : ''}}</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                                @yield('content')
                        </div>
                    </div>
                </div>

        </div>
    </div>

</div>

<!-- /.content-wrapper -->

<div class="modal fade" id="spinnerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="loader"></div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

<!-- simplebar js -->
<script src="{{asset('assets/plugins/simplebar/js/simplebar.js')}}"></script>
<!-- waves effect js -->
<script src="{{asset('assets/js/waves.js')}}"></script>
<!-- sidebar-menu js -->
<script src="{{asset('assets/js/sidebar-menu.js')}}"></script>
<!-- Custom scripts -->
<script src="{{asset('assets/js/app-script.js')}}"></script>

<!-- Chart js -->
<script src="{{asset('assets/plugins/Chart.js/Chart.min.js')}}"></script>
<!--notification js -->
<script src="{{asset('assets/plugins/notifications/js/lobibox.min.js')}}"></script>
<script src="{{asset('assets/plugins/notifications/js/notifications.min.js')}}"></script>
<!-- CDN MASKINPUT-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<!-- CDN BootBox-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<script type="text/javascript">
    // SESSION MESSAGES
    @if(\request()->session()->has('success'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.success("{{ request()->session()->get('success') }}", 'Sucesso!');
    }, 100);
    @endif

    @if(\request()->session()->has('successForm'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.success("{{ request()->session()->get('successForm') }}", 'Sucesso!');
    }, 100);
    @endif

    @if(\request()->session()->has('error'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.error("{{ request()->session()->get('error') }}", 'Erro!');
    }, 100);
    @endif

    @if(\request()->session()->has('danger'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.error("{{ request()->session()->get('danger') }}", 'Erro!');
    }, 100);
    @endif

    @if(\request()->session()->has('info'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.info("{{ request()->session()->get('info') }}", 'Info!');
    }, 100);
    @endif

    @if(\request()->session()->has('warning'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.warning("{{ request()->session()->get('warning') }}", 'Erro!');
    }, 100);
    @endif

</script>

<script>
$(document).ready(function(){
    $('[data-href]').css({'cursor':'pointer'})
    $('[data-href]').on('click', function(e) {
      window.location.href = $(this).data('href')
    })

    $('[data-tooltip]').tooltip();

    $(".input-filter").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".content-filter .line").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
</script>

@yield('script-dashboard')
@yield('scripts')

<script>
    $(document).ready(function () {
        $(".phone").mask("(99) 999999999");
        $(".date").mask("00/00/0000");
        $(".cep").mask("00.000-000");
        $(".uf").mask("AA");
        $(".number10").mask("9999999999");
    });
</script>
@stack('scripts')

<!--<script src="{{ asset('js/app.js') }}"></script>-->

</body>

<!-- Mirrored from uxliner.com/niche/main/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 03:38:50 GMT -->

</html>
