@extends('plataforma.templates.template')

@section('css-dashboard')
  <link rel="stylesheet" href="{{asset('dist/plugins/chartist-js/chartist.min.css')}}">
  <link rel="stylesheet" href="{{asset('dist/plugins/chartist-js/chartist-plugin-tooltip.css')}}">
@endsection

@section('script-dashboard')
  <script src="{{asset('dist/plugins/chartjs/chart.min.js')}}"></script>
  <script src="{{asset('dist/plugins/chartjs/chart-int.js')}}"></script>

  <!-- Chartist JavaScript -->
  <script src="{{asset('dist/plugins/chartist-js/chartist.min.js')}}"></script>
  <script src="{{asset('dist/plugins/chartist-js/chartist-plugin-tooltip.js')}}"></script>
  <script src="{{asset('dist/plugins/functions/chartist-init.js')}}"></script>
@endsection

@section('content')
<!--Dados Financeiros-->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3>Dados Financeiros</h3>
      </div>
      <div class="card-body"> 
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $totalSalesToday }}</h4>
                      <span class="text-white">Valor vendido hoje</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $totalSalesMonth }}</h4>
                      <span class="text-white">Vendas totais do mês</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $winningsTratsToday }}</h4>
                      <span class="text-white">Ganhos da Trats hoje</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-danger">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $winningsTratsMonth }}</h4>
                      <span class="text-white">Reembolsos solicitados hoje</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-danger">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $withdrawalToday }}</h4>
                      <span class="text-white">Reembolsos solicitados no mês</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-danger">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $pendingWithdrawRequest }}</h4>
                      <span class="text-white">Saques realizados no mês</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-warning">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $withdrawalMonth }}</h4>
                      <span class="text-white">Saques solicitados pendentes hoje</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--<div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success ">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">R$ {{ $productAwaitingApproval }}</h4>
                      <span class="text-white">Produtos Aguardando Aprovação</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>-->
        </div>
      </div>
    </div>
  </div>
</div>
<!--FIM Dados Financeiros-->

<!--Dados de Usuários-->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3>Dados de Usuários</h3>
      </div>
      <div class="card-body"> 
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $totalSalesToday }}</h4>
                      <span class="text-white">Usuários Cadastrados na Trats</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $totalSalesMonth }}</h4>
                      <span class="text-white">Novos usuários hoje</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $winningsTratsToday }}</h4>
                      <span class="text-white">Usuários novos este mês</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-warning">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $winningsTratsMonth }}</h4>
                      <span class="text-white">Contas aguardando aprovação de docs</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--FIM Dados de Usuários-->

<!--Dados de Vendas-->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3>Dados de Vendas</h3>
      </div>
      <div class="card-body"> 
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $totalSalesToday }}</h4>
                      <span class="text-white">Vendas já realizadas na Trats</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $totalSalesMonth }}</h4>
                      <span class="text-white">Vendas realizadas hoje</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-success">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $winningsTratsToday }}</h4>
                      <span class="text-white">Vendas realizadas neste mês</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-warning">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $winningsTratsMonth }}</h4>
                      <span class="text-white">Vendas pendentes</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--FIM Dados de Vendas-->

<!--Dados de Produtos-->
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <h3>Dados de Produtos</h3>
      </div>
      <div class="card-body"> 
        <div class="row">
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-primary">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $totalSalesToday }}</h4>
                      <span class="text-white">Produtos Cadastrados</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-secondary">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $totalSalesMonth }}</h4>
                      <span class="text-white">Produtos Reprovados</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-warning">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $winningsTratsToday }}</h4>
                      <span class="text-white">Produtos Pendentes</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-3">
              <div class="card bg-danger">
                <div class="card-body">
                  <div class="media">
                    <div class="media-body text-left">
                      <h4 class="text-white">{{ $winningsTratsMonth }}</h4>
                      <span class="text-white">Produtos que nunca venderam</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--FIM Dados de Produtos--> 
@endsection