@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Perfis para o Usuário</h6>
    </div>
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header bprder-0">
                    Perfis de {{ $user->name }}
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'admin.type.account.user.attach', 'id'=> 'form']) !!}
                    {!! Form::hidden('id', $user->id ) !!}
                    <div class="row" id="row-mod">
                        <div class="col-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="Escolha um Módulo">Escolha um Módulo</label>
                                @if(count($modulesNoLinked))
                                    {!! Form::select('module', $modulesNoLinked, null, ['class' => 'custom-select form-control', 'required'=>'required', 'id'=> 'module']) !!}
                                @else
                                    {!! Form::select('module', [], null, ['class' => 'custom-select form-control', 'required'=>'required', 'id'=> 'module']) !!}
                                @endif
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead>
                <tr>
                  <th>Módulo</th>
                  <th>Perfil</th>
                  <th>Descrição</th>
                  <th class="text-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                @if(count($typeAccounts))
                @foreach ($typeAccounts as $typeAccount)
                <tr>
                  <td>{{ $typeAccount->mod_nome }}</td>
                  <td>{{ $typeAccount->name }}</td>
                  <td>{{ $typeAccount->label }}</td>
                  <td class="col-1 text-center">
                      {!! Form::open(['route' => 'admin.type.account.user.detach']) !!}
                        {!! Form::hidden('id', $user->id) !!}
                        {!! Form::hidden('type_account_id', $typeAccount->id) !!}
                        <button type="submit" title="Excluir registro" data-toggle="tooltip" data-placement="left" class="btn btn-danger waves-effect waves-light"><i class="fa fa-remove"></i></button>
                      {!! Form::close() !!}
                  </td>
                </tr>
                @endforeach
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
            $(".btn-excluir").on("click" , function(e){
                $this = $(this);

                bootbox.confirm("Você tem certeza que deseja excluir esse registro?", function(result) {
                    if(result) {
                        $this.closest('form').submit();
                    }
                });

                e.preventDefault();
            });

            var baseUrl = "{{ route('admin.type.account.user.typeaccount') }}";

            $( "#module" ).change(function() {
                if($(this).val() == 0){
                    $( ".divPerfis" ).remove();
                    $( ".btnAtribuir" ).remove();
                } else {
                    var urlPerfisAjax = baseUrl + '?id=' + $( "#module" ).val();
                    $.getJSON( urlPerfisAjax, function( data ) {
                        $( ".divPerfis" ).remove();
                        $( ".btnAtribuir" ).remove();
                        $( "<div class='col-2 col-lg-2 col-xl-2 divPerfis'>" ).appendTo( $( "#row-mod" ) );
                        $( "<label>Escolha um Perfil:</label>" ).appendTo( $( ".divPerfis" ) );
                        $( "<select class='custom-select form-control perfis' name='type_account_id'>" ).appendTo( $( ".divPerfis" ) );
                        $( "<div class='col-2 col-lg-2 col-xl-2 divPerfis'><label>&nbsp;</label><div class='form-group'><button type='submit' class='btn btn-primary  btnAtribuir'>Atribuir Perfil</button>" ).appendTo( $( "#row-mod" ) );
                        var id;
                        for (id in data) {
                            $( "<option value="+ id +">"+data[id] +"</option>" ).appendTo( $( ".perfis" ) );
                        }
                        console.log( data.length );
                    })
                }
            });

    </script>
@stop