@extends('plataforma.templates.template')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header text-uppercase">Filtrar por produto</div>
                <div class="card-body">
                    <div class="col-md-12">
                        <form action="{{ route('admin.type.account.user.index') }}" method="GET" class="">
                            @csrf
                            <div class="col-lg-9" style="float:left">
                                <div class="form-group">
                                    {!! Form::input('user_name','user_name', Illuminate\Support\Facades\Input::get('user_name'), ['class' => 'form-control','placeholder'=> 'Pesquise por nome']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3" style="float:left"><button class="btn btn-success btn-block" type="submit">Buscar</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card" style="">
        <div class="card-header">Gerenciamento de Perfis para o Usuário</div>
        <div class="card-body">
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="table-responsive" style="">
                        @if(count($users))
                            <table class="table table-striped">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Perfil</th>
                                    <th class="text-center">AÇÃO</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td width="10%">{{$user->id}}</td>
                                        <td style="">{{$user->name}}</td>
                                        <td style=""> @if($user->TypeAccountadmin)  <p><i class="fa fa-circle text-success"></i> Administrador</p> @endif</td>
                                        <td class="text-center" width="25%">
                                            <a  class="btn btn-primary btn-sm" href="{{ route('admin.type.account.user.assign', ['id' => encrypt($user->id)]) }}"><i aria-hidden="true" class="fa fa-cogs"></i> Gerenciar</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right">{!! $users->appends(request()->except('page'))->links() !!}</div>
                    </div>
                    @else
                        <div class="col-md-12">
                            <h6 class="text-center">Nenhum registro encontrado</h6>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection