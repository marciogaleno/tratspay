@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Perfis</h6>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header bprder-0">
            <a href="{{ route('admin.type.account.create') }}" class="btn btn-success waves-effect waves-light m-1">Novo Perfil</a>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Nome</th>
                  <th>Descrição</th>
                  <th>Módulo</th>
                  <th class="text-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($typeAccounts as $typeAccount)
                <tr>
                  <td>{{ ($typeAccount->enable) ? 'Ativo' : 'Inativo' }}</td>
                  <td>{{ $typeAccount->name }}</td>
                  <td>{{ $typeAccount->label }}</td>
                  <td>{{ $typeAccount->module()->first()->name }}</td>
                  <td class="col-1 text-center">
                      <form action="{{ route('admin.type.account.delete')  }}" method="POST">
                          @csrf()
                          <input type="hidden" name="id" value="{{ $typeAccount->id}}" />
                          <button type="submit" title="Excluir registro" data-toggle="tooltip" data-placement="left" class="btn btn-danger waves-effect waves-light"><i class="fa fa-remove"></i></button>
                      </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection