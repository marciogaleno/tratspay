@extends('admin.templates.template')

@section('content')
<div class="row">
  <div class="col-lg-12">
  @if($bankAccounts->count())
    <div class="card check-list">
      <div class="card-header d-flex align-items-center">
        <div class="icheck-material-primary m-5" data-toggle="tooltip" data-placement="top" title="Marcar todos" data-original-title="Marcar todos">
          <input type="checkbox" class="check-list-all" id="account" name="account" value="all">
          <label for="account">&nbsp;</label>
        </div>
        <div class="btn-group m-1" role="group">
            <button type="button" class="btn btn-outline-secondary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Ações
            </button>
            <div class="dropdown-menu">
              <a href="#" class="dropdown-item check-list-action" chk-on="confirm">Confirmar todos</a>
              <a href="#" class="dropdown-item check-list-action" chk-on="cancel">Recusar todos</a>
            </div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="">
                <div class="">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">&nbsp;</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Banco</th>
                        <th scope="col">Agência</th>
                        <th scope="col">Conta</th>
                        <th scope="col-1">&nbsp;</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($bankAccounts as $bankAccount)
                      <tr>
                        <td class="col-1 text-center">
                          <div class="icheck-material-primary">
						                <input type="checkbox" class="check-list-item" id="account-{{ $bankAccount->id }}" value="{{ $bankAccount->id }}">
						                <label for="account-{{ $bankAccount->id }}">&nbsp;</label>
					                </div>
                        </td>
                        <td>{{ $bankAccount->user->name }}</td>
                        <td>{{ \App\Helpers\Helper::ListBank($bankAccount->type) }}</td>
                        <td>{{ sprintf("%s-%s", $bankAccount->bank_branch, $bankAccount->bank_branch_digit) }}</td>
                        <td>{{ sprintf("%s-%s", $bankAccount->bank_account, $bankAccount->bank_account_digit) }}</td>
                        <td class="text-right">
                          <div class="btn-group dropleft">
                            <button type="button" class="btn btn-inverse-dark waves-effect waves-light" data-toggle="dropdown" id="dropdownMenu{{ $bankAccount->id }}">
                              <span class="fa fa-ellipsis-v" aria-hidden="true"></span>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenu{{ $bankAccount->id }}">
                              <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-check"></i> Confirmar</a>
                              <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-close"></i> Recusar</a>
                            </div>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @else
    <p>Sem conta para aprovação</p>
  @endif
  </div>
</div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/plugins/check-list/js/check-list.js') }}"></script>
  <script>

    var checklist = new CheckList({
      el: '.check-list',
      action: '.check-list-action',
      item: '.check-list-item',
      checkall: '.check-list-all' 
    })

    checklist.methods = {
      'confirm': (target, items, el) => {
        console.log(items);
      },
      'cancel': (target, items, el) => {
        console.log(el);
      }
    }

    checklist.build()

  </script>
@endsection