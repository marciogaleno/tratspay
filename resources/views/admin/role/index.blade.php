@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Permissões</h6>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header bprder-0">
            <a href="{{ route('admin.role.create') }}" class="btn btn-success waves-effect waves-light m-1">Nova Permissão</a>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead>
                <tr>
                  <th>Nome</th>
                  <th>Descrição</th>
                  <th>Recurso</th>
                  <th>URL (modulo/recurso/permissão)</th>
                  <th class="text-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($rolesGruop as $resource => $roles)
                  <tr class="bg-light">
                    <td colspan="5"><strong>{{ $resource }}</strong></td>
                  </tr>
                  @foreach ($roles as $role)
                  <tr>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->label }}</td>
                    <td>{{ $role->resource }}</td>
                    <td>/{{ $role->module }}/{{ $role->resource }}/{{ $role->name }}</td>
                    <td class="col-1 text-center">
                        <form action="{{ route('admin.role.delete')  }}" method="POST">
                            @csrf()
                            <input type="hidden" name="id" value="{{ $role->id}}" />
                            <button type="submit" title="Excluir registro" data-toggle="tooltip" data-placement="left" class="btn btn-danger waves-effect waves-light"><i class="fa fa-remove"></i></button>
                        </form>
                    </td>
                  </tr>
                  @endforeach
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection