@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Permissões</h6>
    </div>
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header bprder-0">
                    Nova Permissão
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'admin.role.create']) !!}
                    <div class="row">
                        <div class="col-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="Módulo e Recruso">Módulo/Recruso</label>
                                {!! Form::select('resource', $resources, null, ['class' => 'custom-select form-control', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="Nome">Nome</label>
                                {!! Form::text('name', null, ['class' => 'form-control sty1', 'placeholder' => 'Nome da permissão', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6 col-lg-6 col-xl-6">
                            <div class="form-group">
                                <label for="Descrição">Descrição</label>
                                {!! Form::text('label', null, ['class' => 'form-control sty1', 'placeholder' => 'Descrição do permissão', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::button('Criar Permissão', ['type'=> 'submit','class' => 'btn btn-primary waves-effect waves-light']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection