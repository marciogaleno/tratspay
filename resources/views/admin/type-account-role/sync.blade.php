@extends('admin.templates.template')

@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
@stop

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Permissões para o Perfil</h6>
    </div>
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header bprder-0">
                    Atribuir Permissões para o perfil: {{ $typeAccount->name }}
                </div>
                <div class="card-body">
                    <div class="ibox-content">
                            <div id="jstree">
                                <ul>
                                    @if(count($roles))
                                        @foreach($roles as $role)
                                            <li>{{ ucfirst($role['nome']) }}
                                                @if(count($role['permissoes']))
                                                    <ul>
                                                        @foreach($role['permissoes'] as $perm)
                                                            <li
                                                                    @if($perm['habilitado'])
                                                                    data-jstree='{"selected":"true", "type":"sub"}'
                                                                    @else
                                                                    data-jstree='{"type":"sub"}'
                                                                    @endif
                                                                    id="prm_{{$perm['permissao_id']}}">
                                                                {{ $perm['prm_nome'] }}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <br>
                            <form action="{{ route('admin.type.account.roles.syncrole') }}" method="POST" role="form">
                                @csrf()
                                <input type="hidden" name="roles" id="roles">
                                <input type="hidden" name="type_account" value="{{ $typeAccount->id }}">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <button class="btn btn-primary" id="btn-enviar">Atribuir permissões ao perfil</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('#jstree').jstree({
                'core' : {
                    'check_callback' : true
                },
                'plugins' : [ 'types', 'checkbox' ],
                'types' : {
                    'default' : {
                        'icon' : 'fa fa-folder'
                    },
                    'sub' : {
                        'icon' : 'fa fa-cog'
                    }
                }
            });
        });

        $('#btn-enviar').on('click', function(e){

            e.preventDefault();

            $this = $(this);

            var checked_ids = [];

            var selectedItems = $("#jstree").jstree('get_selected');
            $(selectedItems).each(function(id, element){
                if(element.search(/prm_/) != -1) {
                    checked_ids.push(element.replace(/prm_/, ''));
                }
            });

            $("#roles").val(checked_ids.toString());

            $this.closest('form').submit();
        });
    </script>
@stop