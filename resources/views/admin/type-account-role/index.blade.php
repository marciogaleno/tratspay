@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Perfis</h6>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Módulo</th>
                  <th>Perfil</th>
                  <th>Descrição</th>
                  <th class="text-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($typeAccounts as $typeAccount)
                <tr>
                  <td>{{ ($typeAccount->enable) ? 'Ativo' : 'Inativo' }}</td>
                  <td>{{ $typeAccount->mod_name }}</td>
                  <td>{{ $typeAccount->name }}</td>
                  <td>{{ $typeAccount->label }}</td>
                  <td class="col-1 text-center">
                      <a href="{{ route('admin.type.account.roles.sync', ['id' => encrypt($typeAccount->id)])  }}" class="btn btn-primary waves-effect waves-light">
                        <i class="fa fa-check-circle"></i> Atribuir Permissões
                      </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection