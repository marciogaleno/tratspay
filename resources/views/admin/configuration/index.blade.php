@extends('admin.templates.template')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/plugins/dropzone/css/dropzone.css') }}">
    <style>
    .h-38 {
        height: 38px;
    }
    a.disabled, a.disabled:hover {
        cursor: default;
        color: #ccc !important;
    }
    input.input-search {
        animation-name: show;
        animation-duration: 1s;
        transition: 2;
    }

    @keyframes show {
        from {width: 0%;}
        to {width: 100%;}
    }

    .box-img {
        height: 200px;
    }
    </style>
@endsection

@section('content')
<div class="row">  
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <!-- Geral -->
                    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div>
                            <h5>Geral</h5>
                        </div>
                        {!! Form::open([
                            'method'    => 'PUT',
                            'route'     => ['admin.configuration.update'],
                            'class'     => 'form-horizontal',
                            'enctype' => "multipart/form-data"
                        ]) !!}

                        <div class="form-group">
                            {!! Form::label('background', 'Imagem de Fundo de Login') !!}
                            <div>
                                <figure class="box-img">
                                    <img id="img-login" class="img-fluid h-100 position-relative" src="{{ \App\Src\Model\ConfigAccount::BGLogin() }}" title="Imgame da tela de login"/>
                                </figure>
                            </div>
                            <div>
                                {!! Form::file('background_login', null, ['class' => 'form-control', 'placeholder' => 'Imagem de Fundo de Login', 'accept' => 'image/*']) !!}
                                @if($errors->has('background_login'))
                                    <span class="help-block">{{ $errors->first('background_login') }}</span>
                                @endif
                            </div>
                        </div>
                        {{--<div class="form-group">
                            {!! Form::label('email', 'E-mail do MercadoPago') !!}
                            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('cemailomplement') }}</span>
                            @endif
                        </div>--}}
                        <hr>
                        <div>
                            <h5>Taxa cobrada pela Trats</h5>
                        </div>

                        <div class="d-flex">
                            <div class="form-group">
                                {!! Form::label('taxa', 'Para produto fisico (%)') !!}
                                {!! Form::text('rate_physical_product', $config['rate_physical_product'] ?? null, ['class' => 'form-control', 'placeholder' => '']) !!}

                                @if($errors->has('rate_physical_product'))
                                    <span class="help-block">{{ $errors->first('rate_physical_product') }}</span>
                                @endif
                            </div>
                            <div class="form-group ml-3">
                                {!! Form::label('taxa', 'Para produto virtual (%)') !!}
                                {!! Form::text('rate_virtual_product', $config['rate_virtual_product'] ?? null, ['class' => 'form-control', 'placeholder' => '']) !!}

                                @if($errors->has('rate_virtual_product'))
                                    <span class="help-block">{{ $errors->first('rate_virtual_product') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success waves-light m-1" type="submit">Atualizar Informações</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- Categoria -->
                    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <div class="d-flex justify-content-between align-items-center mb-2 h-38">
                            <h5 class="mb-0">Categorias de Produtos</h5>
                            <div class="d-flex flex-fill align-items-center">
                                <a href="#" class="text-dark search ml-1"><i class="fa fa-search"></i></a>
                                <form action="">
                                    <input type="search" name="q" class="form-control ml-1 d-none input-search" value="{{ old('q') }}" placeholder="Filtrar por nome [Enter]">
                                </form>
                            </div>
                            <div class="d-flex ml-3">
                                <span>1-{{ $categories->perPage() }} de {{ $categories->total() }}</span>
                                <a href="{{ $categories->previousPageUrl() }}" class="text-dark ml-3 {{ !$categories->onFirstPage() ?: 'disabled' }}" data-toggle="tooltip" data-placement="bottom" title="Anterior"><i class="fa fa-angle-left"></i></a>
                                <a href="{{ $categories->nextPageUrl() }}" class="text-dark ml-2 {{ $categories->hasMorePages() ?: 'disabled' }}" data-toggle="tooltip" data-placement="bottom" title="Próximo"><i class="fa fa-angle-right"></i></a>
                            </div>
                        </div>
                        <div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col" class="col-1">
                                            Nome
                                            
                                            @if(Request::has('q'))
                                            <a href="{{ url()->current() }}" class="ml-3">
                                                <span class="badge badge-secondary">{{ Request::input('q') }} <i class="fa fa-refresh"></i></span>
                                            </a>
                                            @endif
                                        </th>
                                        <th scope="col">Atualizado</th>
                                        <th scope="col" class="col-md-1">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td class="col-1">{{ $category->desc }}</td>
                                        <td>{{ date('d/m/Y', strtotime($category->updated_at)) }}</td>
                                        <td class="col-md-1">
                                            <div class="dropdown">
                                                <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
                                                <i class="icon-options text-dark"></i>
                                                </a>
                                                @haspermission('origin/configuration/category')
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item open-modal" href="javascript:void();" data-action="{{ route('admin.configuration.category', ['category'=> $category->id]) }}" data-name="{{ $category->desc }}"><i class="fa fa-pencil"></i> Editar</a>
                                                    <form method="POST" action="{{ route('admin.configuration.category', ['category'=> $category->id]) }}">
                                                        @csrf
                                                        <input name="del" value="true" type="hidden"/>
                                                        <button class="dropdown-item" type="submit"><i class="fa fa-trash"></i> Excluir</button>
                                                    </form>
                                                </div>
                                                @endhaspermission
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="editCategoryLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form method="POST">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="EditCategoryLabel">Editar categoria</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input name="name" id="name" class="form-control">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </form>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ asset('assets/plugins/dropzone/js/dropzone.js') }}"></script>
<script type="text/javascript">
    $('a.disabled').on('click', function(e){
        e.preventDefault();
        return false
    })
    
    var search = $('.input-search');

    $('a.search').on('click', function(e){
        e.preventDefault()
        var btnIcon = $(this).find('i')
        
        if (search.hasClass('d-none') && btnIcon.hasClass('fa-search')) {
            search.removeClass("d-none");
            btnIcon.removeClass("fa-search");
            btnIcon.addClass("fa-close");
        } else {
            search.addClass("d-none");
            btnIcon.removeClass("fa-close");
            btnIcon.addClass("fa-search");
        }

        return false
    })

    $('.dropdown-item .action').on('click', function(e){
        $form = $('.main-modal').find('form');

        if ($action = $(this).data('edit')) {
            //$form.attr('action', $action);
        }
    
    })

    $('input[name=background_login]').change(function(e){
        var input = this;
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#img-login').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    })

    $('.open-modal').on('click', function(e){
        const URL = $(this).data('action')
        const NAME = $(this).data('name')
        $('#editCategory form').attr('action', URL)
        $('#editCategory form #name').val(NAME)
        $('#editCategory').modal('show')
    })

</script>
@endpush