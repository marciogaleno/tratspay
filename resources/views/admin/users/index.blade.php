@extends('admin.templates.template')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-body"> 
        <div class="row">
          <div class="col-md-3">
            <div class="tabs-vertical tabs-vertical-warning">
              <!-- Menu Tab panes -->
                <ul class="nav nav-tabs flex-column">
                  @foreach($typeAccounts as $key => $typeAccount)
                  <li class="nav-item">
                    <a class="nav-link py-4 {{ $key == 0 ? 'active' : '' }}" data-toggle="tab" href="#tabe-{{ $typeAccount->id }}"><span class="hidden-xs">{{ $typeAccount->name }}</span></a>
                  </li>
                  @endforeach
                </ul>
            </div>
            </div>
            <div class="col-md-9">
              <!-- Tab panes -->
              <div class="tab-content">
                @foreach($typeAccounts as $key => $typeAccount)
                <div id="tabe-{{ $typeAccount->id }}" class="container tab-pane {{ $key == 0 ? 'active' : 'fade'}}">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="">
                          <h5 class="card-title">Usuários com perfil: {{ $typeAccount->name }}</h5>
                          <div class="table-responsive">
                            @if($typeAccount->vincular->count())
                            <table class="table">
                              <thead>
                                <tr>
                                  <th scope="col">Nome</th>
                                  <th scope="col">Email</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($typeAccount->vincular as $user)
                                <tr>
                                  <td>{{ $user->name }}</td>
                                  <td>{{ $user->email }}</td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                            @else
                              <p>Sem usuários</p>
                            @endif
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection