@extends('admin.templates.template')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card check-list">
      <div class="card-header d-flex align-items-center">
        <div class="icheck-material-primary ml-2" data-toggle="tooltip" data-placement="top" title="Marcar todos" data-original-title="Marcar todos">
          <input type="checkbox" class="check-list-all" id="account" name="account" value="all">
          <label for="account">&nbsp;</label>
        </div>
        <div class="d-flex flex-fill">
          <div class="btn-group ml-1" role="group">
              <button type="button" class="btn btn-outline-secondary dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-filter"></i>
              </button>
              <div class="dropdown-menu">
                <a href="#" class="dropdown-item check-list-action" chk-on="filter">Usuários com mais vendas</a>
                <a href="#" class="dropdown-item check-list-action" chk-on="filter">Usuários com maior faturamento</a>
                <a href="#" class="dropdown-item check-list-action" chk-on="filter">Usuários com mais produtos</a>
              </div>
          </div>
          <form action="{{ url()->current() }}">
          <div class="input-group ml-3">
              <div class="input-group-prepend">
                <button class="btn btn-secondary" type="button"><i class="fa fa-search"></i></button>
              </div>
              <input type="email" name="q" class="form-control" placeholder="E-mail">
          </div>
          </form>
        </div>
        <div class="d-flex">
            <span class="ml-3">1-{{ $users->perPage() }} de {{ $users->total() }}</span>
            <a href="{{ $users->previousPageUrl() }}" class="text-dark ml-3 {{ !$users->onFirstPage() ?: 'disabled' }}" data-toggle="tooltip" data-placement="bottom" title="Anterior"><i class="fa fa-angle-left"></i></a>
            <a href="{{ $users->nextPageUrl() }}" class="text-dark ml-5 {{ $users->hasMorePages() ?: 'disabled' }}" data-toggle="tooltip" data-placement="bottom" title="Próximo"><i class="fa fa-angle-right"></i></a>
        </div>
      </div>
      <div class="card-body">
        <div class="">
          @if($users->count())
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col-1">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $user)
              <tr dat-href={{ route('admin.users.show', ['id' => encrypt($user->id)]) }}>
                <td class="col-1">
                  <div class="icheck-material-primary">
                    <input type="checkbox" class="check-list-item" id="user-{{ $user->id }}" value="{{ $user->id }}">
                    <label for="user-{{ $user->id }}">&nbsp;</label>
                  </div>
                </td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td class="text-right">
                    <div class="btn-group dropleft">
                      <a href="#" class="text-dark" data-toggle="dropdown" id="dropdownMenu{{ $user->id }}">
                        <span class="fa fa-ellipsis-v" aria-hidden="true"></span>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdownMenu{{ $user->id }}">
                        <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-eye"></i> Detalhe</a>
                        <a href="javaScript:void();" class="dropdown-item"><i class="fa fa-trash"></i> Excluir</a>
                      </div>
                    </div>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @else
            <p>Sem usuários</p>
          @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts')
  <script src="{{ asset('assets/plugins/check-list/js/check-list.js') }}"></script>
  <script>

    var checklist = new CheckList({
      el: '.check-list',
      action: '.check-list-action',
      item: '.check-list-item',
      checkall: '.check-list-all' 
    })

    checklist.methods = {
      'confirm': (target, items, el) => {
        console.log(items);
      },
      'cancel': (target, items, el) => {
        console.log(el);
      }
    }

    checklist.build()
  </script>
@endsection