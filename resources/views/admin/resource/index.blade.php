@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Recursos</h6>
    </div>

    <div class="row">
      <div class="col-12 col-lg-12 col-xl-12">
        <div class="card">
          <div class="card-header bprder-0">
            <a href="{{ route('admin.resource.create') }}" class="btn btn-success waves-effect waves-light m-1">Novo Recurso</a>
          </div>
          <div class="table-responsive">
            <table class="table align-items-center table-flush">
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Recurso</th>
                  <th>Item do Menu</th>
                  <th>Subitem do Menu</th>
                  <th>Módulo</th>
                  <th class="text-center">Ações</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($resources as $resource)
                <tr>
                  <td>{{ ($resource->enable) ? 'Ativo' : 'Inativo' }}</td>
                  <td>{{ $resource->name }}</td>
                  <td><i class="{{ $resource->cr_icon }}"></i> {{ $resource->category_resource }}</td>
                  <td><i class="{{ $resource->icon }}"></i> {{ $resource->label }}</td>
                  <td>{{ $resource->module }}</td>
                  <td class="col-1 text-center">
                      <form action="{{ route('admin.resource.delete')  }}" method="POST">
                          @csrf()
                          <input type="hidden" name="id" value="{{ $resource->id}}" />
                          <button type="submit" title="Excluir registro" data-toggle="tooltip" data-placement="left" class="btn btn-danger waves-effect waves-light"><i class="fa fa-remove"></i></button>
                      </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
@endsection