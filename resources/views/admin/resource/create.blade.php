@extends('admin.templates.template')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header sty-one">
      <h6>Gerenciamento de Recursos</h6>
    </div>
    <div class="row">
        <div class="col-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-header bprder-0">
                    Novo Recurso
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'admin.resource.create']) !!}
                    <div class="row">
                        <div class="col-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="Nome">Módulo</label>
                                {!! Form::select('module',  $modules, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'required'  => true]) !!}
                            </div>
                        </div>
                        <div class="col-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="Icone">Item do Menu</label>
                                {!! Form::select('category',  $categories, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'required'  => true]) !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="Nome">Nome Recurso</label>
                                {!! Form::text('name', null, ['class' => 'form-control sty1', 'placeholder' => 'Nome do recurso', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                                <label for="Descrição">Subitem</label>
                                {!! Form::text('label', null, ['class' => 'form-control sty1', 'placeholder' => 'Nome do subitem', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-3 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="Icone">Icone <small><a href="http://codervent.com/fobia-admin-v2/demo/color-admin/icons-font-awesome.html" target="_blank" title="Abrir lista de icones" data-toggle="tooltip"><i class="fa fa-share-square"></i></a></small></label>
                                {!! Form::text('icon', null, ['class' => 'form-control sty1', 'placeholder' => 'ex.: fa fa-home', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-3 col-lg-3 col-xl-3">
                            <div class="form-group">
                                <label for="Status">Status</label>
                                {!! Form::select('enable', ['1' => 'Ativo', '0' => 'Inativo'], [], ['class' => 'custom-select form-control', 'required'=>'required']) !!}
                            </div>
                        </div>
                        <div class="col-2 col-lg-2 col-xl-2">
                            <div class="form-group">
                                <label for="Ordem">Ordem</label>
                                {!! Form::number('order', null, ['class' => 'form-control sty1', 'placeholder' => 'Posição no menu', 'required'=>'required']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::button('Criar Recurso', ['type'=> 'submit','class' => 'btn btn-primary waves-effect waves-light']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection