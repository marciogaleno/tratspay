@extends('plataforma.templates.template')

@section('content')
<style>
  div.card > div > div > div > div > ul > li{
    padding: 5px;
  }
  .dont-break-out {
  /* These are technically the same, but use both */
  overflow-wrap: break-word;
  word-wrap: break-word;

  -ms-word-break: break-all;
  /* This is the dangerous one in WebKit, as it breaks things wherever */
  word-break: break-all;
  /* Instead use this non-standard one: */
  word-break: break-word;

  /* Adds a hyphen where the word breaks, if supported (No Blink) */
  -ms-hyphens: auto;
  -moz-hyphens: auto;
  -webkit-hyphens: auto;
  hyphens: auto;
  }

</style>
<div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
          <div class="col-sm-9 col-md-12">
            {{-- <h4 class="page-title">Você está no Perfil de {{$producer->user->name}}</h4> --}}
            <h4 class="page-title">Você está em seu Perfil</h4>
          </div>
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
          <div class="col-lg-4">
            <div class="profile-card-4">
              <div class="card">
                <div class="card-body text-center rounded-top bg-dark" >
                  <div class="user-box">
                    @if (isset($producer->user->image))
                      <img src="{!! \App\Helpers\Helper::getPathFileS3('images', $producer->user->image) !!}"
                           alt="User profile picture">
                    @else
                      <img src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png"
                           alt="User profile picture">
                    @endif
                  </div>
                  @if ($producer->user)
                    <h5 class="mb-1 text-white">{{  $producer->user->name }}</h5>
                  @endif
                  <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#changeImageProfile">
                    Alterar imagem
                  </button> 
                </div>
                <div class="card-body">
                  <ul class="list-group shadow-none">
                    <li class="list-group-item">
                      <div class="list-icon">
                        <i class="fa fa-phone-square"></i>
                      </div>
                      <div class="list-details dont-break-out">
                        <span> {{$producer->cellphone}}</span>
                        <small>WhatsApp</small>
                      </div>
                    </li>
                    <li class="list-group-item">
                      <div class="list-icon">
                        <i class="fa fa-envelope"></i>
                      </div>
                      <div class="list-details">
                        <button class="btn btn-sm btn-warning" data-toggle="modal" data-target="#viewEmailUser">
                            Exibir E-mail
                        </button>
                      </div>
                    </li>
                    <li class="list-group-item">
                        <div class="list-icon">
                            <i class="fa fa-globe"></i>
                        </div>
                        <div class="list-details dont-break-out">
                            <button class="btn  btn-block btn-sm btn-warning" data-toggle="modal" data-target="#viewSiteUser">
                                Exibir Site
                            </button>
                        </div>
                    </li> 
                  </ul>
                </div>
                <div class="card-footer text-center">
                  <a href="{{$producer->social_facebook}}" class="btn-social btn-facebook waves-effect waves-light m-1"><i class="fa fa-facebook"></i></a>
                  <a href="{{$producer->social_twitter}}" class="list-inline-item btn-social btn-behance waves-effect waves-light"><i class="fa fa-twitter"></i></a>
                  <a href="{{$producer->social_linkedin}}" class="list-inline-item btn-social btn-dribbble waves-effect waves-light"><i class="fa fa-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8" style="transition: all 0.25s;">
            <div class="card-header text-uppercase">Sobre o usuário</div>
            <div class="card">
              <div class="card-body">
                <div class="row">
                </div>
                <ul class="list-group list-group-flush">
                  <li class="list-group-item"><i class="fa text-primary mr-2 fa-user-o"></i> 
                    <strong>Nome de Usuário:</strong>  {{  $producer->user->name}}
                    &nbsp;
                  </li>
                  <li class="list-group-item" style=""><i class="fa fa-bookmark text-primary mr-2"></i> 
                    <strong>Usuário desde:</strong>   {{ date('d/m/Y', strtotime($producer->created_at)) }}&nbsp;
                  </li>
                  <li class="list-group-item">
                    <i class="fa text-primary mr-2 fa-id-card"></i> <strong>Descrição do Perfil:</strong>  {{  $producer->obs}}
                  </li>
                </ul>
                <div class="col-md-12" style="padding-left: 0px">
                  <div class="card-header text-uppercase">conquistas do usuário</div>
                  <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                  </div>
                  <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-header text-uppercase">Este usuário também é criador dos produtos:</div>
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    @foreach($products as $product)
                      <div class="col-lg-4">
                        <div class="card">
                          @if ($product->image)
                            <img style="width: 200px;height: 200px;object-fit: contain;" src="{{ \App\Helpers\Helper::getPathFileS3('images', $product->image)}}" class="card-img-top" alt="Card image cap">
                          @else
                            <img style="width: 200px;height: 200px;object-fit: contain;" src="{{url('storage/products')}}" class="card-img-top" alt="Card image cap">
                          @endif

                          <div class="card-body">
                            <h5 class="card-title text-dark">{{$product->name}}</h5>
                          </div>
                          <ul class="list-group list-group-flush list shadow-none">
                            <li class="list-group-item d-flex justify-content-between align-items-center">Preço <span  style="font-weight: bold">{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plans[0]->price : 'Planos' }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Quantidade de venda <span  style="font-weight: bold">{{App\Repository\SalesRepository::getQtdVenda($product->id)}}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Afiliados <span style="font-weight: bold">{{$product->affiliates()->count()}}</span></li>
                          </ul>
                          <div class="row">
                            <div class="col-md-12" style="max-width: 90%;">

                              <div class="col-md-6" style="float: right">
                                <button class="btn btn-danger btn-sm m-1" data-toggle="modal" data-target="#dangermodal">Excluir</button>
                              </div>
                              <div class="col-md-6" style="float: left">
                                <a href="{{route('product.edit', $product->id)}}" class="btn btn-inverse-dark btn-sm  waves-effect waves-light m-1 card-link"><i class="fa fa-pencil-square-o""></i> Editar</a>

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    @endforeach
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>

      <!-- Modal -->
<div class="modal fade" id="changeImageProfile" tabindex="-1" role="dialog"
aria-labelledby="changeImageProfileLabel" aria-hidden="true">
<div class="modal-dialog" role="document" style="background-color: #FFFFFF">
<div class="modal-content border-warning">
    <div class="modal-header bg-warning">
        <h5 class="modal-title text-white" id="changeImageProfileLabel">Alterar Imagem</h5>
        <button type="button" class="close text-white" data-dismiss="modal"
                aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">

        {!! Form::model($producer, [
            'method'    => 'POST',
            'route'     => ['client.change.image'],
            'enctype'   => 'multipart/form-data',
        ]) !!}
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    {!! Form::label('image_profile', 'Imagem de Perfil') !!}
                  
                    <div class=" form-group ">
                        <div class="input-group">
                            <label class="input-group-btn">
                                <span class="btn btn-primary">
                                    Buscar&hellip; 
                                    <input data-id="file-avatar" type="file" style="display: none;" id="image_profile" name="image_profile" >
                                </span>
                            </label>
                            <input type="text" style="height: 40px;" id="file-avatar" class="form-control " placeholder="" readonly>
                        </div>
                    </div>

                    @if($errors->has('image_profile'))
                        <span class="help-block">{{ $errors->first('image_profile') }}</span>
                    @endif
                </div>
            </div>
        </div>



        <div class="modal-footer">
            <button type="button" class="btn btn-secondary"
                    data-dismiss="modal">Fechar
            </button>
            <button type="submit" class="btn btn-primary">Trocar Imagem
            </button>
        </div>
        {!! Form::close() !!}
    </div>
</div>
</div>
</div>
<!-- End Modal -->
  <!-- Modal -->
  <div class="modal fade" id="viewEmailUser" tabindex="-1" role="dialog"
  aria-labelledby="changeImageProfileLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
      <div class="modal-content border-warning">
          <div class="modal-header bg-warning">
              <h5 class="modal-title text-white" id="changeImageProfileLabel">Email</h5>
              <button type="button" class="close text-white" data-dismiss="modal"
                      aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-md-12">
                      <p>{{  $producer->user->email}}</p>               
                  </div>
              </div>
        
              <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Fechar</button>
              </form>
              </div>
          </div>
      </div>
  </div>
  </div>
  <!-- End Modal -->

  <!-- Modal -->
  <div class="modal fade" id="viewSiteUser" tabindex="-1" role="dialog"
  aria-labelledby="changeImageProfileLabel" aria-hidden="true">
      <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
          <div class="modal-content border-warning">
              <div class="modal-header bg-warning">
                  <h5 class="modal-title text-white" id="changeImageProfileLabel">Site</h5>
                  <button type="button" class="close text-white" data-dismiss="modal"
                          aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                      <div class="row">
                          <div class="col-md-12">
                          <p>{{ $producer->social_site}}</p> 
                          </div>
                      </div>
              
                  <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Fechar</button>
                  </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- End Modal -->
@endsection

@push('scripts')
  <script type="text/javascript">       
      $(document).on('change', ':file', function() {
        var val = $(this).val();
        var id = "#"+$(this).data("id");
        $(id).attr('placeholder',val);
      });
  </script>
@endpush