@extends('plataforma.templates.template')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header text-uppercase">Detalhes da notificação:</div>
            <div class="container" style="padding: 20px">
                <div class="mail-box-header">
                    <div class="mail-tools tooltip-demo m-t-md">


                        <h3>
                            <span class="font-normal">Produto: </span>{{$notification->product->name}}.
                        </h3>
                        <table class="table table-striped" style="font-size: 14px;margin-top: 27px">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Email</th>
                                <th>Telefone</th>
                                <th>Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>{{$notification->first_name.' '.$notification->last_name}}</td>
                                <td>{{$notification->email}}</td>
                                <td>{{$notification->phone}}</td>
                                <td>{{$notification->created_at->format('d/m/Y')}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="mail-box">


                    <div class="mail-body" style="margin-top: 27px">
                        <p>
                            Olá {{$notification->user->name}}!
                            <br>
                            <br>
                            @if($notification->type == 'sale')
                                Foi registrado uma nova venda, e {{$notification->content}}
                            @else
                                Foi solicitado um reembolso por motivos de: {{$notification->content}}
                            @endif
                        </p>

                    </div>


                    <div class="clearfix"></div>


                </div>
            </div>
        </div>
    </div>
@endsection