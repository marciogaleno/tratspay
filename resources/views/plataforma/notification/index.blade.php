@extends('plataforma.templates.template')
@section('content')
    <div class="card" style="">
        <div class="card-header">Todas as notificações</div>
        <div class="card-body">
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="table-responsive" style="">
                        @if(count($notifications))
                            <table class="table table-sm">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Tipo</th>
                                    <th class="text-center">AÇÃO</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($notifications as $notificacao)
                                    <tr>
                                        <td>{{$notificacao->id}}</td>
                                        <td style="width: 30%">{{$notificacao->first_name.' '.$notificacao->last_name}}</td>
                                        <td style="width: 30%">{{$notificacao->product->name}}</td>
                                        <td>
                                            @if($notificacao->type == "sale")
                                                <p><i class="fa fa-circle text-info"></i> VENDA</p>
                                            @else
                                                <p><i class="fa fa-circle text-warning"></i> REEMBOLSO</p>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a  class="btn btn-primary btn-sm" href="{{url('/notifications/view/'.$notificacao->id)}}"><i aria-hidden="true" class="fa fa-eye"></i> Detalhes</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right">{!! $notifications->appends(request()->except('page'))->links() !!}</div>
                    </div>
                    @else
                        <div class="col-md-12">
                            <h6 class="text-center">Nenhum registro encontrado</h6>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
