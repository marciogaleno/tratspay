@extends('plataforma.templates.tabs') 

@section('styles')
    <style>
        .form-control {
            font-size: 0.7rem;            
        }
    </style>
@endsection

@section('contentTab')

    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Cupons</h4>
        </div>
    </div>
    <hr>    
    

    <div class="card-body">
        
        <h4>
        <div class="card-body">
                <div  class="row m-t-2" id="panelAddPlanos" style="font-size: 14px">
                        <div class="table-responsive">
                            <a href="{{route('product.coupon.add', app('request')->product->id)}}" type="button" class="btn btn-success pull-right" style="margin: 5px 5px"><i class="fa fa-plus"></i> Novo </a>
                            <table class="table">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Cupom</th>
                                <th scope="col">URLs</th>
                                <th scope="col">Preço</th>  
                                <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($coupons)
                                @foreach ($coupons as $coupon)
                                    <tr>
                                        <td>{{$coupon->coupon}}</td>
                                        <td>{!! Form::text('sale_page', url('/c') . '/' . $coupon->plan->checkouts[0]->code . '?cupom=' . $coupon->code , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF']) !!}</td>
                                        <td>R$ {{$coupon->price}}</td>
                                        <td>
                                            {!! Form::open(['route' => ['product.coupon.delete', app('request')->product->id, $coupon->id], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                                <a href="#" title="URLs" id="urlsCoupon"  data-toggle="modal" onclick="openModal({{$coupon->id}})" title="Vendas" class="btn btn-primary btn-sm" rel="nofollow"><i class="fa fa-bar-chart"></i> Vendas</a>
                                                <a href="{{route('product.coupon.edit', [app('request')->product->id, $coupon->id])}}" title="Editar" class="btn btn-primary btn-sm" rel="nofollow"><i class="fa fa-pencil"></i></a>
                                                <button class="btn btn-danger btn-sm" onclick="return confirm('Tem certeza que deseja excluir?')"><i class="fa fa-trash"></i></button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>Nenhuma cupom encontrado</tr>
                                @endif
                            </tbody>
                            </table>
                        </div>
                </div>
        </div>


      
    </div>
      <!-- Modal -->
      <div class="modal fade" id="modalUrlCoupon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">URLs Cupons</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <table class="table" id="TableUrlsCoupon">
                        <thead class="thead-light">
                                <tr>
                                <th scope="col">Clicks</th>
                                <th scope="col">Vendas</th>
                                <th scope="col">CTR</th>  
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>      
                
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>

                {{Form::close()}}
                </form>
                
              </div>
            </div>
        </div>
        

@endsection


@section('scripts')

<script>
        
    function openModal(coupon_id) {
        $('#modalUrlCoupon').modal("show");

        var url = {!! json_encode(url("product/" . app("request")->product->id . "/coupons/urls/")) !!} + '/' + coupon_id;

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
          url : url,
          type : 'GET', 
          data: {
            "_token": "{{ csrf_token() }}",
          },
          beforeSend : function(){
            $.preloader.start();
          }
        })
        .done(function(msg){
            $('#TableUrlsCoupon tbody').empty();
            var trs = '';

            Object.keys(msg).forEach(function(k){
                console.log(msg[k]);
                var tr = '<tr>';
                var td = '<td>' + '' + '</td>';

                td += '<td>' + '' + '</td>';
                td += '<td>' + '' + '</td>';
              
                tr += td + '</tr>';

                trs += tr;
            });

            $('#TableUrlsCoupon').find('tbody').append(trs);
            
            $.preloader.stop();
            
        })
        .fail(function(jqXHR, textStatus, msg){
            $('#tableCheckouts tbody').empty();
        });         
    }

    function closeModal() {
        $('#modalUrlCoupon').modal("hide");
        $.preloader.start();
        //$.preloader.stop();
    }

</script>
@endsection