@extends('plataforma.templates.tabs') 

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/boostrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/boostrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('contentTab')

    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Adicionar Cupom</h4>
        </div>
    </div>
    <hr>    
    

    <div class="card-body">
        @if (isset($coupon))
            {{ Form::model($coupon, ['route' => ['product.coupon.update', app('request')->product->id, $coupon->id], 'method' => 'put']) }}
        @else
            {{ Form::open(['route' => ['product.coupon.create', app('request')->product->id], 'method' => 'post']) }}
        @endif
        
        {!! \App\Helpers\FormHelper::select('plan_id', 'Planos:',  $plans, $errors) !!}
        {!! \App\Helpers\FormHelper::text('price', 'Preço alternativo:', $errors) !!}
        {!! \App\Helpers\FormHelper::textarea('desc', 'Descrição:', $errors) !!}
        {!! \App\Helpers\FormHelper::text('coupon', 'Código cupom:', $errors, ['maxlength' => 10]) !!}
        {!! \App\Helpers\FormHelper::selectSearch('affiliate_id', 'Afiliado:', $affiliates, $errors, ['id' => 'teste', 'placeholder' => 'Nenhum afiliado atrelado']) !!}
        {!! \App\Helpers\FormHelper::url('url_thank_you', 'Página de obrigado:', $errors) !!}
        {!! \App\Helpers\FormHelper::url('url_ticket', 'Página de obrigado boleto:', $errors) !!}
        {!! \App\Helpers\FormHelper::text('validity', 'Validade:', $errors, ['autocomplete' => 'off']) !!}
        {!! \App\Helpers\FormHelper::number('max_purchase', 'Quatidade máxima de compras:', $errors) !!}
        {!! \App\Helpers\FormHelper::text('email_buyer', 'E-mail comprador:', $errors) !!}
        {!! \App\Helpers\FormHelper::select('take_interest', 'Assumir Juros:', [0 => 'Não', 1 => 'Sim'], $errors) !!}

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                <div class="card-header text-uppercase">Configuração Contador</div>
                <div class="card-body">
                        {!! \App\Helpers\FormHelper::select('counter', 'Usar contador:', [0 => 'Não', 1 => 'Sim'], $errors) !!}
                        {!! \App\Helpers\FormHelper::text('text_counter', 'Texto contador:', $errors, ['disabled']) !!}
                        <div class="row">
                            <div class="col-sm-6">           
                                {!! \App\Helpers\FormHelper::colorPicker('background_counter', 'Cor de fundo:', $errors, ['disabled']) !!}
                            </div>
                            <div class="col-sm-6">           
                                {!! \App\Helpers\FormHelper::colorPicker('color_text_counter', 'Cor texto:', $errors, ['disabled']) !!}       
                            </div>
                        </div>
                
                        {!! \App\Helpers\FormHelper::text('time_counter', 'Tempo:', $errors, ['placeholder' => '00:00:00', 'disabled']) !!} 
                </div>
                </div>
            </div>
        </div>
 

        <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn  btn-success"> Salvar</button>
                </div>
        </div>

        {!! Form::close() !!}

    </div>
 

@endsection


 
@section('scripts')
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>
<script src="{{asset('assets/plugins/boostrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/boostrap-datetimepicker/js/locales/bootstrap-datetimepicker.pt-BR.js')}}"></script>
<script src="{{asset('assets/plugins/boostrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>

<script>
    $(".single-select").select2();
    $("#price").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
    $(".colorpicker-component").colorpicker({
        color: '#FFFFFF'
    });
    $("#time_counter").mask("00:00:00");

    $('#validity').datetimepicker({
        todayHighlight: true,
        language:  "pt-BR",
        format: "dd/mm/yyyy hh:ii:ss"
    });
    $(function() {
        $('#counter').trigger('change');
    });

    $("#counter").on("change", function() {
        if (this.value == 1) {
            $("#text_counter").attr("disabled", false);
            $("#background_counter").attr("disabled", false);
            $("#color_text_counter").attr("disabled", false);
            $("#time_counter").attr("disabled", false);
        } else {
            $("#text_counter").attr("disabled", true);
            $("#background_counter").attr("disabled", true);
            $("#color_text_counter").attr("disabled", true);
            $("#time_counter").attr("disabled", true);            
        }
    });

    
</script>
@endsection