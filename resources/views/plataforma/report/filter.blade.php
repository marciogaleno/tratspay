<div class="row">
<div class="col-md-12">

    <form method="GET" action="{{route('report.salesIndication')}}">
        <div class="row">
            <div class="form-group col-md-2  @if ($errors->has('transaction_id')) has-error @endif" style="float: left">
                {!! Form::label('transaction_id', 'Transação*', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::text('transaction_id', Illuminate\Support\Facades\Input::get('transaction_id'), ['class' => 'form-control']) !!}
                    @if ($errors->has('transaction_id')) <p class="help-block">{{ $errors->first('transaction_id') }}</p> @endif
                </div>
            </div>
            <div class="form-group col-md-6  @if ($errors->has('product_name')) has-error @endif" style="float: left">
                {!! Form::label('product_name', 'Produto*', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::select('product_name[]',$products, Illuminate\Support\Facades\Input::get('product_name'), ['class' => 'form-control', 'multiple'=> 'multiple','id'=> 'product_name']) !!}
                    @if ($errors->has('product_name')) <p class="help-block">{{ $errors->first('product_name') }}</p> @endif
                </div>
            </div>

            <div class="form-group col-md-4  @if ($errors->has('ass_cpf')) has-error @endif" style="float: left">
                {!! Form::label('ass_cpf', 'Status:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::select('status[]', App\Helpers\Helper::getAllStatusPayment(), Illuminate\Support\Facades\Input::get('status') ,['class' => 'form-control', 'Placeholder' => 'Selecione', 'multiple'=> 'multiple','id'=> 'status']) !!}
                    @if ($errors->has('ass_cpf')) <p class="help-block">{{ $errors->first('ass_cpf') }}</p> @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-md-3  @if ($errors->has('co_affiliates')) has-error @endif" style="float: left">
                {!! Form::label('affiliates', 'Indicados:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::select('affiliates[]', $indicados,  Illuminate\Support\Facades\Input::get('affiliates'),['class' => 'form-control', 'Placeholder' => 'Selecione', 'multiple'=> 'multiple', 'id'=>'affiliates']) !!}
                    @if ($errors->has('affiliates')) <p class="help-block">{{ $errors->first('affiliates') }}</p> @endif
                </div>
            </div>

            <div class="form-group col-md-3  @if ($errors->has('form_payment')) has-error @endif" style="float: left">
                {!! Form::label('form_payment', 'Forma de pagamento:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::select('form_payment', App\Helpers\Helper::getFormPayment(),  Illuminate\Support\Facades\Input::get('form_payment'),['class' => 'form-control', 'Placeholder' => 'Selecione','multiple'=> 'multiple', 'id' => 'form_payment']) !!}
                    @if ($errors->has('form_payment')) <p class="help-block">{{ $errors->first('form_payment') }}</p> @endif
                </div>
            </div>
{{--            <div class="form-group col-md-3  @if ($errors->has('origem_venda')) has-error @endif" style="float: left">--}}
{{--                {!! Form::label('origem_venda', 'Origem da venda:', ['class' => 'control-label']) !!}--}}
{{--                <div class="controls">--}}
{{--                    {!! Form::select('origem_venda', App\Helpers\Helper::getFormPayment(),  Illuminate\Support\Facades\Input::get('origem_venda'),['class' => 'form-control', 'Placeholder' => 'Selecione']) !!}--}}
{{--                    @if ($errors->has('origem_venda')) <p class="help-block">{{ $errors->first('origem_venda') }}</p> @endif--}}
{{--                </div>--}}
{{--            </div>--}}

            <div class="form-group col-md-3  @if ($errors->has('data_pedido')) has-error @endif" style="float: left">
                {!! Form::label('data_pedido', 'Data do pedido:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {{ Form::date('data_pedido', Illuminate\Support\Facades\Input::get('data_pedido'),['class' => 'form-control']) }}
                    @if ($errors->has('data_pedido')) <p class="help-block">{{ $errors->first('data_pedido') }}</p> @endif
                </div>
            </div>

            <div class="form-group col-md-3  @if ($errors->has('data_finalizada')) has-error @endif" style="float: left">
                {!! Form::label('data_finalizada', 'Data finalizada:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {{ Form::date('data_finalizada', Illuminate\Support\Facades\Input::get('data_finalizada'),['class' => 'form-control']) }}
                    @if ($errors->has('data_finalizada')) <p class="help-block">{{ $errors->first('data_finalizada') }}</p> @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4  @if ($errors->has('cpf_cnpj')) has-error @endif" style="float: left">
                {!! Form::label('cpf_cnpj', 'Cpf:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::text('cpf_cnpj', Illuminate\Support\Facades\Input::get('cpf_cnpj'),['class' => 'form-control', 'id' => 'cpf_cnpj']) !!}
                    @if ($errors->has('cpf_cnpj')) <p class="help-block">{{ $errors->first('cpf_cnpj') }}</p> @endif
                </div>
            </div>

            <div class="form-group col-md-4  @if ($errors->has('email')) has-error @endif" style="float: left">
                {!! Form::label('email', 'Email do comprador:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::email('email', Illuminate\Support\Facades\Input::get('email'),['class' => 'form-control']) !!}
                    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                </div>
            </div>

            <div class="form-group col-md-4  @if ($errors->has('first_name')) has-error @endif" style="float: left">
                {!! Form::label('first_name', 'Nome do comprador:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::text('first_name', Illuminate\Support\Facades\Input::get('first_name'),['class' => 'form-control']) !!}
                    @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                </div>
            </div>


        </div>
        <div class="col-md-12 ">
            <div class=""  style="float: right; padding: 10px">
                <input type="submit" class="form-control btn-primary" value="Localizar"> 
            </div>
        </div>
    </form>
</div>
</div>