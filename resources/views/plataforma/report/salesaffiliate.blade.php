@extends('plataforma.templates.template')
@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-selection__choice{
            float: none !important;
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header" style="background-color: #f5f5f5;padding: 10px;border-top-left-radius: 3px;border-top-right-radius: 3px;">  <i class="fa fa-bar-chart"></i>   Relatório de comissões de {{$page['title']}}</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-header" style="background-color: #f5f5f5;padding: 10px;border-top-left-radius: 3px;border-top-right-radius: 3px;">  <i class="fa fa-filter"></i>  Filtro</div>
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form action="{{ route($page['route']) }}" method="GET" class="">
                                            @csrf
                                            <div class="col-lg-9" style="float:left">
                                                <div class="form-group">
                                                    {!! Form::select('product_id[]',$productsFilter, Illuminate\Support\Facades\Input::get('product_id'), ['class' => 'form-control', 'multiple'=> 'multiple','id'=> 'product_id']) !!}
                                                </div>
                                            </div>
                                            <div class="col-lg-3" style="float:left"><button class="btn btn-success btn-block" type="submit">Buscar</button></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if (isset($_GET['product_id']))
                            <div class="col-md-12">
                                <div class="card" style="">
                                    <div class="card-header"  style="background-color: #f5f5f5;padding: 10px;border-top-left-radius: 3px;border-top-right-radius: 3px;">
                                        <i class="fa fa-tasks"></i>  Comissão de {{$page['title']}}
                                    </div>
                                    <div class="">
                                        @if(count($report))
                                            <table class="table table-striped table-bordered">
                                                <thead class="">
                                                <tr>
                                                    <th scope="col">Nome </th>
                                                    <th scope="col">Total de vendas</th>
                                                    <th scope="col">Valor Comissão</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($report as $item)
                                                    <tr>
                                                        <td>
                                                            {{$item->name}}<br>
                                                        </td>
                                                        <td>
                                                            R$ {{App\Helpers\Helper::moneyBR($item->totalvenda)}}<br>
                                                        </td>
                                                        <td>
                                                            R$  {{App\Helpers\Helper::moneyBR($item->totaldesconto)}}<br>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                            <div class="col-md-12">
                                                <div class="pull-right">{!! $report->links() !!}</div>
                                            </div>
                                        @else
                                            <h5 class="text-center" style="padding: 20px">Nenhum dado encontrado! </h5>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop
@section('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#product_id').select2({ width: 'resolve', placeholder:"Selecione os produtos"});
        });

    </script>
@stop