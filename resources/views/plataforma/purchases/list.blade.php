@extends('plataforma.templates.template')

@section('css-dashboard')
    <link rel="stylesheet" href="{{asset('dist/plugins/chartist-js/chartist.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/plugins/chartist-js/chartist-plugin-tooltip.css')}}">
@endsection

@section('script-dashboard')
    <script src="{{asset('dist/plugins/chartjs/chart.min.js')}}"></script>
    <script src="{{asset('dist/plugins/chartjs/chart-int.js')}}"></script>

    <!-- Chartist JavaScript -->
    <script src="{{asset('dist/plugins/chartist-js/chartist.min.js')}}"></script>
    <script src="{{asset('dist/plugins/chartist-js/chartist-plugin-tooltip.js')}}"></script>
    <script src="{{asset('dist/plugins/functions/chartist-init.js')}}"></script>
@endsection

@section('content')


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header border-0">
                    <h5>Confira os produtos que você já adquiriu através da ColinaPay<h5>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        @if (count($purchases) > 0)
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Transação</th>
                                    <th>Produto</th>
                                    <th>Data da Compra</th>
                                    <th>Valor</th>
                                    <th>Reembolso</th>
                                    <th>Acessar Produto</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($purchases as $purchase)
                                    <tr>
                                        <td>
                                            {{'#'. !$purchase->transaction ? '':$purchase->transaction->code}}
                                        </td>
                                        <td>

                                            <div class="col-sm-3">
                                                <img alt="{{$purchase->product->name}}"
                                                     src="{{\App\Helpers\Helper::getPathFileS3('images', $purchase->product->image) }}" width="100px"><br>
                                                <strong>Nome:</strong>{{$purchase->product->name}}<br>
                                                <strong>Email suporte:</strong> {{$purchase->product->email_support}}<br>
                                                <strong>Garantia:</strong> {{$purchase->product->warranty}} {{$purchase->product->type_deliverie_id}}</br>
                                            </div><!-- /.col -->
                                        </td>
                                        <td>
                                            {{\App\Helpers\Helper::dateToBr($purchase->date, true)}}
                                        </td>
                                        <td>{{'R$ '. $purchase->value}}</td>
                                        <td>
                                            @if(date("Y-m-d H:i:s") <= $purchase->transaction->end_warranty)
                                                <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modal{{$purchase->transaction_id}}">
                                                    <i class="fa fa-link" style="font-size: 15px"></i>  Reembolso
                                                </button>
                                            @else
                                                <span>Não Disponível</span>
                                            @endif
                                        </td>

                                        <td>
                                            @if ($purchase->product->type_deliverie_id == 1)
                                                <a href="{{$purchase->product->url_member_area}}" target="_blank"
                                                   class="btn btn-success btn-sm" title="Acessar Produto">
                                                    <i class="fa fa-link"
                                                       style="font-size: 15px"></i> Acessar
                                                </a>
                                            @endif

                                            @foreach ($purchase->product->files as $key => $file)

                                                @if ($purchase->product->type_deliverie_id == 3)
                                                    {!! Form::open(['route' => ['purchase.download'],
                                                        'method' => 'POST', 'class'=> "buttonEdit", 'style' => 'margin-top:5px']) !!}
                                                    {!! Form::hidden('file_path',  $file->file_path) !!}
                                                    {!! Form::hidden('file_name',  $file->name) !!}
                                                    <button class="btn btn-success btn-sm" title="Donwload"><i class="fa fa-download"
                                                                                                               style="font-size: 15px"></i>&nbsp;&nbsp;{{$file->name_original}}</button>
                                                    {!! Form::close() !!}
                                                @endif
                                            @endforeach
                                        </td>
                                    </tr>


                                    <!-- Modal -->
                                    <div class="modal fade" id="modal{{$purchase->transaction_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Solicitação de reembolso</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {!! Form::open(["id" => "formReembolso"]) !!}
                                                    {!! Form::hidden('transaction_id', $purchase->transaction_id)!!}
                                                    {!! Form::hidden('user_id', $purchase->user_id)!!}
                                                    {!! Form::hidden('product_id', $purchase->product_id)!!}
                                                    <div class="form-group col-md-12  @if ($errors->has('motivo')) has-error @endif" style="float: left">
                                                        {!! Form::label('motivo', 'Selecione um motivo:', ['class' => 'control-label']) !!}
                                                        <div class="controls">
                                                            {!! Form::select('motivo',  \App\Helpers\Helper::getAllMotivosReembolso(), Illuminate\Support\Facades\Input::get('motivo'),['class' => 'form-control']) !!}
                                                            @if ($errors->has('motivo')) <p class="help-block">{{ $errors->first('motivo') }}</p> @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-12  @if ($errors->has('observacao')) has-error @endif" style="float: left">
                                                        {!! Form::label('observacao', 'Observação:', ['class' => 'control-label']) !!}
                                                        <div class="controls">
                                                            {!! Form::text('observacao', Illuminate\Support\Facades\Input::get('observacao'),['class' => 'form-control']) !!}
                                                            @if ($errors->has('observacao')) <p class="help-block">{{ $errors->first('observacao') }}</p> @endif
                                                        </div>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                    <button type="button" id="reembolso" class="btn btn-primary">Solicitar</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <p>Nenhuma compra encontrada</p>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
    <script>
        $("#reembolso").click(function () {

            var data = $('#formReembolso').serializeArray();

            var url = "{{url('/reembolso/save')}}"

            $.ajax({
                type: "POST",
                data:data,
                url: url,
                success: function(result){
                    Swal.fire(
                        {
                            title: "Solicitado",
                            text: "Seu reembolso foi solicitado com sucesso.",
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });

                },
                error: function(result){
                    Swal.fire(
                        'Ooops!',
                        'Algo de errado ocorreu, se o problema persistir, entre em contato com nossa equipe.',
                        'error'
                    )
                }
            });
        })
    </script>
@stop
