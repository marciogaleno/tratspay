@extends('plataforma.templates.template')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/dropify/dropify.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/jm.spinner.css')}}">
@endsection

@section('content')

<!-- Main content -->
<div class="content">
    <div class="row">
        <!-- Tab panes -->
        {!! \App\Helpers\TabsHelperAjax::openBoxTabs($product) !!}
        {!! \App\Helpers\TabsHelperAjax::openHeader($product, 5) !!}
        {!! \App\Helpers\TabsHelperAjax::openContent() !!}
            <div class="box"></div>
        {!! \App\Helpers\TabsHelperAjax::closeContent() !!}
        {!! \App\Helpers\TabsHelperAjax::closeHeader() !!}
        {!! \App\Helpers\TabsHelperAjax::closeBoxTabs() !!}
    </div>
</div>
@endsection

@section('scripts')
<script>
$('[data-toggle="tabajax"]').click(function(e) {
    $('.box').jmspinner();
    e.preventDefault()
    var loadurl = $(this).attr('href')
    console.log(loadurl);
    var targ = $(this).attr('data-target')
    $.get(loadurl, function(data) {

    })
    .done(function(data) {
        $(targ).html(data)
    })
    $(this).tab('show')
});
</script>

<script src="{{asset('assets/plugins/dropify/dropify.min.js')}}"></script>
<script src="{{asset('assets/js/jm.spinner.js')}}"></script>
    <script>
        $('.dropify').dropify({
            defaultFile: {!! json_encode(url('storage/products/' . $product->image)) !!},
            messages: {
            'default': 'Selecione ou arraste e solte um arquivo aqui',
            'replace': 'Selecione ou arraste e solte um arquivo aqui',
            'remove':  'Remover',
            'error':   'Ooops, algume erro aconteceu.'
            },
            error: {
            'fileSize': 'O temanho do arquivo é maior que 1 GB).',
            'imageFormat': 'O formato da imagem não é permitido.'
            }

        });


    </script>
@endsection
