@extends('plataforma.templates.tabs')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/dropify/dropify.min.css')}}">
@endsection

@section('contentTab')
    <style>
        small{
            font-size: 10px
        }
    </style>
    <!-- Tab panes -->
    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Dados Gerais</h4>
        </div>
    </div>
    <hr>

    @if ($product)
        {{ Form::open(['route' => ['product.update', app('request')->product->id ], 'method' => 'put', 'enctype' => "multipart/form-data"]) }}
        <div class="card-body">

            <div class="row">
                <div class="col-sm-6 col-md-7" style="margin-top: 5px; font-size: 15px">
                    <p><strong>Código do produto:</strong> <span> {{$product->id}}<span></p>
                    <p><strong>Key de Integração:</strong> <span> {{$product->uuid}}<span></p>
                    <p><strong>Tipo de Produto:</strong> {{$product->product_type == 'V' ? 'Virtual' : 'Físico'}} </p>
                    <p><strong>Entrega:</strong>  {{$product->typeDeliverie->desc}}</p>
                    <p><strong>Tipo de de Cobrança:</strong>  {{ $product->payment_type == 'SINGPRICE' ? 'Preço Único' : 'Plano'}}</p>
                    <p><strong>Nome do produto:</strong>  {{ $product->name }}</p>
                </div>
            </div>


            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                {!! Form::label('name', 'Nome do Produto:', ['for' => 'name' ]) !!}
                {!! Form::text('name', $product->name, ['class' => 'form-control']) !!}
                @if ($errors->has('name'))
                    <label class="control-label" for="name">{{$errors->first('name')}}</label>
                @endif
            </div>

            <div class="form-group  has-feedback {{ $errors->has('desc') ? 'has-error' : '' }}">
                {!! Form::label('desc', 'Descrição:', ['for' => 'desc', 'style' =>'margin-bottom: 0px']) !!}
                <br>
                <small style="">(A descrição escrita abaixo será destinada para os afiliados entenderem melhor sobre o seu produto. Não use este campo para informações para afiliados, use apenas para descrever o seu produto).</small>
                {!! Form::textarea('desc', $product->desc, ['class' => 'form-control']) !!}
                @if ($errors->has('desc'))
                    <label class="control-label" for="desc">{{$errors->first('desc')}}</label>
                @endif
            </div>

            <div class="form-group  has-feedback {{ $errors->has('email_support') ? 'has-error' : '' }}">
                {!! Form::label('email_support', 'Email de Suporte:', ['for' => 'email_support', 'style'=> 'margin-bottom:0px' ]) !!}
                <br>
                <small style="font-size: 10px">(Este e-mail será usado para o seu cliente entrar em contato com você caso tenha dúvidas sobre este produto).</small>
                {!! Form::text('email_support', $product->email_support, ['class' => 'form-control']) !!}
                @if ($errors->has('email_support'))
                    <label class="control-label" for="email_support">{{$errors->first('email_support')}}</label>
                @endif
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group has-feedback {{ $errors->has('warranty') ? 'has-error' : '' }}">
                        {!! Form::label('warranty', 'Prazo de Garantia:', ['for' => 'warranty' ]) !!}
                        {!! Form::select('warranty',  \App\Helpers\Helper::warranty(), $product->warranty, ['class' => 'custom-select form-control', 'placeholder' => '--Selecione --']) !!}
                        @if ($errors->has('warranty'))
                            <label class="control-label" for="warranty">{{$errors->first('warranty')}}</label>
                        @endif
                    </div>
                </div>

                <div class="col-md-4">
                    @if ($product->payment_type == 'SINGPRICE')
                        <div class="form-group has-feedback {{ $errors->has('price') ? 'has-error' : '' }}">
                            {!! Form::label('price', 'Preço:', ['for' => 'price' ]) !!}
                            {!! Form::text('price', $product->plans[0]->price, ['class' => 'form-control', "id"=>"price_unique"]) !!}
                            @if ($errors->has('price'))
                                <label class="control-label" for="price">{{$errors->first('price')}}</label>
                            @endif
                        </div>
                    @endif
                </div>
                <div class="col-md-4" style="float: right">
                    {!! Form::label('price_unique', 'Você receberá por venda:', ['for' => 'price_unique' ]) !!}
                    {!! Form::text('preco_venda', null, ['class' => 'form-control', "id"=>"preco_venda", "disabled" => "disabled"]) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="form-group has-feedback {{ $errors->has('category_id') ? 'has-error' : '' }}">
                    {!! Form::label('category_id', 'Categoria:', ['for' => 'category_id' ]) !!}
                    {!! Form::select('category_id', $categories, $product->category_id, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                    @if ($errors->has('category_id'))
                        <label class="control-label" for="category_id">{{$errors->first('category_id')}}</label>
                    @endif
                </div>
            </div>

            <div class="row">
            <div class="col-md-6" style="float: right">
                <div class="form-group  has-feedback {{ $errors->has('max_item_by_order') ? 'has-error' : '' }}">
                    {!! Form::label('max_item_by_order', 'Quantidade Máximo Por Compra:', ['for' => 'max_item_by_order' ]) !!}
                    {!! Form::text('max_item_by_order',$product->max_item_by_order, ['class' => 'form-control']) !!}
                    @if ($errors->has('max_item_by_order'))
                        <label class="control-label" for="max_item_by_order">{{$errors->first('max_item_by_order')}}</label>
                    @endif
                </div>

                <small>Limite de itens por compra que um cliente pode fazer na mesma compra.</small>

            </div>

            <div class="col-md-6" style="float: left; margin-bottom: 10px; padding-left: 0px; ">
                <div class="form-group">
                    <div class="form-group has-feedback {{ $errors->has('several_purchases') ? 'has-error' : '' }}">
                        {!! Form::label('several_purchases', 'Permitir Re-Compra:', ['for' => 'several_purchases' ]) !!}
                        {!! Form::select('several_purchases', $severalPurchases, $product->several_purchases, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                        @if ($errors->has('several_purchases'))
                            <label class="control-label" for="several_purchases">{{$errors->first('several_purchases')}}</label>
                        @endif
                    </div>
                    <small>O mesmo cliente pode comprar mais de uma vez o mesmo produto</small>
                </div>
            </div>
            </div>

            <div class="col-md-12" style="padding-left: 0px">
                <div class="form-group  has-feedback {{ $errors->has('url_page_sale') ? 'has-error' : '' }}">
                    {!! Form::label('url_page_sale', 'Url Página de Vendas:', ['for' => 'url_page_sale' ]) !!}
                    {!! Form::text('url_page_sale',$product->salepage->url_page_sale, ['class' => 'form-control']) !!}
                    @if ($errors->has('url_page_sale'))
                        <label class="control-label" for="url_page_sale">{{$errors->first('url_page_sale')}}</label>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-5 col-md-5">
                    {!! Form::label('image', 'Imagem do Produto que será exibida na página de Mercado:', ["style" => "margin-bottom:0px"]) !!}
                    <br>
                    <small style="">A imagem deve possuir o tamanho de 200x200</small>
                    <input type="file" id="input-file-now" class="dropify" name="image" id="mage"
                           data-allowed-file-extensions="jpg jpeg png"
                           data-max-file-size="10M"/>
                    <br>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-1">
                        <i class="fa fa-save"></i>
                        <span>Salvar</span>
                    </button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    @endif
@endsection
@section('scripts')
    <script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script> 
    <script src="{{asset('assets/plugins/dropify/dropify.min.js')}}"></script>
    
    <script>
        $("#price_unique").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

        function changePrice() {
            var price = parseInt($('#price_unique').val());

            if($("#product_type").val() == "F") {
                var value = price - (1+(price / 100 * 6.9));
                $('#preco_venda').val(numberParaReal(value));
            }else {
                var value = price - (1+(price / 100 * 9.9));
                $('#preco_venda').val(numberParaReal(value));
            }
        }

        function numberParaReal(numero){
            return numero.toFixed(2).replace(".",",");
        }

        changePrice();

        $('#price_unique').on('change', function() {
            changePrice();
        })
        $('.dropify').dropify({
            defaultFile: {!! json_encode(\App\Helpers\Helper::getPathFileS3('images',  $product->image)) !!},
            messages: {
                'default': 'Selecione ou arraste e solte um arquivo aqui',
                'replace': 'Selecione ou arraste e solte um arquivo aqui',
                'remove':  'Remover',
                'error':   'Ooops, algume erro aconteceu.'
            },
            error: {
                'fileSize': 'O temanho do arquivo é maior que 1 GB).',
                'imageFormat': 'O formato da imagem não é permitido.'
            }

        });
    </script>
@endsection