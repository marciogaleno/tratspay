@extends('plataforma.templates.tabs')


@section('contentTab')
        <!-- Tab panes -->
            <div class="row">
              <div class="col-sm-9">
                  <h4 class="text-left">Área de Membros</h4>
              </div>
            </div>
            <hr>

            {{ Form::model($product, ['route' => ['product.updateMemberArea', $product->id], 'method' => 'put']) }}
                <div class="col-md-8">
                  <input type="hidden" name="type_member_area" id="type_member_area">
                    <div class="btn-group">
                        <button type="button" id="btnMemberArea" class="btn btn-outline-warning waves-effect waves-light">Área de Membros</button>
                        <button type="button" id="btnTratsClub" class="btn btn-outline-warning waves-effect waves-light ">TratsClub</button>
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('url_member_area') ? 'has-error' : '' }}"
                          id="inputUrlMemberArea" style="margin-top: 30px; margin-bottom: 0px; {{$product->type_member_area == 1 ? '' : 'display:none'}}">
                        {!! Form::label('url_member_area', 'URL de acesso') !!}
                        {!! Form::text('url_member_area', null, ['class' => 'form-control', 'placeholder' => 'Digite a URL de login a sua área de membros', 'type' => 'url']) !!}
                        @if ($errors->has('local_page_sale'))
                            <label class="error" for="url_member_area">{{$errors->first('url_member_area')}}</label>
                        @endif
                        <label class="error" id="url_member_area"></label>
                    </div>

                    <small style="font-size: 10px;  {{$product->type_member_area == 1 ? '' : 'display:none'}}" id="msgn">
                        Como seu produto é oferecido via área de membros é preciso que nos forneça acesso a ele para que possamos aprovar ele na ColinaPay.
                        Por favor crie uma conta de aluno para o e-mail produtos@colinapay.com.br ou outro que possa inserir
                        e informe abaixo o e-mail e a senha da conta.</small><br>

                    <div class="row" style="margin-top: 15px;">
                        <div class="col-md-6">
                            <div class="form-group has-feedback {{ $errors->has('login_member_area') ? 'has-error' : '' }}"
                                 id="inputLoginMemberArea" style=" {{$product->type_member_area == 1 ? '' : 'display:none'}}">
                                {!! Form::label('login_member_area', 'Login de acesso') !!}
                                {!! Form::text('login_member_area', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                @if ($errors->has('login_member_area'))
                                    <label class="error" for="login_member_area">{{$errors->first('login_member_area')}}</label>
                                @endif
                                <label class="error" id="login_member_area"></label>
                            </div>

                        </div>

                        <div class="col-md-6">
                            <div class="form-group has-feedback {{ $errors->has('password_member_area') ? 'has-error' : '' }}"
                                 id="inputPasswordMemberArea" style=" {{$product->type_member_area == 1 ? '' : 'display:none'}}">
                                {!! Form::label('password_member_area', 'Senha de acesso') !!}
                                {!! Form::text('password_member_area', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                @if ($errors->has('password_member_area'))
                                    <label class="error" for="password_member_area">{{$errors->first('password_member_area')}}</label>
                                @endif
                                <label class="error" id="password_member_area"></label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="margin-top: 10px; {{$product->type_member_area == 2 ? '' : 'display:none'}}" id="linkButtonTratsClub">
                      <button type="button" class="btn btn-light waves-effect waves-light m-1">Acessar TratsClub</button>
                    </div>

                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-success waves-effect waves-light m-1">
                            <i class="fa fa-save"></i>
                            <span>Salvar</span>
                        </button>
                    </div>
                </div>
            {!! Form::close() !!}

@endsection
@section('scripts')
<script>
    @if ($product->type_member_area == 1)
    $("#type_member_area").val(1);
    $("#btnMemberArea").addClass('active');
    @else
    $("#type_member_area").val(2);
    $("#btnTratsClub").addClass('active');
    @endif

    $("#btnMemberArea").click(function(){
        $("#btnTratsClub").removeClass('active');
        $("#btnMemberArea").addClass('active');
        $("#linkButtonTratsClub").hide();
        $("#inputUrlMemberArea").show();
        $("#inputPasswordMemberArea").show();
        $("#inputLoginMemberArea").show();
        $("#msgn").show();
        $("#type_member_area").val(1);
    });

    $("#btnTratsClub").click(function(){
        $("#btnMemberArea").removeClass('active');
        $("#btnTratsClub").addClass('active');
        $("#inputUrlMemberArea").hide();
        $("#inputPasswordMemberArea").hide();
        $("#inputLoginMemberArea").hide();
        $("#msgn").hide();
        $("#linkButtonTratsClub").show();
        $("#type_member_area").val(2);
    });


</script>
@endsection
