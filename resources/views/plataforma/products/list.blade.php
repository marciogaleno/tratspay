@extends('plataforma.templates.template')
@section('content')


{{--  CABEÇALHO  --}}
<div class="row">

    <div class="col-md-12">
        <h3 class="page-title">Meus Produtos</h3>
    </div>

    <hr>

    <div class="col-md-12">

        <div class="card border border-warning">

            <div class="card-body text-center">
                <h5 class="card-title text-warning">Quer vender na ColinaPay?</h5>
                <p class="card-text">Na ColinaPay você paga apenas 6,9% + R$ 1 real para venda de produtos físicos ou 9,9% + R$ 1 real para produtos digitais.</p>

                <hr>
                <a href="{{route('product.add')}}" class="btn btn-success waves-effect waves-light m-1"><i class="fa fa-cubes mr-1"></i> Cadastrar Novo Produto</a>
            </div>

        </div>
    </div>
</div>

{{-- Corpo da Página --}}

<div class="row">

    <div class="col-md-12">
        <h4 class="page-title">Produtos já cadastrados</h4>
    </div>

    <hr>

    <div class="col-md-12">
        <div class="row">
            @foreach ($products as $product)

                <div class="col-lg-3">
                    <div class="card">
                        @if ($product->image)
                            <img  style="height: 200px;object-fit: contain;" src="{{ \App\Helpers\Helper::getPathFileS3('images', $product->image)}}" class="card-img-top"  alt="Card image cap">
                        @else
                            <img  style="width: 200px;height: 200px;object-fit: contain;" src="{{url('storage/products')}}" class="card-img-top" alt="Card image cap">
                        @endif

                        <div class="card-body">
                            <h5 class="card-title text-dark">{{$product->name}}</h5>
                        </div>
                        <ul class="list-group list-group-flush list shadow-none">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Preço<span  style="font-weight: bold">{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plans[0]->price : 'Planos' }}</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Quantidade de venda<span  style="font-weight: bold">{{App\Repository\SalesRepository::getQtdVenda($product->id)}}</span>
                            </li>

                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Afiliados<span style="font-weight: bold">{{$product->affiliates()->count()}}</span>
                            </li>
                        </ul>

                        <div class="row text-right">
                            <div class="col-md-12" style="max-width: 90%;">

                                <div class="col-md-12">
                                    <button class="btn btn-danger m-1" data-toggle="modal" data-target="#dangermodal">Excluir</button>
                                    <a href="{{route('product.edit', $product->id)}}" class="btn btn-inverse-dark  waves-effect waves-light m-1 card-link"><i class="fa fa-pencil-square-o""></i> Editar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="modal fade" id="dangermodal">
    <div class="modal-dialog" style="background-color: white">
        <div class="modal-content border-danger">

            <div class="modal-header bg-danger">
                <h5 class="modal-title text-white"><i class="fa fa-trash"></i>Aviso</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>Após solicitar a exclusão do produto isso ocorrerá em 3 dias e essa ação não poderá ser desfeita.</p>
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-inverse-danger" style="margin: 5px;" data-dismiss="modal"><i class="fa fa-times"></i>Fechar</button>
                        <button type="button" class="btn btn-danger" style="margin: 5px;"><i class="fa fa-check-square-o"></i> Sim! Eu Quero Excluir o meu Produto</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
