@extends('plataforma.templates.template')

@section('content')
    <style>
        .peq {
            font-size: 18px;
            line-height: 27px;
        }

        .image {
            height: 200px;
            width: 200px;
            vertical-align: middle;
            border-style: none;
        }
    </style>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-warning text-white">

                    <h5 class="text-white">{{$product->name}}
                        <button class="btn btn-white waves-effect float-right" data-toggle="modal" data-target="#denunciaModal">Denunciar produto</button>
                    </h5>

                </div>

                <div class="card-body">

                    <div class="row" style="margin-top:20px">
                        <div class="col-md-3" style="">
                            <div class="card card-primary">
                                <img src="{{ \App\Helpers\Helper::getPathFileS3('images', $product->image) }}" class="card-img-top image" alt="Card image cap">
                            </div>
                        </div>
                        <div class="col-md-6" style="">
                            <h4>{{$product->name}}</h4>
                            <p class="text-dark"></p>

                            @if($product->payment_type == 'SINGPRICE')
                                <h3 class="peq">Você recebe até: <span style="color:yellowgreen">R$ {{$valueComission}}</span> por venda</h3>
                            @else
                                <h3>Veja detalhes nos planos para ver comissões</h3>
                            @endif

                            <p><strong>Produtor:</strong><b><a href="{{route('user.perfil', $producer->id)}}">
                                        @if($producer->client && $producer->client->fantasia){{$producer->client->fantasia }}@else{{$producer->name}}
                                        @endif
                                    </a></b></p>
                            <p><strong>E-mail de suporte:</strong>
                                @if($product->email_support == null)
                                    Naõ definido
                                @else
                                    {{ $product->email_support }}
                                @endif
                            </p>
                            <p><strong>Tipo de produto:</strong> {{ $product->product_type == 'V' ? 'Virtual' : 'Físico' }}</p>
                            <p><strong>Entrega do produto:</strong> {{$product->typeDeliverie->desc}} </p>
                            <p><strong>Segmento:</strong> {{$category->desc}} </p>
                            <p><strong>Tipo comissão:</strong> Último clique</p>
                            <p><strong>Validade do Cookie:</strong>
                                @if($product->configAffiliation->expiration_coockie == null)
                                    Naõ definido
                                @else
                                    {{  \App\Helpers\Helper::expirationCoockie($product->configAffiliation->expiration_coockie) }}
                                @endif
                            </p>
                            <p><strong>Valor do produto: R$</strong> {{ $product->plansWithoutGlobalScope[0]->price }}</p>
                            <p><strong>Garantia:</strong> {{$product->warranty}} dias</p>
                            <p><strong> Url do produto:</strong> <a href="{{$salePage->url_page_sale}}" target="_blank">{{$salePage->url_page_sale}}</a></p>
                            @if(count($checkouts))
                                <p><strong> Url de Checkout:</strong>
                                    @foreach ($checkouts as $checkout)
                                        <a href="{{ url('/chk/' . $checkout->code)}}" target="_blank">{{url('/chk/' . $checkout->code)}}</a> <br>
                                @endforeach
                            @endif
                        </div>

                        @if (!$isAffiliateUserloggedThisProduct || $isAffiliateUserloggedThisProduct == "I")
                            <div class="col-md-3 text-white" style="">
                                {!! Form::open(['route' => 'product.affiliate', 'method' => 'POST']) !!}
                                <input type="hidden" value="{{$product->id}}" name="product_id">
                                <input type="hidden" value="{{$product->uuid}}" name="uuid">
                                <button class="btn btn-success waves-effect waves-light m-1" data-toggle="modal" data-target="#solicitModal">Solicitar Afiliação</button>
                                {!! Form::close() !!}
                            </div>
                        @else
                            <div class="col-md-3 text-white" style="">
                                <input type="hidden" value="{{$product->id}}" name="product_id">
                                <h6 class="modal-title" style="text-align: center">Você já é afiliado deste produto. Veja Aqui os seus links de afiliado:</h6>
                                <button class="btn btn-success waves-effect waves-light m-1" onclick="redirectToLinks({{ $product->id }})">Seus links de divulgação</button>
                            </div>
                        @endif

                    </div>

                    <hr>

                    <div class="card-body">
                        <ul class="nav nav-pills" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="pill" href="#affiliation"><i class="fa fa-users"></i> <span class="hidden-xs">Afiliação</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#about"><i class="icon-user"></i> <span class="hidden-xs">Sobre</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#media"><i class="fa fa-picture-o"></i> <span class="hidden-xs">Conquistas do produto</span></a>
                            </li>
                            @if ($product->payment_type == 'PLAN')
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="pill" href="#plans"><i class="fa fa-money"></i> <span class="hidden-xs">Planos</span></a>
                                </li>
                            @endif
                        </ul>

                        {!! Form::open(['route' => 'product.complaints', 'method' => 'POST']) !!}
                        <input type="hidden" value="{{$product->id}}" name="product_id">
                        <div class="modal fade" id="denunciaModal">
                            <div class="modal-dialog" style="background-color: white">
                                <div class="modal-content border-danger">
                                    <div class="modal-header bg-danger">
                                        <h5 class="modal-title text-white"><i class="fa fa-trash"></i> Aviso</h5>
                                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Fechar">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Descreva o motivo pelo qual está denunciando este produto. </p>

                                        <div class="form-group">
                                            <div class='col-sm-6'>
                                                {!! Form::select('motivo', ['' => 'Motivo','Falsificação de produto' => 'Falsificação de produto','Cópia de produto' => 'Cópia de produto','Produto de pésssima qualidade' => 'Produto de pésssima qualidade','Outros' => 'Outros'], null, ['class' => 'form-control']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class='col-sm-12'>
                                                {!! Form::textarea('desc', null, ['class' => 'form-control', 'placeholder' => 'Descreva', 'rows' => 5]) !!}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-inverse-danger" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-check-square-o"></i> Denunciar</button>
                                    </div>
                                </div>
                            </div>
                        </div><!--End Modal -->
                    {!! Form::close() !!}

                    <!-- Tab panes -->
                        <div class="tab-content">
                            <div id="affiliation" class="container tab-pane active">
                                <p>{{$product->configAffiliationWithoutGlobalScope->desc_affiliate}}</p>
                            </div>

                            <div id="about" class="container tab-pane">
                                <p>{{$product->desc}}</p>
                            </div>

                            <div id="media" class="container tab-pane">

                            </div>
                            @if ($product->payment_type == 'PLAN')
                                <div id="plans" class="container tab-pane">

                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-dialog" style="background-color: white;display:none">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white"><i class="fa fa-checked"></i> Confirmar</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('product.affiliate') }}" method="post">
                <div class="modal-body">
                    <h6>Você deseja afiliar-se a este produto?</h6>
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox("statusaffiliate[{$product->id}]",$product->id, $product->statusaffiliate) !!} Autorizar
                        </label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-inverse-white" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
                    <button class='btn btn-success' type='submit' ><i class=''></i>Afiliar-se</button>
                </div>
            {!! Form::close() !!}
        </div>

        @endsection
        @section('scripts')
            <script src="{{asset('dist/js/jquery.maskMoney.min.js')}}"></script>
            <script>
                $("#price").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

                function redirectToLinks(id){
                    window.location = {!! json_encode(url('/affiliates/urls')) !!} + '/' + id;
                }
            </script>
@endsection


