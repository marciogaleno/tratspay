@extends('plataforma.templates.template') 

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/dropify/dropify.min.css')}}">
@endsection


@section('content')

<div class="row">
  <div class="col-lg-12">
     <div class="card">
        <div class="card-body"> 
          <ul class="nav nav-tabs nav-tabs-warning nav-justified top-icon" id="tabsAddProduct">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#dados-gerais"><i class="fa fa-folder-open-o"></i> <span class="hidden-xs">Informações Básicas</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#vendas" id="tabSales"><i class="fa fa-cart-plus"></i> <span class="hidden-xs">Precificação</span></a>
            </li>
           <li class="nav-item">
              <a class="nav-link" href="#afiliados" id="tabAffiliation"><i class="fa fa-money"></i> <span class="hidden-xs">Afiliados</span></a>
            </li>
           
           
          </ul>

          <!-- Tab panes -->
          {{Form::open(['id' => 'formAddProduct', 'enctype' => "multipart/form-data"])}}
          <div class="tab-content">

            <div id="dados-gerais" class="container tab-pane active">
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('product_type') ? 'has-error' : '' }}" >
                          {!! Form::label('product_type', 'Tipo de Produto:', ['for' => 'product_type' ]) !!}
                          {!! Form::select('product_type',  \App\Helpers\Helper::productTypes(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'required'  => true, "id"=>"product_type"]) !!}
                          @if ($errors->has('product_type'))
                            <label class="error" for="product_type" id="labelProductType">{{$errors->first('product_type')}}</label>
                          @endif  
                          <label class="error" for="product_type" id="label_product_type"></label>
                        </div>
                      </div>
                </div>


                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                            {!! Form::label('name', 'Nome do Produto:', ['for' => 'name' ]) !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            @if ($errors->has('name'))
                            <label class="error" for="name">{{$errors->first('name')}}</label>
                            @endif  
                            <label class="error" for="name"  id="label_name"></label>
                        </div>
                      </div>
                </div>
                
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('category_id') ? 'has-error' : '' }}">
                            {!! Form::label('category_id', 'Categorias:', ['for' => 'category_id' ]) !!}
                            {!! Form::select('category_id', $categories, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                            @if ($errors->has('category_id'))
                            <label class="control-label" for="category_id">{{$errors->first('category_id')}}</label>
                            @endif 
                            <label class="error" for="name"  id="label_category_id"></label>
                        </div>
                    </div>
                </div>
                
                                
                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group">        
                            {!! Form::label('image', 'Imagem do Produto que será exibida na página de Mercado:', ['for' => 'image', "style" => "margin-bottom:0px"]) !!}
                            <br>
                            <small style="">A imagem deve possuir o tamanho de 200x200</small>
                            <input type="file" id="input-file-now" class="dropify" name="image" id="mage" 
                              data-allowed-file-extensions="jpg jpeg png"/>
                            <br>              
                        </div>
                  </div>
                </div>

                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('desc') ? 'has-error' : '' }}" >
                            {!! Form::label('desc', 'Descricao:', ['for' => 'desc', "style"=> "margin-bottom: 0px;"]) !!}
                            <br><small style="font-size: 9px"> (Essas informações estarão visíveis para os seus afiliados entenderem o seu produto e também para os clientes através do ato de compra)</small>
                            {!! Form::textarea('desc', null, ['class' => 'form-control', 'rows' => 5]) !!}
                            @if ($errors->has('desc'))
                              <label class="error" for="desc">{{$errors->first('desc')}}</label>
                            @endif  
                            <label class="error" for="desc" id="label_desc"></label>
                        </div>
                      </div>
                </div>

                <div class="row"> 
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('email_support') ? 'has-error' : '' }}">
                            {!! Form::label('email_support', 'Email de Suporte:', ['for' => 'email_support' ]) !!}
                            <small style="font-size: 9px">(Seus clientes irão ver este e-mail)</small>
                            {!! Form::text('email_support', null, ['class' => 'form-control']) !!}
                        @if ($errors->has('email_support'))
                            <label class="error" for="email_support">{{$errors->first('email_support')}}</label>
                            @endif 
                            <label class="error" id="label_email_support"></label>
                        </div>
                      </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group has-feedback {{ $errors->has('type_deliverie_id') ? 'has-error' : '' }}">
                            {!! Form::label('type_deliverie_id', 'Como o Produto é Entregue?:', ['for' => 'type_deliverie_id' ]) !!}
                            {!! Form::select('type_deliverie_id',   $deliverieTypes, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'id' => 'type_deliverie_id']) !!}
                            @if ($errors->has('type_deliverie_id'))
                            <label class="error" for="tipo_entrega_id">{{$errors->first('type_deliverie_id')}}</label>
                            @endif 
                            <label class="error" id="label_type_deliverie_id"></label>
    
                        </div>
                      </div>
                  </div>
          
                  <div class="row" style="{{old('type_deliverie_id') <> 1 ? 'display:none': ''}}" id="formGroupUrlMemberArea">
                    <div class="col-md-12">
                      <input type="hidden" name="type_member_area" value="{{old('type_deliverie_id') == 1 ? 1: 0}}" id="type_member_area">
                        <div class="btn-group">
                            <button type="button" id="btnMemberArea" class="btn btn-outline-warning waves-effect waves-light active">Área de Membros</button>
                            <button type="button" id="btnTratsClub" class="btn btn-outline-warning waves-effect waves-light ">TratsClub</button>
                        </div>

                        <div class="form-group has-feedback {{ $errors->has('url_member_area') ? 'has-error' : '' }}" 
                              id="inputUrlMemberArea" style="margin-top: 10px">
                            {!! Form::text('url_member_area', null, ['class' => 'form-control', 'placeholder' => 'Digite a URL de login a sua área de membros', 'type' => 'url']) !!}
                            @if ($errors->has('local_page_sale'))
                            <label class="error" for="url_member_area">{{$errors->first('url_member_area')}}</label>
                            @endif 
                            <label class="error" id="url_member_area"></label>

                        </div>

                        <div class="form-group" style="margin-top: 10px; display:none" id="linkButtonTratsClub">
                           <button type="button" class="btn btn-light waves-effect waves-light m-1">Acessar TratsClub</button>
                        </div>
                    </div>
                  </div>
 
                  <div class="row">
                    <div class="col-md-12">
                          <div class="form-group has-feedback {{ $errors->has('local_page_sale') ? 'has-error' : '' }}">
                            {!! Form::label('local_page_sale', 'Onde irá vender?:', ['for' => 'local_page_sale' ]) !!}
                            {!! Form::select('local_page_sale',   \App\Helpers\Helper::localPageSale(), 'PE', ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                            @if ($errors->has('local_page_sale'))
                            <label class="error" for="local_page_sale">{{$errors->first('local_page_sale')}}</label>
                            @endif 
                            <label class="error" id="label_local_page_sale"></label>

                          </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group has-feedback {{ $errors->has('url_page_sale') ? 'has-error' : '' }}">
                        {!! Form::label('url_page_sale', 'Endereço da Página de Vendas :', ['for' => 'url_page_sale',"style" => "margin-bottom: 0px;" ]) !!}
                          <br><small style="font-size: 9px">(Link da Página de Vendas Principal com checkout da TratsPay)</small>
                        {!! Form::text('url_page_sale', null, ['class' => 'form-control', 'type' => 'url']) !!}
                        @if ($errors->has('url_page_sale'))
                        <label class="error" for="url_page_sale">{{$errors->first('url_page_sale')}}</label>
                        @endif 
                        <label class="error" id="label_url_page_sale"></label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group has-feedback {{ $errors->has('url_page_thank') ? 'has-error' : '' }}">
                        {!! Form::label('url_page_thank', 'Endereço da Página de Obrigado - Cartão :', ['for' => 'url_page_thank', "style" => "margin-bottom: 0px;" ]) !!}
                          <br><small style="font-size: 9px">(Link da página de vendas com venda confirmada imediatamente)</small>
                        {!! Form::text('url_page_thank', null, ['class' => 'form-control', 'type' => 'url']) !!}
                        @if ($errors->has('url_page_thank'))
                        <label class="error" for="url_page_thank">{{$errors->first('url_page_thank')}}</label>
                        @endif 
                        <label class="error" id="label_url_page_thank"></label>

                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group has-feedback {{ $errors->has('url_page_print_ticket') ? 'has-error' : '' }}">
                        {!! Form::label('url_page_print_ticket', 'Endereço de Boleto Impresso:', ['for' => 'url_page_print_ticket', "style" => "margin-bottom: 0px;" ]) !!}
                          <br><small style="font-size: 9px">(Link da página para quem gerou boleto do seu produto)</small>
                        {!! Form::text('url_page_print_ticket', null, ['class' => 'form-control', 'type' => 'url']) !!}
                        @if ($errors->has('url_page_print_ticket'))
                        <label class="error" for="url_page_print_ticket">{{$errors->first('url_page_print_ticket')}}</label>
                        @endif 
                        <label class="error" id="label_url_page_print_ticket"></label>

                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group has-feedback {{ $errors->has('several_purchases') ? 'has-error' : '' }}">
                          {!! Form::label('several_purchases', 'Permitir mais de uma compra de um mesmo cliente?:', ['for' => 'several_purchases', "style" => "margin-bottom: 0px;" ]) !!}
                          <br><small style="font-size: 9px">(Permite que o seu cliente faça mais de uma compra do seu produto)</small>
                          {!! Form::select('several_purchases',   \App\Helpers\Helper::severalPurchases(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                          @if ($errors->has('several_purchases'))
                          <label class="error" for="several_purchases">{{$errors->first('several_purchases')}}</label>
                          @endif 
                          <label class="error" id="label_several_purchases"></label>

                      </div>
                    </div>
                  </div>
        
                  <div class="modal-footer">
                      <button type="button" id="buttonNext01" class="btn btn-gradient-blooker waves-effect waves-light m-1" data-dismiss="modal">Avançar</button>
                  </div>
            </div>
      
   
            <div id="vendas" class="container tab-pane fade">
                
                <div class="row m-t-2">
                    <div class="col-md-6">
                      <div class="form-group has-feedback {{ $errors->has('payment_type') ? 'has-error' : '' }}">
                        {!! Form::label('payment_type', 'Tipo Pagamento:', ['for' => 'payment_type' ]) !!}
                        {!! Form::select('payment_type',   \App\Helpers\Helper::paymentType(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                        @if ($errors->has('payment_type'))
                        <label class="error" for="payment_type">{{$errors->first('payment_type')}}</label>
                        @endif 
                        <label class="error" for="payment_type" id="label_payment_type"></label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group has-feedback {{ $errors->has('warranty') ? 'has-error' : '' }}">
                        {!! Form::label('warranty', 'Garantia:', ['for' => 'warranty' ]) !!}
                        {!! Form::select('warranty',   \App\Helpers\Helper::warranty(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                        @if ($errors->has('warranty'))
                        <label class="error" for="warranty">{{$errors->first('warranty')}}</label>
                        @endif 
                        <label class="error" for="warranty" id="label_warranty"></label>
                      </div>
                    </div>
                  </div>
                  
                  <div class="row m-t-2">
                      <div class="col-md-12" id="panelValorUnico" style="{{old('price_unique') ? '' : 'display:none'}}">
                          <div class="form-group has-feedback {{ $errors->has('price_unique') ? 'has-error' : '' }}">
                              {!! Form::label('price_unique', 'Valor:', ['for' => 'price_unique' ]) !!}
                              {!! Form::text('price_unique', null, ['class' => 'form-control', "id"=>"price_unique"]) !!}
                              @if ($errors->has('price_unique'))
                                  <label class="error" for="price_unique">{{$errors->first('price_unique')}}</label>
                              @endif
                              <label class="error" for="warranty" id="label_price_unique"></label>
                              <label>Você receberá: <span id="labelValueWithoutRate">00,00</span></label>
                          </div>
                          
                      </div>
                  </div>
      
                  <div class="row m-t-2" id="panelAddPlanos" style="{{old('payment_type') == 'PLAN' ? '' : 'display:none'}}">
                      <div class="col-md-12">
                        <button type="button" class="btn btn-success pull-right" data-toggle="modal" onclick="openModal()"><i class="fa fa-plus"></i> Novo Plano </button>
                        <div class="table-responsive">
                            <table class="table" id="tablePlanos">
                              <thead class="thead-light">
                                <tr>
                                  <th scope="col">Planos</th>
                                </tr>
                              </thead>
                              <tbody>
      
                              </tbody>
                            </table>
                          </div>
                      </div>
                  </div>

                  <div class="modal-footer">
                      <button type="button" id="buttonNext02" class="btn btn-gradient-blooker waves-effect waves-light m-1" data-dismiss="modal">Avançar</button>
                  </div>

            </div>



            <div id="afiliados" class="container tab-pane fade">
                    <div class="col-md-12">
                      <div class="form-group has-feedback {{ $errors->has('accept_affiliation') ? 'has-error' : '' }}">
                        {!! Form::label('accept_affiliation', 'Deseja Aceitar Afiliados?:', ['for' => 'accept_affiliation' ]) !!}
                        {!! Form::select('accept_affiliation',   \App\Helpers\Helper::acceptAffiliation(), 0, ['class' => 'custom-select form-control']) !!}
                        @if ($errors->has('accept_affiliation'))
                        <label class="error" for="accept_affiliation">{{$errors->first('accept_affiliation')}}</label>
                        @endif 
                      </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('format_commission', 'Formato da Comissão:', ['for' => 'format_commission' ]) !!}
                                    {!! Form::select('format_commission',   \App\Helpers\Helper::formatCommission(), null, ['class' => 'custom-select form-control']) !!}
                                    @if ($errors->has('format_commission'))
                                        <label class="error" for="format_commission">{{$errors->first('accept_affiliation')}}</label>
                                    @endif
                                    <label class="error" for="format_commission" id="label_format_commission"></label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="value_commission">Valor Comissão:</label>
                                    {!! Form::text('value_commission', null, ['class' => 'form-control', 'id' => 'value_commission']) !!}
                                    @if ($errors->has('value_commission'))
                                        <label class="error" for="value_commission">{{$errors->first('value_commission')}}</label>
                                    @endif
                                    <label class="error" for="value_commission" id="label_value_commission">{{$errors->first('value_commission')}}</label>
                                </div>
                            </div>
                        </div>
                      <i></i>

                        <div class="row">
                          <div class="col-md-12">
                              {!! Form::label('assignment_type', 'Tipo de Atribuição:', ['for' => 'assignment_type', 'style' => 'margin-bottom: 0px' ]) !!}
                              <br>
                              <small style="font-size: 10px">Você pode selecionar se o primeiro afiliado que indicar o cliente irá receber a comissão, se será o último afiliado que indicar o cliente ou se o primeiro e último afiliado que indicarem o cliente irão dividir a comissão.</small>
                              <div class="col-md-3" style="cursor: pointer; padding-left: 0px; padding-top:11px">
                                  <div style = "display: flex;" class="form-group has-feedback {{ $errors->has('assignment_type') ? 'has-error' : '' }}">
                                      {{-- {!! Form::select('assignment_type',   $assignment_type, null, ['class' => 'custom-select form-control']) !!} --}}
                                      <img id="1" src="{{asset('assets/images/config_afiliado1.png')}}" height="200" width="200" class="card-img-top" style="display:none" alt="Card image cap">
                                      <img id="1_a" src="{{asset('assets/images/config_afiliado1_a.png')}}" height="200" width="200" class="card-img-top" alt="Card image cap">
                                      <img id="2" src="{{asset('assets/images/config_afiliado2.png')}}" height="200" width="200" class="card-img-top" alt="Card image cap">
                                      <img id="2_a" src="{{asset('assets/images/config_afiliado2_a.png')}}" height="200" width="200" class="card-img-top" style="display:none" alt="Card image cap">
                                      <input type="hidden" id="assignment_type" name="assignment_type" value="2">
                                      @if ($errors->has('assignment_type'))
                                          <label class="error" for="assignment_type">{{$errors->first('assignment_type')}}</label>
                                      @endif
                                  </div>
                              </div>
                          </div>
                            
                            {{-- <div class="col-md-9" style="float:right"></div> --}}
                        </div>

                      <div class="form-group has-feedback {{ $errors->has('expiration_coockie') ? 'has-error' : '' }}">
                        {!! Form::label('expiration_coockie', 'Validade do Cookie:', ['for' => 'expiration_coockie' ]) !!}
                        {!! Form::select('expiration_coockie',   \App\Helpers\Helper::expirationCoockie(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                        @if ($errors->has('expiration_coockie'))
                        <label class="error" for="expiration_coockie">{{$errors->first('expiration_coockie')}}</label>
                        @endif
                        <label class="error" for="expiration_coockie" id="label_expiration_coockie">{{$errors->first('expiration_coockie')}}</label>
                      </div>

                        <div class="form-group has-feedback {{ $errors->has('promover_produto') ? 'has-error' : '' }}">
                            {!! Form::label('promover_produto', 'Qualquer pessoa pode promover o meu produto?', ['for' => 'promover_produto' ]) !!}
                            {!! Form::select('promover_produto',   ['0'=> 'Não', '1' => 'Sim', '2' => 'Apenas usuários convidados por mim podem se tornarem afiliados'], old('promover_produto'), ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                            @if ($errors->has('promover_produto'))
                                <label class="error" for="promover_produto">{{$errors->first('promover_produto')}}</label>
                            @endif
                            <label class="error" for="promover_produto" id="label_promover_produto">{{$errors->first('promover_produto')}}</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        {!! Form::label('desc_affiliate', 'Descrição para os Afiliados:', ['for' => 'desc_affiliate' ]) !!}
                        {!! Form::textarea('desc_affiliate', null, ['class' => 'form-control', 'rows' => 5]) !!}
                      </div>
                      <div class="form-group" id="checkboxAfiliado">
                        <label><b>Selecione Opções que se encaixam a você:</b></label><br>
                          <div class="form-group">
                              {{ Form::checkbox('producer_refund') }}
                              <span class="custom-control-description ml-0">No caso de reembolsos, o produtor assume todo o valor da venda, sem descontar comissões de afiliado.</span>
                          </div>
                          <div class="form-group">
                              {{ Form::checkbox('hide_data_customer') }}
                              <span class="custom-control-description ml-0">Ocultar Dados de Clientes dos Afiliados</span>
                          </div>
                          <div class="form-group">
                              {{ Form::checkbox('afil_link_chk') }}
                              <span class="custom-control-description ml-0">O afiliado pode ter um Link de Checkout</span>
                          </div>
                          <div class="form-group">
                              {{ Form::checkbox('coupon') }}
                              <span class="custom-control-description ml-0">O Afiliado pode criar cupom de desconto (O desconto será descontado da comissão de afiliado)</span>
                          </div>
                          <div class="form-group">
                              {{ Form::checkbox('product_hide') }}
                              <span class="custom-control-description ml-0">Produto oculto na página de Mercado</span>
                          </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" onclick="save()" class="btn btn-gradient-blooker waves-effect waves-light m-1" data-dismiss="modal">Concluir</button>
                    </div>
              </div>
    
            </div>

            {{Form::close()}}

        </div>
     </div>
  </div>
</div><!--End Row-->

      <!-- Modal -->
<div class="modal fade" id="modalPlanos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="background-color: white;">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Adicionar Plano</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="FormAddPlano">
          <div class="form-group">
            {!! Form::label('name_plan', 'Nome :', ['for' => 'name_plan' ]) !!}
            {!! Form::text('name_plan', null, ['class' => 'form-control', 'required' => true]) !!}
            <label class="error" id="labelNomePlano" for="name_plan"></label>
          </div>
          <div class="form-group">
              {!! Form::label('desc_plan', 'Descrição:', ['for' => 'desc_plan' ]) !!}
              {!! Form::textarea('desc_plan', null, ['class' => 'form-control', 'rows' => 5]) !!}
          </div>
          <div class="form-group">
                <input type="checkbox" name="" id="free_plan">
                <span class="custom-control-description ml-0">Grátis</span> 
          </div>
          <div class="form-group">
              {!! Form::label('price', 'Valor:', ['for' => 'price' ]) !!}
              {!! Form::text('price', null, ['class' => 'form-control', 'id' => 'price']) !!}
              <label class="error" id="labelPrice" for="price"></label>
          </div>
          <div class="form-group">
            {!! Form::label('frequency', 'Frequência de Pagamento:', ['for' => 'frequency' ]) !!}
            {!! Form::select('frequency', \App\Helpers\Helper::frequency(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
            <label class="error" id="labelFrequency" for="frequency"></label>
          </div>
          <div class="form-group" id="formGroupAmountRecurrence" style="display:none">
            {!! Form::label('amount_recurrence', 'Quantidade de Recorrências: (Deixe 0 para ficar até o cliente cancelar)', ['for' => 'amount_recurrence', 'disabled' => true ]) !!}
            {!! Form::number('amount_recurrence', null, ['class' => 'form-control',  'min' => '0']) !!}
            <label class="error" id="labelAmountRecurrence" for="amount_recurrence"></label>
          </div>
          <div class="form-group" id="formGroupFirstInstallment" style="display:none">
            {!! Form::label('first_installment', 'Primeira Parcela:', ['for' => 'first_installment' ]) !!}
            {!! Form::select('first_installment', \App\Helpers\Helper::firstInstallment(), 1, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'disabled' => true]) !!}
          </div>
          <div class="form-group" id="formGroupFreeDays" style="display:none">
            {!! Form::label('free_days', 'Dias Grátis: (A primeira parcela só iniciará a descontar depois desses dias)', ['for' => 'free_days' ]) !!}
            {!! Form::number('free_days', null, ['class' => 'form-control', 'disabled' => true]) !!}
            <label class="error" id="labelFreeDays" for="free_days"></label>
          </div>
          <div class="form-group" id="formGrouDifferentValue" style="display:none">
            {!! Form::label('different_value', 'Valor Diferenciado:', ['for' => 'different_value' ]) !!}
            {!! Form::text('different_value', null, ['class' => 'form-control', 'disabled' => true]) !!}
            <label class="error" id="labelValorDifferentValue" for="different_value"></label>
          </div>

        </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="button" class="btn btn-primary" id="btnAddPlano" onclick="closeModal()">Salvar</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
    <script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script> 
    <script src="{{asset('js/plataforma/validationFormProduct.js')}}"></script> 
    <script src="{{asset('assets/plugins/dropify/dropify.min.js')}}"></script> 
    <script src="{{asset('assets/plugins/notifications/js/lobibox.min.js')}}"></script>
    <script src="{{asset('assets/plugins/notifications/js/notifications.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>

    <script>

            $("#price").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
            $("#price_unique").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
            $("#preco_venda").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
            $("#value_commission").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
            $("#different_value").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

            

            $("#btnMemberArea").click(function(){
                $("#btnTratsClub").removeClass('active');
                $("#btnMemberArea").addClass('active');
                $("#linkButtonTratsClub").hide(); 
                $("#inputUrlMemberArea").show();
                $("#type_member_area").val(1); 
            });

            $("#btnTratsClub").click(function(){
                $("#btnMemberArea").removeClass('active');
                $("#btnTratsClub").addClass('active');
                $("#inputUrlMemberArea").hide();
                $("#linkButtonTratsClub").show(); 
                $("#type_member_area").val(2);
            });
            
            $('#type_deliverie_id').trigger("change");

            $('#type_deliverie_id').on('change', function() {
              if (this.value == 1 ) {
                $("#formGroupUrlMemberArea").show("slow");
              } else {
                $("#formGroupUrlMemberArea").hide("slow");
              }

            });

            $('#payment_type').on('change', function() {
              if (this.value == 'PLAN' ) {
                $("#panelAddPlanos").show("slow");
              } else {
                $("#panelAddPlanos").hide("slow");
              }

              if (this.value == 'SINGPRICE') {
                $("#panelValorUnico").show("slow");
              } else {
                $("#panelValorUnico").hide("slow");
              }

            });

            $('#free_plan').click(function() {
               if ( $(this).prop('checked')) {
                  $("#price").attr('disabled', true);
                  $("#frequency").attr('disabled', true);
                  
                  $("#formGroupAmountRecurrence").hide();
                  $("#amount_recurrence").attr('disabled', true);
                  
                  $("#formGroupFirstInstallment").hide();
                  $("#first_installment").attr('disabled', true);

                  $("#formGrouDifferentValue").hide();
                  $("#different_value").attr('disabled', true);

               } else {
                  $("#price").attr('disabled', false);
                  $("#frequency").attr('disabled', false);
                  
                  $("#formGroupAmountRecurrence").hide();
                  $("#amount_recurrence").attr('disabled', false);
                  
                  $("#formGroupFirstInstallment").hide();
                  $("#first_installment").attr('disabled', false);

                  $("#formGrouDifferentValue").hide();
                  $("#different_value").attr('disabled', false);                 
               }
            });

            $('#frequency').on('change', function() {
              if (this.value != 'UNI' ) {
                $("#formGroupAmountRecurrence").show("slow");
                $("#amount_recurrence").attr('disabled', false);
               
                $("#formGroupFirstInstallment").show("slow");
                $("#first_installment").attr('disabled', false);
              } else {
                $("#formGroupAmountRecurrence").hide("slow");
                $("#amount_recurrence").attr('disabled', true);
               
                $("#formGroupFirstInstallment").hide("slow");
                $("#first_installment").attr('disabled', true);

                $("#formGrouDifferentValue").hide("slow");
                $("#different_value").attr('disabled', true);

                $("#formGroupFreeDays").hide("slow");
                $("#free_days").attr('disabled', true);
              }

            });

            $('#first_installment').on('change', function() {
              if (this.value == 3 ) {
                $("#formGrouDifferentValue").show("slow");
                $("#different_value").attr('disabled', false);
              } else {
                $("#formGrouDifferentValue").hide("slow");
                $("#different_value").attr('disabled', true);
              }

              if (this.value == 4 ) {
                $("#formGroupFreeDays").show("slow");
                $("#free_days").attr('disabled', false);
              } else {
                $("#formGroupFreeDays").hide("slow");
                $("#free_days").attr('disabled', true);
              }

            });

            $('#local_page_sale').on('change', function() {
              if (this.value == 'PE' ) {
                $("#url_page_sale").attr("disabled", false);
                $("#url_page_thank").attr("disabled", false);
                $("#url_page_print_ticket").attr("disabled", false);
              } 

              if (this.value == 'PI' ) {
                $("#url_page_sale").attr("disabled", true);
                $("#url_page_thank").attr("disabled", true);
                $("#url_page_print_ticket").attr("disabled", true);
              } 
            });

            $('#accept_affiliation').on('change', function() {
              if (this.value == 1 ) {
                $("#format_commission").attr("disabled", false);
                $("#value_commission").attr("disabled", false);
                $("#expiration_coockie").attr("disabled", false);
                $("#desc_affiliate").attr("disabled", false);
                $("#checkboxAfiliado").show();
              } else {
                $("#format_commission").attr("disabled", true);
                $("#value_commission").attr("disabled", true);
                $("#expiration_coockie").attr("disabled", true);
                $("#desc_affiliate").attr("disabled", true);
                $("#checkboxAfiliado").hide();               
              }
            });

            if ($("#accept_affiliation option:selected").val() == 1) {
              $("#format_commission").attr("disabled", false);
              $("#value_commission").attr("disabled", false);
              $("#validade_coockie").attr("disabled", false);
              $("#desc_afiliado").attr("disabled", false);
              $("#checkboxAfiliado").show();
            } else {
              $("#format_commission").attr("disabled", true);
              $("#value_commission").attr("disabled", true);
              $("#expiration_coockie").attr("disabled", true);
              $("#desc_affiliate").attr("disabled", true);
              $("#checkboxAfiliado").hide();
            }

          function openModal() {
              $('#FormAddPlano').trigger("reset");
              $("#FormAddPlano").find('.form-group').removeClass('has-error');
              $("#FormAddPlano").find('.control-label').text('');


              $("#formGroupAmountRecurrence").hide();
              $("#amount_recurrence").attr('disabled', true);

              $("#formGroupFirstInstallment").hide();
              $("#first_installment").attr('disabled', true);

              
              $("#formGrouDifferentValue").hide();
              $("#different_value").attr('disabled', true);

              $("#formGroupFreeDays").hide();
              $("#free_days").attr('disabled', true);

              $("#price").attr('disabled', false);
              $("#frequency").attr('disabled', false);

              $('#modalPlanos').modal("show");
          }
 
          function closeModal() {
              $("#price").attr('disabled', true);

              var error = false;
              var namePlan = $("#name_plan");
              var freePlan =$("#free_plan").prop('checked');
              var price = $("#price");
              var frequency = $("#frequency");
              var amountRecurrence = $("#amount_recurrence");
              var firstInstallment = $("#first_installment");
              var differentValue = $("#different_value");
              var freeDays = $("#free_days");
  
              
              if (!namePlan.val()) {
                namePlan.closest('.form-group').removeClass('has-success').addClass('has-error');
                $("#labelNomePlano").text("O campo nome do plano é obrigatório.");
                error = true;
              }

              if (!freePlan) {
                if (!price.val()) {
                  price.closest('.form-group').removeClass('has-success').addClass('has-error');
                  $("#labelPrice").text("O campo valor do plano é obrigatório.");
                    error = true;
                  }
                  if (!frequency.val()) {
                    frequency.closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#labelFrequency").text("O campo periodicidade é obrigatório.");
                    error = true;
                  }

                  if (frequency.val() && frequency.val() != 'UNI' && !amountRecurrence.val() ) {
                    amountRecurrence.closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#labelAmountRecurrence").text("O campo quantidade de recorrência é obrigatório.");
                    error = true;               
                  }

                  if (firstInstallment.val() == 3 && !differentValue.val() ) {
                    differentValue.closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#labelValorDifferentValue").text("O campo valor diferenciado é obrigatório.");
                    error = true;               
                  }

                  if (firstInstallment.val() == 4 && !freeDays.val() ) {
                    
                    freeDays.closest('.form-group').removeClass('has-success').addClass('has-error');
                    $("#labelFreeDay").text("O campo dias grátis é obrigatório.");
                    error = true;               
                  }


              }

              if (!error) {
                  $('#modalPlanos').modal("hide");
                  addPlano();
              }
                
          }

          function addPlano() {
              var contadorLinhas =0;
              var name_plan = $("#name_plan").val();
              var desc_plan = $("#desc_plan").val();
              var free_plan = $("#free_plan").prop('checked');
              var price = $("#price").val();
              var frequency = $("#frequency").val();
              var frequency_text = $("#frequencyoption:selected").text();
              var amount_recurrence = $("#amount_recurrence").val();
              var first_installment = $("#first_installment").val();
              var first_installment_text = $("#first_installment option:selected").text();
              var free_days = $("#free_days").val();
              var different_value = $("#different_value").val();

              contadorLinhas = contadorLinhas + 1;
              var idTrTablePlano = 'rowPlano-' + contadorLinhas;

              var tr = '<tr id="' + idTrTablePlano  +'" >';

              var td = '<td>'
              td += '<div class="row m-t-2">';
              td += '<div class="col-md-4">'
              
              td += '<strong>Nome Plano: &nbsp</strong>' + name_plan + '<br>';

              if (free_plan) {
                td += '<strong>Grátis: &nbsp</strong>Sim<br>';
              }
             

              if ( !free_plan ) {
                td += '<strong>Valor Plano: &nbsp</strong>' + price + '<br>'
                td += '<strong> Periodicidade: &nbsp</strong>' + frequency_text + '<br>';

                if ( amount_recurrence && frequency != 'UNI' && amount_recurrence == 0)
                td += '<strong>Quantidade Recorrencia: &nbsp</strong> Até o cliente cancelar<br>';
                
                if ( amount_recurrence && frequency != 'UNI' && amount_recurrence != 0)
                td += '<strong>Primeira parcela: &nbsp</strong>' + amount_recurrence + '<br>';

                if ( first_installment && frequency != 'UNI' )
                td += '<strong>Primeira parcela: &nbsp</strong>' + first_installment_text + '<br>';
              
                if ( free_days && frequency != 'UNI' )
                td += '<strong>Dias Grátis: &nbsp</strong>' + free_days + '<br>';

                if ( different_value && frequency != 'UNI')
                td += '<strong>Preço Diferenciado: &nbsp</strong>' + different_value + '<br>';
              }
          
              td += '</div>';
              
                              
              td += '<div class="col-md-6">';
              td += '<strong>Descrição Plano: &nbsp</strong><p>' + desc_plan + '</p><br>';
              td += '</div>'

              td += '<div class="col-md-2">';
              td += '<button type="button" class="btn btn-danger pull-right" onclick="excluirPlano(\'' + idTrTablePlano + '\')"><i class="fa fa-trash"></i> Excluir </button>';
              td += '</div>'


              td += '</div>';
              td += '</td>';
              
              tr += td;

              $("#FormAddPlano :input").each(function(){
                if ( $(this).val() ) {
                  tr  += '<input type="hidden" name="' + $(this).attr('name') + '[]" value="' + $(this).val() +  '">';
                } 
              });

              tr += '</tr>';
            
              $('#tablePlanos tbody').append(tr);
              
          }

          function excluirPlano(trtablePlano) {
              $("#" + trtablePlano).remove();
          }

          function initForm() {
              $('#FormAddPlano').trigger("reset");
              $("#FormAddPlano").find('.form-group').removeClass('has-error');
              $("#FormAddPlano").find('.control-label').text('');
          }

          $('.dropify').dropify({
              messages: {
              'default': 'Selecione ou arraste e solte um arquivo aqui',
              'replace': 'Selecione ou arraste e solte um arquivo aqui',
              'remove':  'Remover',
              'error':   'Ooops, algume erro aconteceu.'
              },
              error: {
              'fileSize': 'O temanho do arquivo é maior que 1 GB).',
              'imageFormat': 'O formato da imagem não é permitido.'
              }

          });

            $('#price_unique').keyup(function() {
                var price = $('#price_unique').val();
                price = parseFloat(price.replace(".", "").replace(",", "."));

                const TAXA_PRODUCT_FISICO = 6.9
                const TAXA_PRODUCT_VIRTUAL = 9.9

                // Para não mostrar valor negativo enquanto digita um valor menor que 1 real
                if (price < 1) {
                  $("#labelValueWithoutRate").text(numberParaReal(price));
                  return;
                } 
                console.log($("#product_type").val());
                if($("#product_type").val() == "F") {
                    var value = price - (1+(price * (TAXA_PRODUCT_FISICO / 100)));
                    $("#labelValueWithoutRate").text(numberParaReal(value));
                }else {
                    var value = price - (1+(price * (TAXA_PRODUCT_VIRTUAL / 100)));
                    $("#labelValueWithoutRate").text(numberParaReal(value));
                }
                

            })

            function numberParaReal(numero){
                return numero.toFixed(2).replace(".",",");
            }

            function save()
            {
                var url = {!! json_encode(url('products/store')) !!};
                var data = new FormData($("#formAddProduct")[0]);

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
              
                $.ajax({
                url : url,
                type : 'POST', 
                data: data,
                processData: false,
                contentType: false,
                beforeSend : function(){
                    $.preloader.start();
                }
                })
                .done(function(msg, textStatus, xhr){
                    var status = xhr.status;
                    $.preloader.stop();
                    var resp = JSON.parse(msg);
                    
                    notification("success", "Sucesso", "Produto criado com sucesso!");
                    window.location = {!! json_encode(url('product/')) !!} + '/' + resp.product_id + '/edit';
                        
                }).fail(function(data){
                    $.preloader.stop();
                    // var errors = JSON.parse(data);
                    // console.log(data);
                    notification("error", "Erro", "Erro ao salvar. Por favor, tente novamente.");
                }); 
            }

            function notification(type, title, message){
                Lobibox.notify(type, {
                    pauseDelayOnHover: true,
                    continueDelayOnInactiveTab: false,
                    position: 'center top',
                    showClass: 'fadeInDown',
                    hideClass: 'fadeOutDown',
                    width: 600,
                    title: title,
                    msg: message
                });
            }
            
             function images(){
                $("#1_a").click(function(){
                  document.getElementById('1_a').style.display = "none";
                  document.getElementById('2_a').style.display = "none";
                  //document.getElementById('3_a').style.display = "none";

                  document.getElementById('1').style.display = "block";
                  document.getElementById('2').style.display = "block";
                  //document.getElementById('3').style.display = "block";
                  $("#assignment_type").val('2');
                });  
                $("#2").click(function(){
                  document.getElementById('1').style.display = "none";
                  document.getElementById('2').style.display = "none";
                  //document.getElementById('3_a').style.display = "none";

                  document.getElementById('1_a').style.display = "block";
                  document.getElementById('2_a').style.display = "block";
                  //document.getElementById('3').style.display = "block";
                  $("#assignment_type").val('1');
                });
                /*$("#3").click(function(){
                  document.getElementById('1').style.display = "none";
                  document.getElementById('2_a').style.display = "none";
                  document.getElementById('3').style.display = "none";

                  document.getElementById('1_a').style.display = "block";
                  document.getElementById('2').style.display = "block";
                  document.getElementById('3_a').style.display = "block";
                  $("#assignment_type").val('3');
                });*/
            }images();


    </script>

@endsection