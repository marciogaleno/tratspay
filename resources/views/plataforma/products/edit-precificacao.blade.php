@extends('plataforma.templates.template') 
@section('content')

<div class="content-header sty-one">
    <h1 class="text-black">Blank Page</h1>
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li class="sub-bread"><i class="fa fa-angle-right"></i> Pages</li>
        <li><i class="fa fa-angle-right"></i> Blank Page</li>
    </ol>
</div>

<!-- Main content -->
<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" href="{{url('/produtos/manager/infoProduto/' . $produto->uuid)}}">
                      <span>
                        <i class="ti-home"></i>
                        Dados Gerais
                      </span>
                      
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="#">
                      <span>
                        <i class="fa fa-cart-arrow-down"></i>
                        Precificação
                      </span>
                      
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#checkout" role="tab">
                      <span>
                        <i class="fa fa-credit-card"></i>
                        Checkout
                      </span>
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#coproducoes" role="tab">
                      <span>
                        <i class="fa fa-users"></i>
                        Co-Produções
                      </span>
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#afiliados" role="tab">
                      <span>
                        <i class="fa fa-money"></i>
                        Afiliados
                      </span>
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#midia" role="tab">
                      <span>
                        <i class="fa fa-picture-o"></i>
                        Mídia
                      </span>
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#campanhas" role="tab">
                      <span>
                        <i class="fa fa-bar-chart"></i>
                        Campanhas
                      </span>
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#cupons" role="tab">
                      <span>
                        <i class="fa fa-handshake-o"></i>
                        Cupons
                      </span>
                    </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#integracoes" role="tab">
                      <span>
                        <i class="fa fa-key"></i>
                        Integrações
                      </span>
                    </a>
                        </li>
                    </ul>


                    <!-- Tab panes -->
                    <div class="tab-content tabcontent-border">

                        <div class="tab-pane active" id="precificacaoTab">

                            <div class="pad-20">
                                <h3>Precificação</h3>
                                <hr>
                                
                                @if ($produto) 
                                @if ($produto->tipo_pagamento == 'PRECOUNICO')
                                    {{ Form::open(['url' => '/produtos/manager/precificacao/updatePrecoUnico/' . $produto->uuid, 'method' => 'put']) }}
                                @else
                                    {{ Form::open(['url' => '/produtos/manager/precificacao/updatePlano/' . $produto->uuid, 'method' => 'put']) }}
                                @endif

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                                <div class="form-group has-feedback {{ $errors->has('tipo_pagamento') ? 'has-error' : '' }}">
                                                    {!! Form::label('tipo_pagamento', 'Tipo Pagamento:', ['for' => 'tipo_pagamento' ]) !!} 
                                                    {!! Form::select('tipo_pagamento', $tiposPagamento, $produto->tipo_pagamento, ['class' => 'custom-select form-control', 'disabled' => true]) !!} 
                                                    @if ($errors->has('tipo_pagamento'))
                                                    <label class="control-label" for="tipo_pagamento">{{$errors->first('tipo_pagamento')}}</label>                                  
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group has-feedback {{ $errors->has('garantia') ? 'has-error' : '' }}">
                                                    {!! Form::label('garantia', 'Prazo de Garantia:', ['for' => 'garantia' ]) !!} 
                                                    {!! Form::select('garantia', $prazosGarantia, $produto->garantia, ['class' => 'custom-select form-control', 'placeholder' => '--Selecione --']) !!} 
                                                @if ($errors->has('garantia'))
                                                    <label class="control-label" for="garantia">{{$errors->first('garantia')}}</label>                     
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @if ($produto->tipo_pagamento == 'PRECOUNICO')
                                        <div class="row">
                                            <div class="col-md-12" id="panelValorUnico">
                                                <div class="form-group has-feedback {{ $errors->has('valor') ? 'has-error' : '' }}">
                                                    {!! Form::label('valor', 'Valor:', ['for' => 'valor' ]) !!}
                                                    {!! Form::text('valor', $produto->planos[0]->valor, ['class' => 'form-control']) !!}
                                                    @if ($errors->has('valor'))
                                                    <label class="control-label" for="valor">{{$errors->first('valor')}}</label>
                                                    @endif 
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                    <div class="row">
                                            <button type="button" class="btn btn-success" data-toggle="modal" onclick="openModal()"><i class="fa fa-plus"></i> Novo Plano </button>
                                            <div class="table-responsive">
                                                <table class="table" id="tablePlanos">
                                                  <thead class="thead-light">
                                                    <tr>
                                                      <th scope="col">Nome</th>
                                                      <th scope="col">Periodicade</th>
                                                      <th scope="col">valor</th>
                                                      <th scope="col">Ações</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody>
                                                    @foreach ($produto->planos as $plano)
                                                        <tr>
                                                            <td>{{$plano->nome}}</td>
                                                            <td>{{$plano->periodicidade }}</td>
                                                            <td>{{$plano->valor}}</td>
                                                            <td><a href="{{url('/planos/edit/' . $plano->id)}}" class="btn btn-info" >Editar</a></td>
                                                        </tr>
                                                    @endforeach
                                                  </tbody>
                                                </table>
                                              </div>
                                          </div>
                                    @endif


                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn  btn-primary"> Salvar</button>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                @endif

                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('scripts')
<script src="{{asset('dist/js/jquery.maskMoney.min.js')}}"></script>
<script>
    $("#valor").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
</script>
@endsection