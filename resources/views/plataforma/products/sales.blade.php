@extends('plataforma.templates.tabs')
@section('styles')
<style>
    .my-header {
    padding: .75rem 1.25rem;
    font-weight: 600;
    font-size: 14px;
    color: #1a262b;
    margin-bottom: -20px;
    }

   .dias {
        flex: 0 0 0%;
        max-width: 19%;
        margin-right: -8px;
        margin-left: -9px;
    }

    select.form-control:not([size]):not([multiple]) {
        height: calc(2.25rem + 2px);
        margin-left: -1px;
    }

    .texto {
        margin-left: 80px;
        margin-top: -20px;
    }

    .my-select {
        display: block;
        width: 97%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        margin-top: -7px;
    }

    .tabla {
        width: 100%;
        margin-left: auto;
    }
</style>
@endsection

@section('contentTab')
    <!-- Tab panes -->
    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Vendas</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="row my-header">
                    <div class="col-lg-8 text-uppercase"> <i class="fa fa-area-chart"></i> Faturamento X Dia </div>
                    <div class="dias">
                        <span>Dias:</span>
                    </div>
                    <div class="col-lg-3">
                        <form>
                        <select id="diasFaturaFXD" onchange="selectDay(1)" class="my-select">
                            <option value="7" selected="">7</option>
                            <option value="15">15</option>
                            <option value="30">30</option>
                        </select>
                        </form>
                    </div>
                </div><hr>
            <div class="card-body">
                 <canvas id="faturamentoDiario"></canvas>
             </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="row my-header">
                <div class="col-lg-8 text-uppercase"> <i class="fa fa-area-chart"></i> Vendas X Afiliados</div>
                <div class="dias">
                    <span>Dias:</span>
                </div>
                <div class="col-lg-3">
                    <form>
                    <select id="diasFaturaVXA" onchange="selectDay(2)" class="my-select">
                        <option value="7" selected="">7</option>
                        <option value="15">15</option>
                        <option value="30">30</option>
                    </select>
                    </form>
                </div>
                <div class="texto">TOP 10</div>
            </div><hr>
            <div class="card-body">
                 <canvas id="vendasAfiliados"></canvas>
             </div>
          </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
          <div class="card">
              <div class="row my-header">
                    <div class="col-lg-8 text-uppercase"> <i class="fa fa-area-chart"></i> Vendas X Dia </div>
                    <div class="dias">
                        <span>Dias:</span>
                    </div>
                    <div class="col-lg-3">
                        <form>
                        <select id="diasFaturaVXD" onchange="selectDay(3)" class="my-select">
                            <option value="7" selected="">7</option>
                            <option value="15">15</option>
                            <option value="30">30</option>
                        </select>
                        </form>
                    </div>
                </div><hr>
            <div class="card-body">
                 <canvas id="vendasDiario"></canvas>
             </div>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-money"></i> Lista de vendas deste produto
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Comprador</label>
                        <input class="form-control" name="comprador" type="text" id="comprador">
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <select class="form-control multiple-select" multiple="multiple">
                            <option value="All" selected="">Todos</option>
                            <option value="A">Ativo</option>
                            <option value="I">Inativo</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Forma de Pagamento</label>
                        <select class="form-control multiple-select" multiple="multiple">
                            <option value="All" selected="">Todos</option>
                            <option>Cataõ</option>
                            <option>Boleto</option>
                            <option>Transferencia</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect waves-light m-1 pull-left"> 
                        <i class="fa fa-search"></i>
                        <span>Pesquisar</span>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- DATA TABLE-->
                <div id="mi_contenido"></div>
    </div>


@endsection

@section('scripts')

<script>

    $(document).ready(function() {
        loadCharts();
        $('.multiple-select').select2();
        initializeTable();
    });

    function loadCharts(){
        var dias = ['','19/07', '20/07', '21/07', '22/07', '23/07', '24/07'];
        var data = [0, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80];
        faturamentoXDia(dias,data);
        vendasXAfiliados(dias,data);
        vendasXDia(dias,data);
    }

    function faturamentoXDia(dias, data){       
        if ($('#faturamentoDiario').length) {			
            var ctx = document.getElementById('faturamentoDiario').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dias,
                    datasets: [{
                        label: 'Vendas',
                        data: data,
                        backgroundColor: "rgba(0, 140, 255, 0.5)",
                        borderColor: "rgba(0, 140, 255)",
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    steps: 6,
                                    stepValue: 0.20,
                                    max: 1.00
                                }
                            }]
                    }
                }
            });
            
        }
    };

    function vendasXAfiliados(dias, data){
        if ($('#vendasAfiliados').length) {			
            var ctx = document.getElementById('vendasAfiliados').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dias,
                    datasets: [{
                        label: 'Vendas',
                        data: data,
                        backgroundColor: "rgba(0, 140, 255, 0.5)",
                        borderColor: "rgba(0, 140, 255)",
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    steps: 6,
                                    stepValue: 0.20,
                                    max: 1.00
                                }
                            }]
                    }
                }
            });
            
        }
    };

    function vendasXDia(dias, data){
        if ($('#vendasDiario').length) {			
            var ctx = document.getElementById('vendasDiario').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: dias,
                    datasets: [{
                        label: 'Vendas',
                        data: data,
                        backgroundColor: "rgba(0, 140, 255, 0.5)",
                        borderColor: "rgba(0, 140, 255)",
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                                display: true,
                                ticks: {
                                    beginAtZero: true,
                                    steps: 6,
                                    stepValue: 0.20,
                                    max: 1.00
                                }
                            }]
                    }
                }
            });
            
        }
    };

    function selectDay(control){
        var numDias = '';
        if(control == 1){
            numDias = $("#diasFaturaFXD option:selected").val();
        }else if(control == 2){
            numDias = $("#diasFaturaVXA option:selected").val();
        }else{
            numDias = $("#diasFaturaVXD option:selected").val();
        } 
        createByDays(numDias, control);
    }

    function createByDays(numDias, control){
        var priorDate = '';
        var past_day = '';
        var past_month = '';
        var dates = [];
        
        var today = new Date();
        var present_day = String(today.getDate()).padStart(2, '0');
        var present_month = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!

        var x = '';
        for(i=1;i<numDias;i++){
            priorDate = new Date(new Date().setDate(today.getDate()-i));
            past_day = priorDate.getDate();
            past_month = priorDate.getMonth();
            dates.push(past_day+"/"+past_month);
        }
        dates = dates.reverse();    
        
        data = '';
        if(numDias == 7){
            data = [0, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80];
        }else if(numDias == 15){
            data = [0, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80 , 0.90 , 0.95];
        }else{
            data = [0, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80 , 0.90 , 0.95, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80, 0.20, 0.40, 0.18, 0.70, 0.40, 0.80 , 0.90 , 0.95, 0.98];
        }

        if(control == 1){
            faturamentoXDia(dates, data);
        }else if(control == 2){
            vendasXAfiliados(dates, data);
        }else{
            vendasXDia(dates, data);
        }
    }
</script>

<script>
{{-- START DATA TABLE --}}

function initializeTable(){
    var html =  '<div class="row tabla">'+
                    '<div class="col-lg-12">'+
                        '<div class="card">'+
                            '<div class="card-body">'+
                                '<div class="table-responsive">'+
                                    '<table id="default-datatable" class="table table-bordered">'+
                                        '<thead>'+
                                            '<tr>'+
                                                '<th>Data</th>'+
                                                '<th>Comprador</th>'+
                                                '<th>E-mail</th>'+
                                                '<th>Valor de Venda</th>'+
                                                '<th>Comissao</th>'+
                                                '<th>Status</th>'+
                                                '<th>Detalhe</th>'+
                                            '</tr>'+
                                        '</thead>'+
                                        '<tbody id="contenido">'+
                                        '</tbody>'+
                                        '<tfoot>'+
                                            '<tr>'+
                                                '<th>Data</th>'+
                                                '<th>Comprador</th>'+
                                                '<th>E-mail</th>'+
                                                '<th>Valor de Venda</th>'+
                                                '<th>Comissao</th>'+
                                                '<th>Status</th>'+
                                                '<th>Detalhe</th>'+
                                            '</tr>'+
                                        '</tfoot>'+
                                    '</table>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                '</div>';
    $('#mi_contenido').append(html);
    makeDataTable();
}

function makeDataTable(){
    var id = $('#p_id').val();
    var formData = { id:id };
    var table = $('#default-datatable').DataTable({
        "language": {
                        "sProcessing": "A processar...",
                        "sLengthMenu": "Registros por página:  _MENU_",
                        "sZeroRecords": "Não foram encontrados resultados",
                        "sInfo": "Mostrando de _PAGE_ até _PAGES_ do total de _PAGES_ registros",
                        "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                        "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                        "sInfoPostFix": "",
                        "sSearch": "Procurar:",
                        "sUrl": "",
                        "oPaginate": {
                            "sPrevious": "Anterior",
                            "sNext": "Próxima",
                        }    
                    },
    });
    {{-- $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '',
        type: 'POST',
        data: formData,
        success: function (retorno) {
            html = '<tr class="odd">'+
                        '<td class="text-center ">'+
                            image +
                        '</td>'+
                        '<td class="info "><b>'+ retorno.product[i].name+'</b><br>'+
                            '<span class="text-muted">' +retorno.product[i].email+ '<br>'+
                            '<label>Comissão: </label>'+
                            '<input class="real_afiliado" type="text" id="comissao_SN4602136" style="width: 55px;" value="'+retorno.comision+'">%'+
                            '<button data-ref="SN4602136" data-loading-text="Salvando..." class="btn btn-success btn-sm btn-gradient btnSalvarComissao">Salvar</button>'+
                            '</span>'+
                        '</td>'+
                        '<td class=" ">'+retorno.vendas+'</td>'+
                        '<td class=" ">'+desde[0]+'</td>'+
                        '<td class="">'+status +'</td>'+ 
                    '</tr>';
            $('#contenido').append(html);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    }); --}}
}

function SearchByFilter(id){
    var estatus = $("#estatus_afiliado option:selected").val();
    var p_id = $('#p_id').val();
        var formData = { 
        product_id : p_id,  
        affiliate_id : id,  
        estatus:estatus 
        };
        $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/affiliates/status-affiliates',
        type: 'POST',
        data: formData,
        success: function (retorno) {
            event.preventDefault();
            ocultar();
            $("#mi_contenido").empty();
            initializeTable();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}
{{-- END DATA TABLE --}}
</script>
@endsection
