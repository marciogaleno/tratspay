@extends('plataforma.templates.template') 
@section('content')
<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h4 class="page-title">Minhas Afiliações</h4>

        </ol>
    </div>
    <div class="col-sm-3">
        <div class="btn-group float-sm-right">

            <span class="caret"></span>
            </button>
            <div class="dropdown-menu">

            </div>
        </div>
    </div>
</div>

<div class="row">
@if ($products)
    
@foreach ($products as $product) 

<div class="col-lg-3">
        <div class="card">
            @if ($product->image)
                <img src="{{ url('storage/products/' . $product->image)}}" class="card-img-top" alt="Card image cap">
            @else
                <img src="{{url('storage/products')}}" class="card-img-top" alt="Card image cap">
            @endif

            <div class="card-body">
                <h5 class="card-title text-dark">{{$product->name}}</h5>
            </div>
            <ul class="list-group list-group-flush list shadow-none">
            <li class="list-group-item d-flex justify-content-between align-items-center">Preço <span>{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>
            <li class="list-group-item d-flex justify-content-between align-items-center">Quantidade de venda: <span style="font-weight: bold">2</span></li>
            </ul>
            <div class="card-body">
                <a href="{{route('afilliate.detailsProduct', $product->uuid)}}" class="btn btn-success waves-effect waves-light m-1 card-link"><i class="fa fa-pencil-square-o""></i> Viualizar</a>
            </div>
       </div>
</div>

@endforeach
@endif
</div>


@endsection