@extends('plataforma.templates.template')

@section('content')


<!-- Content Header (Page header) -->
<div class="content-header sty-one">
  <h6>Seja Bem Vindo, {{Auth::user()->client->fantasia}}</h6>
</div>

<div class="row mt-4">
  <div class="col-12 col-lg-6 col-xl-3">
    <div class="card bg-primary ">
      <div class="card-body">
        <div class="media">
          <div class="media-body text-left">
            <h4 class="text-white">R$ {{$salesToday}}</h4>
            <span class="text-white">Ganhos Hoje</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-6 col-xl-3">
    <div class="card bg-secondary ">
      <div class="card-body">
        <div class="media">
          <div class="media-body text-left">
            <h4 class="text-white">R$ {{$salesMonth}}</h4>
            <span class="text-white">Ganhos do mês</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-6 col-xl-3">
    <div class="card bg-warning ">
      <div class="card-body">
        <div class="media">
          <div class="media-body text-left">
            <h4 class="text-white">R$ {{$totalPending}}</h4>
            <span class="text-white">Saldo Pendente</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-6 col-xl-3">
    <div class="card bg-success ">
      <div class="card-body">
        <div class="media">
          <div class="media-body text-left">
            <h4 class="text-white">R$ {{$totalAvailable}}</h4>
            <span class="text-white">Saldo Disponível</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row mt-4">
  <div class="col-12 col-lg-8 col-xl-8">

    <div class="card">
      <div class="card-header">
          <div class="row">
              <div class="col-3 col-lg-4 col-xl-4">
                  Vendas
              </div>
              <!--
              <div class="col-3 col-lg-3 col-xl-3">
                  <input id="demo4" type="text" value="" name="demo4" class="form-control-sm form-control" style="display: block;">
              </div>
              <div class="col-3 col-lg-3 col-xl-3">
                  <input id="demo4" type="text" value="" name="demo4" class="form-control-sm form-control" style="display: block;">
              </div>
              <div class="col-2 col-lg-2 col-xl-2">
                  <button type="button" class="btn btn-primary btn-sm waves-effect waves-light">
                    Filtrar
                  </button>
              </div>-->
          </div>
      </div>
      <div class="card-body">
        <canvas id="dash-chart-5" class="chartjs-render-monitor" height="370" width="1009"></canvas>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header border-0">
                Mais Vendidos
          </div>
             <div class="table-responsive">
               <table class="table align-items-center table-flush">
                <thead>
                 <tr>
                   <th>Foto</th>
                   <th>Produto</th>
                   <th>Vendas Realizadas </th>
                   <th>Total Ganho</th>
                   <!--<th>Porcentagem de seu faturamento</th>-->
                 </tr>
                 </thead>
                 <tbody>
                   @forelse ($bestSellersProducts as $product)
                    <tr class="text-left">
                      <td><img alt="Image placeholder" src="{{\App\Helpers\Helper::getPathFileS3('images', $product->image)}}" class="product-img" alt="{{$product->name}}" width="50"></td>
                        <td>{{$product->name}}</td>
                        <td>{{$product->quant}}</td>
                        <td>{{$product->total}}</td>
                        <!--<td>20%</td>-->
                      </tr>
                   @empty
                      <tr class="text-center">
                      </tr>
                   @endforelse

                 </tbody>
               </table>
             </div>
        </div>
      </div>
    </div><!--End Row-->

    <div class="row">
      <div class="col-md-12">
          <div class="card">
              <div class="card-header text-uppercase">Exibir dados para os últimos</div>
              <div class="card-body">
                      <form action="" method="GET" class="">
                          @csrf
                          <div class="row">
                              <div class="col-md-10" style="float:left">
                                  <div class="form-group">
                                      {!! Form::select('date', ['vendasmes'=>'Mês','last15days'=>'Últimos 15 dias','last7days'=>'Últimos 7 dias',], Illuminate\Support\Facades\Input::get('date'), ['class'=> 'form-control', "placeholder" => "-- Selecione --"]) !!}
                                  </div>
                              </div>
                              <div class="col-md-2 ">
                                  <div class=""><button class="btn btn-success " type="submit"> Filtrar</button></div>
                              </div>
                          </div>
                      </form>
              </div>
          </div>
      </div>
    </div>

    <div class="row ">
        <div class="col-12 col-lg-6 col-xl-6">
          <div class="card bg-danger ">
            <div class="card-body">
              <div class="media">
                <div class="media-body text-left">
                  <h4 class="text-white">0</h4>
                  <span class="text-white">Vendas com reclamações abertas</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-6">
          <div class="card bg-secondary ">
            <div class="card-body">
              <div class="media">
                <div class="media-body text-left">
                  <h4 class="text-white">{{$widgetFiltered['totalBoletos']}}</h4>
                  <span class="text-white"> Boletos Impressos </span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-6">
          <div class="card bg-warning ">
            <div class="card-body">
              <div class="media">
                <div class="media-body text-left">
                  <h4 class="text-white">{{$widgetFiltered['totalCliques']}}</h4>
                  <span class="text-white">Cliques</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-6">
          <div class="card bg-success ">
            <div class="card-body">
              <div class="media">
                <div class="media-body text-left">
                  <h4 class="text-white">{{$widgetFiltered['totalVendas']}}</h4>
                  <span class="text-white">Vendas</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

  </div>

  <div class="col-12 col-lg-4 col-xl-4">
    <div class="card">
      <div class="card-header">

          <div class="dropdown-item user-details">
                  <div class="media">
                      <div class="avatar">
                        @if (isset(Auth::user()->image) && Auth::user()->image)
                        <img class="mr-3 side-user-img" src="{!!\App\Helpers\Helper::getPathFileS3('images', Auth::user()->image) !!}" alt="user avatar">
                        @else
                            <img class="mr-3 side-user-img" src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png" alt="user avatar">
                        @endif                      </div>
                      <div class="media-body">
                          <h6 class="mt-2 user-title">{{Auth::user()->name}}</h6>
                          <p>Membro desde:
                            {{date('d/m/Y', strtotime(Auth::user()->created_at))}}
                          </span>
                      </div>
                  </div>
                </div>

      </div>
      <div class="card-body">

          <div class="media-body text-right">
              <span class="text-black">Saldo Total</span>
              <h4 style="color:green">R$ {{number_format($totalSales, 2, ',', '.')}}</h4>
          </div>
          <div class="media-body text-right">
              <span class="text-black">Saldo Disponível</span>
              <h4 style="color:green">R$ {{$totalAvailable}}</h4>
          </div>

          <div class="media-body text-right text-white">
              <a href="{{route('cashout.index')}}" class="btn btn-success waves-effect">
                  <i aria-hidden="true" class="fa fa-money"></i> Sacar
              </a>
          </div>


      </div>
    </div>

{{--    <div class="card">--}}
{{--      <div class="card-header">--}}

{{--          <i class="fa fa-money" aria-hidden="true"></i> Indique e Ganhe--}}

{{--      </div>--}}
{{--      <div class="card-body">--}}

{{--          <div class="media-body text-left">--}}
{{--              <span class="text-black">Divulgue a TractorPay e receba comissão.</span>--}}
{{--          </div>--}}
{{--          <br>--}}
{{--          <div class="media-body text-left">--}}
{{--              <span class="text-black">Seu link de afiliado:</span>--}}
{{--          </div>--}}
{{--          {!! Form::text('url',  url('/i/' . Auth::user()->indication_code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}--}}

{{--          <div class="media-body text-center mt-3">--}}
{{--              <a  class="btn btn-success" href="{{route('indication.index')}}"><i class="fa fa-info"></i> Saiba mais</a>--}}
{{--          </div>--}}

{{--      </div>--}}
{{--    </div>--}}

    <div class="card">
      <div class="card-header">
          <i class="fa fa-calendar" aria-hidden="true"></i> Próximos Valores Disponíveis
      </div>
      <div class="card-body">
          <div class="media-body text-center">
              <button class="btn btn-warning" data-toggle="modal" data-target="#viewNextCash" >Próximos 7 dias</button>
          </div>
      </div>
    </div>

{{--    <div class="card">--}}
{{--      <div class="card-header">--}}
{{--          <i class="fa fa-info" aria-hidden="true"></i> Possui alguma dúvida rápida?--}}
{{--      </div>--}}
{{--      <div class="card-body">--}}
{{--          <div class="media-body text-center">--}}
{{--              <button class="btn btn-warning" data-toggle="modal" data-target="#viewQuestions" >Clique aqui</button>--}}
{{--          </div>--}}
{{--      </div>--}}
{{--    </div>--}}

  </div>
</div>

<!--
<div class="row">
  <div class="col-8 col-lg-8 col-xl-8">
    <div class="card">
      <div class="card-header">
        Vendas
        <div class="card-action">
          <div class="dropdown">
            <a href="javascript:void();" class="dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown">
              <i class="icon-options"></i>
            </a>
            <div class="dropdown-menu animated fadeIn dropdown-menu-right">
              <a class="dropdown-item" href="javascript:void();">Relatório Completo</a>
            </div>
          </div>
        </div>
      </div>
      <div class="card-body">
          <div class="row mt-4">
              <div class="col-4 col-lg-4 col-xl-4">
                <div class="card bg-secondary ">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body text-left">
                        <h4 class="text-white">R$ {{$salesToday}}</h4>
                        <span class="text-white">Ganhos Hoje</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-4 col-lg-4 col-xl-4">
                <div class="card bg-secondary ">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body text-left">
                        <h4 class="text-white">R$ {{$salesMonth}}</h4>
                        <span class="text-white">Ganhos do mês</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
      </div>
    </div>
  </div>
  <div class="col-4 col-lg-4 col-xl-4">
    <div class="card">
      <div class="card-header">

          <li class="dropdown-item user-details">
              <a href="javaScript:void();">
                  <div class="media">
                      <div class="avatar">
                          <img class="align-self-start mr-3" src="{!!\App\Helpers\Helper::getPathFileS3('images', Auth::user()->image) !!}" alt="user avatar">
                      </div>
                      <div class="media-body">
                          <h6 class="mt-2 user-title">Marcio Galeno</h6>
                      </div>
                  </div>
              </a>
          </li>

      </div>
      <div class="card-body">

          <div class="media-body text-right">
              <span class="text-black">Vendas últimos 7 dias</span>
              <h4 style="color:green">R$ 82.56</h4>
          </div>
          <div class="media-body text-right">
              <span class="text-black">Saldo Total</span>
              <h4 style="color:green">R$ 82.56</h4>
          </div>

          <div class="media-body text-right">
              <button type="button" class="btn btn-success waves-effect waves-light m-1">
                  <i aria-hidden="true" class="fa fa-money"></i> Sacar
              </button>
          </div>


      </div>
    </div>
  </div>
</div>-->

  <!-- Modal -->
  <div class="modal fade" id="viewNextCash" tabindex="-1" role="dialog"
    aria-labelledby="changeImageProfileLabel" aria-hidden="true">
        <div class="modal-dialog " role="document" style="background-color: #FFFFFF">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title text-white" id="changeImageProfileLabel"> Próximos Valores Disponíveis</h5>
                    <button type="button" class="close text-white" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                              <table class="table table-bordered">
                                <thead>
                                  <tr class="text-center">
                                    <th colspan="2">Próximos 7 dias</th>
                                  </tr>
                                  <tr>
{{--                                    <th >Data de pagamento</th>--}}
                                    <th >Data de recebimento</th>
                                    <th >Valor</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($salesNextWeek as $item)
                                    <tr>
{{--                                        <td>{{\Carbon\Carbon::create($item->datepayment)->format('d/m/Y')}}</td>--}}
                                        <td>{{\Carbon\Carbon::create($item->date)->format('d/m/Y')}}</td>
                                        <td>R$ {{\App\Helpers\Helper::moneyBR($item->value)}}</td>
                                    </tr>
                                  @endforeach
                                </tbody>
                              </table>

                            </div>
                        </div>

                    <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Fechar</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

     <!-- Modal -->
  <div class="modal fade" id="viewQuestions" tabindex="-1" role="dialog"
  aria-labelledby="changeImageProfileLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg " role="document" style="background-color: #FFFFFF">
          <div class="modal-content border-warning">
              <div class="modal-header bg-warning">
                  <h5 class="modal-title text-white" id="changeImageProfileLabel"> Possui alguma dúvida rápida?</h5>
                  <button type="button" class="close text-white" data-dismiss="modal"
                          aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                      <div class="row">
                          <div class="col-md-12">
                              <div id="accordion1">
                                  <div class="card mb-2">
                                    <div class="card-header border border-warning">
                                        <button class="btn btn-link text-warning shadow-none" data-toggle="collapse" data-target="#collapse-1" aria-expanded="true" aria-controls="collapse-1">
                                            O que é a ColinaPay?
                                        </button>
                                    </div>

                                    <div id="collapse-1" class="collapse show" data-parent="#accordion1">
                                      <div class="card-body">
                                          A ColinaPay é uma plataforma de afiliados e intermediadora de pagamentos especializada na venda de produtos físicos e digitais.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="card mb-2">
                                    <div class="card-header border border-warning">
                                        <button class="btn btn-link  text-warning shadow-none collapsed" data-toggle="collapse" data-target="#collapse-2" aria-expanded="false" aria-controls="collapse-2">
                                            Eu pago algo para ser usuário da ColinaPay?
                                        </button>
                                    </div>
                                    <div id="collapse-2" class="collapse" data-parent="#accordion1">
                                      <div class="card-body">
                                          Não, você não paga nada por isso. O cadastro é completamente gratuito e você pode promover um produto em nossa plataforma ou criar o seu próprio produto.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="card mb-2">
                                    <div class="card-header border border-warning">
                                        <button class="btn btn-link text-warning shadow-none collapsed" data-toggle="collapse" data-target="#collapse-3" aria-expanded="false" aria-controls="collapse-3">
                                            Em quanto tempo após a venda de um produto eu recebo o dinheiro?
                                        </button>
                                    </div>
                                    <div id="collapse-3" class="collapse" data-parent="#accordion1">
                                      <div class="card-body">
                                          Após realizar uma venda, é preciso aguardar um prazo de 7 dias para que o valor fique disponível em sua conta e após isso poderá pedir o saque para a sua conta bancária que ocorrerá em até 3 dias úteis.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="card  mb-2">
                                    <div class="card-header border border-warning">
                                        <button class="btn btn-link text-warning shadow-none collapsed" data-toggle="collapse" data-target="#collapse-4" aria-expanded="false" aria-controls="collapse-4">
                                            Eu pago algo para cadastrar um produto?
                                        </button>
                                    </div>
                                    <div id="collapse-4" class="collapse" data-parent="#accordion1">
                                      <div class="card-body">
                                          Você só paga se vender, cobramos uma taxa de 9.9% + R$ 1 real em produtos digitais  e 6,9% em produtos físicos.
                                      </div>
                                    </div>
                                  </div>
                                  <div class="card">
                                      <div class="card-header border border-warning">
                                          <button class="btn btn-link text-warning shadow-none collapsed" data-toggle="collapse" data-target="#collapse-5" aria-expanded="false" aria-controls="collapse-5">
                                              Eu possuo outras dúvidas, o que fazer?
                                          </button>
                                      </div>
                                      <div id="collapse-5" class="collapse" data-parent="#accordion1">
                                        <div class="card-body">
                                            Você pode acessar a nossa página de suporte clicando aqui
                                            <a target="_blank" href="http://suporte.colinapay.com.br">( http://suporte.colinapay.com.br ) </a> ou enviar um e-mail para suporte@colinapay.com.br
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                          </div>
                      </div>

                  <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Fechar</button>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- End Modal -->


@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="{{asset('assets/plugins/Chart.js/Chart.min.js')}}"></script>
<script>
  // chart 5
var ctx = document.getElementById('dash-chart-5').getContext('2d');

var gradientStroke1 = ctx.createLinearGradient(0, 0, 0, 300);
gradientStroke1.addColorStop(0, '#008cff');
gradientStroke1.addColorStop(1, '#008cff');

var data = {!! $salesLastSevenDays !!};

  var chartColors = {
      orange: 'rgb(255, 159, 64)',
      yellow: 'rgb(255, 205, 86)',
      green: 'rgb(75, 192, 192)',
      blue: 'rgb(54, 162, 235)',
      purple: 'rgb(153, 102, 255)',
  };

  var dateMin = moment().subtract(7,'d').format('YYYY-MM-DD');

  var config = {
      type: 'line',
      data: {
          datasets: [{
              label: "Vendas",
              backgroundColor: chartColors.orange,
              borderColor: chartColors.orange,
              data: data,
              fill: false,
          }]
      },
      options: {
          tooltips: {
          callbacks: {
                        label: function(tooltipItem, data) {
                            let value = tooltipItem.yLabel;
                            if(parseInt(value) >= 1000){
                                return 'R$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            } else {
                                return 'R$' + value;
                            }
                        },
                    }
            },
          responsive: true,
          title: {
              display: true,
              text: 'Detalhamento de vendas da semana'
          },
          hover: {
              mode: 'nearest',
              intersect: true
          },
          scales: {
              xAxes: [{
                      type: 'time',
                      time: {
                          min: dateMin,
                          max: moment().toDate().getTime(),
                          tooltipFormat:'DD/MM/YYYY',
                          unit: 'day',
                          unitStepSize: 1
                      }
                  }],
              yAxes: [{
                  display: true,
                  scaleLabel: {
                      display: true,
                      labelString: 'Valor vendido',
                      fontColor: "black"
                  },
                  ticks: {
                      beginAtZero: true,
                      callback: function(value, index, values) {
                          if(parseInt(value) >= 1000){
                              return 'R$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                          } else {
                              return 'R$' + value;
                          }
                      }
                  }
              }]
          }
      }
  };
  // {
  //     type: 'line',
  //         data: {
  //     //labels: [1, 2, 3, 4, 5, 6, 7, 8],
  //     datasets: [{
  //         label: 'Vendas',
  //         pointBorderWidth: 2,
  //         fill: true,
  //         data: data,
  //         borderColor: gradientStroke1,
  //         borderWidth: 3
  //
  //     }]
  // },
  //     options: {
  //         legend: {
  //             display: true,
  //                 //position: 'bottom',
  //                 labels: {
  //                 boxWidth:40
  //             }
  //         },
  //         tooltips: {
  //             displayColors:false
  //         },
  //         elements: {
  //             line: {
  //                 tension: 0
  //             }
  //         },
  //         // tooltips: {
  //         //     callbacks: {
  //         //                   label: function(tooltipItem, data) {
  //         //                       return "R$ " + tooltipItem.yLabel;
  //         //                   },
  //         //               }
  //         //       },
  //         scales: {
  //             xAxes: [{
  //                 type: 'time',
  //                 time: {
  //                     min: dateMin,
  //                     max: moment().toDate().getTime(),
  //                     tooltipFormat:'MM/DD/YYYY',
  //                     unit: 'day',
  //                     unitStepSize: 2
  //                 }
  //             }],
  //                 yAxes: [{
  //                 gridLines: {
  //                     color: "black",
  //                     borderDash: [2, 5],
  //                 },
  //                 scaleLabel: {
  //                     display: true,
  //                     labelString: "Quantidade de vendas",
  //                     fontColor: "green"
  //                 },
  //                 ticks: {
  //                     max: 12,
  //                     min: 0,
  //                     stepSize: 1,
  //                     autoSkip:true
  //                 }
  //
  //             }]
  //         }
  //     }

var myChart = new Chart(ctx,config);

</script>
@endsection
