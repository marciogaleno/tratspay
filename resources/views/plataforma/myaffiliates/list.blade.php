@extends('plataforma.templates.template')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Meus Afiliados&nbsp;</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-uppercase">QUAIS AFILIADOS VOCÊ QUER VER ?</div>
                    <div class="card-body">
                            <form action="{{ route('myaffiliates.list') }}" method="GET" class="">
                                <div class="col-lg-3" style="float:left">
                                    <div class="form-group">
                                        {!! Form::label('product_id', 'Produto',['id'=>'label_product_id', 'style' => 'margin-bottom:0px']) !!}
                                        {!! Form::select('product_id', $products,  Illuminate\Support\Facades\Input::get('product_id'), ['class'=> 'form-control', "placeholder" => "-- Selecione --"]) !!}
                                    </div>
                                </div>

                                <div class="col-lg-3" style="float:left">
                                    <div class="form-group">
                                        {!! Form::label('affiliates', 'EXIBIR',['id'=>'label_affiliates', 'style' => 'margin-bottom:0px']) !!}
                                        {!! Form::select('affiliates', ['ALL_AFFILIATES' => 'Todos os Afiliados', 'SALES_AFFILIATES' => "Afiliados com vendas", 'SALES_WITHOUT_AFFILIATES' => "Afiliados sem vendas"],  Illuminate\Support\Facades\Input::get('affiliates'), ['class'=> 'form-control', "placeholder" => "-- Selecione --"]) !!}
                                    </div>
                                </div>
                                <div class="col-lg-3" style="float:left">
                                    <div class="form-group">
                                        {!! Form::label('affiliates_sub', 'Classificar afiliados por:',['id'=>'label_affiliates_sub', 'style' => 'margin-bottom:0px']) !!}
                                        {!! Form::select('affiliates_sub', ['NAME' => 'Por nome', 'MORE_SALES' => "Com mais vendas", 'LESS_SALES' => "Com menos vendas"],  Illuminate\Support\Facades\Input::get('affiliates_sub'), ['class'=> 'form-control', "placeholder" => "-- Selecione --"]) !!}
                                    </div>
                                </div>
                                <div class="col-lg-3 mt-3" style="float:left"><button class="btn btn-success btn-block" type="submit">Buscar</button></div>
                            </form>
                      
                    </div>
                </div>
                <div class="card">
                    <div class="card-header text-uppercase">O seu resultado de busca gerou:</div>
                        <div class="card-body">
                          <div class="row">
                            <div class="col-lg-6">
                                <span>Afiliados:</span>
                                <h4 class="text-success">{{count($affiliates)}}</h4>
                            </div>
                            <div class="col-lg-6">
                                <span>Vendas geradas por eles:</span>
                                <h4 class="text-primary">{{$totalSales}}</h4>
                            </div>


                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header text-uppercase">seus afiliados:</div>
                    <div class="card-body">
                        <div class="row">
                            @if(count($affiliates))
                                @foreach($affiliates as $affiliate)
                                    <div class="col-lg-6" style="">
                                        <div class="card-body">
                                            <div class="media">
                                            <img class="mr-3 rounded" src="{!!\App\Helpers\Helper::getPathFileS3('images', $affiliate->image) !!}" alt="user avatar" style="max-width: 100px;max-height: 100px;border-radius: 50%;">
                                            <div class="media-body">
                                                <h5 class="mt-0">{{$affiliate->name}}:</h5><p>
                                                </p> E-mail: {{$affiliate->email}}
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="pull-right">{!! $affiliates->appends(request()->except('page'))->links() !!}</div>--}}
{{--                                    </div>--}}
                        </div>
                            @else
                                <div class="col-md-12 text-center">
                                    <div class="text-uppercase" style="font-weight: 600;color:gray">NENHUM REGISTRO ENCONTRADO</div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
     
   </div>
@stop