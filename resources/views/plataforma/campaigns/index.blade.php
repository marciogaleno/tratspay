@extends('plataforma.templates.tabs') 

@section('styles')
    <style>
        .form-control {
            font-size: 0.7rem;            
        }
    </style>
@endsection


@section('contentTab')

    <div class="row">
            <div class="col-sm-9">
                <h4 class="text-left">Lista de Campanhas</h4>
                <small>As campanhas servem para você poder usar os pixels de suas fontes de tráfego.</small>
            </div>
    </div>
    <hr>        


    <div class="card-body">
            <div  class="row m-t-2" id="panelAddPlanos" style="font-size: 14px">
                    <div class="table-responsive">
                        <a href="{{route('campaign.add', [app('request')->product->id])}}" type="button" class="btn btn-success pull-right" style="margin: 5px 5px"><i class="fa fa-plus"></i> Novo </a>
                        <table class="table" id="tableplans">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">URL</th>
                            <th scope="col">Clicks/Unícos</th>
                            <th scope="col">Vendas</th>
                            <th scope="col">CTR</th>
                            <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($campaings as $campaing)
                                <tr>
                                    <td>{{$campaing->name}}</td>
                                    <td> {!! Form::text('sale_page', url('/r') . '/' . $campaing->product->producer[0]->pivot->code . '?cmp=' . $campaing->code , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF']) !!}
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        {!! Form::open(['route' => ['campaign.delete', app('request')->product->id, $campaing->code], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                            <a href="{{route('campaign.edit', [app('request')->product->id, $campaing->code] )}}" class="btn btn-primary btn-sm" rel="nofollow" title="Editar"><i class="fa fa-pencil"></i></a>
                                            <button class="btn btn-danger btn-sm" title="Excluir"><i class="fa fa-trash"  onclick="return confirm('Tem certeza que deseja excluir?')"></i></button>
                                        {!! Form::close() !!}
                                    </td>

                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
            </div>
    </div>



@endsection

@section('scripts')

<script>
    
    function validate(form) {
        var valid = confirm('Você tem certeza que deseja excluir?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


</script>
@endsection