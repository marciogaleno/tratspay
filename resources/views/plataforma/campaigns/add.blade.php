@extends('plataforma.templates.tabs') 
@section('contentTab')

    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Adicionar Campanha</h4>
        </div>
    </div>
    <hr>    
    

    <div class="card-body">
        {{ Form::open(['route' => ['campaign.create', app('request')->product->id], 'method' => 'post']) }}

        
        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
            {!! Form::label('name', 'Nome:', ['for' => 'name' ]) !!}
            {!! Form::text('name', old('url'), ['class' => 'form-control']) !!}
  
            @if ($errors->has('name'))
              <label class="control-label" for="name">
                {{$errors->first('name')}}
              </label>
            @endif 
          </div>

        <div class="form-group {{ $errors->has('type_url') ? 'has-error' : '' }}">
            {!! Form::label('type_url', 'destino:', ['for' => 'type_url' ]) !!}
            {!! Form::select('type_url', \App\Helpers\Helper::typesUrl(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'id' => 'type_url']) !!}

            @if ($errors->has('type_url'))
                <label class="error" for="type">
                {{$errors->first('type_url')}}
                </label>
            @endif                                         
        </div>

        <div class="form-group {{ $errors->has('checkout_code') ? 'has-error' : '' }}" id="checkout_code" style="display:none">
            {!! Form::label('checkout_code', 'Checkouts:', ['for' => 'checkout_code' ]) !!}
            {!! Form::select('checkout_code', $checkouts, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'id' => 'checkout_code']) !!}

            @if ($errors->has('checkout_code'))
                <label class="error" for="checkout_code">
                {{$errors->first('checkout_code')}}
                </label>
            @endif                                         
        </div>

        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}" id="url_alternative_code" style="display:none">
            {!! Form::label('url_alternative_code', 'URL Alternativas:', ['for' => 'url_alternative_code   ' ]) !!}
            {!! Form::select('url_alternative_code', $urlAlternatives, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

            @if ($errors->has('url_alternative_code'))
                <label class="error" for="url_alternative_code">
                {{$errors->first('url_alternative_code')}}
                </label>
            @endif                                         
        </div>

        <div class="form-group {{ $errors->has('pixel') ? 'has-error' : '' }}">
                {!! Form::label('pixel', 'Pixel de Conversão:', ['for' => 'pixel' ]) !!}
                {!! Form::select('pixel', \App\Helpers\Helper::typesPixel(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'id' => 'pixel']) !!}
    
                @if ($errors->has('pixel'))
                    <label class="error" for="type">
                    {{$errors->first('pixel')}}
                    </label>
                @endif                                         
        </div>

        <div class="form-group {{ $errors->has('exec_ticket') ? 'has-error' : '' }}" id="exec_ticket">
            {!! Form::label('exec_ticket', 'Executar no boleto:', ['for' => 'exec_ticket   ' ]) !!}
            {!! Form::select('exec_ticket', [1 => 'Sim', 0 => 'Não'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
            {!! Form::label('exec_ticket', 'EXECUTAR PIXEL NA IMPRESSÃO DE BOLETOS') !!}

            @if ($errors->has('exec_ticket'))
                <label class="error" for="exec_ticket">
                {{$errors->first('exec_ticket')}}
                </label>
            @endif                                         
        </div>

        <div class="form-group has-feedback {{ $errors->has('pixel_id') ? 'has-error' : '' }}" id="pixel_id">
                {!! Form::label('pixel_id', 'ID Pixel:', ['for' => 'pixel_id' ]) !!}
                {!! Form::text('pixel_id', old('pixel_id'), ['class' => 'form-control']) !!}
                {!! Form::label('pixel_id', 'O PIXEL DE RASTREAMENTO SÓ SERÁ EXECUTADO QUANDO UMA VENDA FOR REALIZADA.') !!}

                @if ($errors->has('pixel_id'))
                  <label class="control-label" for="pixel_id">
                    {{$errors->first('pixel_id')}}
                  </label>
                @endif 
        </div>

        <div class="form-group has-feedback {{ $errors->has('label') ? 'has-error' : '' }}" id="label" style="display:none">
                {!! Form::label('label', 'Label:', ['for' => 'label' ]) !!}
                {!! Form::text('label', old('label'), ['class' => 'form-control']) !!}
      
                @if ($errors->has('label'))
                  <label class="control-label" for="label">
                    {{$errors->first('label')}}
                  </label>
                @endif 
        </div>

        <div class="form-group has-feedback {{ $errors->has('value') ? 'has-error' : '' }}" id="value">
                {!! Form::label('value', 'valor:', ['for' => 'value' ]) !!}
                {!! Form::text('value', old('value'), ['class' => 'form-control', 'id' => 'valueInput']) !!}
                {!! Form::label('value', "(OPCIONAL) SE VOCÊ DEIXAR EM BRANCO O VALOR ENVIADO SERÁ DO TOTAL DA SUA COMISSÃO SOBRE A VENDA REALIZADA. ESSE VALOR SOMENTE SERÁ ENVIADO SE O CAMPO ACIMA TRACK FOR 'PURCHASE'") !!}

                @if ($errors->has('value'))
                  <label class="control-label" for="value">
                    {{$errors->first('value')}}
                  </label>
                @endif 
        </div>

        <div class="form-group {{ $errors->has('exec_checkout') ? 'has-error' : '' }}" id="exec_checkout" style="display:none">
                {!! Form::label('exec_checkout', 'Executar no Checkout:', ['for' => 'exec_checkout   ' ]) !!}
                {!! Form::select('exec_checkout', [1 => 'Sim', 0 => 'Não'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
                {!! Form::label('exec_checkout', 'EXECUTAR PIXEL NO CHECKOUT (INITIATECHECKOUT)') !!}

                @if ($errors->has('exec_checkout'))
                    <label class="error" for="exec_checkout">
                    {{$errors->first('exec_checkout')}}
                    </label>
                @endif                                         
        </div>

        <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn  btn-success"> Salvar</button>
                </div>
        </div>

        {!! Form::close() !!}


      
    </div>
 

@endsection

@section('scripts')
<script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>

<script>
        $("#valueInput").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
        
        $('#type_url').on('change', function() {
            
            if (this.value == 2 ) {
                $("#checkout_code").show("slow");
                $("#checkout_code").attr('disabled', false);
            } else {
                $("#checkout_code").hide("slow");
                $("#checkout_code").attr('disabled', true);
            }

            if (this.value == 3) {
                $("#url_alternative_code").show("slow");
                $("#checkout_code").attr('disabled', false);
            } else {
                $("#url_alternative_code").hide("slow");
                $("#checkout_code").attr('disabled', true);
            }

        });

        $('#pixel').on('change', function() {
            
            if (this.value == 'fb2' ) {
                $("#exec_checkout").show("slow");
                $("#exec_checkout").attr('disabled', false);
            } else {
                $("#exec_checkout").hide();
                $("#exec_checkout").attr('disabled', true);                    
            }

            if (this.value == 'adw' ) {
                $("#label").show("slow");
                $("#label").attr('disabled', false);
            } else {
                $("#label").hide();
                $("#label").attr('disabled', true);                    
            }

        });
</script>
@endsection