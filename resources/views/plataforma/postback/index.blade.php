@extends('plataforma.templates.template')
@section('content')

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

<div class="col-lg-12">
    <div class="card">
        <div class="card-header text-uppercase">Como funciona a nossa API?</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>O nosso postback garante que um processo seja enviado de um servidor para a TratsPay podendo
                        assim
                        realizar qualquer integração necessária.</p>
                    <p style=""><a href="#">Clique Aqui Para Ler a Documentação Completa do Postback</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card-header text-uppercase">Sua Chave Geral da Conta é: {{Auth::user()->token}} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header text-uppercase">Configuração de Postback</div>
        <div class="card-body">
            @if (isset($postback))
                {!! Form::model($postback, ['route' => ['postback.update', $postback->id], 'method' => 'put', 'id' => 'formPostback']) !!}
            @else
                {!! Form::open(['route' => 'postback.create', 'method' => 'post', 'id' => 'formPostback']) !!}
            @endif
           
            <div class="row">
                <div class="col-md-12" style="">
                    <div class="row">
                        <div class="col-md-8">
                            {!! Form::label('product_id', 'Produto:') !!}
                            {!! Form::select('product_id', $products, null, ['class' => 'form-control single-select','placeholder' => 'Todos os Produtos', 'id' => 'inputProduct']) !!}
                        </div>
                            
                      
                        <div class="col-md-4">
                            <div class="form-group">
                                {!! Form::label('type', 'Tipo:') !!}
                                {!! Form::select('type', $types, null, ['class' => 'form-control single-select']) !!}
                        </div>
                
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::label('url', 'Url:') !!}
                                {!! Form::text('url', null, ['class' => 'form-control', "placeholder" => "Digite a URL a ser executada", "id" => "inputUrl"]) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- Small Size Modal -->

                            <!--<button class="btn btn-light m-1" data-toggle="modal" data-target="#smallsizemodal">Clique
                                Aqui
                                para ver as variaveis do Postback</button>-->
                            <!-- Modal -->
                            <!--<div class="modal fade" id="smallsizemodal">
                                <div class="modal-dialog modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"><i class="fa fa-plug"></i> Variáveis da TratsPay
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p><small>{src} = SRC enviado via parametro de URL</small></p>
                                            <p>
                                                <small>{utm_source} = UTM_SOURCE enviado via parametro de URL</small>
                                            </p>
                                            <p>
                                                <small>{utm_medium} = UTM_MEDIUM enviado via parametro de URL</small>
                                            </p>
                                            <p>


                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><i
                                                    class="fa fa-times"></i> Fechar</button>

                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <p><br><u>Eventos:</u><br></p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4" style="">
                    <div class="icheck-material-dark">
                        @if (isset($postback))
                            {!! Form::checkbox('pending', null, null, ['id' => 'pending']) !!}
                        @else
                            {!! Form::checkbox('pending', null, 1, ['id' => 'pending']) !!}
                        @endif
                        {!! Form::label('pending', 'Aguardando Pagamento') !!}
                    </div>
                    <div class="icheck-material-dark">
                        @if (isset($postback))
                            {!! Form::checkbox('refund', null, null, ['id' => 'refund']) !!}
                        @else
                            {!! Form::checkbox('refund', null, 1, ['id' => 'refund']) !!}
                        @endif
                        
                        {!! Form::label('refund', 'Reembolso') !!}
                    </div>
                    <div class="icheck-material-dark">

                        @if (isset($postback))
                            {!! Form::checkbox('abandon_checkout', null, null, ['id' => 'abandon_checkout']) !!}
                        @else
                            {!! Form::checkbox('abandon_checkout', null, 1, ['id' => 'abandon_checkout']) !!}
                        @endif

                       
                        {!! Form::label('abandon_checkout', 'Abandono de Checkout - Apenas para Produtores') !!}
                    </div>
                </div>
                <div class="col-md-4" style="">
                    <div class="icheck-material-dark">

                        @if (isset($postback))
                            {!! Form::checkbox('finished', null, null, ['id' => 'finished']) !!}
                        @else
                            {!! Form::checkbox('finished', null, 1, ['id' => 'finished']) !!}
                        @endif
  
                        {!! Form::label('finished', 'Finalizada / Aprovada') !!}
                    </div>
                    <div class="icheck-material-dark">
                        
                        @if (isset($postback))
                            {!! Form::checkbox('blocked', null, null, ['id' => 'blocked']) !!}
                        @else
                            {!! Form::checkbox('blocked', null, 1, ['id' => 'blocked']) !!}
                        @endif
                  
                        {!! Form::label('blocked', 'Bloqueada') !!}
                    </div>
                    <div class="icheck-material-dark">

                        @if (isset($postback))
                            {!! Form::checkbox('canceled', null, null, ['id' => 'canceled']) !!}
                        @else
                            {!! Form::checkbox('canceled', null, 1, ['id' => 'canceled']) !!}
                        @endif

                        
                        {!! Form::label('canceled', 'Cancelada / Recusada') !!}
                    </div>
                </div>
                <div class="col-md-3" style="">
                    <div class="icheck-material-dark">

                        @if (isset($postback))
                            {!! Form::checkbox('completed', null, null, ['id' => 'completed']) !!}
                        @else
                            {!! Form::checkbox('completed', null, 1, ['id' => 'completed']) !!}
                        @endif

                        
                        {!! Form::label('completed', 'Completada / Fim da garantia') !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 mt-2">
                    <button type="submit" name="action" value="salvar" class="btn btn-success waves-effect waves-light m-1">Salvar
                    </button>
                    @if (isset($postback))
                        <a href="{{route('postback.index')}}" class="btn btn-danger waves-effect waves-light m-1">Cancelar
                        </a>    
                    @endif

                </div>
                <div class="col-md-3"></div>
                <div class="col-md-5">
                    <button type="button" name="action" value="test" onclick="testPostback()" class="btn btn-primary waves-effect waves-light m-1">
                        Testar
                    </button>
                    
                    <span>(Testar um evento por vez)</span>
                    
                </div>
               
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>



<div class="col-lg-12">
    <div class="card">

        
        <div class="card-header text-uppercase">Postbacks cadastrados</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12" style="">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-sm">
                                        <thead class="thead-light">
                                            <tr>
                                                <th scope="col">Produto:</th>
                                                <th scope="col">TIPO</th>
                                                <th scope="col">URL</th>
                                                <th scope="col">Evento</th>
                                                <th scope="col">STATus</th>
                                                <th scope="col">Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (isset($postbacks))
                                                @foreach ($postbacks as $postback)
                                                <tr>
                                                        <td>
                                                            <div class="col-sm-3">
                                                                @if ($postback->product)
                                                                    {{$postback->product->name}} 
                                                                @else
                                                                    Todos
                                                                @endif
                                                                   
                                                            </div><!-- /.col -->
                                                      
                                                        </td>
                                                        <td>
                                                            @if ($postback->type == 1)
                                                                Postback
                                                            @else
                                                                Iframe ou Pixel
                                                            @endif                                                            
                                                            
                                                        </td>
                                                        <td>
                                                            <div class="col-sm-4">
                                                                    {{$postback->url}}
                                                            </div>
                                                           
                                                        </td>
                                                        <td class="col-sm-4">
                                                            @if ($postback->pending)
                                                                <span>Aguardando Pagamento</span>
                                                                <br>
                                                            @endif
                                                            @if ($postback->refund)
                                                                <span>Reembolso</span>
                                                                <br>
                                                            @endif
                                                            @if ($postback->abandon_checkout)
                                                                <span>Abondono Checkout</span>
                                                                <br>
                                                            @endif
                                                            @if ($postback->finished)
                                                                <span>Finalizada</span>
                                                                <br>
                                                            @endif
                                                            @if ($postback->blocked)
                                                                <span>Bloqueada</span>
                                                                <br>
                                                            @endif
                                                            @if ($postback->canceled)
                                                                <span>Cancelada</span>
                                                                <br>
                                                            @endif
                                                            @if ($postback->completed)
                                                                <span>Completada</span>
                                                                <br>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($postback->status)
                                                                Ativo
                                                            @else
                                                                Inativo
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {!! Form::open(['route' => ['postback.delete', $postback->id], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                                            <a href="{{route('postback.edit', [$postback->id])}}" class="btn btn-primary btn-sm" rel="nofollow"><i class="fa fa-pencil"></i></a>
                                                            <button class="btn btn-danger btn-sm"  onclick="return confirm('Você tem certeza que deseja excluir?')"><i class="fa fa-trash" ></i></button>
                                                            {!! Form::close() !!}
                                                        </td>
                                                    </tr>               
                                                @endforeach
                                             
                                            @endif

       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


      <!-- Modal -->
      <div class="modal fade" id="modalReponseHttpPostback" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">Teste Posback</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <label id="lalbelCodeResponse"></label>

                <div class="form-group has-feedback">
                    {!! Form::label('response', 'Reposta:', ['for' => 'response' ]) !!}
                    {!! Form::textarea('response', old('response'), ['class' => 'form-control', 'id' => 'response']) !!}
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
              </div>
          </div>
        </div>
    </div>
    
@push('scripts')
    
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!--Select Plugins Js-->
    <script type="text/javascript">
        $('.single-select').select2();

        
        $('#default-datatable').DataTable();

        function testPostback()
        {
            if (!$("#inputUrl").val()) {
                alert("Por favor, digite uma URL.");
                return;
            }

            var url = {!! json_encode(url('tools/postback/test')) !!};

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           
            $.ajax({
            url : url,
            type : 'POST', 
            data: $("#formPostback").serialize(),
            beforeSend : function(){
                $.preloader.start();
            }
            })
            .done(function(msg, textStatus, xhr){
                var status = xhr.status;
                $('#tableCheckouts tbody').empty();
                $('#lalbelCodeResponse').html("").html("Código HTTP: " + status);
                $('#response').val(JSON.stringify(msg));
                $('#modalReponseHttpPostback').modal('toggle');
                $.preloader.stop();
                    
            }).fail(function(jqXHR, textStatus, msg){
                var status = jqXHR.status;
                $('#lalbelCodeResponse').html("").html("Código HTTP: " + status);
                $('#response').val(JSON.stringify(msg));
                $('#modalReponseHttpPostback').modal('toggle');
                $.preloader.stop();
            }); 
        }
        
    </script>
@endpush

@endsection