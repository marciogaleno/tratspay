@extends('plataforma.templates.template')

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@section('content')
  <!--Select Plugins-->

<div class="row">
    <div class="col-lg-4">
        <div class="profile-card-4">
            <div class="card">
                <div class="card-body text-center rounded-top bg-warning" >
                    <div class="user-box">
                        @if (isset($user->user->image))
                            <img src="{!! \App\Helpers\Helper::getPathFileS3('images', $user->user->image) !!}"
                                 alt="User profile picture">
                        @else
                            <img src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png"
                                 alt="User profile picture">
                        @endif
                    </div>
                    @if ($user->user)
                        <h5 class="mb-1 text-white">{{  $user->user->name }}</h5>
                    @endif
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#changeImageProfile">
                        Alterar imagem
                    </button>
                    <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#changePassword">
                        Alterar senha
                    </button>
                </div>
                <div class="card-body">
                    <ul class="list-group shadow-none">
                        <li class="list-group-item">
                            <div class="list-icon">
                                <i class="fa fa-phone-square"></i>
                            </div>
                            <div class="list-details dont-break-out">
                                <span> {{$user->cellphone}}</span>
                                <small>WhatsApp</small>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="list-icon">
                                <i class="fa fa-envelope"></i>
                            </div>
                            <div class="list-details ">
                                <button class="btn btn-sm btn-warning btn-block" data-toggle="modal" data-target="#viewEmailUser">
                                    Exibir E-mail
                                </button>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="list-icon">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="list-details">
                                <button class="btn  btn-block btn-sm btn-warning" data-toggle="modal" data-target="#viewSiteUser">
                                    Exibir Site
                                </button>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="card-footer text-center">
                    <a href="{{$user->social_facebook}}" class="btn-social btn-facebook waves-effect waves-light m-1"><i class="fa fa-facebook"></i></a>
                    <a href="{{$user->social_twitter}}" class="list-inline-item btn-social btn-behance waves-effect waves-light"><i class="fa fa-twitter"></i></a>
                    <a href="{{$user->social_linkedin}}" class="list-inline-item btn-social btn-dribbble waves-effect waves-light"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills nav-pills-warning nav-justified">
                    <li class="nav-item">
                        <a href="#dados-cadastrais" data-target="#dados-cadastrais" data-toggle="pill" class="nav-link active"><i class="icon-user"></i> <span class="hidden-xs">Dados Cadastrais</span></a>
                    </li>
                    <li class="nav-item">
                        <a href="#dados-financeiros" data-target="#dados-financeiros" data-toggle="pill" class="nav-link"><i class="fa fa-money"></i> <span class="hidden-xs">Dados Financeiros</span></a>
                    </li>

                </ul>
                <div class="tab-content p-3">



                    <div class="tab-pane active" id="dados-cadastrais">
                        {!! Form::model($user, [
                            'method'    => 'PUT',
                            'route'     => ['client.update'],
                            'class'     => 'form-horizontal'
                        ]) !!}


                        <div class="form-group {{ $errors->has('person') ? 'has-error' : '' }}">
                            {!! Form::label('person', 'Tipo Pessoa') !!}
                            {!! Form::select('person', App\Helpers\Helper::typePerson(),old('person'), ['class' => 'form-control']) !!}

                            @if($errors->has('person'))
                                <span class="help-block">{{ $errors->first('person') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('cpf_cnpj') ? ' has-error' : '' }}">
                            {!! Form::label('cpf_cnpj', 'CPF/CNPJ',['id'=>'label_cpf_cnpj', 'style' => 'margin-bottom:0px']) !!}
                            <br>
                            <small style="font-size: 10px">Após salvar os seus dados, você não poderá editar seu CPF ou CNPJ sem entrar em contato conosco via e-mail:
                            </small><b style="font-size: 10px">suporte@colinapay.com.br</b>
                            <input id="validate_cpf" type="hidden" value="{{ $exists }}">
                            {!! Form::text('cpf_cnpj', $user->user->cpf_cnpj, ['class' => 'form-control', 'placeholder' => 'CPF ou CNPJ', 'id' => 'cpf_cnpj']) !!}
                            @if ($errors->has('cpf_cnpj'))
                                <span class="help-block"><strong>{{ $errors->first('cpf_cnpj') }}</strong></span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            {!! Form::label('name', 'Nome',['id' => 'label_name']) !!}
                            {!! Form::text('name', $user->user->name, ['class' => 'form-control', 'placeholder' => 'Nome']) !!}

                            @if($errors->has('name'))
                                <span class="help-block">{{ $errors->first('name') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('fantasia', 'Como gostaria de ser chamado? ( Exibido para os seus Afiliados / Produtores )') !!}
                            {!! Form::text('fantasia', $user->user->fantasia, ['class' => 'form-control', 'placeholder' => 'Nome Fantasia']) !!}

                            @if($errors->has('fantasia'))
                                <span class="help-block">{{ $errors->first('fantasia') }}</span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('birth') ? ' has-error' : '' }}">
                            {!! Form::label('birth', 'Nascimento)',['id' => 'label_birth']) !!}
                            {!! Form::text('birth', $user->birth, ['class' => 'form-control date', 'placeholder' => 'DD/MM/AAAA']) !!}

                            @if($errors->has('birth'))
                                <span class="help-block">{{ $errors->first('birth') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::email('email',  $user->user->email, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('email'))
                                <span class="help-block">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('cellphone', 'Telefone/WhatsApp') !!}
                            {!! Form::text('cellphone', $user->cellphone, ['class' => 'form-control phone', 'placeholder' => '']) !!}

                            @if($errors->has('cellphone'))
                                <span class="help-block">{{ $errors->first('cellphone') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Mensagem de Perfil</label>
                            <textarea rows="5" name="obs" id="obs"
                                      class="form-control form-control-line">{{$user->obs}}</textarea>
                        </div>
                        <div class="form-group">
                            {!! Form::label('social_facebook', 'Link do Facebook') !!}
                            {!! Form::text('social_facebook', $user->social_facebook, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('social_facebook'))
                                <span class="help-block">{{ $errors->first('social_facebook') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('social_twitter', 'Link do Twitter') !!}
                            {!! Form::text('social_twitter', $user->social_twitter, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('social_twitter'))
                                <span class="help-block">{{ $errors->first('social_twitter') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('social_linkedin', 'Link do LinkedIN') !!}
                            {!! Form::text('social_linkedin', $user->social_linkedin, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('social_linkedin'))
                                <span class="help-block">{{ $errors->first('social_linkedin') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            {!! Form::label('social_site', 'Link do Site') !!}
                            {!! Form::text('social_site', $user->social_site, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('social_site'))
                                <span class="help-block">{{ $errors->first('social_site') }}</span>
                            @endif
                        </div>

                        <h4> Endereço </h4>
                        <div class="form-group">
                            {!! Form::label('postal_code', 'CEP') !!}
                            <div class="input-group mb-3">
                                {!! Form::text('postal_code', $user->postal_code, ['class' => 'form-control cep', 'placeholder' => '']) !!}
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-sm waves-effect waves-light" id="btnPostalCode" type="button">Buscar</button>
                                </div>
                            </div>

                            @if($errors->has('postal_code'))
                                <span class="help-block">{{ $errors->first('postal_code') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('street', 'Endereço') !!}
                            {!! Form::text('street', $user->street, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('street'))
                                <span class="help-block">{{ $errors->first('street') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('number', 'Numero') !!}
                            {!! Form::text('number', $user->number, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('number'))
                                <span class="help-block">{{ $errors->first('number') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('complement', 'Complemento') !!}
                            {!! Form::text('complement', $user->complement, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('complement'))
                                <span class="help-block">{{ $errors->first('complement') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('district', 'Bairro') !!}
                            {!! Form::text('district', $user->district, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('district'))
                                <span class="help-block">{{ $errors->first('district') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('city', 'Cidade') !!}
                            {!! Form::text('city', $user->city, ['class' => 'form-control', 'placeholder' => '']) !!}

                            @if($errors->has('city'))
                                <span class="help-block">{{ $errors->first('city') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            {!! Form::label('state', 'UF') !!}
                            {!! Form::text('state', $user->state, ['class' => 'form-control uf', 'placeholder' => '']) !!}

                            @if($errors->has('state'))
                                <span class="help-block">{{ $errors->first('state') }}</span>
                            @endif
                        </div>

                        <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                            {!! Form::label('country', 'País') !!}
                            {!! Form::select('country', App\Helpers\Helper::ListCountries(), $user->country ,['class' => 'form-control single-select']) !!}

                            @if($errors->has('country'))
                                <span class="help-block">{{ $errors->first('country') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <hr/>
                            <button class="btn btn-success waves-light m-1" type="submit">Atualizar Informações</button>
                        </div>
                        {!! Form::close() !!}

                    </div>

                    <div class="tab-pane" id="dados-financeiros">
                        <div class="card-body">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Banco</th>
                                    <th>Agência</th>
                                    <th>Conta</th>
                                    {{--<th>Status</th>--}}
                                    <th>Ação</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($user->bankAccounts as $cBank)
                                    <tr id="linha{{ $cBank->id }}">
                                        <td>{!! \App\Helpers\Helper::listBank($cBank->bank) !!}</td>
                                        <td>{{ $cBank->bank_branch }}</td>
                                        <td>{{ $cBank->bank_account }}</td>
                                        {{--<td>{!! App\Helpers\Helper::statusBankAccount($cBank->status,true)!!}</td>--}}
                                        <td>
                                            <button class="btn btn-danger btn-sm"><i class="fa fa-trash deletar" data-id="{{ $cBank->id }}"></i></button>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="5">
                                            <span class="text-center text-danger">Sem dados bancários cadastrados</span>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>


                            <button type="button" class="btn btn-primary mt-4" data-toggle="modal"
                                    data-target="#addBankAccount">
                                <i class="fa fa-plus"></i>  Cadastrar Nova Conta
                            </button>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addBankAccount">
        <div class="modal-dialog modal-lg">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">

                    <div class="" style=" width: 100%;">
                        <h5 class="modal-title text-white" id="addBankAccountLabel">Adicionar Conta Bancária</h5>
                    </div>

                    <hr><small style="font-size: 10px; font-weight: 400;color: white;">A sua conta bancária deve ser a mesma do titular da conta na ColinaPay e após adicionar uma ela precisará ser validada por nossa equipe.</small>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="modal-body">

                    {!! Form::model($bankAccount, [
                        'method'    => 'POST',
                        'route'     => ['client.add.bank.account'],
                        'enctype'   => 'multipart/form-data',
                    ]) !!}

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-6" style="float: left; padding-left: 0px">
                                    <div class="form-group {{ $errors->has('person') ? 'has-error' : '' }}">

                                        {!! Form::label('type_people', 'Tipo Pessoa') !!}
                                        {!! Form::select('type_people', ['PF' => 'Pessoa Fisica', 'PJ' => 'Pessoa Juridica'], old('type_people'), ['class' => 'form-control', 'id' => 'type_people']) !!}

                                        @if($errors->has('type_people'))
                                            <span class="help-block">{{ $errors->first('type_people') }}</span>
                                        @endif

                                    </div>
                                </div>

                                <div class="col-md-6"style="float: right; padding-right: 0px">
                                    <div class="form-group{{ $errors->has('cpf_cnpj') ? ' has-error' : '' }}">

                                        {!! Form::label('cpf_cnpj_banco', 'CPF/CNPJ',['id' => 'label_cpf_cnpj_banco']) !!}
                                        {!! Form::text('cpf_cnpj_banco', null, ['class' => 'form-control' ,'id' => 'cpf_cnpj_banco']) !!}
                                        @if ($errors->has('cpf_cnpj_banco'))
                                            <span class="help-block"><strong>{{ $errors->first('cpf_cnpj_banco') }}</strong></span>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12"style="float: right; padding-right: 0px">
                                <div class="form-group{{ $errors->has('titular_conta') ? ' has-error' : '' }}">

                                    {!! Form::label('titular_conta', 'Nome do titular da conta',['id' => 'label_titular_conta']) !!}
                                    {!! Form::text('titular_conta', $user->user->titular_conta, ['class' => 'form-control', 'placeholder' => '']) !!}

                                    @if($errors->has('titular_conta'))
                                        <span class="help-block">{{ $errors->first('titular_conta') }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-7">
                                <div class="form-group {{ $errors->has('bank') ? 'has-error' : '' }}">

                                    {!! Form::label('bank', 'Banco') !!}
                                    {!! Form::select('bank', App\Helpers\Helper::listBank(), '',['class' => 'form-control single-select']) !!}

                                    @if($errors->has('bank'))
                                        <span class="help-block">{{ $errors->first('bank') }}</span>
                                    @endif

                                </div>
                            </div>

                            <div class="col-md-5">

                                <div class="{{ $errors->has('type') ? 'has-error' : '' }}">

                                    {!! Form::label('type', 'Tipo Conta:') !!}
                                    {!! Form::select('type', App\Helpers\Helper::typeBankAccount(), old('type'), ['class' => 'form-control']) !!}

                                    @if($errors->has('type'))
                                        <span class="help-block">{{ $errors->first('type') }}</span>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                {!! Form::label('bank_branch', 'Agencia:') !!}
                                {!! Form::text('bank_branch', null, ['class' => 'form-control number10', 'placeholder' => '']) !!}

                                @if($errors->has('bank_branch'))
                                    <span class="help-block">{{ $errors->first('bank_branch') }}</span>
                                @endif
                            </div>

                            <div class="col-md-1">
                                {!! Form::label('bank_branch_digit', 'Digito:') !!}
                                {!! Form::text('bank_branch_digit', null, ['class' => 'form-control number10', 'placeholder' => '']) !!}

                                @if($errors->has('bank_branch_digit'))
                                    <span class="help-block">{{ $errors->first('bank_branch_digit') }}</span>
                                @endif
                            </div>

                            <div class="col-md-4">
                                {!! Form::label('bank_account', 'Conta:') !!}
                                {!! Form::text('bank_account', null, ['class' => 'form-control number10', 'placeholder' => '']) !!}

                                @if($errors->has('bank_account'))
                                    <span class="help-block">{{ $errors->first('bank_account') }}</span>
                                @endif
                            </div>

                            <div class="col-md-1">
                                {!! Form::label('bank_account_digit', 'Digito:') !!}
                                {!! Form::text('bank_account_digit', null, ['class' => 'form-control number10', 'placeholder' => '']) !!}

                                @if($errors->has('bank_account_digit'))
                                    <span class="help-block">{{ $errors->first('bank_account_digit') }}</span>
                                @endif
                            </div>

                            <div class="col-md-2">

                                {!! Form::label('variation', 'Variação:') !!}
                                {!! Form::text('variation', null, ['class' => 'form-control number10', 'placeholder' => '']) !!}

                                @if($errors->has('variation'))
                                    <span class="help-block">{{ $errors->first('variation') }}</span>
                                @endif

                            </div>
                        </div>
                    </div>

                    <div class="modal-footer" style="margin-top: 20px;">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="changeImageProfile" tabindex="-1" role="dialog" aria-labelledby="changeImageProfileLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title text-white" id="changeImageProfileLabel">Alterar Imagem</h5>
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    {!! Form::model($bankAccount, [
                        'method'    => 'POST',
                        'route'     => ['client.change.image'],
                        'enctype'   => 'multipart/form-data',
                    ]) !!}
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                {!! Form::label('image_profile', 'Imagem de Perfil') !!}

                                <div class=" form-group ">
                                    <div class="input-group">
                                        <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                                Buscar&hellip;
                                                <input data-id="file-avatar" type="file" style="display: none;" id="image_profile" name="image_profile" >
                                            </span>
                                        </label>
                                        <input type="text" style="height: 40px;" id="file-avatar" class="form-control " placeholder="" readonly>
                                    </div>
                                </div>

                                @if($errors->has('image_profile'))
                                    <span class="help-block">{{ $errors->first('image_profile') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                        <button type="submit" class="btn btn-primary">Trocar Imagem</button>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="changePassword" tabindex="-1" role="dialog"
         aria-labelledby="changeImageProfileLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title text-white" id="changeImageProfileLabel">Alterar Senha</h5>
                    <button type="button" class="close text-white" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="formPassword">
                        {{csrf_field()}}
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    {!! Form::label('password_old', 'Senha atual') !!}
                                    {!! Form::input('password', 'password_old', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                </div>

                                <div class="col-md-12 form-group">
                                    {!! Form::label('password', 'Nova senha') !!}
                                    {!! Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                </div>

                                <div class="col-md-12 form-group">
                                    {!! Form::label('password_confirmation', 'Confirme a nova senha') !!}
                                    {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            <button type="button" class="btn btn-primary" id="btnUpdatePassword">Alterar Senha</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

     <!-- Modal -->
    <div class="modal fade" id="modalDBA" tabindex="-1" role="dialog"
         aria-labelledby="changeImageProfileLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title text-white" id="changeImageProfileLabel">Deletar conta bancária</h5>
                    <button type="button" class="close text-white" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            Ao realizar a exclusão de uma conta bancária, essa ação não poderá ser desfeita. Tem certeza que deseja excluir?
                        </div>
                    </div>

                    <div class="modal-footer">
                        <form id="formDBA"  method="post">
                            <input type="hidden" id="id_BA">
                            <button type="button" class="btn btn-primary" id="accept">Aceito
                            </button>
                            <button type="button" class="btn btn-secondary" id="cancel">Cancelar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="viewEmailUser" tabindex="-1" role="dialog" aria-labelledby="changeImageProfileLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title text-white" id="changeImageProfileLabel">Email</h5>
                    <button type="button" class="close text-white" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>{{ $user->user->email}}</p>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Fechar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- End Modal -->

    <!-- Modal -->
    <div class="modal fade" id="viewSiteUser" tabindex="-1" role="dialog" aria-labelledby="changeImageProfileLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="background-color: #FFFFFF">
            <div class="modal-content border-warning">
                <div class="modal-header bg-warning">
                    <h5 class="modal-title text-white" id="changeImageProfileLabel">Site</h5>
                    <button type="button" class="close text-white" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>{{ $user->social_site}}</p>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" id="cancel" data-dismiss="modal">Fechar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal -->

</div>


@endsection

{{--  --}}
@push('scripts')

    <!--Select Plugins Js-->
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

    <script type="text/javascript">

        $(document).on('change', ':file', function() {
            var val = $(this).val();
            var id = "#"+$(this).data("id");
            $(id).attr('placeholder',val);
        });

        $('.single-select').select2();

        if($('#validate_cpf').val() == "true"){
            $('#cpf_cnpj').attr('disabled', 'disabled');
        }
        $("#btnUpdatePassword").click(function () {
            var data = $("#formPassword").serializeArray();
            var url = "{{url('/updatepassword')}}"

            $.ajax({
                type: "POST",
                data:data,
                url: url,
                success: function(result){
                    Swal.fire(
                        'Sucesso!',
                        "Senha alterada com sucesso",
                        'success'
                    )
                },
                error: function(result){
                    Swal.fire(
                        'Ooops!',
                        result.responseJSON.error,
                        'error'
                    )
                }
            });
        })
        function checkPerson() {
            var tipo = $("#person").val();

            if (tipo == 2) {
                $("#label_name").html('Razão Social');
                $("#label_cpf_cnpj").html('CNPJ');
                $("#label_birth").html('Data de Abertura');
                $("#cpf_cnpj").mask("99.999.999/9999-99");
            } else {
                $("#label_name").html('Nome Completo');
                $("#label_cpf_cnpj").html('CPF');
                $("#label_birth").html('Nascimento');
                $("#cpf_cnpj").mask("999.999.999-99");
            }
        }

        $("#person").change(function () {
            $('#cpf_cnpj').val('');
            checkPerson();
        });
        checkPerson();

        $("#type_people").change(function () {
            checkPersonBancario();
        });
        checkPersonBancario();
        function checkPersonBancario() {
            var tipo = $("#type_people").val();

            if (tipo == 2) {
                $("#label_cpf_cnpj_banco").html('CNPJ');
                $("#cpf_cnpj_banco").mask("99.999.999/9999-99");
            } else {
                $("#label_cpf_cnpj_banco").html('CPF');
                $("#cpf_cnpj_banco").mask("999.999.999-99");
            }
        }

        $("#btnPostalCode").click(function () {
            var cep = $(".cep").val().replace(/\D/g, '');
            if (cep != "") {
                var validacep = /^[0-9]{8}$/;
                if (validacep.test(cep)) {
                    $("#street").val("consultando...");
                    $("#city").val("consultando...");
                    $("#state").val("consultando...");

                    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                        if (!("erro" in dados)) {
                            $("#street").val(dados.logradouro);
                            $("#city").val(dados.localidade);
                            $("#state").val(dados.uf);
                            $("#district").val(dados.bairro);
                        } else {
                            limpa_formulário_cep();
                            console.log("CEP não encontrado.");
                        }
                    });
                } else {
                    limpa_formulário_cep();
                    console.log("Formato de CEP inválido.");
                }
            }
        });

        document.getElementById("cancel").addEventListener("click", function(event){
            $('#modalDBA').modal('hide');
        });

        $(document).on("click",".deletar",function(){
            event.preventDefault();
            $('#modalDBA').modal('show');
            $("#id_BA").val($(this).attr("data-id"));
        });

        $(document).on("click","#accept",function(){
            var id = $('#id_BA').val();
            var formData = {
                id:id
            };
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/client/bank-account/delete',
                type: 'DELETE',
                data: formData,
                success: function(result){
                    Swal.fire(
                        'Sucesso!',
                        "Sua conta foi deletada com sucesso",
                        'success'
                    )
                    $('#modalDBA').modal('hide');
                    document.getElementById('linha'+id).style.display = "none";
                },
                error: function(){
                    Swal.fire(
                        'Ooops!',
                        result.responseJSON.error,
                        'error'
                    )
                }
            });
        });

        $("#type_people").change(function () {
            var tipo = $("#type_people").val();
            $('#cpf_cnpj_banco').val('');
            if (tipo == "PJ") {
                $("#label_cpf_cnpj_banco").html('CNPJ');
                $("#cpf_cnpj_banco").mask("99.999.999/9999-99");
            } else {
                $("#label_cpf_cnpj_banco").html('CPF');
                $("#cpf_cnpj_banco").mask("999.999.999-99");
            }
        });
    </script>
@endpush



