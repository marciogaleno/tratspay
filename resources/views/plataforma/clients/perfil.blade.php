@extends('plataforma.templates.template')

@section('content')

        <!-- Conteúdo de título do topo -->
        <div class="content-header sty-one">
            <h1 class="text-black">{{ $titlePage }}</h1>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>

            </ol>
        </div>

        <!-- Fim do título / Inicio do perfil em barra lateral  -->
        <div class="content">
            <div class="row">
                <div class="col-lg-4">
                    <div class="user-profile-box m-b-3">
                        <div class="box-profile text-white"><img
                                    class="profile-user-img img-responsive img-circle m-b-2" src="dist/img/img1.jpg"
                                    alt="User profile picture">
                            <h3 class="profile-username text-center">InovaLabs</h3>
                            <p class="text-center">&copy; inovalabsce</p>

                        </div>
                    </div>
                    <div class="info-box">
                        <div class="box-body"><strong><i class="fa fa-book margin-r-5"></i> Conquistas</strong>
                            <p class="text-muted"> Aqui ficam as medalhas</p>


                            <hr>
                            <strong><i class="fa fa-envelope margin-r-5"></i> E-mail </strong>
                            <p class="text-muted">inovalabs@gmail.com</p>
                            <hr>
                            <strong><i class="fa fa-phone margin-r-5"></i> Telefone/WhatsApp</strong>
                            <p>(123) 456-7890 </p>


                            <hr>
                            <strong><i class="fa fa-phone margin-r-5"></i> Redes Sociais</strong>
                            <p>
                            <div class="text-left"><a class="btn btn-social-icon btn-facebook"><i
                                            class="fa fa-facebook"></i></a> <a class="btn btn-social-icon btn-google"><i
                                            class="fa fa-google-plus"></i></a> <a
                                        class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a> <a
                                        class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a></div>
                        </div>
                        <!-- /.Fim de configuração de widget / Inicio de Tabs do lado direito -->
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="info-box">
                        <div class="card tab-style1">
                            <!-- Navegação de Abas-->
                            <ul class="nav nav-tabs profile-tab" role="tablist">

                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#descricao"
                                                        role="tab" aria-expanded="false">Descrição</a></li>

                            </ul>
                            <!-- Aba Inicial de Informações -->
                            <div class="tab-content">

                                <!--Aba de Dados Cadastrais-->
                                <div class="tab-pane active" id="descricao" role="tabpanel" aria-expanded="false">
                                    <div class="card-body">
                                        <div class="row">


                                        </div>
                                        Aqui fica as informações preenchidas no formulário de perfil


                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection