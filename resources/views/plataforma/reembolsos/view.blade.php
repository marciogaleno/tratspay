@extends('plataforma.templates.template')
@section('content')

    <div class="panel panel-danger">
        <div class="box-header with-border">
            <h3 class="box-title text-danger" style="font-weight:400">
                {{$reembolso->user->name}}
            </h3>

        </div>
        <div class="box-body">
            <table class="table table-striped" style="font-size: 14px;">
                <thead>
                <tr>
                    <th>Email</th>
                    <th>Prazo</th>
                    <th>Status</th>
                    <th>Motivo</th>
                    <th>Descrição do motivo</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$reembolso->user->email}}</td>
                    <td>{{\Carbon\Carbon::create($reembolso->prazo)->format('d/m/Y')}}</td>
                    <td>
                        @if($reembolso->status == "A")
                            <p><i class="fa fa-circle text-info"></i> APROVADO</p>
                        @elseif($reembolso->status == "R")
                            <p><i class="fa fa-circle text-danger"></i> RECUSADO</p>
                        @else
                            <p><i class="fa fa-circle text-warning"></i> PENDENTE</p>
                        @endif
                    </td>
                    <td>{{\App\Helpers\Helper::getMotivosReembolso($reembolso->motivo)}}</td>
                    <td>{{$reembolso->observacao}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="card" style="">
        <div class="card-header">Detalhes do produto</div>
        <div class="card-body">
            <div class="panel-container show">
                <div class="panel-content">
                    <div class="table-responsive" style="">
                        <div class="col-md-4"style="float: left">
                            <div class="card animated fadeInDown" style="padding:10px;height: 321px;max-height: 321px;-webkit-animation-duration: 1s">
                                @if ($reembolso->product->image)
                                    <img src="{!! \App\Helpers\Helper::getPathFileS3('images', $reembolso->product->image) !!}" class="card-img-top" alt="Card image cap" style="max-height: 150px; min-height: 150px">
                                @else
                                    <img src="{{url('storage/app/public/products')}}" class="card-img-top" alt="Card image cap">
                                @endif

                                <div class="card-body" style="flex: none; padding: 5px;text-align: center;">
                                    <h5 class="card-title text-dark" style=" white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 175px;">{{$reembolso->product->name}}</h5>
                                </div>
                                <ul class="list-group list-group-flush list shadow-none text-dark" style="font-size: 12px">
                                    <li class="list-group-item  align-items-center" style="max-height: 40px;">Preço: <span  style="font-weight: bold">{{$reembolso->product->payment_type == 'SINGPRICE' ? 'R$ ' . $reembolso->product->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>
                                    <li class="list-group-item  align-items-center" style="max-height: 40px;;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 300px;">Comissão: <span  style="font-weight: bold">R$ {{$reembolso->product->configAffiliationWithoutGlobalScope->value_commission}}</span></li>
                                    <li class="list-group-item  align-items-center" style="max-height: 40px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 300px;">Produtor: <span style="font-weight: bold;">{{$reembolso->product->producer->first()->name}}</span></li>
                                </ul>

                            </div>
                        </div>
                        <div class="col-md-8" style="float: right">
                            <div class="card-body" style="flex: none; padding: 5px;text-align: center;">
                                @if($reembolso->status == "P")
                                    <h3 class="box-title text-danger" style="font-weight:400">
                                        Deseja aprovar o reembolso?
                                    </h3>
                                    <small>Após o prazo de 7 dias, caso você não aprove ou recuse o reembolso, nós daremos ele automaticamente ao cliente.</small>

                                    <div class="col-md-12" style="margin-top: 50px">

                                            <div class="btn-group btn-group-lg" role="group" aria-label="...">
                                                <a href="{{url('/reembolso/edit/'.$reembolso->id.'/A')}}" type="button" class="btn btn-primary">APROVAR</a>
                                                <a href="{{url('/reembolso/edit/'.$reembolso->id.'/R')}}" type="button" class="btn btn-danger">RECUSAR</a>
                                            </div>
                                    </div>
                                @else
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
