@extends('plataforma.templates.tabs') 
@section('contentTab')

    <!-- Tab panes -->
    <div class="row">
            <div class="col-sm-9">
                <h4 class="text-left">Lista de Co-Produtores</h4>
            </div>
    </div>
    <hr>
    <div class="table-responsive">
            <a href="{{route('product.coproducer.add', app('request')->product->id)}}" type="button" class="btn btn-success pull-right" style="margin: 5px 5px"><i class="fa fa-plus"></i> Novo </a>
            <table class="table table-sm" id="tableplans">
            <thead class="thead-light">
                <tr>
                <th scope="col">Co-Produtor</th>
                <th scope="col">Tipo</th>
                <th scope="col">Comissão</th>
                <th scope="col">Situação</th>
                <th scope="col">Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($coproducers as $coproducer)
                    <tr>
                        <td>
                            <div class="col-sm-3">
                                    <strong>Nome:</strong> {{$coproducer->user->name}}<br>
                                    <strong>Email:</strong>  {{$coproducer->email}}<br>
                            </div><!-- /.col -->    
                           
                        </td>
                        <td>{{ \App\Helpers\Helper::getType($coproducer->type)}}</td>
                        <td>
                            {{ 
                              $coproducer->value_commission . ($coproducer->format_commission == 'percent' ? '%' : '')
                            }}
                        </td>
                        <td>
                            <span class="badge  {{$coproducer->status == 'A' ? 'badge-success' : 'badge-danger'}} m-1">
                                    {{$coproducer->status == 'A' ? 'Ativo' : 'Inativo'}}
                            </span>
                            
                        </td>
                        <td>
                            {!! Form::open(['route' => ['product.coproducer.delete',  app('request')->product->id, $coproducer->id], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                    <a href="{{url('product/' . app('request')->product->id . '/coproducers/' . $coproducer->id)}}" class="btn btn-primary btn-sm" rel="nofollow" title="Editar"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger btn-sm" title="Excluir"><i class="fa fa-trash"></i></button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            </tbody>
            </table>
    </div>

@endsection

@section('scripts')

<script>
    
    function validate(form) {
        var valid = confirm('Você tem certeza que deseja fazer essa operação?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


</script>
@endsection