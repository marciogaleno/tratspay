@extends('plataforma.templates.tabs')

@section('contentTab')

    <!-- Topo do Painel -->
    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Adicionar Co-Produtor</h4>
        </div>
        <div class="col-sm-3 text-right">
            <a  href="{{route('product.coproducer.list', [app('request')->product->id])}}" class="btn btn-warning waves-effect waves-light m-1  text-right">
                <i class="fa fa-arrow-left"></i>
                <span>Voltar</span>
            </a>
        </div>
    </div>

    <hr>

    {{-- DEFINIÇÕES DO FORMULÁRIO --}}
    {{ Form::open(['route' => ['product.coproducer.create', app('request')->product->id], 'method' => 'post', 'id'=> 'form-submit']) }}

    <div class="card-body">

        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
            {!! Form::label('email', 'Email do co-produtor/gerente cadastrado na ColinaPay:', ['for' => 'email']) !!}
            {!! Form::text('email', old('email'), ['class' => 'form-control','placeholder' => 'Preencha com o email do co-produtor']) !!}

            @if ($errors->has('email')
                || $errors->has('email_coproducer_not_exists')
                || $errors->has('coproducer_exists')
            )
                <label class="error" for="email">
                    {{$errors->first('email')}}
                    {{$errors->first('email_coproducer_not_exists')}}
                    {{$errors->first('coproducer_exists')}}
                </label>
            @endif
        </div>

        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
            {!! Form::label('type', 'Tipo:', ['for' => 'type',"style"=> "margin-bottom: 0px;" ]) !!}
            <br><small  style="font-size: 10px">Co-Produtor: Recebe comissão baseada no valor que sobra do produto, descontando afiliados, taxas da ColinaPay e gerente.</small>
            <br><small  style="font-size: 10px">Gerente de Afiliados: Recebe comissão baseado no valor do produto.</small>
            {!! Form::select('type', $tipo, 1, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

            @if ($errors->has('type') || $errors->has('email_coproducer_not_exists'))
                <label class="error" for="type">
                    {{$errors->first('type')}}
                </label>
            @endif
        </div>


        @if($product->payment_type == 'PLAN')

            <hr>
            <div class="row">
                <div class="col-md-12">
                    <h5 class="text-center" style="color: gray;">Detalhamento dos Planos de Comissão</h5>
                </div>
            </div>
            <hr/>

            @foreach($product->plans as $plan)
                <div class="row">

                    <div class="col-md-4 text-left">
                        <div class="form-group">
                            @php $plan_name_id = 'plan_name'.$plan->id @endphp

                            {!! Form::label('plan', 'Plano:', ['for' => 'plan']) !!}
                            {!! Form::label('plan_name', $plan->name, ['class' => 'form-control', 'id' => $plan_name_id]) !!}
                        </div>
                    </div>

                    <div class="col-md-3 text-left ">
                        <input type="hidden" name="{{$plan->name}}" class="input-{{$plan->id}}">
                        {!! Form::label('value_commission', 'Valor da Comissões:', ['for' => 'value_commission']) !!}
                        {!! Form::text('value_commission', null, ['class' => 'form-control parent-plans', 'id' => $plan->id, 'placeholder' => '0,00']) !!}

                        @if ($errors->has('value_commission'))
                            <label class="error" for="value_commission">{{$errors->first('value_commission')}}</label>
                        @endif
                    </div>

                    <div class="col-md-3 text-left">
                        @php $format_commission_id = $plan->id.'-commission' @endphp
                        {!! Form::label('format_commission', 'Formato da comissão:', ['for' => 'format_commission' ]) !!}
                        {!! Form::select('format_commission', $formatCommission, null, ['class' => 'custom-select form-control', 'id' => $format_commission_id]) !!}

                        @if ($errors->has('format_commission'))
                            <label class="error" for="format_commission">{{$errors->first('accept_affiliation')}}</label>
                        @endif
                    </div>

                    <div class="col-md-2 text-center">
                        @php $open_modal_btn_id = '$open_modal_btn_id'.$plan->id @endphp

                        <button type="button" class="btn btn-success "  style="margin: 20px;">
                            <i class="fa fa-calculator"></i>
                            <span>Simular</span>
                        </button>
                    </div>
                </div>
                <hr/>
            @endforeach

        @else

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('format_commission', 'Formato da comissão:', ['for' => 'format_commission' ]) !!}
                        {!! Form::select('format_commission',   $formatCommission, null, ['class' => 'custom-select form-control']) !!}

                        @if ($errors->has('format_commission'))
                            <label class="error" for="format_commission">{{$errors->first('accept_affiliation')}}</label>
                        @endif
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="value_commission">Valor da Comissões:</label>
                        {!! Form::text('value_commission', null, ['class' => 'form-control', 'id' => 'value_commission', 'placeholder' => '0,00']) !!}

                        @if ($errors->has('value_commission'))
                            <label class="error" for="value_commission">{{$errors->first('value_commission')}}</label>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="" style="margin-top: 25px">
                        <label></label>
                        <button type="button" class="btn btn-success " id="openModal">
                            Simular comissão
                        </button>
                    </div>
                </div>
            </div>

        @endif

        <div class="form-group">
            <label for="deadline" style="margin-bottom: 0px;">Data limite:</label>
            <br> <small style="font-size: 9px">( Data máxima que o co-produtor ou gerente irá receber a comissão, após a data especificada ele não receberá mais comissões, porém se deixar em branco irá receber eternamente )</small>
            {!! Form::text('deadline', null, ['class' => 'form-control', 'id' => 'deadline', 'placeholder' => '00/00/0000']) !!}

            @if ($errors->has('deadline'))
                <label class="error" for="deadline">{{$errors->first('deadline')}}</label>
            @endif
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <!-- Button trigger modal -->
                <a href="" class="btn btn-warning">
                    <i class="fa fa-arrow-left"></i>
                    <span>Voltar</span>
                </a>
                <button type="button" id="btn-submit" class="btn  btn-primary">
                    <i class="fa fa-save"></i>
                    <span>Salvar</span>
                </button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>

    <script>
        $('#btn-submit').click(function () {

            var invalid = false;
            $(".parent-plans").map(function() {
                let value = $( this ).val();

                if(value == '') {
                    invalid = true
                }

                let nameSelect = $( this ).attr('id')+"-commission";
                var select = $("#"+nameSelect).val()
                var object = {value: value, type: select}
                $(".input-"+$( this ).attr('id')).val(JSON.stringify(object))
            })

            if(invalid) {
                alert('Preencha todos os planos');
                return false
            }

            $('#form-submit').submit();
        })

        $(function(){
            $('#deadline').datepicker({
                todayHighlight: true,
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });

            $("#value_commission").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

            $('#openModal').click(function () {

                if($('#value_commission').val() == null || $('#value_commission').val() == "") {
                    alert('preencha o valor da comissão');
                    return;
                }

                $('#exampleModal').modal('show')
            })
        })

        function numberParaReal(numero){
            return numero.toFixed(2).replace(".",",");
        }
    </script>

@endsection
