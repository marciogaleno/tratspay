@extends('plataforma.templates.tabs')
@section('contentTab')

@section('styles')
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
@endsection

    <!-- Tab panes -->

            <div class="row">
                <div class="col-sm-9">
                    <h4 class="text-left">Editar Co-Produtor</h4>
                </div>
                <div class="col-sm-3">
                    <a  href="{{route('product.coproducer.list', [app('request')->product->id])}}" class="btn btn-warning waves-effect waves-light m-1  text-right">
                        <i class="fa fa-arrow-left"></i>
                        <span>Voltar</span>
                    </a>
                </div>
            </div>
            <hr>
            {{ Form::model($coproducer, ['route' => ['product.coproducer.update',  app('request')->product->id, $coproducer->id], 'method' => 'put', 'id'=>'form-submit']) }}
                <div class="card-body">
                    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                        {!! Form::label('email', 'Email :', ['for' => 'email']) !!}
                        {!! Form::text('email', old('email'), ['class' => 'form-control','placeholder' => 'Digite o email do co-produtor']) !!}

                        @if ($errors->has('email')
                            || $errors->has('email_coproducer_not_exists')
                            || $errors->has('coproducer_exists')
                            )
                            <label class="error" for="email">
                            {{$errors->first('email')}}
                            {{$errors->first('email_coproducer_not_exists')}}
                            {{$errors->first('coproducer_exists')}}
                            </label>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        {!! Form::label('type', 'Tipo:', ['for' => 'type' ]) !!}
                        {!! Form::select('type', $tipo, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('type') || $errors->has('email_coproducer_not_exists'))
                            <label class="error" for="type">
                            {{$errors->first('type')}}
                            </label>
                        @endif
                    </div>

{{--                    <div class="form-group">--}}
{{--                        {!! Form::label('format_commission', 'Formato da comissão:', ['for' => 'format_commission' ]) !!}--}}
{{--                        {!! Form::select('format_commission',   $formatCommission, null, ['class' => 'custom-select form-control']) !!}--}}

{{--                        @if ($errors->has('format_commission'))--}}
{{--                        <label class="error" for="format_commission">{{$errors->first('accept_affiliation')}}</label>--}}
{{--                        @endif--}}
{{--                    </div>--}}

{{--                    <div class="form-group">--}}
{{--                        <label for="value_commission">Valor Comissão:</label>--}}
{{--                        {!! Form::text('value_commission', null, ['class' => 'form-control', 'id' => 'value_commission', 'placeholder' => '0,00']) !!}--}}

{{--                        @if ($errors->has('value_commission'))--}}
{{--                        <label class="error" for="value_commission">{{$errors->first('value_commission')}}</label>--}}
{{--                        @endif--}}
{{--                    </div>--}}
                    <div class="form-group">
                        <label for="deadline">Data limite:</label>
                        {!! Form::text('deadline', \App\Helpers\Helper::dateToBr($coproducer->deadline, false), ['class' => 'form-control', 'id' => 'deadline', 'placeholder' => '00/00/0000']) !!}

                        @if ($errors->has('deadline'))
                        <label class="error" for="deadline">{{$errors->first('deadline')}}</label>
                        @endif
                    </div>
                    @foreach(app('request')->product->plans as $plan)
                        <div class="row">
                            <div class="col-md-4 text-left">
                                <div class="form-group">
                                    @php $plan_name_id = 'plan_name'.$plan->id @endphp

                                    {!! Form::label('plan', 'Plano:', ['for' => 'plan']) !!}
                                    {!! Form::label('plan_name', $plan->name, ['class' => 'form-control', 'id' => $plan_name_id]) !!}
                                </div>
                            </div>

                            <?php $coProducerPlan = \App\Helpers\Helper::getCoproducerPlan($plan->id, app('request')->product->id, $coproducer->id) ?>
                            <div class="col-md-3 text-left ">
                                <input type="hidden" name="{{$plan->name}}" class="input-{{$plan->id}}">
                                {!! Form::label('value_commission', 'Valor da Comissões:', ['for' => 'value_commission']) !!}
                                {!! Form::text('coproducer_plan_value',$coProducerPlan->value ?? null, ['class' => 'form-control parent-plans', 'id' => $plan->id, 'placeholder' => '0,00']) !!}

                                @if ($errors->has('value_commission'))
                                    <label class="error" for="value_commission">{{$errors->first('value_commission')}}</label>
                                @endif
                            </div>

                            <div class="col-md-3 text-left">
                                @php $format_commission_id = $plan->id.'-commission' @endphp
                                {!! Form::label('format_commission', 'Formato da comissão:', ['for' => 'format_commission' ]) !!}
                                {!! Form::select('coproducer_plan_type', $formatCommission, $coProducerPlan->type ?? null, ['class' => 'custom-select form-control', 'id' => $format_commission_id]) !!}

                                @if ($errors->has('format_commission'))
                                    <label class="error" for="format_commission">{{$errors->first('accept_affiliation')}}</label>
                                @endif
                            </div>

                            <div class="col-md-2 text-center">
                                @php $open_modal_btn_id = '$open_modal_btn_id'.$plan->id @endphp

                                <button type="button" class="btn btn-success "  style="margin: 20px;">
                                    <i class="fa fa-calculator"></i>
                                    <span>Simular</span>
                                </button>
                            </div>
                        </div>
                        <hr/>
                    @endforeach
                    <div class="row">
                        <div class="col-md-12 text-right">
                        <a href="{{route('product.coproducer.list', [app('request')->product->id])}}" class="btn btn-warning"> Voltar</a>
                            <button type="button" id="btn-submit" class="btn  btn-primary"> Salvar</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}

@endsection

@section('scripts')

<script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
    $('#btn-submit').click(function () {

        var invalid = false;
        $(".parent-plans").map(function() {
            let value = $( this ).val();

            if(value == '') {
                invalid = true
            }

            let nameSelect = $( this ).attr('id')+"-commission";
            var select = $("#"+nameSelect).val()
            var object = {value: value, type: select}
            $(".input-"+$( this ).attr('id')).val(JSON.stringify(object))
        })

        if(invalid) {
            alert('Preencha todos os planos');
            return false
        }

        $('#form-submit').submit();
    })
    $("#value_commission").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

    $('#deadline').datepicker({
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });
</script>

@endsection
