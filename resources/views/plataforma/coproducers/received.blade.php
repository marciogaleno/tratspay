@extends('plataforma.templates.template')
@section('content')

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h4 class="page-title">Co-Produções</h4>

        </ol>
    </div>
    <div class="col-sm-3">
        <div class="btn-group float-sm-right">

            <span class="caret"></span>
            </button>
            <div class="dropdown-menu">

            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <!-- Tab panes -->

                <div class="table-responsive">

                    <table class="table" id="tableplans">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">Produto</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Comissão</th>
                                <th scope="col">Situação</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($coproducers as $coproducer)
                            <tr>
                                <td>
                                    <div class="col-sm-3">
                                            <strong>Produto: </strong>{{$coproducer->name_product}}<br>
                                            <strong>Nome Produtor: </strong>{{$coproducer->name_producer}}<br>
                                            <strong>Email: </strong>{{$coproducer->email_producer}}<br>
                                    </div><!-- /.col -->
                              
                                </td>
                                <td>{{ \App\Helpers\Helper::getType($coproducer->type)}}</td>
                                <td>
                                    {{ 
                                        $coproducer->format_commission == 'percent' ?
                                        \App\Helpers\Helper::getValueCommission($coproducer->value_commission) . '%' :
                                        'R$ ' . \App\Helpers\Helper::getValueCommission($coproducer->value_commission) 
                                        }}
                                </td>
                                <td>
                                    <span
                                        class="badge  {{$coproducer->status == 'A' ? 'badge-success' : 'badge-danger'}} m-1">
                                        {{$coproducer->status == 'A' ? 'Ativo' : 'Inativo'}}
                                    </span>

                                </td>
                                <td>
                                    @if ($coproducer->status == 'A')
                                        {!! Form::open(['route' => ['coproducer.cancel', $coproducer->id ], 'method' => 'POST', 'onSubmit' => "return validate(this)"]) !!}
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Cancelar</button>
                                        {!! Form::close() !!}
                                    @else 
                                        {!! Form::open(['route' => ['coproducer.approve', $coproducer->id ], 'method' => 'POST', 'onSubmit' => "return validate(this)"]) !!}
                                                <button class="btn btn-success btn-sm"><i class="fa fa-thumbs-up"></i> Aprovar</button>
                                        {!! Form::close() !!}
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>

</div>

@endsection

@section('scripts')

<script>
    function validate(form) {
        var valid = confirm('Você tem certeza que deseja fazer essa operação?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


</script>
@endsection