@extends('plataforma.templates.template')
@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-selection__choice{
            float: none !important;
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body" style="">

                    <div class="row">
                        <div class="col-md-6 padding-l-r-zero "  style="margin-right: auto;margin-left: auto;" id="divResumo">

                            <div class="panel panel-default"style="margin-bottom: 15px" >
                                <div class="panel-heading " style="background-color: #f5f5f5;padding: 10px;border-top-left-radius: 3px;border-top-right-radius: 3px;">
                                    <div class="panel-title"> <i class="fa fa-filter"></i> Filtros</div>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-bordered " id="tabelaVendasResumo">
                                        <thead>
                                        <tr role="row">
                                            <th>Vendas</th>
                                            <th>Comissão</th>
                                            <th>Valor Venda</th>
                                        </tr>
                                        </thead>

                                        <tbody role="alert" aria-live="polite" aria-relevant="all">
                                        <tr class="">
                                            <td id="totalVendas">0</td>
                                            <td id="totalComissao">R$ {{\App\Helpers\Helper::moneyBR(0)}}</td>
                                            <td id="totalValorVenda">R$ {{\App\Helpers\Helper::moneyBR(0)}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive" style="">
                        <table class="table table-sm">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col" style="width: 5%;">Co-produtor</th>
                                <th scope="col">Comissão</th>

                                {{--                                    <th scope="col">Comissão</th>--}}
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            {{--                            @forelse ($coaffiliates as $commission)--}}
                            {{--                                <tr>--}}
                            {{--                                    <td><a href="{{route('sale.details', $commission->id)}}">{{$commission->code}}</a></td>--}}
                            {{--                                    <td>--}}
                            {{--                                        <a href="{{route('sale.details', $commission->id)}}">{{$commission->name}}</a>--}}
                            {{--                                    </td>--}}
                            {{--                                    <td>--}}
                            {{--                                        <strong>Nome Comprador: </strong>{{$commission->first_name . ' ' . $commission->las_name}}<br>--}}
                            {{--                                        <strong>Email comprador: </strong>{{$commission->email}}<br>--}}
                            {{--                                        <strong>Telefone comprador: </strong>{{$commission->phone ? $commission->phone : ''}}<br>--}}
                            {{--                                    </td>--}}
                            {{--                                    <td>{{App\Helpers\Helper::paymentForms($commission->payment_type_id)}}</td>--}}
                            {{--                                    <td>{{App\Helpers\Helper::getStatusPayment($commission->status)}}</td>--}}
                            {{--                                    <td>{{\App\Helpers\Helper::dateToBr($commission->date_created, false)}}</td>--}}
                            {{--                                    --}}{{--                                        <td>{{App\Helpers\Helper::moneyBR($commission->value)}}</td>--}}
                            {{--                                    <td>--}}
                            {{--                                        <a  href="{{route('sale.details', $commission->id)}}" target="_blank"  class="btn btn-primary btn-sm"><i aria-hidden="false" style="color:#FFFFFF" class="fa fa-search"></i></a>--}}
                            {{--                                    </td>--}}
                            {{--                                </tr>--}}
                            {{--                            @empty--}}
                            {{--                                <tr>Nenhuma comissão encontrada</tr>--}}
                            {{--                            @endforelse--}}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
{{--                    <div class="pull-right">{!! $coaffiliates->links() !!}</div>--}}
                </div>
            </div>
        </div>
    </div>
    </div>

    </div>
@stop
@section('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $("#cpf_cnpj").mask("999.999.999-99");
            $('#form-payment').select2();
            $('#product_name').select2({ width: 'resolve' });
            $('#co_affiliates').select2();
        });

    </script>
@stop