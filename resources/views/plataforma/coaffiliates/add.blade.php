@extends('plataforma.templates.template')
@section('content')

@section('styles')
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
@endsection

<div class="row">
{!! \App\Helpers\TabsAffiliateHelper::openBoxTabs($product) !!}
{!! \App\Helpers\TabsAffiliateHelper::openHeader($product, 2) !!}
{!! \App\Helpers\TabsAffiliateHelper::openContent() !!}

            {{ Form::open(['route' => ['coaffiliate.create', $product->id], 'method' => 'post']) }}

                <div class="card-body">

                    <div class="row">
                            <div class="col-sm-10">
                                <h4 class="text-left">Adicionar Co-Afiliado</h4>
                            </div>

                            <div class="col-sm-10">
                                <h6 class="text-left">Nessa aba você poderá adicionar co-afiliados a este produto que irá receber comissões a cada venda gerada por você.</h6>
                            </div>

                            <div class="col-sm-2">
                                <a  href="{{route('product.checkout.view', [$product->id])}}" class="btn btn-warning waves-effect waves-light m-1  text-right">
                                    <i class="fa fa-arrow-left"></i>
                                    <span>Voltar</span>
                                </a>
                            </div>
                        </div>

                    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                        {!! Form::label('email', 'E-mail cadastrado na ColinaPay :', ['for' => 'email']) !!}
                        {!! Form::text('email', old('email'), ['class' => 'form-control','placeholder' => 'Digite o email do co-produtor']) !!}

                        @if ($errors->has('email')
                            || $errors->has('email_coproducer_not_exists')
                            || $errors->has('coproducer_exists')
                            )
                            <label class="error" for="email">
                            {{$errors->first('email')}}
                            {{$errors->first('email_coproducer_not_exists')}}
                            {{$errors->first('coproducer_exists')}}
                            </label>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('format_commission', 'Formato da Comissão de Co-afiliado:', ['for' => 'format_commission' ]) !!}
                        {!! Form::select('format_commission',   $formatCommission, null, ['class' => 'custom-select form-control']) !!}

                        @if ($errors->has('format_commission'))
                        <label class="error" for="format_commission">{{$errors->first('accept_affiliation')}}</label>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="value_commission">Valor da Comissão de Co-afiliado:</label>
                        {!! Form::text('value_commission', null, ['class' => 'form-control', 'id' => 'value_commission', 'placeholder' => '0,00']) !!}

                        @if ($errors->has('value_commission'))
                        <label class="error" for="value_commission">{{$errors->first('value_commission')}}</label>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="deadline">Data que o co-afiliado ainda irá receber comissão:</label>
                        {!! Form::text('deadline', null, ['class' => 'form-control', 'id' => 'deadline', 'placeholder' => '00/00/0000']) !!}

                        @if ($errors->has('deadline'))
                        <label class="error" for="deadline">{{$errors->first('deadline')}}</label>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right">
                        <a href="{{route('product.coproducer.list', [$product->id])}}" class="btn btn-warning"> Voltar</a>
                            <button type="submit" class="btn  btn-primary"> Salvar</button>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
{!! \App\Helpers\TabsAffiliateHelper::closeContent() !!}
{!! \App\Helpers\TabsAffiliateHelper::closeHeader() !!}
{!! \App\Helpers\TabsAffiliateHelper::closeBoxTabs() !!}

@endsection

@section('scripts')

<script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script>
    $("#value_commission").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

    $('#deadline').datepicker({
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });
</script>

@endsection
