@extends('plataforma.templates.template') 
@section('content')

<div class="row">

                <!-- Nav tabs -->
    {!! \App\Helpers\TabsAffiliateHelper::openBoxTabs($product) !!}
    {!! \App\Helpers\TabsAffiliateHelper::openHeader($product, 2) !!}
    {!! \App\Helpers\TabsAffiliateHelper::openContent() !!}
    <!-- Tab panes -->

        <div class="row">
                <div class="col-sm-9">
                    <h4 class="text-left">Co-Afiliação</h4>
                    <small> Ao adicionar um co-afiliado, você estará dividindo a sua comissão de afiliado com 1 ou mais pessoas</small>
                </div>
        </div>
        <hr>   

        
<!-- Tab panes -->
    <div id="tabe-13" class="container tab-pane  active show">
        
            <div class="table-responsive">
                    <a href="{{route('coaffiliate.add', $product->id)}}" type="button" class="btn btn-success pull-right" style="margin: 5px 5px"><i class="fa fa-plus"></i> Novo </a>
                    <table class="table table-sm">
                    <thead class="thead-light">
                        <tr>
                        <th scope="col">Co-Afiliado</th>
                        <th scope="col">Comissão</th>                                               
                        <th scope="col">Situação</th>
                        <th scope="col">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($coaffiliates) > 0)
                        @foreach ($coaffiliates as $coaffiliate)
                       
                            <tr id="linha{{ $coaffiliate->id }}">
                                <td class="col-xs-1">
                                    <div class="col-sm-3">
                                            <strong>Nome:</strong>{{$coaffiliate->user_name}}<br>
                                            <strong>Email suporte:</strong>  {{$coaffiliate->email}}<br>
                                    </div><!-- /.col -->
                                </td>
                                <td class="col-xs-1">
                                    {{ 
                                    $coaffiliate->value_commission . ($coaffiliate->format_commission == 'percent' ? '%' : '')
                                    }}
                                </td>
                                <td class="col-xs-1">
                                    <span class="badge  {{$coaffiliate->status == 'A' ? 'badge-success' : 'badge-danger'}} m-1">
                                            {{$coaffiliate->status == 'A' ? 'Ativo' : 'Inativo'}}
                                    </span>
                                    
                                </td>
                                <td>
                                    <a href="#" class="btn btn-primary btn-sm" rel="nofollow" title="Editar"><i class="fa fa-pencil"></i></a>
                                    <button class="btn btn-danger btn-sm deletar" data-id={{ $coaffiliate->id }} title="Excluir"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                    </table>
            </div>
        
    </div>

    <!--Warning Modal -->
    <div class="modal fade" id="cancelCoAffiliation">
        <div class="modal-dialog">
        <div class="modal-content border-danger">
            <div class="modal-header bg-danger">
            <h5 class="modal-title text-white"><i class="fa fa-danger"></i>Atenção</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            <h5 class="modal-title" id="changeImageProfileLabel">Deseja realmente deletar a conta?</h5>
            <div class="modal-footer">
                <form id="formDBA"  method="post">
                <input type="hidden" id="id_CA">
                <button type="button" class="btn btn-inverse-danger" data-dismiss="modal"  id="cancel"><i class="fa fa-times"></i>Cancelar</button>
                <button type="button" class="btn btn-danger" id="accept"><i class="fa fa-check-square-o"></i>Aceito</button>
                </form>
            </div>
        </div>
        </div>
    </div><!--End Modal -->

                   
                   
    {!! \App\Helpers\TabsAffiliateHelper::closeContent() !!}
    {!! \App\Helpers\TabsAffiliateHelper::closeHeader() !!}
    {!! \App\Helpers\TabsAffiliateHelper::closeBoxTabs() !!}
 
   
</div>

@endsection

@section('scripts')

<script>
    
    document.getElementById("cancel").addEventListener("click", function(event){
        $('#cancelCoAffiliation').modal('hide');
    });

    $(document).on("click",".deletar",function(){
        event.preventDefault();
        $('#cancelCoAffiliation').modal('show');
        $("#id_CA").val($(this).attr("data-id"));
    });

    $(document).on("click","#accept",function(){
        var id = $('#id_CA').val();
        var formData = {
                id:id
            };
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: '/coaffiliates/delete',
            type: 'DELETE',
            data: formData,
            success: function(result){
                event.preventDefault();
                if(result == "true"){
                    Swal.fire(
                        'Sucesso!',
                        "Sua conta foi deletada com sucesso",
                        'success'
                    )
                    $('#cancelCoAffiliation').modal('hide');
                    document.getElementById('linha'+id).style.display = "none";
                }else{
                     Swal.fire(
                        'Erro!',
                        "Sua conta nao foi deletada",
                        'danger'
                    )
                }
            },
            error: function(){
                Swal.fire(
                    'Ooops!',
                    result.responseJSON.error,
                    'error'
                )
            }
        });
    });

</script>
@endsection