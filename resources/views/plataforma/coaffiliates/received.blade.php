@extends('plataforma.templates.template')
@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">

                @if (count($coaffiliates) > 0)
                <!-- Tab panes -->
                <div id="tabe-13" class="container tab-pane  active show">

                    <div class="table-responsive">
                        <table class="table table-sm" id="tableplans">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Comissão</th>
                                    <th scope="col">Situação</th>
                                    <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($coaffiliates as $coaffiliate)
                                <tr>
                                    <td class="col-xs-1">
                                        <div class="col-sm-3">
                                            <strong>Nome: </strong>{{$coaffiliate->name_product}}<br>
                                            <strong>Afiliado: </strong> {{$coaffiliate->name_affiliate}}<br>
                                            <strong>Email: </strong> {{$coaffiliate->email_affiliate}}<br>
                                        </div><!-- /.col -->
                                    </td>
                                    <td class="col-xs-1">
                                        {{ 
                                    $coaffiliate->format_commission == 'percent' ?
                                    \App\Helpers\Helper::getValueCommission($coaffiliate->value_commission) . '%' :
                                    'R$ ' . \App\Helpers\Helper::getValueCommission($coaffiliate->value_commission) 
                                    }}
                                    </td>
                                    <td class="col-xs-1">
                                        <span
                                            class="badge  {{$coaffiliate->status == 'A' ? 'badge-success' : 'badge-danger'}} m-1">
                                            {{$coaffiliate->status == 'A' ? 'Ativo' : 'Inativo'}}
                                        </span>

                                    </td>
                                    <td>
                                        @if ($coaffiliate->status == 'A')
                                            {!! Form::open(['route' => ['coaffiliate.cancel', $coaffiliate->id ], 'method' => 'POST', 'onSubmit' => "return validate(this)"]) !!}
                                                    <button class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Cancelar</button>
                                            {!! Form::close() !!}
                                        @else 
                                            {!! Form::open(['route' => ['coaffiliate.approve', $coaffiliate->id ], 'method' => 'POST', 'onSubmit' => "return validate(this)"]) !!}
                                                    <button class="btn btn-success btn-sm"><i class="fa fa-thumbs-up"></i> Aprovar</button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                @else
                    <p>Nenhuma co-afiliação encontrada</p>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
    function validate(form) {
        var valid = confirm('Você tem certeza que deseja realizar essa operação?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


</script>
@endsection