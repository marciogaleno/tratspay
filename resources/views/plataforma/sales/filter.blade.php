<div class="row">
<div class="col-md-12">

    <form method="GET" action="{{route('sale.list')}}">

        {{-- PRIMEIRA LINHA --}}
        <div class="row">

            {{-- TRANSAÇÃO --}}
            <div class="form-group col-md-4  @if ($errors->has('transaction_id')) has-error @endif" style="float: left">
                {!! Form::label('transaction_id', 'Transação*', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::text('transaction_id', Illuminate\Support\Facades\Input::get('transaction_id'), ['class' => 'form-control']) !!}
                    @if ($errors->has('transaction_id')) <p class="help-block">{{ $errors->first('transaction_id') }}</p> @endif
                </div>
            </div>

            {{-- STATUS --}}
            <div class="form-group col-md-4  @if ($errors->has('status_transaction')) has-error @endif" style="float: left">
                {!! Form::label('status_transaction', 'Status:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::select('status_transaction[]', App\Helpers\Helper::getAllStatusPayment(),  Illuminate\Support\Facades\Input::get('status_transaction'),['class' => 'form-control', 'Placeholder' => 'Selecione', 'multiple'=> 'multiple', 'id' => 'status_transaction']) !!}
                    @if ($errors->has('status_transaction')) <p class="help-block">{{ $errors->first('status_transaction') }}</p> @endif
                </div>
            </div>

            {{-- DATA DA COMPRA --}}
            <div class="form-group col-md-4  @if ($errors->has('data_pedido')) has-error @endif" style="float: left">
                {!! Form::label('data_pedido', 'Data da Compra:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {{ Form::date('data_pedido', Illuminate\Support\Facades\Input::get('data_pedido'),['class' => 'form-control']) }}
                    @if ($errors->has('data_pedido')) <p class="help-block">{{ $errors->first('data_pedido') }}</p> @endif
                </div>
            </div>

        </div>


        {{-- SEGUNDA LINHA --}}
        <div class="row">

            {{-- CLIENTE --}}
            <div class="form-group col-md-6  @if ($errors->has('co_client')) has-error @endif" style="float: left">
                {!! Form::label('clients', 'Cliente:', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::text('clients', Illuminate\Support\Facades\Input::get('clients'), ['class' => 'form-control', 'id'=>'clients']) !!}
                    @if ($errors->has('clients')) <p class="help-block">{{ $errors->first('clients') }}</p> @endif
                </div>
            </div>

            {{-- PRODUTO --}}
            <div class="form-group col-md-6  @if ($errors->has('product_name')) has-error @endif" style="float: left">
                {!! Form::label('product_name', 'Produto*', ['class' => 'control-label']) !!}
                <div class="controls">
                    {!! Form::select('product_name[]',$products, Illuminate\Support\Facades\Input::get('product_name'), ['class' => 'form-control', 'multiple'=> 'multiple','id'=> 'product_name']) !!}
                    @if ($errors->has('product_name')) <p class="help-block">{{ $errors->first('product_name') }}</p> @endif
                </div>
            </div>

        </div>

        <div class="col-md-12 ">
            <div class=""  style="float: right; padding: 10px">
                <input type="submit" class="form-control btn-primary" value="Localizar">
            </div>
        </div>

    </form>
</div>
</div>
