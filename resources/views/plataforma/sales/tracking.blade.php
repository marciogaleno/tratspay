@extends('plataforma.templates.template')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Rastreio&nbsp;</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-uppercase"> <i class="fa fa-truck"></i> Rastreio de produtos enviados</div>
                    <div class="card-body">
                            <form action="" method="GET" class="">
                                @csrf
                                
                                <div class="row">
                                    <div class="col-md-4" style="float:left">
                                        <div class="form-group">
                                            {!! Form::label('product_id', 'Empresa de logística:',['id'=>'label_product_id',]) !!}
                                            {!! Form::select('product_id', ['1'=>'Correios'], null, ['class'=> 'form-control', "placeholder" => "-- Selecione --"]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="float:left">
                                        <div class="form-group">
                                            {!! Form::label('complement', 'Transação:') !!}
                                            {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '123456']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-4" style="float:left">
                                        <div class="form-group">
                                            {!! Form::label('complement', 'Rastreio:') !!}
                                            {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => 'RA123456789BR']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-md-2 text-right">
                                        <div class=""><button class="btn btn-success " type="submit"><i class="fa fa-search"></i> Buscar</button></div>
                                    </div>
                                </div>
                            </form>
                      
                    </div>
                </div>
                <div class="card">
                    <div class="card-header text-uppercase"><i class="fa fa-file-text"></i> Importação de arquivo CSV</div>
                        <div class="card-body">
                            

                         

                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Atenção para o código que deve ser informado no arquivo CSV para o campo 
                                        <strong> Empresa de logística</strong>    na tabela abaixo:</p>                                   
                                    </div>
                                </div>
    
                                <div class="row mt-2 mb-2">
                                    <div class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
                                        <table  class="table table-bordered">
                                            <thead>
                                                <tr class="table-dark">
                                                    <th>Empresa</th>
                                                    <th>Código</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Correios</td>
                                                    <td>1</td>
                                                </tr>
                                            </tbody>
                                        </table>   
                                    </div>
                                </div>
    
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Importe arquivos separados com vírgula seguindo o seguinte padrão: Empresa de logística,#transação,Código de rastreio (1,123456,RA123456789BR) <a href="#">Arquivo modelo</a> </p>
                                        <p>* O arquivo de modelo considera os correios como Empresa de logística.</p>
                                    </div>
                                </div>
                          
                            <div class="row">
                                <div class="col-md-12">
                                     <strong>Arquivo CSV de rastreio:</strong> 
                                     <input type="file" name="" id="">
                                     <button class="btn btn-success" type="submit"> <i class="fa fa-wrench" aria-hidden="true"></i> Processar</button>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header text-uppercase"> 
                        <i class="fa fa-list"></i> Transações processadas
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td>Transação</td>
                                                <td>Validação</td>
                                                <td>Ações</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    

               

            </div>
        </div>
     
   </div>
@stop