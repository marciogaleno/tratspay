@extends('plataforma.templates.template')

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection

@section('content')

<div class="col-lg-12" style="">
    <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-sm-9">
              <h5 class="text-black">Transação #{{$commissionUser->transaction->id}}</h5>
            </div>
          </div>
        </div>
        <div class="card-body" style="">
            <div class="row">
                <div class="col-3">
                    <img src="{{\App\Helpers\Helper::getPathFileS3('images', $commissionUser->product->image)}}" style="width: 200px;height: 200px;object-fit: contain;">
                    <br><br>

                    @if ($commissionUser->commission_type == 'producer' || $commissionUser->commission_type == 'coproducer')
                      @if (isset($reembolso->id))  
                      <div>
                          <a type="submit" href="{{route('reembolso.view', $reembolso->id)}}" target="_blank" class="btn btn-success waves-effect waves-light m-1">Reembolsar</a>
                        </div>
                      @endif
                        @if ($commissionUser->transaction->status == 'finished' || true)
                            <div>
                                <button type="submit" data-toggle="modal" data-target="#modalConfirmResendData" class="btn btn-block btn-warning waves-effect waves-light m-1">Reenviar dados</button>
                            </div>
                        @endif

                        @if ($commissionUser->transaction->payment_type_id == 'ticket' && $commissionUser->transaction->status == 'pending')
                          <div>
                              <button type="submit" data-toggle="modal" data-target="#modalConfirmResendTicket" class="btn btn-block btn-success waves-effect waves-light m-1">Reenviar boleto por email</button>
                          </div>
                          <div class="">
                              <a class="btn btn-block btn-info waves-effect waves-light m-1" href="{{route('checkout.payment.ticket', $commissionUser->transaction->id)}}" target="_blank">
                                  Exibir Boleto
                              </a>
                          </div>
                        @endif

                    @endif
                </div>

                <div class="col-9">
                    <table class="table">
                        <tbody>
                            <tr>
                              <td>Transação:</td> <td>{{'#' . $commissionUser->transaction->id}}</td>
                            </tr>   
                            <tr>                             
                              <td>Produto: </td><td>{{$commissionUser->product->name}}</td>
                            </tr>                                
                            <tr>                             
                              <td>Meio de pagamento: </td><td>{{App\Helpers\Helper::paymentForms($commissionUser->transaction->payment_type_id)}}</td>
                            </tr>                                
                            <tr>                             
                              <td>Data da Venda: </td><td>{{\App\Helpers\Helper::dateToBr($commissionUser->transaction->date_created, true)}}</td>
                            </tr>
                            @if ($commissionUser->commission_type == 'producer' || $commissionUser->commission_type == 'coproducer')                           
                              <tr>                             
                                <td>Comprador: </td><td>{{$commissionUser->transaction->first_name . ' ' . $commissionUser->transaction->last_name}}</td>   
                              </tr>   
                            @endif  
                            @if ($commissionUser->commission_type == 'producer' || $commissionUser->commission_type == 'coproducer')                           
                              <tr>                             
                                <td>Email comprador: </td>
                                <td>
                                  {{$commissionUser->transaction->email}}
                                  <a href="" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalEditEmail">
                                 
                                      <i aria-hidden="true" class="fa fa-edit"></i> Alterar
                                  
                                  </a>
                                </td>   
                              </tr>   
                            @endif  
                            @if ($commissionUser->transaction->end_warranty)                           
                              <tr>                             
                                <td>Garantia: </td><td>{{\App\Helpers\Helper::getRemainingWarrantyDays($commissionUser->transaction->end_warranty, true)}}</td>   
                              </tr>   
                            @endif  
                            <tr>                             
                                <td>Status da Venda: </td><td>{{App\Helpers\Helper::getStatusPayment($commissionUser->transaction->status)}}</td>
                            </tr>                                                             
                            <tr>                             
                                <td>Valor da venda: </td><td>R$ {{number_format($commissionUser->transaction->transaction_amount, 2, ",", ".")}}</td>
                            </tr>                                                             
                            <tr>                             
                                <td>Seu recebimento: </td><td>R$ {{number_format($commissionUser->value, 2, ",", ".")}}</td>
                            </tr>  
                            @if ($commissionUser->commission_type == 'producer' || $commissionUser->commission_type == 'coproducer')                                                            
                              <tr>                             
                                  <td>Taxa plataforma: </td><td>R$ {{number_format($commissionUser->transaction->rate_total, 2, ",", ".")}}</td>
                              </tr>
                            @endif                                                           
                        </tbody>
                      </table>                       
                </div>
            </div>
            <br><br>
            @if ($commissionUser->commission_type == 'producer' || $commissionUser->commission_type == 'coproducer')
            <div class="row">
                <div class="col-6">
                    <h6 style="position: absolute;bottom:0px">Disposição das comissões:</h6>
                </div>
                <div class="col-6">
                  @if (!$hasCommissionAffiliate && ($commissionUser->commission_type == 'producer' || $commissionUser->commission_type == 'coproducer'))
                    <div class="text-right">
                        <button type="button" data-toggle="modal" data-target="#modalAdicionarAfiliado" id="btnAdicionarAfiliado" onclick="getAffiliates({{$commissionUser->product_id}})" class="btn btn-primary waves-effect waves-light m-1">Adicionar afiliado</button>
                    </div>
                  @endif
                </div>
            </div>
            
            
            <div class="row">
                <div class="col-12">
                      <table class="table">
                          <thead class="thead-light">
                              <tr>
                                  <th scope="col">Nome</th>
                                  <th scope="col">Tipo</th>
                                  <th scope="col">E-mail</th>
                                  <th scope="col">Valor</th>
                              </tr>
                          </thead>
                          <tbody>
                            @foreach ($allCommissions as $commission)
                              @if($commission->commission_type <> 'system')
                                  <tr>
                                      <td>{{$commission->user->name}}</td>
                                      <td>{{\App\Helpers\Helper::getCommissionType($commission->commission_type)}}</td>
                                      <td>{{$commission->user->email}}</td>
                                      <td>R$ {{number_format($commission->value, 2, ',', '.')}}</td>
                                  </tr>
                              @endif
                            @endforeach
                                <tr>
                                    <td>Taxa plataforma</td>
                                    <td></td>
                                    <td></td>
                                    <td>R$ {{number_format($commissionUser->transaction->rate_total, 2, ",", ".")}}</td>
                                </tr>                                                                                             
                          </tbody>
                          <tfoot>
                              <td></td>
                              <td></td>
                              <td><div class="text-right">Total:</div></td>
                              <td>R$ {{number_format($commissionUser->transaction->transaction_amount, 2, ",", ".")}}</td>
                          </tfoot>
                      </table>
                </div>             
            </div>
            @endif
        </div>
    </div>
</div>

<div class="modal fade" id="modalAdicionarAfiliado">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Adicionar afiliado </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        {!! Form::open(['route' => 'sale.reprocessCommission', 'method' => 'post']) !!}

          <div class="modal-body">       
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">            
                          {!! Form::hidden('transaction_code', $commissionUser->transaction->code) !!}
                          {!! Form::label('affiliate_id', 'Afiliado:', ['for' => 'affiliate' ]) !!}
                          {!! Form::select('affiliate_id', [], null, ['class' => 'form-control single-select', 'id' => 'affiliateList']) !!}
                        </div>
                    </div>
                </div>
           
          </div>

          <div class="modal-footer">
              <button type="submit" data-toggle="modal" onclick="loading()" class="btn btn-primary waves-effect waves-light m-1">
                  Adicionar
              </button>
          </div>

          {!! Form::close() !!}
      </div>
    </div>
</div>
<div class="modal fade" id="modalEditEmail">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">E-mail Comprador</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {!! Form::open(['route' => 'sale.editEmailUser', 'method' => 'post']) !!}
          <div class="modal-body">            
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">            
                          {!! Form::hidden('transaction_code', $commissionUser->transaction->code) !!}
                          {!! Form::hidden('old_email', $commissionUser->transaction->email) !!}
                          {!! Form::label('email', 'E-mail:', ['for' => 'affiliate' ]) !!}
                          {!! Form::text('email', $commissionUser->transaction->email, ['class' => 'form-control', 'id' => 'email']) !!}
                        </div>
                    </div>
                </div>
            
          </div>

          <div class="modal-footer">
              <button type="submit" onclick="loading()" class="btn btn-primary waves-effect waves-light m-1">
                  Alterar
                </button>
          </div>

          {!! Form::close() !!}
      </div>
      </div>
</div>

<div class="modal fade" id="modalConfirmResendData">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Reenviar dados de acesso para afiliado</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {!! Form::open(['route' => 'sale.resendDataAffiliate', 'method' => 'post']) !!}
        <div class="modal-body">
                
                <div class="row">
                  <div class="col-12">
                      Ao clicar em re-enviar dados, nós iremos re-processar a venda, enviando ao seu cliente um novo e-mail de compra e novo acesso/informações de seu produto
                  </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">            
                            {!! Form::hidden('transaction_code', $commissionUser->transaction->code) !!}
                            {!! Form::hidden('email', $commissionUser->transaction->email) !!}
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" onclick="loading()" class="btn btn-success waves-effect waves-light m-1">Confirmar</button>          
        </div>

        {!! Form::close() !!}
      </div>
    </div>
</div>

<div class="modal fade" id="modalConfirmResendTicket">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Confirmar reenvio de boleto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {!! Form::open(['route' => 'sale.resendTicket', 'method' => 'post']) !!}
        <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">            
                            {!! Form::hidden('transaction_code', $commissionUser->transaction->code) !!}
                        </div>
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="submit" onclick="loading()" class="btn btn-success waves-effect waves-light m-1">Confirmar</button>          
        </div>

        {!! Form::close() !!}
      </div>
    </div>
</div>

</div>
 
  
@endsection

@section('scripts')
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>

    $('.single-select').select2();

    @if (!$hasCommissionAffiliate && $commissionUser->commission_type == 'producer')

          function loading() {
              $.preloader.start();
          } 

          function getAffiliates($product_id) {
              var url = {!! json_encode(url('sales/affiliatesByProduct')) !!} + '/' + $product_id;

              $.ajax({
                url : url,
                type : 'GET',
                beforeSend : function(){
                  $.preloader.start();
                }
              })
              .done(function(msg){
                  var options = '';
                  Object.keys(msg).forEach(function(k){
                    options += '<option value="' + msg[k].id + '">' +  msg[k].name  + ' - ' +  msg[k].email + '</option>';
                  });

                  $('#affiliateList')
                      .find('option')
                      .remove()
                      .end()
                      .append(options)
                      .val('whatever')
                  ;
                  $.preloader.stop();
              })
              .fail(function(jqXHR, textStatus, msg){
                  $('#tableCheckouts tbody').empty();
              }); 
          }

    @endif



</script>
@endsection