@extends('plataforma.templates.template')

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/tui-chart/tui-chart.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/plugins/datetimepicker-jr/bootstrap-datetimepicker.min.css')}}" />
    <style>
        .tui-chart > svg {
            width: 100% !important;
            height: 100%;
        }
        .tui-chart {
            width: 100% !important;
        }
    </style>
@endsection

@section('content')

    <div class="col-lg-12" style="">
        <div class="card" style="background-color: #f1f1f1">
            <div class="card-header">
                <div class="row">
                    <h5 class="text-black"> <i class="fa fa-filter"></i> Filtrar</h5>
                </div>
            </div>
            <div class="card-body" style="">
                <form method="GET" action="#">
                    <div class="row">
                        <div class="form-group col-md-6  @if ($errors->has('product_name')) has-error @endif" style="float: left">
                            {!! Form::label('product_name', 'Produto*', ['class' => 'control-label']) !!}
                            <div class="controls">
                                {!! Form::select('product_name[]',$products, Illuminate\Support\Facades\Input::get('product_name'), ['class' => 'form-control', 'multiple'=> 'multiple','id'=> 'product_name']) !!}
                                @if ($errors->has('product_name')) <p class="help-block">{{ $errors->first('product_name') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group col-md-2  @if ($errors->has('data_inicio')) has-error @endif" style="float: left">
                            <label>Data de inicio</label>
                            <div class='' id='data_inicio'>
                                <input type='text' name="data_inicio" value="{{Illuminate\Support\Facades\Input::get('data_inicio')}}" class="form-control" />
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>

                        <div class="form-group col-md-2  @if ($errors->has('data_fim')) has-error @endif" style="float: left">
                            <label>Data fim</label>
                            <div class='' id='data_fim'>
                                <input type='text' name="data_fim" value="{{Illuminate\Support\Facades\Input::get('data_fim')}}" class="form-control" />
                                <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="">
                                <div class=""  style="float: right; padding: 7px">
                                    <label></label>
                                    <input type="submit" class="form-control btn-block btn-primary" value="Visualizar">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @if(count($data))
    <div class="col-lg-12" style="">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <h5 class="text-black"> <i class="fa fa-filter"></i> Resultado</h5>
                </div>
            </div>
            <div class="card-body" style="">
                <div class="card-body">
                    <div class="row">
                    </div>
                    <div class="row">
                        <div class="col-md-4" style="">
                            <div class="form-group"><label>Resumo de Vendas</label></div>
                            <div class="card-body text-center border-success border">
                                <h3 class="text-success">{{$data['qtdVenda']}}</h3>
                                <span>Vendas Realizadas</span>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="form-group"><label class="text-light">Boletos Impressos</label></div>
                            <div class="card-body text-center border border-warning">
                                <h3 class="text-success">R$ {{App\Helpers\Helper::moneyBR($data['ticketMedio'])}} </h3>
                                <span>Ticket Médio</span>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="form-group"><label class="text-light">Boletos Impressos</label></div>
                            <div class="card-body text-center border border-info">
                                <h3 class="text-success">R$ {{App\Helpers\Helper::moneyBR($data['lucroTotal'])}}</h3>
                                <span>Lucro Total</span>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                        </table>
                    </div>
                </div>

                <div class="card-body">
                    <div class="col-md-5" style="float:right">
                        <div class="">
                            <div class="" style="">
                                <div class="form-group"><label>Tempo médio de conversão</label></div>
                                <div class="card-body text-center border border-success">
                                    <h3 class="text-success">6:12 min</h3>
                                    <span>Tempo entre o primeiro acesso na página até a compra</span>
                                </div>
                            </div>
                            <div class="" style="">
                                <div class="card-body text-center border border-success mt-5">
                                    <h3 class="text-success">11:23 min</h3>
                                    <span>Tempo médio no checkout antes de finalizar o pedido</span>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-7" style="float:left">
                        <div id="chart-area"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @else
        <h5 class="text-black text-center">Nenhum registro encontrado</h5>
    @endif
@stop
@section('scripts')
{{--    <script src="{{asset('assets/plugins/datetimepicker-jr/dependency/jquery-v2.2.2.min.js')}}"></script>--}}
    <script src="{{asset('assets/plugins/datetimepicker-jr/dependency/moment.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datetimepicker-jr/dependency/moment-pt-br.js')}}"></script>
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

    <script src="{{asset('assets/plugins/datetimepicker-jr/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('assets/plugins/tui-chart/tui-chart.js')}}"></script>
    <script>
        var container = document.getElementById('chart-area');
        var data = {
            categories: ['Browser'],
            series: {!! json_encode($pieChart)!!}
        };
        var options = {
            chart: {
                width: 500,
                height: 400,
                title: 'RESUMO DE CONVERSÃO'
            },
            tooltip: {
                suffix: '%'
            }
        };
        var theme = {
            series: {
                colors: [
                    '#83b14e', '#458a3f', '#295ba0', '#2a4175', '#289399',
                    '#289399', '#617178', '#8a9a9a', '#516f7d', '#dddddd'
                ]
            }
        };

        // For apply theme

        // tui.chart.registerTheme('myTheme', theme);
        // options.theme = 'myTheme';

        tui.chart.pieChart(container, data, options);

        $(document).ready(function() {
            var icons = {
                next:"fa fa-chevron-right",
                previous:"fa fa-chevron-left",
                date: "da da-calendar"
            }
            $('#data_inicio').datetimepicker({
                format: 'L',
                icons:icons,
                locale:'pt-br'
            });

            $('#data_inicio').focusin(function () {
                $('#data_inicio').datetimepicker('show');

            });

            $('#data_fim').focusin(function () {
                $('#data_fim').datetimepicker('show');

            })

            $('#data_fim').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'L',
                icons:icons,
                locale:'pt-br'
            });

            $("#data_inicio").on("dp.change", function (e) {
                $('#data_fim').data("DateTimePicker").minDate(e.date);
            });
            $("#data_fim").on("dp.change", function (e) {
                $('#data_inicio').data("DateTimePicker").maxDate(e.date);
            });
            $('#product_name').select2();
        });

    </script>
@stop