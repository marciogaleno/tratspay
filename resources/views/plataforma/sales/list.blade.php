@extends('plataforma.templates.template')

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-selection__choice{
            float: none !important;
            display: inline-block !important;
        }
    </style>
@endsection

@section('content')
    <div class="">
        <div class="row" style="">
            <div class="col-md-12 pull-right"  style="padding: 15px">
                {{--                <button class="btn btn-success pull-right" data-toggle="collapse" data-target="#demo"><i class="fa fa-filter"></i> Filtrar</button>--}}
                <div class="btn-group pull-right" role="group" aria-label="Basic example">
                    <a href="{{url('/sales?vendashoje=true')}}" type="button" class="btn btn-primary ">Vendas de Hoje</a>
                    <a href="{{url('/sales?vendasmes=true')}}" type="button" class="btn btn-primary ">Vendas do mês </a>
                    <a href="{{url('/sales?boletosimpressos=true')}}" type="button" class="btn btn-primary ">Boletos impressos hoje</a>
                    <a href="{{url('/sales?boletospendentes=true')}}" type="button" class="btn btn-primary ">Boletos pendentes</a>
                </div>
            </div>
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body" style="">
                        @include('plataforma.sales.filter')
                        @if(count($commissions))
                            <div class="row">
                                <div class="col-md-6 padding-l-r-zero "  style="margin-right: auto;margin-left: auto;" id="divResumo">

                                    <div class="panel panel-default"style="margin-bottom: 15px" >
                                        <div class="panel-heading " style="background-color: #f5f5f5;padding: 10px;border-top-left-radius: 3px;border-top-right-radius: 3px;">
                                            <div class="panel-title"> <i class="fa fa-tasks"></i> Resumo</div>
                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-bordered " id="tabelaVendasResumo">
                                                <thead>
                                                <tr role="row">
                                                    <th>Quantidade de Vendas</th>
                                                    <th>Valor vendido</th>
                                                    <th>Comissões Recebidas</th>
                                                </tr>
                                                </thead>

                                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                <tr class="">
                                                    <td id="totalVendas">{{$commissions->total()}}</td>
                                                    <td id="totalValorVenda">R$ {{\App\Helpers\Helper::moneyBR($totalTransactions)}}</td>
                                                    <td id="totalComissao">R$ {{\App\Helpers\Helper::moneyBR($totalComissions)}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive" style="">
                                <table class="table table-sm">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col" style="width: 5%;">Transação</th>
                                        <th scope="col">Produto</th>
                                        <th scope="col">Cliente</th>
                                        <th scope="col">Afiliados</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Data Compra</th>
                                        {{--                                    <th scope="col">Comissão</th>--}}
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @forelse ($commissions as $commission)
                                        <tr>
                                            <td><a href="{{route('sale.details', $commission->code_transaction)}}">{{$commission->code_transaction}} @if($commission->payment_type_id == 'credit_card')<i class="fa fa-credit-card"></i> @else <i class="fa fa-file-text-o"></i> @endif </a></td>
                                            <td>
                                                <a href="{{route('product.edit', $commission->product_id)}}">{{$commission->name}}</a>
                                            </td>
                                            <td>
                                                <strong>Nome: </strong>{{$commission->first_name . ' ' . $commission->last_name}}<br>
                                                <strong>Email: </strong>{{$commission->email}}<br>
                                                <strong>Telefone: </strong>{{$commission->phone ? $commission->phone : ''}}<br>
                                            </td>
                                            <td>{{App\Repository\SalesRepository::getAffiliatesSales($commission->transaction_id)}}</td>
                                            <td>{{App\Helpers\Helper::getStatusPayment($commission->status)}}</td>
                                            <td>{{\App\Helpers\Helper::dateToBr($commission->date_created, false)}}</td>
                                            {{--                                        <td>{{App\Helpers\Helper::moneyBR($commission->value)}}</td>--}}
                                            <td>
                                                <a  href="{{route('sale.details', $commission->code_transaction)}}" target="_blank"  class="btn btn-primary btn-sm"><i aria-hidden="false" style="color:#FFFFFF" class="fa fa-search"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>Nenhuma comissão encontrada</tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right">{!! $commissions->appends(request()->except('page'))->links() !!}</div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $("#cpf_cnpj").mask("999.999.999-99");
            $('#status').select2();
            $('#status_transaction').select2();
            $('#product_name').select2({
                width: 'resolve',
                language: {
                    noResults: function (params) {
                        return "Nenhum produto encontrado.";
                    }
                }
            });
            $('#affiliates').select2({
                language: {
                    noResults: function (params) {
                        return "Nenhum afiliado encontrado.";
                    }
                }
            });
        });

    </script>
@stop
