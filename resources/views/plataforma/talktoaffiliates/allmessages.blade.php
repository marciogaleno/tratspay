@extends('plataforma.templates.template')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Minhas Mensagens</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <!-- Left sidebar -->
                            <div class="col-lg-3 col-md-4">
                                <div class="card mt-3 shadow-none">
                                    <div class="list-group shadow-none">
                                        <a href="{{url('/talktoaffiliates/allmessages')}}" class="list-group-item @if(!isset($_GET['trash'])) active @endif"><i class="fa fa-inbox mr-2"></i>Inbox <b></b></a>
                                        <a href="{{url('/talktoaffiliates/allmessages?trash')}}" class="list-group-item @if(isset($_GET['trash'])) active @endif"><i class="fa fa-trash-o mr-2"></i>Lixeira&nbsp;<b></b></a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Left sidebar -->
                            <!-- Right Sidebar -->
                            <div class="col-lg-9 col-md-8">
                                <div class="row">
                                    <div class="col-lg-8">
                                        <div class="btn-toolbar" role="toolbar">
                                            <div class="btn-group mr-1">
                                                <button type="button" class="btn btn-outline-primary waves-effect waves-light" id="btnRefresh"><i class="fa fa-refresh"></i></button>
                                                <button type="button" class="btn btn-outline-primary waves-effect waves-light" id="btnTrash"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="position-relative has-icon-right">
                                            <form action="{{ route('talktoaffiliates.allmessages') }}" method="GET" class="">
                                                @csrf
                                                <input type="text" name="content" value="{{Illuminate\Support\Facades\Input::get('content')}}" class="form-control" placeholder="Buscar mensagem">
                                            </form>
                                            <div class="form-control-position">
                                                <i class="fa fa-search text-info"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- End row -->
                                <div class="card mt-3 shadow-none border border-light">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            @if(count($messages))
                                                <table class="table table-hover">
                                                    <tbody>
                                                    @foreach($messages as $message)
                                                        <tr>
                                                            <td>
                                                                <div class="mail-checkbox">
                                                                    <input id="{{$message->id}}" class="filled-in chk-col-primary" type="checkbox" checked="checked">
                                                                    <label for="checkbox1">
                                                                    </label>
                                                                </div>
                                                            </td>
                                                            <td class="mail-rateing">
{{--                                                                <form action="{{ route('talktoaffiliates.allmessages.put') }}" method="GET" class="">--}}
{{--                                                                    @csrf--}}
{{--                                                                    <button type="submit"><i class="fa fa-star" style="@if($message->favorite == 1)color:#e4ff00 @else color: grey @endif" ></i></button>--}}
{{--                                                                </form>--}}
                                                            </td>
                                                            <td>
                                                                <a href="{{url('/talktoaffiliates/view/'.$message->id)}}">{{$message->user->name}}</a>
                                                            </td>
                                                            <td style=" white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 254px;">
                                                                <a href="{{url('/talktoaffiliates/view/'.$message->id)}}" class="text-left" ><i class="fa fa-circle text-info mr-1"></i>{{strip_tags($message->content)}}</a></td>
    {{--                                                        <td>--}}
    {{--                                                            <i class="fa fa-paperclip"></i>--}}
    {{--                                                        </td>--}}
                                                            <td class="text-right"> 08:23 AM </td>
                                                        </tr>
                                                    @endforeach

                                                    </tbody>
                                                </table>
                                                @else
                                                <h5 class=" text-center page-title">Nenhuma mensagem</h5>
                                            @endif
                                        </div>
                                        <hr>
                                        <div class="row">

                                            <div class="col-5">
                                                <div class="btn-group float-right">
                                                    {!! $messages->links() !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- card body -->
                                </div> <!-- card -->
                            </div> <!-- end Col-9 -->
                        </div><!-- End row -->
                    </div>
                </div>
            </div>
        </div><!-- End row -->
    </div>
@stop
@section('scripts')
    <script>
       $('#btnTrash').click(function () {
           var itens=[]

           $('input[type="checkbox"]').each(function() {
               if ($(this).is(":checked")) {
                   itens.push($(this).attr('id'))

               }
           });
           var data = {
               "_token": "{{ csrf_token() }}",
               "ids": itens
           }
           var url = '{{url('/talktoaffiliates/allmessages/edit')}}'
           $.ajaxSetup({
               headers: {
                   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
               }
           });
           $.ajax({
               url: url,
               type: 'PUT',
               data: data,
               success: function(result) {
                   window.location.reload();
               },
               error: function(result) {
                    alert('erro ao excluir mensagens')
               }
           });
       })

       $('#btnRefresh').click(function () {
           window.location.reload();
       });
    </script>
@stop