@extends('plataforma.templates.template')
@section('styles')
    <link href="{{asset('assets/plugins/summernote/dist/summernote-bs4.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-selection__choice{
            float: none !important;
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Fale com os seus afiliados</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>

        <!-- End Breadcrumb-->
        {!! Form::open(['route' => 'talktoaffiliates.send', 'method' => 'post', 'enctype' => 'multipart/form-data', 'onsubmit', 'return postForm()']) !!}
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-uppercase">Primeiro selecione o produto</div>
                    <div class="card-body">
                        <div class="col-md-12">
                           <div class="col-lg-9" style="float:left">
                                <div class="form-group">
                                    {!! Form::select('product_id[]',$productsFilter, Illuminate\Support\Facades\Input::get('product_id'), ['class' => 'form-control', 'multiple'=> 'multiple','id'=> 'product_id']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3" style="float:left"><button class="btn btn-success btn-block" id="btnSelecionar" type="button">Selecionar</button></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="divSummerNote" style="display: none">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header text-uppercase bg-white">Digite aqui sua mensagem</div>
                        <div class="card-body">
                            <p class="container">
                                <textarea class="input-block-level" id="summernote" name="content" rows="18">
                            </textarea>
                            </p>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
                <div class="col-md-3"></div>
                <div class="col-md-3"><button type="submit" class="btn btn-success btn-lg">enviar mensagem</button></div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/summernote/dist/summernote-bs4.min.js')}}"></script>
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 300
        });

        $("#btnSelecionar").click(function () {
            $('#divSummerNote').css('display', 'block');
        })

        $('#product_id').change(function () {
            if($(this).val() == null){
                $('#divSummerNote').css('display', 'none');
            }
        })

        $('#product_id').select2({ width: 'resolve', placeholder:"Selecione os produtos"});
    });
    var postForm = function() {
        var content = $('textarea[name="content"]').html($('#summernote').code());
    }
</script>
@stop