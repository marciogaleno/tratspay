@extends('plataforma.templates.template')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header text-uppercase">Detalhes do vendedor:</div>
                <div class="card-body" style="padding: 0px">
                    <div class="">
                        <table class="table table-striped" style="font-size: 14px;">
                            <thead>
                            <tr>
                                <th>Vendedor</th>
                                <th>Email</th>
                                <th>Data</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><img class="mr-3 side-user-img" src="{!!\App\Helpers\Helper::getPathFileS3('images', $message->user->image) !!}" alt="user avatar" style="max-width: 42px;border-radius: 50%;"> {{$message->user->name}}</td>
                                <td>{{$message->user->email}}</td>
                                <td>{{$message->created_at->format('d/m/Y')}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header text-uppercase">Mensagem:</div>
                <div class="card-body">
                    <div class="col-md-12">

                        {!! $message->content !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection