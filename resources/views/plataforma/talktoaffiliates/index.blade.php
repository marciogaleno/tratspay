@extends('plataforma.templates.template')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Falar com os Afiliados</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <!--End Row-->
        <!--End Row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-uppercase">Você tem se comunicado?</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <p class="text-left"><b>O que é este recurso?</b></p>
                                <p class="text-left">O recurso "Falar com Afiliados" permite que você envie uma mensagem para todos os seus afiliados em busca de melhor comunicação.</p>
                            </div>
                            <div class="col-lg-4">
                                <p class="text-center"><b>Como usar?</b></p>
                                <p class="text-center">Nós recomendamos que você envie mensagens para os seus afiliados quando decidir fazer alguma alteração em seu produto, como desconto, novas páginas de vendas, etc.</p>
                            </div>
                            <div class="col-lg-4">
                                <p class="text-right"><b>É um serviço pago?</b>
                                </p><p class="text-right">Este serviço é oferecido de forma gratuita pela TratsPay e você pode utilizar sem nenhuma preocupação ou custos adicionais.&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Row-->
        <!--End Row-->
        <div class="row mt-30">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body"><a href="{{url('/talktoaffiliates/write')}}"><button type="button" class="btn btn-success waves-effect waves-light btn-lg btn-block">CLIQUE AQUI PARA ENVIAR UMA MENSAGEM PARA OS SEUS AFILIADOS</button> <div>
                            </div>
                        </a></div><a href="{{url('/talktoaffiliates/write')}}">
                    </a></div><a href="{{url('/talktoaffiliates/write')}}">
                    <!--End Row-->
                    <!--End Row-->
                </a></div><a href="{{url('/talktoaffiliates/write')}}">
                <!-- End container-fluid-->
            </a></div><a href="{{url('/talktoaffiliates/write')}}">
            <!--End content-wrapper-->
            <!--Start Back To Top Button-->
        </a><a href="javaScript:void();" class="back-to-top" style=""><i class="fa fa-angle-double-up"></i> </a>
        <!--End Back To Top Button-->
        <!--Start footer-->
        <!--End footer-->
    </div>
@stop