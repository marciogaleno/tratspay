<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ColinaPay - Checkout </title>
    <!-- Responsivo -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">

    <!-- Bootstrap core CSS-->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <!-- Bootstrap Grid-->
    <link href="{{asset('assets/css/bootstrap-grid.css')}}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{asset('assets/css/default.css')}}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{asset('assets/css/custom.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.3.1/css/all.min.css" rel="stylesheet"/>

    <!-- Icons CSS-->
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />


{{--    @if($checkout->template == 2)--}}
{{--        <link href="{{asset('css/checkout_template2.css')}}" rel="stylesheet"/>--}}
{{--        <link href="{{asset('assets/css/bootstrapValidator.css')}}" rel="stylesheet"/>--}}
{{--        <link href="{{asset('assets/css/all.css')}}" rel="stylesheet"/>--}}
{{--        <link href="{{asset('assets/css/woff2.css')}}" rel="stylesheet"/>--}}

{{--    @endif--}}
    <link href="{{asset('css/checkout_template2.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/bootstrapValidator.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/all.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/woff2.css')}}" rel="stylesheet"/>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    -->
    <script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>

    @if(env('GERENCIANET_PRODUCAO'))
        <script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://api.gerencianet.com.br/v1/cdn/08498def484633a5c3f060fda9983847/'+v;s.async=false;s.id='08498def484633a5c3f060fda9983847';if(!document.getElementById('08498def484633a5c3f060fda9983847')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
    @else
        <script type='text/javascript'>var s=document.createElement('script');s.type='text/javascript';var v=parseInt(Math.random()*1000000);s.src='https://sandbox.gerencianet.com.br/v1/cdn/08498def484633a5c3f060fda9983847/'+v;s.async=false;s.id='08498def484633a5c3f060fda9983847';if(!document.getElementById('08498def484633a5c3f060fda9983847')){document.getElementsByTagName('head')[0].appendChild(s);};$gn={validForm:true,processed:false,done:{},ready:function(fn){$gn.done=fn;}};</script>
    @endif

    <script>
        Mercadopago.setPublishableKey("{{config('mercadopago.credencial.public_key')}}");
        Mercadopago.getIdentificationTypes();
    </script>

</head>
<body>

@yield('content')

<!-- jQuery 3 -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/tether.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>

<script src="{{asset('js/plataforma/bootstrapValidator.js')}}"></script>

<!-- CDN MASKINPUT-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

@yield('script-dashboard')
@yield('scripts')

<script>

    setInterval(function time() {
        var d = new Date();
        var hours = 48 - d.getHours();
        var min = 60 - d.getMinutes();
        if ((min + '').length == 1) {
            min = '0' + min;
        }
        var sec = 60 - d.getSeconds();
        if ((sec + '').length == 1) {
            sec = '0' + sec;
        }
        jQuery('#the-final-countdown span').html(hours + ':' + min + ':' + sec)
    }, 1000);


    $(document).ready(function () {
        $(".phone").mask("(99) 999999999");
        $(".date").mask("00/00/0000");
        $(".cep").mask("00.000-000");
        $(".uf").mask("AA");
        $(".number10").mask("9999999999");
    });
</script>

@stack('scripts')

</body>
</html>
