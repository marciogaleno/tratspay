<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ColinaPay - Painel</title>
    <!-- Responsivo -->
    <meta name="viewport" content="width=device-width, minimum-scale=1, maximum-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
    <!-- notifications css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/notifications/css/lobibox.min.css')}}"/>
    <!-- Vector CSS -->
    <link href="{{asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet"/>
    <!-- simplebar CSS-->
    <link href="{{asset('assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet"/>
    <!-- Bootstrap core CSS-->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <!-- animate CSS-->
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Icons CSS-->
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <!-- Sidebar CSS-->
    <link href="{{asset('assets/css/sidebar-menu.css')}}" rel="stylesheet"/>
    <!-- Custom Style-->
    <link href="{{asset('assets/css/app-style.css')}}" rel="stylesheet"/>
    <!--Data Tables -->
    <link href="{{asset('assets/plugins/bootstrap-datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/bootstrap-datatable/css/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css">
    <!--Select Plugins-->
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <!--multi select-->
    <link href="{{asset('assets/plugins/jquery-multi-select/multi-select.css')}}" rel="stylesheet" type="text/css">
    <!-- bootstrap-datepicker -->
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet" type="text/css">

@yield('css-dashboard')
@yield('css-cadastro-produto')
@yield('styles')
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>



    <![endif]-->
    <style>
        .loader {
            border: 10px solid #f3f3f3; /* Light grey */
            border-top: 10px solid #3498db; /* Blue */
            border-radius: 50%;
            width: 100px;
            height: 100px;
            animation: spin 1s linear infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        .table, .small, small {
            font-size: 0.8rem;
            font-weight: 400;
        }

        .table thead th {
            font-size: 0.7rem;
        }

        .card .table td, .card .table th {
            padding-right: 0.5rem;
            padding-left: 0.5rem;
        }

        .btn-sm {
            padding: 4px 10px;
        }

    </style>

</head>

<body>
<!-- Start wrapper-->
<div id="wrapper">
    <!--Start sidebar-wrapper-->
    <div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
        <div class="brand-logo">
            <a href="{{url('/')}}">
                <p> <center> <img src="{{asset('assets/images/logo-trats-mini.png')}}" >
            </a>
        </div>
        <div class="user-details">
            <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">
                <div class="avatar">
                    @if (isset(Auth::user()->image) && Auth::user()->image)
                        <img class="mr-3 side-user-img" src="{!!\App\Helpers\Helper::getPathFileS3('images', Auth::user()->image) !!}" alt="user avatar">
                    @else
                        <img class="mr-3 side-user-img" src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png" alt="user avatar">
                    @endif

                </div>
                <div class="media-body">
                    <h6 style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 145px;" class="side-user-name">{{App\Helpers\Helper::splitname(Auth::user()->name)}}</h6>
                </div>
            </div>
            <div id="user-dropdown" class="collapse">
                <ul class="user-setting-menu">
                    <li><a href="{{route('user.myprofile')}}"><i class="icon-user"></i>  Meu Perfil</a></li>
                    <li><a  target="_blank" href="http://suporte.tratspay.com.br"><i class="fa fa-gears"></i><span>Suporte</span>

                        </a>
                    </li>

                    <li><a href="{{route('cashout.index')}}"><i class="icon-settings"></i> Saques</a></li>
                    <li><a href="{{url('/logout')}}"><i class="icon-power"></i> Logout</a></li>
                </ul>
            </div>
        </div>


        {{-- ITENS MENU --}}
        <ul class="sidebar-menu do-nicescrol">

            @if(Auth::user()->TypeAccountadmin)
            <li>
                <a href="#" class="waves-effect">
                    <i class="fa fa-university"></i>
                    <span>Central</span>
                    <i class="fa fa-angle-left float-right123
"></i>
                </a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('admin.index')}}">Dados da Plataforma</a></li>
                    <li><a href="{{route('admin.cashout.index')}}">Solicitações de Saques</a></li>
                    <li><a href="{{route('admin.reembolso.index')}}">Solicitações de Reembolsos</a></li>
                    <li><a href="{{route('admin.type.account.user.index')}}">Todos os usuários</a></li>
                </ul>
            </li>
            @endif
            <li>
                <a href="#" class="waves-effect">
                    <i class="icon-home"></i>
                    <span>Comece Aqui</span>
                    <i class="fa fa-angle-left float-right"></i>
                </a>
                <ul class="sidebar-submenu">
                    <li><a href="{{url('/')}}"></i>Home</a></li>
                    <li><a href="{{ route('client.edit') }}"></i>Dados Cadastrais</a></li>
                    <li><a href="{{route('purchase.list')}}"></i>Compras</a></li>
                    @if (Auth::user()->accept_terms)
                        <li><a href="{{route('cashout.index')}}"></i>Saques</a></li>
                    @endif
                </ul>
            </li>

            @if (Auth::user()->accept_terms)
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-diamond"></i><span>Produtos</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="{{route('product.add')}}">Criar Novo Produto</a></li>
                        <li><a href="{{route('product.list')}}">Todos os Produtos</a></li>
                        <li><a href="{{route('coproducer.received')}}">Co-Produções</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="fa fa-money"></i><span>Vendas</span>
                        <i class="fa fa-angle-left float-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="{{route('sale.list')}}">Todas as Vendas</a></li>
                        <li><a href="#">Vendas por Cliente</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-pie-chart"></i><span>Relatórios</span>
                        <i class="fa fa-angle-left float-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="{{url('/sales/detail')}}">Detalhamento de Vendas</a></li>
                        <li><a href="relatorio-assinaturas.html">Assinaturas</a></li>
                        <li><a href="#">Origem de Venda</a></li>
                        <li><a href="#">Teste A/B</a></li>
                        <li><a href="#">Vendas por Produto</a></li>
                        <li><a href="#">Lucros em Co-Produção</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="waves-effect">
                        <i class="icon-magnet"></i>
                        <span>Ferramentas</span>
                        <i class="fa fa-angle-left float-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="#">Nota Fiscal</a></li>
                        <li><a href="{{route('postback.index')}}">Postback</a></li>
                        <li><a href="#">API</a></li>
                        <li><a href="{{route('tool.leadAutomator')}}">Automatização de Leads</a></li>
                        <li><a href="{{route('sale.tracking')}}">Trácking de Tráfego</a></li>
                        <li><a href="{{route('tool.linkManager')}}">Gerenciador de Links</a></li>
                    </ul>
                </li>

            @endif
        </ul>
    </div>


    <!--Fim de Menu da sidebar-->

    <!--Start topbar header-->
    <header class="topbar-nav">
        <nav class="navbar navbar-expand fixed-top bg-dark">
            <ul class="navbar-nav mr-auto align-items-center">
                <li class="nav-item">
                    <a class="nav-link toggle-menu" href="javascript:void();">
                        <i class="icon-menu menu-icon"></i>
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center right-nav-link">
{{--                <li class="nav-item dropdown-lg">--}}
{{--                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">--}}
{{--                        <i class="icon-envelope-open"></i>--}}
{{--                        <span class="badge badge-primary  badge-up" id="badgeCountMessages"></span>--}}
{{--                    </a>--}}
{{--                    <div class="dropdown-menu dropdown-menu-right animated fadeIn">--}}
{{--                        <ul class="list-group list-group-flush">--}}
{{--                           <div id="countMessages"></div>--}}
{{--                            <div id="messages"></div>--}}
{{--                            <li class="list-group-item">--}}
{{--                                <a href="javaScript:void();">--}}
{{--                                    <div class="media">--}}
{{--                                        <div class="avatar"><img class="align-self-start mr-3" src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png" alt="user avatar"></div>--}}
{{--                                        <div class="media-body">--}}
{{--                                            <h6 class="mt-0 msg-title">Jhon Deo</h6>--}}
{{--                                            <p class="msg-info">Lorem ipsum dolor sit amet...</p>--}}
{{--                                            <small>Today, 4:10 PM</small>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </li>--}}


{{--                            <li class="list-group-item"><a href="{{url('/talktoaffiliates/allmessages')}}">Ver todas as mensagens</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </li>--}}
                <li class="nav-item dropdown-lg">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret waves-effect" data-toggle="dropdown" href="javascript:void();">
                        <i class="fa fa-money"></i><span class="badge badge-primary badge-up" id="badgeCountNotifications"></span></a>
                    <div class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <ul class="list-group list-group-flush">
                            <div id="countNotifications"></div>
                            <div id="notifications"></div>

                            <li class="list-group-item"><a href="{{url('/notifications')}}">Ver todas as notificações</a></li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                        <span class="user-profile">


                            @if (isset(Auth::user()->image) && Auth::user()->image)
                                <img src="{!!\App\Helpers\Helper::getPathFileS3('images', Auth::user()->image) !!}" class="img-circle" alt="user avatar">
                            @else
                                <img src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png" class="img-circle" alt="user avatar">
                            @endif

                        </span>

                    </a>
                    <ul class="dropdown-menu dropdown-menu-right animated fadeIn">
                        <li class="dropdown-item user-details">
                            <a href="javaScript:void();">
                                <div class="media">
                                    <div class="avatar">

                                        @if (isset(Auth::user()->image) && Auth::user()->image)
                                            <img class="align-self-start mr-3" src="{!! \App\Helpers\Helper::getPathFileS3('images', Auth::user()->image) !!}" alt="user avatar">
                                        @else
                                            <img class="align-self-start mr-3" src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png" alt="user avatar">
                                        @endif


                                    </div>
                                    <div class="media-body">
                                        <h6 class="mt-2 user-title">{{App\Helpers\Helper::splitname(Auth::user()->name)}}</h6>
                                        <p class="user-subtitle">{{Auth::user()->email}}</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="dropdown-divider"></li>
                        <li class="dropdown-item"><i class="icon-power mr-2"></i>
                            <a href="{{url('logout')}}">Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <!--End topbar header-->

    <div class="clearfix"></div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="container-fluid">

{{--                <div class="col-md-12 text-right">--}}
{{--                    <a href="{{route('indication.index')}}"><span class="badge badge-success m-1" style="font-size:12px; padding: 7px"><i class="fa fa-money"></i> Indique e Ganhe</span></a>--}}
{{--                </div>--}}

            <!--<div class="row pt-2 pb-2">
                <div class="col-sm-9">
                    <h4 class="page-title">Simple Tables</h4>
                    <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javaScript:void();">Fobia</a></li>
                    <li class="breadcrumb-item"><a href="javaScript:void();">Tables</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Simple Tables</li>
                 </ol>
               </div>
             <div class="col-sm-3">
               <div class="btn-group float-sm-right">
                <button type="button" class="btn btn-outline-primary waves-effect waves-light"><i class="fa fa-cog mr-1"></i> Setting</button>
                <button type="button" class="btn btn-outline-primary dropdown-toggle dropdown-toggle-split waves-effect waves-light" data-toggle="dropdown">
                <span class="caret"></span>
                </button>
                <div class="dropdown-menu">
                  <a href="javaScript:void();" class="dropdown-item">Action</a>
                  <a href="javaScript:void();" class="dropdown-item">Another action</a>
                  <a href="javaScript:void();" class="dropdown-item">Something else here</a>
                  <div class="dropdown-divider"></div>
                  <a href="javaScript:void();" class="dropdown-item">Separated link</a>
                </div>
              </div>
             </div>
             </div>-->




                @if ($errors->any())
                    <div class="alert alert-light-danger alert-dismissible" role="alert">
                        <div class="alert-icon">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span>Ocorreu um erro ao Salvar. Por favor, verificar os dados informados.</span>
                        </div>
                    </div>

                    <div class="alert alert-icon-danger alert-dismissible" role="alert">
                        <div class="alert-icon icon-part-danger">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                         <span>
                           <ul>
                            @foreach ($errors->all() as $error)
                                   <li>{{ $error }}</li>
                               @endforeach
                           </ul>
                         </span>
                        </div>
                    </div>

                @endif

                @if(Session::has('success'))
                    <div class="alert alert-light-success alert-dismissible" role="alert">
                        <div class="alert-icon contrast-alert">
                            <i class="icon-check"></i>
                        </div>
                        <div class="alert-message">
                            <span>{{Session::get('success')}}</span>
                        </div>
                    </div>
                @endif
                @if(Session::has('error'))

                    <div class="alert alert-light-danger alert-dismissible" role="alert">
                        <div class="alert-icon">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span> {{Session::get('error')}}</span>
                        </div>
                    </div>

                @endif

                <div class="row pt-2 pb-2">
                    <div class="col-sm-8">
                        <h4 class="page-title">{{isset($titlePage) && !empty($titlePage) ? $titlePage : ''}}</h4>
                    </div>
                    @if (Route::current()->getName() && isset($totalCashouts))
                        <div class="col-sm-4  text-right">
                            <h6> Você já ganhou <span class="text-success">R$ {{number_format($totalCashouts, 2, ",", ".")}} </span> na ColinaPay!</h6>
                        </div>
                    @endif
                </div>

                @yield('content')

        </div>
    </div>

</div>

<!-- /.content-wrapper -->

<div class="modal fade" id="spinnerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <div class="modal-content">
                <div class="modal-body">

                    <div class="loader"></div>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

<!-- simplebar js -->
<script src="{{asset('assets/plugins/simplebar/js/simplebar.js')}}"></script>
<!-- waves effect js -->
<script src="{{asset('assets/js/waves.js')}}"></script>
<!-- sidebar-menu js -->
<script src="{{asset('assets/js/sidebar-menu.js')}}"></script>
<!-- Custom scripts -->
<script src="{{asset('assets/js/app-script.js')}}"></script>

<!--Data Tables js-->
<script src="{{asset('assets/plugins/bootstrap-datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datatable/js/buttons.colVis.min.js')}}"></script>

<!--Datapicker-->
<script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script charset="UTF-8" src="{{asset('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>

<!-- Chart JS -->
<script src="{{asset('assets/plugins/Chart.js/Chart.min.js')}}"></script>
<script src="{{asset('assets/plugins/Chart.js/chartjs-script.js')}}"></script>

<!--Select Plugins Js-->
<script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>

<!--Multi Select Js-->
<script src="{{asset('assets/plugins/jquery-multi-select/jquery.multi-select.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-multi-select/jquery.quicksearch.js')}}"></script>

<!--notification js -->
<script src="{{asset('assets/plugins/notifications/js/lobibox.min.js')}}"></script>
<script src="{{asset('assets/plugins/notifications/js/notifications.min.js')}}"></script>
<!-- CDN MASKINPUT-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>

<script src="{{asset('assets/js/jquery.preloaders.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>


<script type="text/javascript">
    // SESSION MESSAGES
    @if(\request()->session()->has('success'))
    setTimeout(function() {
        toaste.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.success("{{ request()->session()->get('success') }}", 'Sucesso!');
    }, 100);
    @endif

    @if(\request()->session()->has('successForm'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.success("{{ request()->session()->get('successForm') }}", 'Sucesso!');
    }, 100);
    @endif

    @if(\request()->session()->has('error'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.error("{{ request()->session()->get('error') }}", 'Erro!');
    }, 100);
    @endif

    @if(\request()->session()->has('danger'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.error("{{ request()->session()->get('danger') }}", 'Erro!');
    }, 100);
    @endif

    @if(\request()->session()->has('info'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.info("{{ request()->session()->get('info') }}", 'Info!');
    }, 100);
    @endif

    @if(\request()->session()->has('warning'))
    setTimeout(function() {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 10000
        };
        toastr.warning("{{ request()->session()->get('warning') }}", 'Erro!');
    }, 100);
    @endif

    //messages header
    bucket = '{{env('AWS_BUCKET_IMAGES')}}';
    var url= '';

    if (bucket == 'tratspay-images') {
        url = 'https://tratspay-images.s3-sa-east-1.amazonaws.com/';
    } else {
        url = 'https://tratspay-images-test.s3-sa-east-1.amazonaws.com/';
    }
    //https://image.flaticon.com/icons/svg/145/145867.svg
    $.get("/getmessages", function(resultado){
        if(resultado.length == 0) {
            var text = "<li class='list-group-item'>" +
                "<a href='javaScript:void();'>" +
                "<div class='media'>" +
                "<div class='media-body'>" +
                "<h6 class='mt-0 msg-title' style='padding: 10px;font-size: 13px;color: gray;'>Nenhuma mensagem encontrada</h6>" +
                "</div>" +
                "</div>" +
                "</a>" +
                "</li>"

            $("#messages").html(text);
            return;
        }

        var totalmessage = "<li class='list-group-item d-flex justify-content-between align-items-center'>" +
        "Você possui "+ resultado.length +" mensagens" +
        "<span class='badge badge-danger'>"+resultado.length+"</span></li>";

        $("#countMessages").html(totalmessage);
        $("#badgeCountMessages").html(resultado.length);

        for (let i = 0; i < resultado.length; ++i) {
            var urlBase = '{{url('/talktoaffiliates/view/')}}';

            var text = "<li class='list-group-item'>" +
                "<a href='"+urlBase+'/'+resultado[i].id +"'>" +
                "<div class='media'>" +
                "<div class='avatar'><img class='align-self-start mr-3' src='"+url+resultado[i].image+"' alt='user avatar'></div>" +
                "<div class='media-body'>" +
                "<h6 class='mt-0 msg-title' style=' white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 190px;'>"+resultado[i].name+"</h6>" +
                "<p class='msg-info' style=' white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 190px;'>"+jQuery(resultado[i].content).text()+"</p>" +
                "</div>" +
                "</div>" +
                "</a>" +
                "</li>"
            $('#messages').append(text);
        }


        // $("#mensagem").html(resultado);
    })

    //----------NOTIFICATIONS-----------
    $.get("/getnotifications", function(resultado){
        if(resultado.length == 0) {
            var text = "<li class='list-group-item'>" +
                "<a href='javaScript:void();'>" +
                "<div class='media'>" +
                "<div class='media-body'>" +
                "<h6 class='mt-0 msg-title' style='padding: 10px;font-size: 13px;color: gray;'>Nenhuma notificação encontrada</h6>" +
                "</div>" +
                "</div>" +
                "</a>" +
                "</li>"

            $("#notifications").html(text);
            return;
        }

        var totalmessage = "<li class='list-group-item d-flex justify-content-between align-items-center'>" +
            "Você possui "+ resultado.length +" notificações" +
            "<span class='badge badge-primary'>"+resultado.length+"</span></li>";

        $("#countNotifications").html(totalmessage);
        $("#badgeCountNotifications").html(resultado.length);

        for (let i = 0; i < resultado.length; ++i) {
            var urlBase = '{{url('/sales')}}';
            var icon = resultado[i].type == 'sale' ? 'fa fa-money fa-2x mr-3 text-success' : 'icon-bell fa-2x mr-3 text-danger';
            var type = resultado[i].type == 'sale' ? 'Nova venda realizada' : 'Novo reembolso solicitado';


            var text = "<li class='list-group-item'>" +
            "<a href='"+urlBase+"'>" +
            "<div class='media'>" +
            "<i class='"+icon+"'></i>" +
            "<div class='media-body'>" +
            "<h6 class='mt-0 msg-title'>"+type+"</h6>" +
            "<p class='msg-info' style=' white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 190px;'>"+resultado[i].content+"</p>" +
            "</div>" +
            "</div>" +
            "</a>" +
            " </li>";

            $('#notifications').append(text);
        }
    })
</script>

@yield('script-dashboard')
@yield('scripts')

<script>
    $(document).ready(function () {
        $(".phone").mask("(99) 999999999");
        $(".date").mask("00/00/0000");
        $(".cep").mask("00.000-000");
        $(".uf").mask("AA");
        $(".number10").mask("9999999999");
    });
</script>
@stack('scripts')

<!--<script src="{{ asset('js/app.js') }}"></script>-->

</body>

<!-- Mirrored from uxliner.com/niche/main/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 03:38:50 GMT -->

</html>
