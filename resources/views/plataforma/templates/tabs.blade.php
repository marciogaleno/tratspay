@extends('plataforma.templates.template') 

@section('content')

<div class="row">
                
    {!! \App\Helpers\TabsHelper::openBoxTabs() !!}
    <!-- Nav tabs -->
    {!! \App\Helpers\TabsHelper::openHeader($numberTab)!!}
    {!! \App\Helpers\TabsHelper::openContent() !!}
    <!-- Tab panes -->

    @yield('contentTab')

    {!! \App\Helpers\TabsHelper::closeContent() !!}
    {!! \App\Helpers\TabsHelper::closeHeader() !!}
    {!! \App\Helpers\TabsHelper::closeBoxTabs() !!}
</div>

@endsection
