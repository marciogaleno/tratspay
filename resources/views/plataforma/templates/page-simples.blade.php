<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Fobia - Bootstrap 4 Web Application UI Kit</title>
    <!--favicon-->
    <link rel="icon" href="{{asset('assets/images/favicon.ico')}}" type="image/x-icon">
    <!-- simplebar CSS-->
    <link href="{{asset('assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
    <!-- Bootstrap core CSS-->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" />
    <!-- animate CSS-->
    <link href="{{asset('assets/css/animate.css')}}" rel="stylesheet" type="text/css" />
    <!-- Icons CSS-->
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <!-- Sidebar CSS-->
    <link href="{{asset('assets/css/sidebar-menu.css')}}" rel="stylesheet" />
    <!-- Custom Style-->
    <link href="{{asset('assets/css/app-style.css')}}" rel="stylesheet" />

</head>

<body>

    <!-- Start wrapper-->
    <div id="wrapper">

        <div class="container">
            <div class="row">
                <div class="col-md-12" style="margin-top: 20px">
                    <div class="text-center">
                        <a href="{{url('/')}}">
                            <p>
                                <center> <img src="{{asset('assets/images/logo-trats-mini.png')}}" width="250">
                        </a>
                    </div>
                    <div class="text-center" style="margin-top: 20px">
                        <h4 class="text-black">Afilie-se e lucre!</h4>

                        <h6 class="text-black">Você foi convidado por {{$producer->name}} Pereira para se tornar afiliado
                            ao produto {{$product->name}}.</h6>
                        <h6 class="text-black">Após se afiliar, você receberá um link de afiliado e poderá ganhar
                            dinheiro vendendo este produto pela internet!</h6>
                    </div>

                    @if ($errors->any())
                    <div class="alert alert-light-danger alert-dismissible" role="alert">
                        <div class="alert-icon">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span>Ocorreu um erro ao Salvar. Por favor, verificar os dados informados.</span>
                        </div>
                    </div>

                    <div class="alert alert-icon-danger alert-dismissible" role="alert">
                        <div class="alert-icon icon-part-danger">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </span>
                        </div>
                    </div>

                    @endif

                    @if(Session::has('success'))
                    <div class="alert alert-light-success alert-dismissible" role="alert">
                        <div class="alert-icon contrast-alert">
                            <i class="icon-check"></i>
                        </div>
                        <div class="alert-message">
                            <span>{{Session::get('success')}}</span>
                        </div>
                    </div>
                    @endif
                    @if(Session::has('error'))

                    <div class="alert alert-light-danger alert-dismissible" role="alert">
                        <div class="alert-icon">
                            <i class="icon-close"></i>
                        </div>
                        <div class="alert-message">
                            <span> {{Session::get('error')}}</span>
                        </div>
                    </div>

                    @endif
                </div>
            </div>

            <div class="card">

                <div class="card-body">
                    <!-- Content Header (Page header) -->
                    @yield('content')
                </div>
            </div>
        </div>

    </div>
    <!--wrapper-->


    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>

    <!-- simplebar js -->
    <script src="{{asset('assets/plugins/simplebar/js/simplebar.js')}}"></script>
    <!-- waves effect js -->
    <script src="{{asset('assets/js/waves.js')}}"></script>
    <!-- sidebar-menu js -->
    <script src="{{asset('assets/js/sidebar-menu.js')}}"></script>
    <!-- Custom scripts -->
    <script src="{{asset('assets/js/app-script.js')}}"></script>

</body>

</html>