<header class="main-header">
    <!-- Logo -->
    <a href="{{route('dashboard')}}" class="logo black-bg">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="{{ asset('dist/img/logo-trats-mini.png')}}" alt=""></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{ asset('dist/img/logo-trats-mini.png')}}" alt=""></span> </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar blue-bg navbar-static-top">
        <!-- Sidebar toggle button-->
        <ul class="nav navbar-nav pull-left">
            <li><a class="sidebar-toggle" data-toggle="push-menu" href="#"></a></li>
        </ul>

        <!-- Barra de Navegação-->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Estilo do CSS de menu de dropdhow está em  dropdown.less-->
                <li class="dropdown messages-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i
                                class="fa fa-envelope-o"></i>
                        <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Você tem 4 novas mensagens</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="{{ asset('dist/img/img1.jpg')}}"
                                                                    class="img-circle" alt="User Image"> <span
                                                    class="profile-status online pull-right"></span></div>
                                        <h4>Alex C. Patton</h4>
                                        <p>Aviso para todos os afiliados</p>
                                        <p><span class="time">9:30 AM</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="dist/img/img3.jpg" class="img-circle"
                                                                    alt="User Image"> <span
                                                    class="profile-status offline pull-right"></span></div>
                                        <h4>Nikolaj S. Henriksen</h4>
                                        <p>Lançamos uma nova versão do...</p>
                                        <p><span class="time">10:15 AM</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="dist/img/img2.jpg" class="img-circle"
                                                                    alt="User Image"> <span
                                                    class="profile-status away pull-right"></span></div>
                                        <h4>Kasper S. Jessen</h4>
                                        <p>Você precisa de ajuda para promover o </p>
                                        <p><span class="time">8:45 AM</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left"><img src="dist/img/img4.jpg" class="img-circle"
                                                                    alt="User Image"> <span
                                                    class="profile-status busy pull-right"></span></div>
                                        <h4>Florence S. Kasper</h4>
                                        <p>O que você precisa para...</p>
                                        <p><span class="time">12:15 AM</span></p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="mensagens.html">Ver todas as mensagens</a></li>
                    </ul>
                </li>
                <!-- O CSS de notificação se encontra em  dropdown.less -->
                <li class="dropdown messages-menu"><a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i
                                class="fa fa-bell-o"></i>
                        <div class="notify"><span class="heartbit"></span> <span class="point"></span></div>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">Notificações</li>
                        <li>
                            <ul class="menu">
                                <li>
                                    <a href="#">
                                        <div class="pull-left icon-circle red"><i class="icon-lightbulb"></i></div>
                                        <h4>Alex C. Patton</h4>
                                        <p>Aprovou a sua afiliação em...</p>
                                        <p><span class="time">9:30 AM</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left icon-circle blue"><i class="fa fa-coffee"></i></div>
                                        <h4>Nikolaj S. Henriksen</h4>
                                        <p>Gerou um novo boleto em....</p>
                                        <p><span class="time">1:30 AM</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left icon-circle green"><i class="fa fa-paperclip"></i></div>
                                        <h4>Kasper S. Jessen</h4>
                                        <p>Comprou o Produto...</p>
                                        <p><span class="time">9:30 AM</span></p>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="pull-left icon-circle yellow"><i class="fa  fa-plane"></i></div>
                                        <h4>Florence S. Kasper</h4>
                                        <p>Solicitou o reembolso em</p>
                                        <p><span class="time">11:10 AM</span></p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer"><a href="notificacoes.html">Ver todas as notificações</a></li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu p-ph-res"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{!! \App\Helpers\Helper::imageProfile() !!}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{Auth::user()->name}}</span> </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <div class="pull-left user-img"><img src="{!! \App\Helpers\Helper::imageProfile() !!}"
                                                                 class="img-responsive" alt="User"></div>
                            <p class="text-left">{{Auth::user()->name}}
                                <small>{{Auth::user()->name}}</small>
                            </p>

                        </li>
                        <li><a href="{{ route('client.perfil') }}"><i class="icon-profile-male"></i> Meu Perfil</a></li>

                        <li role="separator" class="divider"></li>
                        <li><a href="{{ route('client.edit') }}"><i class="icon-gears"></i> Dados Cadastrais</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{url('logout')}}"><i class="fa fa-power-off"></i> Sair da Conta</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
