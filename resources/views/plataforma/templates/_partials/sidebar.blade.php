<aside class="main-sidebar">
    <div class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="image text-center"><img src="{!! \App\Helpers\Helper::imageProfile() !!}" class="img-circle"
                alt="User Image"></div>
            <div class="info">
                <p>{{{Auth::user()->name}}}</p>

            </div>

            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">PESSOAL</li>
                <li class="active treeview"><a href="index.html"> <i class="fa fa-home"></i> <span>Comece Aqui</span>
                        <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="index.html">Home</a></li>
                        <li><a href="indicar-afiliados.html">Indicar Afiliados</a></li>
                        <li><a href="dados-cadastrais.html">Dados Cadastrais</a></li>
                        <li><a href="saque.html">Saque</a></li>
                        <li><a href="minhas-compras.html">Minhas Compras</a></li>
                    </ul>
                </li>
                <li class="treeview"><a href="{{route('market.index')}}"> <i class="fa fa-cart-plus"></i> <span>Mercado</span> <span
                                class="pull-right-container"> </span> </a>

                </li>
                <li class="treeview"><a href="#"> <i class="fa fa-graduation-cap "></i> <span>Produtor</span> <span
                                class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('product.list')}}">Meus Produtos</a></li>
                        <li><a href="meus-afiliados.html">Meus Afiliados</a></li>
                        <li><a href="falar-afiliados.html">Falar com Afiliados</a></li>
                        <li><a href="co-producoes.html">Co-Produções</a></li>
                        <li><a href="recuperar-vendas.html">Recuperação de Vendas</a></li>
                    </ul>
                </li>
                <li class="treeview"><a href="#"> <i class="fa fa-briefcase"></i> <span>Afiliado</span> <span
                                class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('affiliate.list') }}" class="active">Minhas Afiliações</a></li>
                        <li><a href="{{ route('affiliate.list-request') }}">Minhas Solicitações</a></li>
                        <li><a href="co-afiliacoes.html">Co-Afiliacoes</a></li>

                    </ul>
                </li>
                <li class="header">FINANCEIRO</li>
                <li class="treeview"><a href="#"> <i class="fa fa-money"></i> <span>Vendas</span> <span
                                class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="todas-vendas.html">Todas as Vendas</a></li>
                        <li><a href="venda-indicados.html">Venda de Indicados</a></li>
                        <li><a href="recuperar-vendas.html">Recuperar Vendas</a></li>
                        <li><a href="rastreio.html">Rastreio</a></li>
                    </ul>
                </li>
                <li class="treeview"><a href="#"> <i class="fa fa-bar-chart"></i> <span>Relatórios</span> <span
                                class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i> </span> </a>
                    <ul class="treeview-menu">
                        <li><a href="detalhamento-vendas.html">Detalhamento de Vendas</a></li>
                        <li><a href="relatorio-assinaturas.html">Assinaturas</a></li>
                        <li><a href="relatorio-co-afiliados.html">Relatório de Co-Afiiliados</a></li>
                        <li><a href="relatorio-co-produtor.html">Relatório de Co-Produtores</a></li>
                        <li><a href="relatorio-gerente.html">Relatório de Gerente</a></li>
                        <li><a href="relatorio-gerente.html">Ranking de Afiliados</a></li>
                        <li><a href="movimentao-financeira.html">Movimentação Financeira</a></li>
                        <li><a href="vendas-afiliados.html">Venda de Afiliados</a></li>
                    </ul>
                </li>

                <li class="header">FERRAMENTAS</li>
                <li class="treeview"><a href="api.html"><i class="fa fa-share-alt"></i> <span>API</span> <span
                                class="pull-right-container">  </span> </a>

                </li>
                <li class="treeview"><a href="postback.html"> <i class="fa fa-files-o"></i> <span>PostBack</span> <span
                                class="pull-right-container">  </span> </a>

                </li>
                <li class="treeview"><a href="automatizar-leads.html"> <i class="fa fa-paper-plane-o"></i> <span>Automatizar Leads</span>
                        <span class="pull-right-container">  </span> </a>

                </li>
                <li class="treeview"><a href="sms.html"> <i class="fa fa-envelope-open-o"></i> <span>SMS</span> <span
                                class="pull-right-container">  </span> </a>

                </li>
                <li class="treeview"><a href="criador-paginas.html"> <i class="fa fa-sitemap"></i> <span>Criador de Páginas</span>
                        <span class="pull-right-container"> </span> </a>

                </li>
                <li class="treeview"><a href="estrutura-afiliado.html"> <i class="fa fa-server"></i> <span>Estrutura de Afiliado</span>
                        <span class="pull-right-container"> </a>

                </li>
                <li class="treeview"><a href="trecker-vendas.html"> <i class="fa fa-random"></i>
                        <span>Tracker de Vendas</span> <span class="pull-right-container">  </span> </a>

                </li>
                <li class="treeview"><a href="area-membros.html"> <i class="fa fa-users"></i>
                        <span>Área de Membros</span> <span class="pull-right-container">  </span> </a>

                </li>
            </ul>
        </div>
    </div>
</aside>