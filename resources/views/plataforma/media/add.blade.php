@extends('plataforma.templates.tabs') 
@section('contentTab')

    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Adicionar Mídia</h4>
        </div>
    </div>
    <hr>    
    

    <div class="card-body">
        {{ Form::open(['route' => ['product.media.create', app('request')->product->id], 'method' => 'post']) }}

        <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
            {!! Form::label('type', 'Tamanho:', ['for' => 'type' ]) !!}
            {!! Form::select('type', \App\Helpers\Helper::typesMedia(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

            @if ($errors->has('type'))
                <label class="error" for="type" id="labeltype">
                {{$errors->first('type')}}
                </label>
            @endif                                         
        </div>

        <div class="form-group has-feedback {{ $errors->has('url') ? 'has-error' : '' }}">
          {!! Form::label('url', 'Url:', ['for' => 'url' ]) !!}
          {!! Form::text('url', old('url'), ['class' => 'form-control']) !!}

          @if ($errors->has('url'))
            <label class="control-label" for="url">
              {{$errors->first('url')}}
            </label>
          @endif 
        </div>

        <div class="row">
                <div class="col-md-12 text-right">
                    <button type="submit" class="btn  btn-success"> Adicionar</button>
                </div>
        </div>

        {!! Form::close() !!}

        <h5> Mídias Cadastradas</h5>
        <h4>
        <div class="card-body">
                <div  class="row m-t-2" id="panelAddPlanos" style="font-size: 14px">
                        <div class="table-responsive">
                            <table class="table" id="tableplans">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Banner</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">URL</th>  
                                <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($medias)
                                @foreach ($medias as $media)
                                    <tr>
                                        <td><img src="{{$media->url}}" width="100"></td>
                                        <td>{{$media->type}}</td>
                                        <td><input type="text" value="{{$media->url}}" readonly class="form-control" style="background-color: #FFFFFF"></td>
                                        <td>
                                            {!! Form::open(['route' => ['product.media.delete', app('request')->product->id, $media->id], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"  onclick="return confirm('Are you sure you want to rollback deletion of candidate table?')"></i> Excluir</button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                @else
                                    <tr>Nenhuma mídia encontrada</tr>
                                @endif
                            </tbody>
                            </table>
                        </div>
                </div>
        </div>


      
    </div>
 

@endsection