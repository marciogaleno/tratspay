@extends('plataforma.templates.template')
@section('styles')
<link rel="stylesheet" href="{{asset('dist/css/tabs.css')}}">
@endsection

@section('content')

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header text-uppercase">Solicite seu saque</div>
      <div class="card-body">
        <div class="row">
          <div class="col-lg-4">
            <div class="card gradient-ohhappiness">
              <div class="card-body">
                <div class="media">
                  <div class="media-body text-left">
                    @if ($account)
                        <h4 class="text-white">R$ {{number_format($account->balance, 2, ",", ".")}}</h4>
                    @else
                        <h4 class="text-white">R$ 0.0</h4>
                    @endif

                    <span class="text-white">Saldo Disponível</span>
                  </div>
                  <div class="align-self-center"><span id="dash-chart-4"></span></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-8">
            <!-- Large Size Modal -->
            @if($account &&   $account->balance > 0)
                <button class="btn btn-success m-1" data-toggle="modal" data-target="#largesizemodal">Solicitar Saque</button>
            @endif
            <br>
            <small style="font-size: 10px">O seu saque será processado em até 3 dias úteis após sua solicitação na ColinaPay</small>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header text-uppercase">Aqui você pode ver os saques já solicitados</div>
      <div class="card-body">

        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">Valor</th>
                <th scope="col">Banco</th>
                <th scope="col">Conta Bancária</th>
                <th scope="col">Data</th>
                <th scope="col">status</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($cashouts as $cashout)
                    <tr>

                      <th scope="row">R$ {{$cashout->value}}</th>
                      <td>{{$cashout->bank_name}}</td>
                      <td>
                        {{$cashout->bank_branch . '-' . $cashout->bank_branch_digit . '/' . $cashout->bank_account . '-' . $cashout->bank_account_digit}}
                      </td>
                      <td>{{\App\Helpers\Helper::dateToBr($cashout->date_solicitation, true)}}</td>
                      <td>{{\App\Helpers\Helper::statusCashout($cashout->status_cashout)}}</td>

                    </tr>
              @endforeach
            </tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="largesizemodal">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" contenteditable="true">Solicite o seu saque</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>

      <div class="modal-body">

        <div class="row">
            <div class="col-lg-4">
              @if ($account)
                <h5 class="text-dark" style="line-height: 0.3;">Disponível: <span class="text-success">R$ {{number_format($account->balance - 4.90, 2, ",", ".")}}</span></h5>
                <span style="font-size:12px">Valor com tarifa: R$ {{number_format($account->balance, 2, ",", ".")}}</span>
              @else
                <h5 class="text-dark">Disponível: <span class="text-success">R$ 0.0</span></h5>
              @endif

              <h6 class="text-dark" style="margin-top: 20px">Detalhes do saque:</h6>
              <p style="font-size: 12px">Existe uma taxa única de R$ 4,90 por saque solicitado</p>

              <p style="font-size: 10px">
                  Em dias não úteis o processamento de saque ocorrerá no próximo dia útil. Saques realizados durante a manhã podem ocorrer no mesmo dia.
              </p>
            </div>
            <div class="col-lg-8">

            {!! Form::open(['route' => ['cashout.create'], 'method' => 'post', 'id' => 'formCashout']) !!}
                <div class="form-group {{ $errors->has('bank_account_id') ? 'has-error' : '' }}">
                    {!! Form::label('bank_account_id', 'Conta bancária:', ['for' => 'bank_account_id' ]) !!}
                    {!! Form::select('bank_account_id', $formatedBankAccounts, null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'id' => 'type_url', 'required']) !!}
                </div>

                <div class="form-group {{ $errors->has('bank_account_id') ? 'has-error' : '' }}">
                  {!! Form::label('value', 'Valor do saque:', ['for' => 'bank_account_id' ]) !!}
                  <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text">R$</span>
                      </div>
                      @php

                      if ($account) {
                        $value = $account->balance - 4.90;
                        $value = number_format($value, 2, ",", ".");
                      } else {
                        $value = 0.0;
                      }

                      @endphp
                      {!! Form::text('value',  $value , ['class' => 'form-control', 'id' => 'inputValue', 'onkeyup' => 'calculeRate()', 'required']) !!}
                    </div>
                </div>


            <div class="table-responsive">
              <table class="table table-striped">
                 <tbody>
                   <tr>
                     <th scope="row">Valor a receber</th>
                     @if ($account)
                          <td> <span id="lbValue">R$ {{number_format($account->balance - 4.90, 2, ",", ".")}} <span></td>
                      @else
                          <td> <span id="lbValue">R$ 0.0 <span></td>
                      @endif
                   </tr>
                   <tr>
                     <th><span class="text-danger">Tarifa de saque<span></th>
                     <td>
                        <span class="text-danger" id="lbValueRate">
                              @if ($account)
                                R$ 4,90
                              @endif
                        </span>
                     </td>
                   </tr>
                   <tr>
                     <th scope="row">Total a ser debitado</th>
                      <td>
                          <span class="text-success" id="lbValueTotal">
                                R$
                                @if ($account)
                                    @if ($account->balance < 500)
                                        {{number_format($account->balance, 2, ",", ".")}}
                                    @else
                                        {{number_format($account->balance, 2, ",", ".")}}
                                    @endif
                                @endif

                          </span>

                      </td>
                   </tr>
                 </tbody>
               </table>
            </div>

          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i> Solicitar Saque</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

@endsection

@section('scripts')
<script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-validation/js/jquery.validate.min.js')}}"></script>
<script>
    $("#inputValue").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

    function calculeRate()
    {
        var value = parseFloat($("#inputValue").val().replace(".", "").replace(",", "."));
        var rate = 4.90;
        var valueTotal = 0.0;
        valueTotal = value + rate;
        rate = rate;

        $("#lbValue").text(value.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));
        $("#lbValueRate").text(rate.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));
        $("#lbValueTotal").text(valueTotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));

    }

    function formatReal( valor )
    {
        valor = valor.toString().replace(/\D/g,"");
        valor = valor.toString().replace(/(\d)(\d{8})$/,"$1.$2");
        valor = valor.toString().replace(/(\d)(\d{5})$/,"$1.$2");
        valor = valor.toString().replace(/(\d)(\d{2})$/,"$1,$2");
        return 'R$ ' + valor
    }

    $("#formCashout").validate({
        rules: {
          bank_account_id: "required",
          value: "required"
        },
        messages: {
          bank_account_id: "Campo obrigatório",
          value: "Campo obrigatório"
        }
    });

</script>
@endsection
