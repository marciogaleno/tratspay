
@extends('plataforma.templates.tabs') 
@section('styles')
<link rel="stylesheet" href="{{asset('dist/css/tabs.css')}}">
@endsection

@section('contentTab')


    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Novo Checkout</h4>
        </div>
        <div class="col-sm-3">
            <a  href="{{route('product.checkout.view', [app('request')->product->id])}}" class="btn btn-warning waves-effect waves-light m-1  text-right"> 
                <i class="fa fa-arrow-left"></i>
                <span>Voltar</span>
            </a>
        </div>
    </div>
    
    <hr>

        {{ Form::open(['route' => ['product.checkout.create', app('request')->product->id], 'method' => 'POST']) }}

        <div class="card-body">
            <div class="form-group has-feedback">
                <div class="form-group">
                    {!! Form::label('plan_id', 'Selecione um plano :', ['for' => 'plan_id' ]) !!}
                    {!! Form::select('plan_id',  $plans, null , ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione um plano --', 'id' => 'plan_id']) !!}
                </div>

                @if ($errors->has('plan_id'))
                <label class="error" for="desc">
                    {{$errors->first('plan_id')}}
                </label>
                @endif 
            </div>
            <div class="form-group has-feedback {{ $errors->has('desc') ? 'has-error' : '' }}">
                {!! Form::label('desc', 'Descrição :', ['for' => 'desc' ]) !!}
                {!! Form::text('desc', null, ['class' => 'form-control']) !!}

                @if ($errors->has('desc'))
                <label class="control-label" for="desc">
                    {{$errors->first('desc')}}
                </label>
                @endif 
            </div>

            <div class="card">
                <div class="card-header text-white bg-dark">Formas de pagamento</div>
                <div class="card-body">
                    <div class="row m-t-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::checkbox('pay_card') }}
                                <span class="custom-control-description ml-0">cartão</span> 
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::checkbox('pay_ticket') }}
                                <span class="custom-control-description ml-0">Boleto</span> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header text-white bg-dark">Dados adicionais do checkout</div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('invoice_desc', 'Descrição na fatura :', ['for' => 'invoice description' ]) !!}
                        {!! Form::text('invoice_desc', null, ['class' => 'form-control']) !!}

                        @if ($errors->has('invoice description'))
                        <label class="error" for="invoice description">
                            {{$errors->first('invoice description')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('template', 'Template do Checkout :', ['for' => 'template' ]) !!}
                        {!! Form::select('template', [1 => "Modelo 1", 2 => "Modelo 2"], null, ['class' => 'custom-select form-control']) !!}

                        @if ($errors->has('template'))
                            <label class="error" for="template">
                                {{$errors->first('template')}}
                            </label>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::label('days_expir_ticket', 'Dias até o Boleto Vencer:', ['for' => 'invoice description' ]) !!}
                        {!! Form::select('days_expir_ticket', [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('days_expir_ticket'))
                        <label class="error" for="days_expir_ticket">
                            {{$errors->first('days_expir_ticket')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('request_address', 'Solicitar endereço: ', ['for' => 'request_address' ]) !!}
                        {!! Form::select('request_address', [0 => 'Não', 1 => 'Sim'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('request_address'))
                        <label class="error" for="request_address">
                            {{$errors->first('request_address')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('request_cpf', 'Solicitar CPF: ', ['for' => 'request_cpf' ]) !!}
                        {!! Form::select('request_cpf', [0 => 'Não', 1 => 'Sim'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('request_cpf'))
                        <label class="error" for="request_cpf">
                            {{$errors->first('request_cpf')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('request_phone', 'Solicitar telefone: ', ['for' => 'request_phone' ]) !!}
                        {!! Form::select('request_phone', [0 => 'Não', 1 => 'Sim'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('request_phone'))
                        <label class="error" for="request_phone">
                            {{$errors->first('request_phone')}}
                        </label>
                        @endif 
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-md-12 text-right">
                    <a href="{{route('product.checkout.view', [app('request')->product->id])}}" class="btn btn-warning waves-effect waves-light m-1"> 
                        <i class="fa fa-arrow-left"></i>
                        <span>Voltar</span>
                    </a>
                    <button type="submit" class="btn btn-success waves-effect waves-light m-1"> 
                        <i class="fa fa-save"></i>
                        <span>Salvar</span>
                    </button>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    
@endsection