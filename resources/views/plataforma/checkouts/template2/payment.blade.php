@extends('plataforma.templates.checkout')
@section('content')
    <div class="container centered">
        @php
            $banner_1 = false;
            $banner_2 = false;
        @endphp

        @if($checkout->banner_1 && Storage::disk('images')->exists($checkout->banner_1))
            @php
                $banner_1 = $checkout->banner_1;
            @endphp
            <div class=" text-center " style="margin-top: 2px">
                <img src="{{ \App\Helpers\Helper::getPathFileS3('images',  $checkout->banner_1)}}" class="img-fluid">
                <!--<img src="http://tratspay-images.s3-sa-east-1.amazonaws.com/23/banners/uZr8XbSVQolljjakkAD7EnTRGY8T5DPHmwTW0MVX.jpeg" class="img-fluid">-->
            </div>
        @endif

        @if($checkout->banner_2 && Storage::disk('images')->exists($checkout->banner_2))
            @php
                $banner_2 = $checkout->banner_2;
            @endphp
        @endif

        <div class="box-central">
            <!-- Parte do cabeçalho do checkout -->
            <div class="box-header">
                <div class="row alinhamento-vertical">
                    <div class="col-md-8 col-sm-12">
                        @if(!$banner_1)
                            <h5>{{ $checkout->planChk->productChk->name }}</h5>
                        @endif
                        <span class="box-description">Para suporte, envie um e-mail para <strong>{{ $checkout->planChk->productChk->email_support }}.</strong></span><br>
                        <span class="box-description">Você selecionou: <strong>{{ $checkout->desc }}.</strong></span>
                    </div>
                    <div class="col-4 d-none d-md-block">
                        <div class="alert alert-success float-right"><i class="fas fa-lock"></i> COMPRA SEGURA</div>
                    </div>
                </div>

                @if($errors->any())
                    <div class="col-md-12 text-center">
                        <div class="alert alert-danger">
                            {{$errors->first()}}
                        </div>
                    </div>
                @endif

                <div class="col-md-12 text-center" id="alertCard"></div>

            </div>

            <div id="dadosCliente">
                <div class="row">
                    <div class="{{ $banner_2 ? 'col-md-8' : 'col-md-12' }}">
                        <form id="cliente">
                            <div class="box-step">
                                <div class="box-title">
                                    <div class="box-title-number">1</div>
                                    <div class="box-title-text">Dados Pessoais</div>
                                </div>
                                <div class="box-description">Digite seus dados pessoais abaixo para iniciar a sua
                                    compra.
                                </div>
                                <div class="alert alert-primary d-none" id="userError" role="alert">
                                    Ops! Aparentemente deu algo errado nessa parte, por favor, corrija!
                                </div>


                                <div class="form-row">
                                    <div class="form-group {{ $banner_2 ? 'col-md-12' : 'col-md-12' }}">
                                        <div class="inner-addon left-addon">
                                            <i class="far fa-user"></i>
                                            {!! Form::text('name', old('name'),$attributes = $errors->has('name') ? ['class' => 'form-control form-control-lg is-invalid' , 'id'=>'name','placeholder'=>'Seu nome completo'] : ['class' => 'form-control form-control-lg', 'id'=>'name','placeholder'=>'Seu nome completo']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $banner_2 ? 'col-md-12' : 'col-md-12' }}">
                                        <div class="inner-addon left-addon">
                                            <i class="far fa-envelope"></i>
                                            {!! Form::text('email', old('email'),$attributes = $errors->has('email') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'email','placeholder'=>'Seu e-mail'] : ['class' => 'form-control form-control-lg', 'id'=>'email','placeholder'=>'Seu e-mail']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group {{ $banner_2 ? 'col-md-12' : 'col-md-4' }}">
                                        <div class="inner-addon left-addon">
                                            <i class="far fa-check-circle"></i>
                                            {!! Form::text('phone', old('phone'),$attributes = $errors->has('phone') ? ['class' => 'form-control form-control-lg is-invalid phone', 'id'=>'phone','placeholder'=>'Telefone'] : ['class' => 'form-control form-control-lg phone', 'id'=>'phone','placeholder'=>'Telefone']) !!}
                                        </div>
                                    </div>

                                    @if($checkout->request_cpf)
                                        <div class="form-group {{ $banner_2 ? 'col-md-12' : 'col-md-4' }}">
                                            <div class="inner-addon left-addon">
                                                <i class="far fa-check-circle"></i>
                                                {!! Form::text('docCli', old('docCli'),$attributes = $errors->has('docCli') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'docCli','placeholder'=>'CPF'] : ['class' => 'form-control form-control-lg', 'id'=>'docCli','placeholder'=>'CPF']) !!}
                                                <input name="docTypeCli" id="docTypeCli" value="cpf" type="hidden">
                                            </div>
                                            <small style="display: none;" class="help-block"
                                                   data-bv-validator="callback"
                                                   data-bv-for="cpf" data-bv-result="NOT_VALIDATED"><strong
                                                        style="color: red;">CPF
                                                    Inválido</strong></small>
                                            <small style="display: none;" class="help-block"
                                                   data-bv-validator="stringLength"
                                                   data-bv-for="cpf" data-bv-result="NOT_VALIDATED">Please enter a value
                                                with
                                                valid
                                                length
                                            </small>
                                        </div>

                                    @endif
                                </div>
                            </div>

                            @if($checkout->request_address)
                                <div class="box-step">
                                    <div class="box-title">
                                        <div class="box-title-number">2</div>
                                        <div class="box-title-text">Endereço de Entrega</div>
                                    </div>
                                    <div class="box-description">Digite o seu CEP para onde vamos enviar o seu pedido
                                        abaixo.
                                    </div>
                                    <div class="alert alert-primary d-none" id="cepError" role="alert">
                                        Ops! Aparentemente deu algo errado nessa parte, por favor, corrija!
                                    </div>
                                    <div class="form-group">
                                        <div class="inner-addon left-addon">
                                            <i class="far fa-map"></i>
                                            {!! Form::text('zipcode', old('zipcode'),$attributes = $errors->has('zipcode') ? ['class' => 'form-control form-control-lg is-invalid cep', 'id'=>'zipcode','placeholder'=>'CEP'] : ['class' => 'form-control form-control-lg cep', 'id'=>'zipcode','placeholder'=>'CEP']) !!}
                                        </div>
                                    </div>
                                    <div id="cepContent" class="d-none  ">
                                        <div class="form-row">
                                            <div class="form-group col-md-10">
                                                {!! Form::text('address', old('address'),$attributes = $errors->has('address') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'address','placeholder'=>'Endereço'] : ['class' => 'form-control form-control-lg', 'id'=>'address','placeholder'=>'Endereço']) !!}
                                            </div>
                                            <div class="form-group col-md-2">
                                                {!! Form::text('address_number', old('address_number'),$attributes = $errors->has('address_number') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'address_number','placeholder'=>'Numero'] : ['class' => 'form-control form-control-lg', 'id'=>'address_number','placeholder'=>'Numero']) !!}
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                {!! Form::text('address_complement', old('address_complement'),$attributes = $errors->has('address_complement') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'address_complement','placeholder'=>'Complemento'] : ['class' => 'form-control form-control-lg', 'id'=>'address_complement','placeholder'=>'Complemento']) !!}

                                            </div>
                                            <div class="form-group col-md-3">
                                                {!! Form::text('address_district', old('address_district'),$attributes = $errors->has('address_district') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'address_district','placeholder'=>'Bairro'] : ['class' => 'form-control form-control-lg', 'id'=>'address_district','placeholder'=>'Bairro']) !!}

                                            </div>
                                            <div class="form-group col-md-3">
                                                {!! Form::text('address_city', old('address_city'),$attributes = $errors->has('address_city') ? ['class' => 'form-control form-control-lg is-invalid', 'id'=>'address_city','placeholder'=>'Cidade'] : ['class' => 'form-control form-control-lg', 'id'=>'address_city','placeholder'=>'Cidade']) !!}

                                            </div>
                                            <div class="form-group col-md-2">
                                                {!! Form::select('address_state', $states,'SC', ['class' => 'form-control form-control-lg ', 'id' =>'address_state', 'id'=>'address_state']) !!}
                                                {!! Form::hidden('address_country', 'BR') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        </form>

                        <div class="box-footer">
                            <div class="container">
                                <div class="row alinhamento-vertical">
                                    <div class="box-left col-sm-12 text-center d-md-none mobile-padding">
                                        <h6><i class="fas fa-lock"></i> Esta é uma página segura.</h6>
                                    </div>
                                    <div class="box-left col-md-6 d-none d-md-block">
                                        <h6><i class="fas fa-lock"></i> Você está em uma página segura.</h6>
                                    </div>
                                    <div class="box-right col-md-6 col-sm-12">
                                        <button class="btn btn-lg btn-primary nowrap btn-block" id="BtnDadosCliente"
                                                type="button"
                                                style="font-size:16px">
                                            <i class="fas fa-lock"></i> Continuar
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    @if($banner_2)
                        <div class="col-md-4 text-center">
                            <img src="{{\App\Helpers\Helper::getPathFileS3('images',  $checkout->banner_2) }}"
                                 class="img-fluid"> -->
                            <!--<img src="http://tratspay-images.s3-sa-east-1.amazonaws.com/23/banners/QsEbym3Ce3UTWkFkbChn26StaWc2j6KHSQRRv0RO.jpeg" class="img-fluid">-->
                        </div>
                    @endif
                </div>
            </div>

            <div id="accordion" style="display: none;">
                @if($checkout->pay_card)
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link" data-toggle="collapse"
                                        data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                    <div class="box-step">
                                        <div class="box-title">
                                            <div class="box-title-number"><i class="far fa-credit-card"></i></div>
                                            <div class="box-title-text">Pagar com Cartão</div>
                                        </div>
                                    </div>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                             data-parent="#accordion">
                            <div class="card-body">
                                <div class="box-step">
                                    <form action="{{route('checkout.payment',$checkout->code)}}" method="post" id="pay"
                                          name="pay">
                                        {!! Form::token() !!}
                                        {!! Form::hidden('traking_code', $trats_id) !!}
                                        <input type="hidden" name="charge_id" value="{{$chargeId}}">
                                        <div class="row alinhamento-vertical">

                                            <div class="col-sm-12 col-md-6">
                                                <div class="box-cartaos">
                                                    <div class="info_cartao_checkout">
                                                        <span id="info_card_nome">NOME NO CARTAO</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <span id="info_card_mes">MM</span>/<span
                                                                id="info_card_ano">AAAA</span><br/><br/>
                                                        <span id="info_card_numero">0000 0000 0000 0000</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6" style="margin: 10px 0px 0px 0px;">

                                                <div class="form-group">
                                                    <div class="inner-addon left-addon">
                                                        <i class="fas fa-credit-card-front"></i>

                                                        <input class="form-control" type="text" id="cardNumber"
                                                               data-checkout="cardNumber"
                                                               placeholder="Número do Cartão"
                                                               onselectstart="return false"
                                                               onpaste="return false" onCopy="return false"
                                                               onCut="return false"
                                                               onDrag="return false" onDrop="return false"
                                                               autocomplete=off/>
                                                        <div class="imgCard" id="imgCard"></div>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <div class="inner-addon left-addon">

                                                            <select class="form-control" id="cardExpirationMonth"
                                                                    data-checkout="cardExpirationMonth"
                                                                    placeholder="-- Mês --"
                                                                    onselectstart="return false"
                                                                    onpaste="return false"
                                                                    onCopy="return false"
                                                                    onCut="return false" onDrag="return false"
                                                                    onDrop="return false"
                                                                    autocomplete=off>
                                                                <option value="">-- Mês --</option>
                                                                @for($x=1;$x<=12;$x++)
                                                                    <option value="{{$x}}">{{$x}}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <div class="inner-addon left-addon">


                                                            <select class="form-control" id="cardExpirationYear"
                                                                    data-checkout="cardExpirationYear"
                                                                    placeholder="-- Ano --"
                                                                    onselectstart="return false"
                                                                    onpaste="return false"
                                                                    onCopy="return false"
                                                                    onCut="return false" onDrag="return false"
                                                                    onDrop="return false"
                                                                    autocomplete=off>
                                                                <option value="">-- Ano --</option>
                                                                @for($x=date('Y');$x<=(date('Y')+30);$x++)
                                                                    <option value="{{$x}}">{{$x}}</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <div class="inner-addon left-addon">
                                                            <input class="form-control" type="text"
                                                                   id="securityCode"
                                                                   data-checkout="securityCode" placeholder="CVV"
                                                                   onselectstart="return false"
                                                                   onpaste="return false"
                                                                   onCopy="return false"
                                                                   onCut="return false" onDrag="return false"
                                                                   onDrop="return false"
                                                                   autocomplete=off/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-4">
                                                        <div class="inner-addon left-addon">
                                                            <select class="form-control" id="docType"
                                                                    data-checkout="docType"></select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-8">
                                                        <div class="inner-addon left-addon">
                                                            <input class="form-control" type="text" id="docNumber"
                                                                   name="docNumber"
                                                                   data-checkout="docNumber"
                                                                   placeholder="Documento do Títular"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-row">
                                                    <div class="form-group col-md-12">
                                                        <input class="form-control" type="text" id="cardholderName"
                                                               data-checkout="cardholderName"
                                                               placeholder="Nome no Cartão"/>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <select class="form-control" id="installmentsOption"
                                                            name="installmentsOption">
{{--                                                        @if(count($parcelas))--}}
{{--                                                        @foreach($parcelas as $num => $parcela)--}}
{{--                                                            <option>--}}
{{--                                                                {{$num}} X de--}}
{{--                                                                R$ {!! number_format($parcela,2,",",".")!!}--}}

{{--                                                            </option>--}}
{{--                                                        @endforeach--}}
{{--                                                        @endif--}}
                                                    </select>
                                                    <span style="color: #666; font-size: 12px">Taxa de 2,99% a.m.</span>
                                                </div>
                                            </div>

                                            <div class="container box-footer">
                                                <div class="row alinhamento-vertical">
                                                    <div class="box-left col-md-6 d-none d-md-block">
                                                        <h6><i class="fa fa-lock"></i> Você está em uma página
                                                            segura.
                                                        </h6>
                                                        <img src="{{asset('assets/images/site_seguro.png')}}"
                                                             height="50">
                                                    </div>
                                                    <div class="box-right col-md-6 col-sm-12">
                                                        <button class="btn btn-lg btn-primary nowrap btn-block"
                                                                style="font-size:16px"
                                                                type="submit"><i
                                                                    class="fas fa-lock"></i> Finalizar Compra
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="paymentMethodId" id="paymentMethodId"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                @if($checkout->pay_ticket)
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 class="mb-0">
                                <button type="button" class="btn btn-link collapsed" data-toggle="collapse"
                                        data-target="#collapseTwo"
                                        aria-expanded="false" aria-controls="collapseTwo">
                                    <div class="box-step">
                                        <div class="box-title">
                                            <div class="box-title-number"><i class="fa fa-money-bill"></i></div>
                                            <div class="box-title-text">Pagar com Boleto</div>
                                        </div>
                                    </div>
                                </button>
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion"
                             style="">
                            <div class="card-body">
                                <div class="box-boleto">
                                    <h6><i class="far fa-engine-warning"></i> Informações sobre o pagamento via boleto
                                    </h6>
                                    <ul>
                                        <li>Valor à vista R$ <span>{{ $checkout->planChk->price }}</span> não pode ser
                                            parcelado;
                                        </li>
                                        <li>Prazo de até 2 dias úteis para compensar.</li>

                                    </ul>
                                    <form action="{{route('checkout.payment',$checkout->code)}}" method="post"
                                          id="payBol" name="payBol">
                                        <input type="hidden" name="charge_id" value="{{$chargeId}}">
                                        @if(!$checkout->request_cpf)
                                            <div class="form-group col-md-6">
                                                <div class="inner-addon left-addon">
                                                    <i class="far fa-check-circle"></i>
                                                    {!! Form::text('docCliBol', old('docCliBol'),['class' => 'form-control form-control-lg cpf', 'id'=>'docCliBol','placeholder'=>'CPF/CNPJ']) !!}
                                                </div>
                                            </div>

                                        @endif


                                        <div class="container box-footer">
                                            <div class="row alinhamento-vertical">
                                                <div class="box-left col-md-6 col-sm-12">
                                                    <button class="btn btn-lg btn-secondary nowrap btn-block btn-tst"
                                                            type="button" data-toggle="collapse"
                                                            data-target="#collapseOne"
                                                            aria-expanded="true" aria-controls="collapseOne"
                                                            style="font-size:16px"><i
                                                                class="fa fa-angle-left"></i> Voltar
                                                    </button>
                                                </div>
                                                <div class="box-right col-md-6 col-sm-12">

                                                    <input type="hidden" name="paymentMethodId" value="bolbradesco"/>
                                                    <button class="btn btn-lg btn-primary nowrap btn-block"
                                                            type="submit" style="font-size:16px"><i
                                                                class="fa fa-check"></i> Finalizar Compra
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalProcessando" data-backdrop="false" tabindex="-1" role="dialog"
         aria-labelledby="modalProcessandoLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h3 class="text-success">Processando Pagamento...</h3>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                             role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                             style="width: 100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalError" data-backdrop="false" tabindex="-1" role="dialog"
         aria-labelledby="modalErrorLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h5 class="text-danger">Falha no Processamento.</h5>
                    <div class="modalErrors">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->


    <!--/FOOTER-->

@endsection

@push('scripts')
    <script>
        // $(document).ready(function () {
        //     $gn.ready(function(checkout) {
        //
        //         var callback = function(error, response) {
        //             if(error) {
        //                 // Trata o erro ocorrido
        //                 console.error(error);
        //             } else {
        //                 // Trata a resposta
        //                 console.log(response);
        //             }
        //         };
        //
        //         checkout.getPaymentToken({
        //             brand: 'visa', // bandeira do cartão
        //             number: '4012001038443335', // número do cartão
        //             cvv: '123', // código de segurança
        //             expiration_month: '05', // mês de vencimento
        //             expiration_year: '2018' // ano de vencimento
        //         }, callback);
        //
        //     });
        // });
        /* --------------------------
            *	Error Handler	*
        -------------------------- */
        function mostraError(nameError, erroText, erroType) {
            var corErro;
            switch (erroType) {
                case 1:
                    corErro = "secondary";
                case 2:
                    corErro = "success";
                    break;
                case 3:
                    corErro = "danger";
                    break;
                case 4:
                    corErro = "warning";
                    break;
                case 5:
                    corErro = "info";
                    break;
                case 6:
                    corErro = "light";
                    break;
                case 7:
                    corErro = "dark";
                    break;
                default:
                    corErro = "primary";
                    break;
            }
            // Oculta Erros anteriores
            ocultarError(nameError);
            // Seta a cor do erro para a desejada
            $(nameError).removeClass('alert-primary').addClass('alert-' + corErro);
            // Remove o Display:None, mostra a div.
            $(nameError).removeClass('d-none')
            // Mostra a mensagem de erro desejada.
            $(nameError).text(erroText);
        }

        function ocultarError(nameError) {
            $(nameError).addClass('d-none');
        }


        /* Checar se o e-mail é válido */
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        /* ----------------------------
            Validações do formulário
        ---------------------------- */
        function isEmpty(campo, error, msg) {
            var input = $.trim($(campo).val());

            if (input === '') {
                mostraError(error, msg, 3);
                return false;
            } else {
                ocultarError(error);
                return true;
            }
        }

        // Declarando variaveis para que sejam globais.
        $(document).ready(function () {
            $('#email').blur(function () {
                var email_cond = isEmpty('#email', '#userError', 'Por favor, digite o seu endereço de e-mail.');
            });

            $('#name').blur(function () {
                nomecompleto = isEmpty('#name', '#userError', 'Por favor, digite o seu nome completo.');

            });

            $('#phone').blur(function () {
                phone = isEmpty('#phone', '#userError', 'Por favor, digite o número do seu telefone.');
            });

            $('#docCli').blur(function () {
                cpf = isEmpty('#docCli', '#userError', 'Por favor, informe o seu CPF.');
            });

            $('#zipcode').blur(function () {
                cep = isEmpty('#zipcode', '#cepError', 'Você precisa digitar o número do CEP.');
            });

            $('#address').blur(function () {
                rua = isEmpty('#address', '#cepError', 'Você precisa informar o seu endereço.');
            });

            $('#address_number').blur(function () {
                numero = isEmpty('#address_number', '#cepError', 'Você precisa informar o número do seu endereço.');
            });

            $('#bairro').blur(function () {
                numero = isEmpty('#address_number', '#cepError', 'Você precisa informar o bairro onde você reside.');
            })

            $('#cidade').blur(function () {
                cidade = isEmpty('#cidade', '#cepError', 'Você precisa informar a cidade onde você reside.');
            });

            $('#uf').blur(function () {
                uf = isEmpty('#uf', '#cepError', 'Você precisa informar o estado onde você reside.');
            });

        });


        function ValidarCPF(cpf) {
            exp = /\.|\-/g;
            cpf = cpf.toString().replace(exp, "");
            if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
                return false;
            add = 0;
            for (i = 0; i < 9; i++)
                add += parseInt(cpf.charAt(i)) * (10 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(9)))
                return false;
            add = 0;
            for (i = 0; i < 10; i++)
                add += parseInt(cpf.charAt(i)) * (11 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(10)))
                return false;

            return true;
        }

        /* Faz a checagem se todos os campos estão preenchidos */
        //  Vincula o handler deste evento para o evento de "submit" do javascript
        $('#BtnDadosCliente').click(function () {

            ocultarError('#userError');
            var r = true;

            var entradas = new Array("name", "docCli", "email", "dtFim", "phone", "zipcode", "address", "address_number", "address_district", "address_city");
            for (var x = 0; x <= entradas.length; x++) {
                var item = entradas[x];
                if (document.getElementById(item)) {
                    var valor = document.getElementById(item).value;
                    if (valor.length == "") {
                        $("#" + item).addClass("is-invalid");
                        $("#" + item).removeClass("is-valid");
                        r = false;
                    } else {

                        $("#" + item).removeClass("is-invalid");
                        $("#" + item).addClass("is-valid");

                        if (entradas[x] == "docCli") {
                            if (ValidarCPF(valor)) {
                                $("#" + item).removeClass("is-invalid");
                                $("#" + item).addClass("is-valid");
                            } else {
                                mostraError("#userError", "CPF Inválido!", 3);
                                return false;
                            }
                        }
                    }
                }
            }

            if (r) {
                $('#dadosCliente').hide();
                $('#accordion').show();
            }

            mostraError("#userError", "Por favor, Verifique os campos necessários", 3);
            return false;

        });


        $(document).ready(function () {

            function limpa_formulario_cep() {
                // Limpa valores do formulário de cep.
                $("#address").val("");
                $("#address_district").val("");
                $("#address_city").val("");
                $("#address_state").val("");
                $("#ibge").val("");
            }

            //Quando o campo cep perde o foco.
            $("#zipcode").blur(function () {

                //Nova variável "cep" somente com dígitos.
                var cep = $(this).val().replace(/\D/g, '');

                //Verifica se campo cep possui valor informado.
                if (cep != "") {

                    //Expressão regular para validar o CEP.
                    var validacep = /^[0-9]{8}$/;

                    //Valida o formato do CEP.
                    if (validacep.test(cep)) {

                        //Preenche os campos com "..." enquanto consulta webservice.
                        $("#address").val("...");
                        $("#address_district").val("...");
                        $("#address_city").val("...");
                        $("#address_state").val("...");
                        $("#ibge").val("...");

                        //Consulta o webservice viacep.com.br/
                        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                            if (!("erro" in dados)) {
                                //Atualiza os campos com os valores da consulta.
                                $("#address").val(dados.logradouro);
                                $("#address_district").val(dados.bairro);
                                $("#address_city").val(dados.localidade);
                                $("#address_state").val(dados.uf).change();
                                $("#ibge").val(dados.ibge);
                                ocultarError('#cepError');
                                $("#cepContent").removeClass('d-none')
                                $("#address_number").focus();
                            } //end if.
                            else {
                                //CEP pesquisado não foi encontrado.
                                limpa_formulario_cep();
                                // Envia um erro para o usuário, com a cor 3 (DANGER)!
                                mostraError("#cepError", "O CEP digitado não foi encontrado! Por favor, corrija.", 3);
                            }
                        });
                    } //end if.
                    else {
                        //cep é inválido.
                        limpa_formulario_cep();
                        mostraError("#cepError", "O CEP digitado tem um formato inválido!", 3);
                    }
                } //end if.
                else {
                    //cep sem valor, limpa formulário.
                    limpa_formulario_cep();
                }
            });
        });


        $(document).ready(function () {

            $('#cliente').bootstrapValidator({
                feedbackIcons: {
                    valid: 'far fa-check',
                    invalid: '',
                    validating: ''
                },
                fields: {
                    cpf: {
                        validators: {
                            callback: {
                                message: '<strong style="color: red;">CPF Inválido</strong>',
                                callback: function (value) {

                                    cpf = value.replace(/[^\d]+/g, '');
                                    if (cpf == '') return false;

                                    if (cpf.length != 11) return false;

                                    // testa se os 11 digitos sÃ£o iguais, que nÃ£o pode.
                                    var valido = 0;
                                    for (i = 1; i < 11; i++) {
                                        if (cpf.charAt(0) != cpf.charAt(i)) valido = 1;
                                    }
                                    if (valido == 0) return false;


                                    aux = 0;
                                    for (i = 0; i < 9; i++)
                                        aux += parseInt(cpf.charAt(i)) * (10 - i);
                                    check = 11 - (aux % 11);
                                    if (check == 10 || check == 11)
                                        check = 0;
                                    if (check != parseInt(cpf.charAt(9)))
                                        return false;

                                    aux = 0;
                                    for (i = 0; i < 10; i++)
                                        aux += parseInt(cpf.charAt(i)) * (11 - i);
                                    check = 11 - (aux % 11);
                                    if (check == 10 || check == 11)
                                        check = 0;
                                    if (check != parseInt(cpf.charAt(10)))
                                        return false;
                                    return true;


                                }
                            }
                        }
                    }
                }
            });
        });

        $(document).ready(function () {
            $('#cardholderName').keyup(function () {
                $('#info_card_nome').html(this.value);
            });

            $('#cardExpirationMonth').change(function () {
                $('#info_card_mes').html(this.value);
            });

            $('#cardExpirationYear').change(function () {
                $('#info_card_ano').html(this.value);
            });
        });

        function addEvent(el, eventName, handler) {
            if (el.addEventListener) {
                el.addEventListener(eventName, handler);
            } else {
                el.attachEvent('on' + eventName, function () {
                    handler.call(el);
                });
            }
        };

        function getBin() {
            var ccNumber = document.querySelector('input[data-checkout="cardNumber"]');
            $('#info_card_numero').text(ccNumber.value.replace(/[ .-]/g, ''));
            return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
        };

        function guessingPaymentMethod(event) {
            var bin = getBin();
            $('#imgCard').html('');

            if (event.type == "keyup") {
                if (bin.length >= 6) {
                    Mercadopago.getPaymentMethod({
                        "bin": bin
                    }, setPaymentMethodInfo);

                    /**
                     Mercadopago.getInstallments({
                        "bin": getBin(),
                        "amount": {{ $checkout->planChk->getOriginal('price') }}
                    }, setInstallmentInfo);
                     **/
                }
            } else {
                setTimeout(function () {
                    if (bin.length >= 6) {
                        Mercadopago.getPaymentMethod({
                            "bin": bin
                        }, setPaymentMethodInfo);

                        /**
                         Mercadopago.getInstallments({
                            "bin": getBin(),
                            "amount": {{ $checkout->planChk->getOriginal('price') }}
                        }, setInstallmentInfo);
                         **/
                    }
                }, 100);
            }
        };

        function setInstallmentInfo(status, installments) {
            var html_options = "";
            for (i = 0; installments[0].payer_costs && i < installments[0].payer_costs.length; i++) {
                html_options += "<option value='" + installments[0].payer_costs[i].installments + "'>" + installments[0].payer_costs[i].recommended_message + "</option>";
            }
            ;
            $("#installmentsOption").html(html_options);
        };

        function setPaymentMethodInfo(status, response) {

            if (status == 200) {

                $('#imgCard').html('<img src="' + response[0].thumbnail + '">');
                // do somethings ex: show logo of the payment method
                var form = document.querySelector('#pay');

                if (document.querySelector("input[name=paymentMethodId]") == null) {
                    var paymentMethod = document.createElement('input');
                    paymentMethod.setAttribute('name', "paymentMethodId");
                    paymentMethod.setAttribute('type', "hidden");
                    paymentMethod.setAttribute('value', response[0].id);

                    form.appendChild(paymentMethod);
                } else {
                    document.querySelector("input[name=paymentMethodId]").value = response[0].id;
                }
            }
        };

        addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'keyup', guessingPaymentMethod);
        addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'change', guessingPaymentMethod);

        doSubmit = false;

        $gn.ready(function (checkout) {
            let value ='{{$checkout->planChk->price}}'.replace(/[^\d]+/g,'');

            checkout.getInstallments(parseInt(value),'visa', function(error, response){
                if(error) {
                    // Trata o erro ocorrido
                    console.log(error);
                } else {
                    var html_options = "";
                    for (i = 0; i < response.data.installments.length; i++) {
                        let item = response.data.installments[i];
                        html_options += "<option value='" +item.installment+ "'>" + item.installment + " x de R$"+item.currency +"</option>";
                    }
                    ;
                    $("#installmentsOption").html(html_options);
                }
            });

            $('#pay').submit(function (e) {

                var callback = function (error, response) {
                    $('#preloader').fadeOut('slow', function () {
                        $(this).remove();
                    });

                    if (error) {
                        openModalErro(error)
                    } else {
                        openModal();
                        $('#alertCard').html('<div class="alert alert-success" role="alert">Processando Pagamento...</div>');
                        var form = document.querySelector('#pay');

                        $('#cliente :input').not(':submit').clone().hide().appendTo('#pay');

                        var card = document.createElement('input');
                        card.setAttribute('name', "payment_token");
                        card.setAttribute('type', "hidden");
                        card.setAttribute('value', response.data.payment_token);
                        form.appendChild(card);
                        $('#paymentMethodId').val('credit_card');
                        doSubmit = true;
                        form.submit();
                    }
                }
                e.preventDefault();

                if (!doSubmit) {
                    var $form = document.querySelector('#pay');

                    checkout.getPaymentToken({
                        brand: 'visa', // bandeira do cartão
                        number: $('#cardNumber').val(), // número do cartão
                        cvv: $('#securityCode').val(), // código de segurança
                        expiration_month: $('#cardExpirationMonth').val(), // mês de vencimento
                        expiration_year: $('#cardExpirationYear').val() // ano de vencimento
                    }, callback);

                    return false;
                }
            })
        });

        function retornoErro($erro) {

            var description;

            switch ($erro) {

                case "205":
                    description = "Digite o número do seu cartão.";
                    break;
                case "208":
                    description = "Escolha um mês.";
                    break;
                case "209":
                    description = "Escolha um ano.";
                    break;
                case "212":
                    description = "Informe seu documento.";
                    break;
                case "213":
                    description = "Informe seu documento.";
                    break;
                case "214":
                    description = "Informe seu documento.";
                    break;
                case "220":
                    description = "Informe seu banco emissor.";
                    break;
                case "221":
                    description = "Digite o nome e sobrenome.";
                    break;
                case "224":
                    description = "Digite o código de segurança.";
                    break;
                case "E301":
                    description = "Há algo de errado com número do cartão. Digite novamente.";
                    break;
                case "E302":
                    description = "Confira o código de segurança.";
                    break;
                case "316":
                    description = "Por favor, digite um nome válido.";
                    break;
                case "322":
                    description = "Confira o tipo de documento.";
                    break;
                case "323":
                    description = "Confira o nome no cartão.";
                    break;
                case "324":
                    description = "Confira seu documento.";
                    break;
                case "325":
                    description = "Confira o mês de validade.";
                    break;
                case "326":
                    description = "Confira o ano de validade.";
                    break;

            }

            $(".modalErrors").append('<span><i class="fas fa-exclamation"></i> ' + description + '</span><br/>');

        }

        function sdkResponseHandler(status, response) {

            $('#preloader').fadeOut('slow', function () {
                $(this).remove();
            });

            if (status != 200 && status != 201) {

                $('#alertCard').html('<div class="alert alert-danger">Verifique os dados do seu Cartão</div>');

                for (i in response.cause) {
                    retornoErro(response.cause[i].code);
                }

                console.log(response);
                openModalErro();
            } else {
                openModal();
                $('#alertCard').html('<div class="alert alert-success" role="alert">Processando Pagamento...</div>');
                var form = document.querySelector('#pay');

                $('#cliente :input').not(':submit').clone().hide().appendTo('#pay');

                var card = document.createElement('input');
                card.setAttribute('name', "token");
                card.setAttribute('type', "hidden");
                card.setAttribute('value', response.id);
                form.appendChild(card);
                doSubmit = true;
                form.submit();
            }
        };

        // addEvent(document.querySelector('#pay'), 'submit', doPay);

        addEvent(document.querySelector('#payBol'), 'submit', doPayBol);

        function doPayBol(event) {

            event.preventDefault();

            openModal();
            var form = document.querySelector('#payBol');
            $('#cliente :input').not(':submit').clone().hide().appendTo('#payBol');
            doSubmit = true;
            $('#paymentMethodId').val('banking_billet')
            form.submit();
        }


        $("#docCli").mask("999.999.999-99");
        $("#docCliBol").mask("999.999.999-99");
        $("#phone").mask("(99) 999999999");

        function openModal() {
            if (!$("#modalProcessando").parent().is('body'))
                $("#modalProcessando").appendTo("body");
            $('#modalProcessando').modal('show')
        }

        function openModalErro(infoError) {
            if (!$("#modalError").parent().is('body'))
                $("#modalError").appendTo("body");

            $('#modalError').modal('show');
        }

        @if($errors->any())
        openModalErro();
        @endif

    </script>
@endpush
