@extends('plataforma.templates.template') 

@section('styles')
<link rel="stylesheet" href="{{asset('dist/css/tabs.css')}}">

@endsection
@section('content')

<div class="row">
                
                {!! \App\Helpers\TabsHelper::openBoxTabs() !!}
                <!-- Nav tabs -->
                {!! \App\Helpers\TabsHelper::openHeader(2, true) !!}
                {!! \App\Helpers\TabsHelper::openContent() !!}
                <!-- Tab panes -->

                    <div class="row">
                            <div class="col-sm-9">
                                <h4 class="text-left">Lista de Checkouts</h4>
                                <small>Aqui você pode criar diversos modelos de checkout para testar qual apresenta uma maior conversão e realizar os seus testes.</small>
                            </div>
                    </div>
                    <hr>
                    {{ Form::open() }}

                        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
                        {!! Form::label('plan_id', 'Planos:', ['for' => 'plan_id' ]) !!}
                        {!! Form::select('plan_id',  $plans, ($planId ? $planId : null) , ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione um plano --', 'id' => 'plan_id']) !!}
                        </div>
                        
                    {{ Form::close() }}

                        <div class="table-responsive">
                            <a href="{{route('product.checkout.add', [app('request')->product->id])}}" type="button" class="btn btn-success pull-right" style="margin: 5px 5px"><i class="fa fa-plus"></i> Novo </a>
                            
                            <table class="table" id="tableCheckouts">
                            <thead class="thead-light">
                                <tr>
                                <th scope="col">Código</th>
                                <th scope="col">Nome do checkout</th>
                                <th scope="col">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($checkouts as $checkout)
                                    <tr>
                                        <td>{{$checkout->code}}</td>
                                        <td>{{$checkout->desc}}</td>
                                        <td>
                                            {!! Form::open(['route' => ['product.checkout.delete', app('request')->product->id, $checkout->code], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                                    <a href="{{route('product.checkout.edit', [app('request')->product->id, $checkout->code])}}" class="btn btn-primary btn-sm" rel="nofollow"><i class="fa fa-pencil"></i> Editar</a>
                                                    <button class="btn btn-danger btn-sm"><i class="fa fa-trash"  onclick="return confirm('Are you sure you want to rollback deletion of candidate table?')"></i> Excluir</button>
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>


                {!! \App\Helpers\TabsHelper::closeContent() !!}
                {!! \App\Helpers\TabsHelper::closeHeader() !!}
                {!! \App\Helpers\TabsHelper::closeBoxTabs() !!}
</div>

@endsection

@section('scripts')

<script>
    
    function validate(form) {
        var valid = confirm('Você tem certeza que deseja excluir?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


    $("#plan_id").on('change', function() {

        var url = {!! json_encode(url('product/' . app('request')->product->id . '/checkouts/search/')) !!} + '/' + $(this).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
          url : url,
          type : 'GET', 
          data: {
            "_token": "{{ csrf_token() }}",
          },
          beforeSend : function(){
              //$('#spinnerModal').modal("show");
          }
        })
        .done(function(msg){
            $('#tableCheckouts tbody').empty();
            var trs = '';
            Object.keys(msg).forEach(function(k){
                var tr = '<tr>';
                var td = '<td>' + msg[k].code + '</td>';
                
                td += '<td>' + msg[k].desc + '</td>'
                
                td  += '<td>';

                td  += '<form action="' + {!! json_encode(url('product/' . app('request')->product->id . '/checkouts/delete/')) !!} + '/' + msg[k].code + '" onSubmit="return validate(this)"  method="POST">';
                td  += '<input type="hidden" name="_method" value="delete" />';
                td  += '<input type="hidden" name="_token" value="' + {!! json_encode(csrf_token()) !!} + '"/>';
                td  += '<a href=" ' + {!! json_encode(url('product/'. app('request')->product->id . '/checkouts/edit/')) !!}  + '/' + msg[k].code + '" class="btn btn-primary btn-sm" rel="nofollow"><i class="fa fa-pencil"></i> Editar</a>&nbsp';
                td  += '<button class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Excluir</button>';
                td  += '</form>'
                
                td += '</td>';

                tr += td + '</tr>';

                trs += tr;
            });

            $('#tableCheckouts').find('tbody').append(trs);

        })
        .fail(function(jqXHR, textStatus, msg){
            $('#tableCheckouts tbody').empty();
        }); 
    });

    function validate(form) {
        var valid = confirm('Você tem certeza que deseja excluir?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


</script>
@endsection