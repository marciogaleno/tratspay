@extends('plataforma.templates.checkout')
@section('content')

    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="clear-fix ">
                        <div class="float-left">
                            <img src="{{asset('assets/images/clock.png')}}" class="img-fluid">
                        </div>
                        <div class="float-left pl-2">
                            <div id="the-final-countdown">
                                <span>35:53:29</span>
                                <p style="padding-left:0px;">
                                    <small>HORAS</small>
                                    <small>MIN</small>
                                    <small>SEG</small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="media right-text">
                        <div class="media-left">
                            <h3>Aviso:</h3>
                        </div>
                        <div class="media-body pl-2">
                            <p>Devido à grande demanda popular na TV, nossas lojas estão tendo dificuldades em manter
                                seu estoque. À partir de hoje (18/3/2019)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--/HEADER-->
    <!--MAIN-->
    <main>
        <!--BANNER AREA-->
        <section class="banner-area">
            <div class="container-fluid">
                <div class="banner-area-inner banner-area-inner2">
                    <div class="banner-img text-center">
                        <div class="row">
                            <div class="col-md-12"><img src="{{asset('assets/images/page2banner.png')}}"
                                                        class="img-fluid rounded" alt="Banner Image"></div>
                        </div>
                    </div>
                    <div class="banner-gaurantee banner-gaurante-page2">
                        <div class="row">
                            <div class="col-md-6 mt-2">
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{asset('assets/images/100.html')}}" class="img-fluid" alt="100%">
                                    </div>
                                    <div class="media-body pl-3">
                                        <p>Nós garantimos: se ao final da compra você não ficar satisfeito, devolvemos o
                                            seu dinheiro.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mt-2">
                                <div class="banner-area-text-box">
                                    <p>Estoque: <span class="green">Disponivel</span></p>
                                    <p>Risco de Esgotar: <span class="red">ALTO</span></p>
                                    <p>Preços válidos até às <span class="black">23h59 de 01/04/2019</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="banner-icon-area banner-icon-area-page2">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{asset('assets/images/icon1.png')}}" class="img-fluid" alt="Icon 1">
                                    </div>
                                    <div class="media-body">
                                        <p>ENTREGA PARA TODO O BRASIL</p>
                                        <span>VIA CORREIOS</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{asset('assets/images/icon2.png')}}" class="img-fluid pt-2"
                                             alt="Icon 1">
                                    </div>
                                    <div class="media-body">
                                        <p>PAGUE COM CARTÃO DE CRÉDITO</p>
                                        <span>EM ATÉ 12X</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{asset('assets/images/icon3.png')}}" class="img-fluid" alt="Icon 1">
                                    </div>
                                    <div class="media-body">
                                        <p>SITE SEGURO</p>
                                        <span>PODE CONFIAR</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="media">
                                    <div class="media-left">
                                        <img src="{{asset('assets/images/icon4.png')}}" class="img-fluid" alt="Icon 1">
                                    </div>
                                    <div class="media-body">
                                        <p>LIGUE PARA NÓS</p>
                                        <span>(11) 3280-4677</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/BANNER AREA-->

        <!--FORM AREA-->
        <section class="form-area form-area-page2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-side">
                            <div class="form-side-header">
                                <h2>cadastro</h2>
                            </div>
                            <div class="form-side-inner">
                                    <form id="cliente">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Nome Completo</label>
                                            {!! Form::text('name', old('name'),$attributes = $errors->has('name') ? ['class' => 'form-control is-invalid' , 'id'=>'name'] : ['class' => 'form-control', 'id'=>'name']) !!}
                                        </div>
                                        @if($checkout->request_address OR $checkout->pay_card)
                                            <div class="col-md-4">
                                                <label>Tipo</label>
                                                <select name="docTypeCli" id="docTypeCli" class="form-control ">
                                                    <option value="cpf">CPF</option>
                                                    <option value="cnpj">CNPJ</option>
                                                </select>
                                            </div>
                                            <div class="col-md-8">
                                                <label id="checkCpf">CPF/CNPJ</label>
                                                {!! Form::text('docCli', old('docCli'),$attributes = $errors->has('docCli') ? ['class' => 'form-control is-invalid', 'id'=>'docCli'] : ['class' => 'form-control', 'id'=>'docCli']) !!}
                                            </div>
                                        @endif

                                        <div class="@php echo ($checkout->request_phone)? "col-md-6" : "col-md-12" @endphp ">
                                            <label>E-mail</label>
                                            {!! Form::text('email', old('email'),$attributes = $errors->has('email') ? ['class' => 'form-control is-invalid', 'id'=>'email'] : ['class' => 'form-control', 'id'=>'email']) !!}
                                        </div>

                                        @if($checkout->request_phone)
                                            <div class="col-md-6">
                                                <label>Telefone</label>
                                                {!! Form::text('phone', old('phone'),$attributes = $errors->has('phone') ? ['class' => 'form-control is-invalid phone', 'id'=>'phone'] : ['class' => 'form-control phone', 'id'=>'phone']) !!}
                                            </div>
                                        @endif
                                    </div>

                                    @if($checkout->request_address)
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>CEP</label>
                                                {!! Form::text('zipcode', old('zipcode'),$attributes = $errors->has('zipcode') ? ['class' => 'form-control is-invalid cep', 'id'=>'zipcode'] : ['class' => 'form-control cep', 'id'=>'zipcode']) !!}
                                            </div>

                                            <div class="col-md-4">
                                                <label style="visibility: hidden">Buscar</label>
                                                <button class="btn btn-info" type="button" id="btnPostalCode">Buscar
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Endereço</label>
                                                {!! Form::text('address', old('address'),$attributes = $errors->has('address') ? ['class' => 'form-control is-invalid', 'id'=>'address'] : ['class' => 'form-control', 'id'=>'address']) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label>Número</label>
                                                {!! Form::text('address_number', old('address_number'),$attributes = $errors->has('address_number') ? ['class' => 'form-control is-invalid', 'id'=>'address_number'] : ['class' => 'form-control', 'id'=>'address_number']) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Complemento</label>
                                                {!! Form::text('address_complement', old('address_complement'),$attributes = $errors->has('address_complement') ? ['class' => 'form-control is-invalid', 'id'=>'address_complement'] : ['class' => 'form-control', 'id'=>'address_complement']) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label>Bairro</label>
                                                {!! Form::text('address_district', old('address_district'),$attributes = $errors->has('address_district') ? ['class' => 'form-control is-invalid', 'id'=>'address_district'] : ['class' => 'form-control', 'id'=>'address_district']) !!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Município</label>
                                                {!! Form::text('address_city', old('address_city'),$attributes = $errors->has('address_city') ? ['class' => 'form-control is-invalid', 'id'=>'address_city'] : ['class' => 'form-control', 'id'=>'address_city']) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label>Estado</label>
                                                {!! Form::select('address_state', $states,'SC', ['class' => 'form-control', 'id' =>'address_state', 'id'=>'address_state']) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label>País</label>
                                                {!! Form::select('address_country', ['BR'=>'Brasil'],'BR', ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                    @endif

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-side">
                            <div class="form-side-header">
                                <h2>RESUMO DA COMPRA</h2>
                            </div>
                            <div class="total-table-inner">
                                <div class="clear-fix pb-5">
                                    <div class="float-left">
                                        <p>Produto:</p>
                                        <span>{{ $checkout->planChk->productChk->name }}</span>
                                    </div>
                                    <div class="float-right">
                                        <p>Valor:</p>
                                        <span>RS {{ $checkout->planChk->price }}</span>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left">
                                        <p>Tipo:</p>
                                        <p>Preço:</p>
                                        <p>Frete:</p>
                                        <p>Desconto:</p>
                                    </div>
                                    <div class="media-body pl-3">
                                        <p class="text-bold"> {{ $checkout->planChk->productChk->categorieChk->desc }}</p>
                                        <p class="text-bold"> {{ $checkout->planChk->price }}</p>
                                        <p class="text-bold"> Grátis</p>
                                        <p class="text-bold"> {{ $checkout->planChk->price }}</p>
                                    </div>
                                </div>

                                <div class="form-side-footer">
                                    <div class="clear-fix text-right">
                                        <h3>Total R$ {{ $checkout->planChk->price }}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr/>

                        <div class="form-side">
                            <div class="form-side-header">
                                <h2>FORMAS DE PAGAMENTO</h2>
                            </div>
                            <div class="form-side-inner">

                                <div class="row">

                                    @if($errors->any())
                                        <div class="col-md-12 text-center">
                                            <div class="alert alert-danger">
                                                {{$errors->first()}}
                                            </div>
                                        </div>
                                    @endif

                                    <div class="col-md-12 text-center" id="alertCard"></div>
                                    @if($checkout->pay_card)
                                        <div class="col-md-12 text-center">
                                            <img src="{{asset('assets/images/cc1.png')}}" class="img-fluid"
                                                 alt="CC">
                                            <img src="{{asset('assets/images/cc2.png')}}" class="img-fluid"
                                                 alt="CC">
                                            <img src="{{asset('assets/images/cc3.png')}}" class="img-fluid"
                                                 alt="CC">
                                            <img src="{{asset('assets/images/cc4.png')}}" class="img-fluid"
                                                 alt="CC">
                                            <img src="{{asset('assets/images/cc5.png')}}" class="img-fluid"
                                                 alt="CC">
                                            <img src="{{asset('assets/images/cc6.png')}}" class="img-fluid"
                                                 alt="CC">
                                            @if(!$checkout->pay_ticket)
                                                <br/><br/>
                                            @endif
                                        </div>
                                    @endif

                                </div>

                                @if($checkout->pay_card && $checkout->pay_ticket)
                                    <div class="row mt-3 mb-3">
                                        <div class="col-md-7">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio1" name="customRadio"
                                                       onchange="selectPay('PayCard');"
                                                       class="custom-control-input" checked="checked">
                                                <label class="custom-control-label" for="customRadio1">Cartão de
                                                    Crédito</label>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="customRadio2" name="customRadio"
                                                       onchange="selectPay('PayBol');"
                                                       class="custom-control-input">
                                                <label class="custom-control-label"
                                                       for="customRadio2">Boleto</label>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($checkout->pay_card)
                                    <div style="display: none" id="blockCard">
                                        <form action="{{route('checkout.payment',$checkout->code)}}" method="post"
                                              id="pay"
                                              name="pay">
                                            {!! Form::token() !!}
                                            {!! Form::hidden('traking_code', $trats_id) !!}

                                            <div class="row">
                                                <div class="col-md-12">

                                                    <label for="cardNumber">Número do Cartão</label>
                                                    <input class="form-control" type="text" id="cardNumber"
                                                           data-checkout="cardNumber"
                                                           placeholder="" onselectstart="return false"
                                                           onpaste="return false" onCopy="return false"
                                                           onCut="return false"
                                                           onDrag="return false" onDrop="return false"
                                                           autocomplete=off/>
                                                    <div class="imgCard" id="imgCard"></div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 pr-0">
                                                    <label>Validade</label>
                                                    <select class="form-control" id="cardExpirationMonth"
                                                            data-checkout="cardExpirationMonth" placeholder=""
                                                            onselectstart="return false" onpaste="return false"
                                                            onCopy="return false"
                                                            onCut="return false" onDrag="return false"
                                                            onDrop="return false"
                                                            autocomplete=off>
                                                        @for($x=1;$x<=12;$x++)
                                                            <option value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-md-3 pr-0">
                                                    <label style="visibility: hidden;">eqw</label>
                                                    <select class="form-control" id="cardExpirationYear"
                                                            data-checkout="cardExpirationYear" placeholder=""
                                                            onselectstart="return false" onpaste="return false"
                                                            onCopy="return false"
                                                            onCut="return false" onDrag="return false"
                                                            onDrop="return false"
                                                            autocomplete=off>
                                                        @for($x=date('Y');$x<=(date('Y')+7);$x++)
                                                            <option value="{{$x}}">{{$x}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Cod. de Segurança</label>
                                                    <input class="form-control" type="text" id="securityCode"
                                                           data-checkout="securityCode" placeholder=""
                                                           onselectstart="return false" onpaste="return false"
                                                           onCopy="return false"
                                                           onCut="return false" onDrag="return false"
                                                           onDrop="return false"
                                                           autocomplete=off/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="cardholderName">Nome no Cartão:</label>
                                                    <input class="form-control" type="text" id="cardholderName"
                                                           data-checkout="cardholderName" placeholder=""/>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">

                                                    <label for="docType">Documento:</label>
                                                    <select class="form-control" id="docType"
                                                            data-checkout="docType"></select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="docNumber">Documento Titular:</label>
                                                    <input class="form-control" type="text" id="docNumber" name="docNumber"
                                                           data-checkout="docNumber"
                                                           placeholder=""/>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="installmentsOption">Parcelas:</label>

                                                    <select class="form-control" id="installmentsOption"
                                                            name="installmentsOption">
                                                        @foreach($parcelas as $num => $parcela)
                                                            <option value="{{$num}}">
                                                                {{$num}} X de
                                                                R$ {!! number_format($parcela,2,",",".")!!}

                                                            </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <hr/>
                                                    <input type="hidden" name="paymentMethodId"/>
                                                    <input class="btn btn-block " type="submit" value="COMPRAR AGORA"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                                @if($checkout->pay_ticket)
                                    <div style="display: none" id="blockBol">
                                        <div class="row" style="font-size: 13px;">
                                            <div class="col-md-4 text-center"><br/>
                                                <img src="{{asset('assets/images/boleto.png')}}" class="img-fluid"
                                                     alt="Boleto">
                                            </div>
                                            <div class="col-md-8">
                                                Atenção:<br/>
                                                1 - Boleto (somente à vista)<br/>
                                                2 - Pagamentos com Boleto Bancário levam até 3 dias úteis para serem
                                                compensados e então terem os produtos liberados<br/>
                                                3 - Depois do pagamento, fique atento ao seu e-mail para receber os
                                                dados de acesso ao produto (verifique também a caixa de SPAM)<br/>
                                            </div>
                                        </div>
                                        <form action="{{route('checkout.payment',$checkout->code)}}" method="post"
                                              id="payBol" name="payBol">

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <hr/>
                                                    <input type="hidden" name="paymentMethodId" value="bolbradesco"/>
                                                    <input class="btn btn-block " type="submit"
                                                           value="COMPRAR AGORA"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/FORM AREA-->


        <!-- Modal -->
        <div class="modal fade" id="modalProcessando" data-backdrop="false" tabindex="-1" role="dialog"
             aria-labelledby="modalProcessandoLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <h3 class="text-success">Processando Pagamento...</h3>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-success"
                                 role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 100%"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modalError" data-backdrop="false" tabindex="-1" role="dialog"
             aria-labelledby="modalErrorLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <h5 class="text-danger">Falha no Processamento.</h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-danger"
                                 role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 100%"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <!--/MAIN-->

    <!--FOOTER-->
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-img">
                        <img src="{{asset('assets/images/footerlogo.jpg')}}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <p>TERMOS E CONDIÇÕES &nbsp;&nbsp;&nbsp;&nbsp;POLÍTICA DE PRIVACIDADE</p>
                    <span>Traspay tda Me | CNPJ 00.000.000/0000-00<br>(11) 99999-9999 | info@colinapay.com.br</span>
                </div>
            </div>
        </div>
    </footer>


    <!--/FOOTER-->

@endsection

@push('scripts')

    <script>
        $('#docTypeCli').change(function () {
            $("#docCli").val("");
            if (this.value == 'cpf') {
                $("#docCli").mask("999.999.999-99");
            } else {
                $("#docCli").mask("99.999.999/9999-99");
            }
        });

        $('#docCli').blur(function () {
            var tipo = $('#docTypeCli').val();
            var teste = ValidarCPF(this.value);

            if (tipo == 'cpf') {
                if (teste) {
                    $('#checkCpf').html('<span class="text-success">CPF ok!</span>');
                } else {
                    $('#checkCpf').html('<span class="text-danger">CPF Inválido</span>');
                }

            } else {
                $('#checkCpf').html('CNPJ');
            }
        });

        $('.cep').blur(function (){
            buscaCep();
        });

        function validaDados() {

            var r = true;

            var entradas = new Array("name", "docCli", "email", "dtFim", "phone", "zipcode", "address", "address_number", "address_district", "address_city");
            for (var x = 0; x <= entradas.length; x++) {
                var item = entradas[x];
                if (document.getElementById(item)) {
                    var valor = document.getElementById(item).value;
                    if(valor.length == "")
                    {
                        $("#"+item).addClass("is-invalid");
                        $("#"+item).removeClass("is-valid");
                        $("#"+item).focus();
                        r = false;
                    }else{
                        $("#"+item).removeClass("is-invalid");
                        $("#"+item).addClass("is-valid");
                    }
                }
            }

            if (r)
                return true;

            return false;

        }


        function checkCpf() {
            var tipo = $('#docTypeCli').val();
            var teste = ValidarCPF($('#docCli').val());

            if (tipo == 'cpf') {
                if (!teste) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        }


        function ValidarCPF(cpf) {
            exp = /\.|\-/g;
            cpf = cpf.toString().replace(exp, "");
            if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
                return false;
            add = 0;
            for (i = 0; i < 9; i++)
                add += parseInt(cpf.charAt(i)) * (10 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(9)))
                return false;
            add = 0;
            for (i = 0; i < 10; i++)
                add += parseInt(cpf.charAt(i)) * (11 - i);
            rev = 11 - (add % 11);
            if (rev == 10 || rev == 11)
                rev = 0;
            if (rev != parseInt(cpf.charAt(10)))
                return false;

            return true;
        }

        function addEvent(el, eventName, handler) {
            if (el.addEventListener) {
                el.addEventListener(eventName, handler);
            } else {
                el.attachEvent('on' + eventName, function () {
                    handler.call(el);
                });
            }
        };

        @if($checkout->pay_card)
        selectPay('PayCard');

        function getBin() {
            var ccNumber = document.querySelector('input[data-checkout="cardNumber"]');
            return ccNumber.value.replace(/[ .-]/g, '').slice(0, 6);
        };

        function guessingPaymentMethod(event) {
            var bin = getBin();
            $('#imgCard').html('');

            if (event.type == "keyup") {
                if (bin.length >= 6) {
                    Mercadopago.getPaymentMethod({
                        "bin": bin
                    }, setPaymentMethodInfo);

                    /**
                     Mercadopago.getInstallments({
                        "bin": getBin(),
                        "amount": {{ $checkout->planChk->getOriginal('price') }}
                    }, setInstallmentInfo);
                     **/
                }
            } else {
                setTimeout(function () {
                    if (bin.length >= 6) {
                        Mercadopago.getPaymentMethod({
                            "bin": bin
                        }, setPaymentMethodInfo);

                        /**
                         Mercadopago.getInstallments({
                            "bin": getBin(),
                            "amount": {{ $checkout->planChk->getOriginal('price') }}
                        }, setInstallmentInfo);
                         **/
                    }
                }, 100);
            }
        };

        function setInstallmentInfo(status, installments) {
            console.log(installments[0].payer_costs);
            var html_options = "";
            for (i = 0; installments[0].payer_costs && i < installments[0].payer_costs.length; i++) {
                html_options += "<option value='" + installments[0].payer_costs[i].installments + "'>" + installments[0].payer_costs[i].recommended_message + "</option>";
            }
            ;
            $("#installmentsOption").html(html_options);
        };

        function setPaymentMethodInfo(status, response) {


            if (status == 200) {

                $('#imgCard').html('<img src="' + response[0].thumbnail + '">');
                // do somethings ex: show logo of the payment method
                var form = document.querySelector('#pay');

                if (document.querySelector("input[name=paymentMethodId]") == null) {
                    var paymentMethod = document.createElement('input');
                    paymentMethod.setAttribute('name', "paymentMethodId");
                    paymentMethod.setAttribute('type', "hidden");
                    paymentMethod.setAttribute('value', response[0].id);

                    form.appendChild(paymentMethod);
                } else {
                    document.querySelector("input[name=paymentMethodId]").value = response[0].id;
                }
            }
        };

        addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'keyup', guessingPaymentMethod);
        addEvent(document.querySelector('input[data-checkout="cardNumber"]'), 'change', guessingPaymentMethod);


        function sdkResponseHandler(status, response) {

            $('#preloader').fadeOut('slow', function () {
                $(this).remove();
            });

            if (status != 200 && status != 201) {

                $('#alertCard').html('<div class="alert alert-danger">Verifique os dados do seu Cartão</div>');

                openModalErro();
            } else {
                openModal();
                $('#alertCard').html('<div class="alert alert-success" role="alert">Processando Pagamento...</div>');
                var form = document.querySelector('#pay');

                $('#cliente :input').not(':submit').clone().hide().appendTo('#pay');

                var card = document.createElement('input');
                card.setAttribute('name', "token");
                card.setAttribute('type', "hidden");
                card.setAttribute('value', response.id);
                form.appendChild(card);
                doSubmit = true;
                form.submit();
            }
        };


        doSubmit = false;
        addEvent(document.querySelector('#pay'), 'submit', doPay);


        function doPay(event) {

            event.preventDefault();

            if (!validaDados())
                return false;

            if (!doSubmit) {
                var $form = document.querySelector('#pay');

                Mercadopago.createToken($form, sdkResponseHandler); // The function "sdkResponseHandler" is defined below

                return false;
            }
        }

        @endif

        addEvent(document.querySelector('#payBol'), 'submit', doPayBol);

        function doPayBol(event) {

            event.preventDefault();

            if (!validaDados()) {
                return false;
            }

            openModal();
            var form = document.querySelector('#payBol');
            $('#cliente :input').not(':submit').clone().hide().appendTo('#payBol');
            doSubmit = true;
            form.submit();
        }

        function selectPay(block) {
            if (block == 'PayBol') {
                $('#blockBol').show();
                $('#blockCard').hide();
            } else {
                $('#blockCard').show();
                $('#blockBol').hide();
            }
        }

        @if(!$checkout->pay_card && $checkout->pay_ticket)
        selectPay('PayBol');

        @endif

        function buscaCep() {

            var cep = $(".cep").val().replace(/\D/g, '');
            if (cep != "") {
                var validacep = /^[0-9]{8}$/;
                if (validacep.test(cep)) {
                    $("#address").val("consultando");
                    $("#address_city").val("consultando");

                    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                        console.log(dados);

                        if (!("erro" in dados)) {
                            $("#address").val(dados.logradouro);
                            $("#address_city").val(dados.localidade);
                            $("#address_district").val(dados.bairro);
                            $("#address_complement").val(dados.complemento);
                            $("#address_state").val(dados.uf).change();
                        } else {
                            console.log("CEP não encontrado.");
                        }
                    });
                } else {
                    console.log("Formato de CEP inválido.");
                }
            }
        }

        $("#btnPostalCode").click(function () {
            buscaCep();
        });

        $("#docCli").mask("999.999.999-99");
        $("#phone").mask("(99) 999999999");

        function openModal() {
            if (!$("#modalProcessando").parent().is('body'))
                $("#modalProcessando").appendTo("body");
            $('#modalProcessando').modal('show')
        }

        function openModalErro(infoError) {
            if (!$("#modalError").parent().is('body'))
                $("#modalError").appendTo("body");

            $('#modalError').modal('show');
        }

        @if($errors->any())
        openModalErro();
        @endif

    </script>
@endpush
