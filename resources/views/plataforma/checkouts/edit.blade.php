@extends('plataforma.templates.tabs') 
@section('styles')
<link rel="stylesheet" href="{{asset('dist/css/tabs.css')}}">
<link rel="stylesheet" href="{{asset('assets/plugins/dropify/dropify.min.css')}}">
@endsection

@section('contentTab')

    <div class="row">
        <div class="col-sm-9">
            <h4 class="text-left">Editar Checkout</h4>
        </div>
        <div class="col-sm-3">
            <a  href="{{route('product.checkout.view', [app('request')->product->id])}}" class="btn btn-warning waves-effect waves-light m-1  text-right"> 
                <i class="fa fa-arrow-left"></i>
                <span>Voltar</span>
            </a>
        </div>
    </div>
    
    <hr>

        {{ Form::model($checkout, ['route' => ['product.checkout.update', app('request')->product->id, $checkout->code], 'method' => 'put', 'enctype' => "multipart/form-data"]) }}

        <div class="card-body">
            <div class="form-group has-feedback {{ $errors->has('desc') ? 'has-error' : '' }}">
                {!! Form::label('code', 'Código: ' . $checkout->code, ['for' => 'desc' ]) !!}
            </div>
            <div class="form-group has-feedback {{ $errors->has('desc') ? 'has-error' : '' }}">
                {!! Form::label('desc', 'Nome do Checkout :', ['for' => 'desc' ]) !!}
                {!! Form::text('desc', null, ['class' => 'form-control']) !!}

                @if ($errors->has('desc'))
                <label class="control-label" for="desc">
                    {{$errors->first('desc')}}
                </label>
                @endif 
            </div>

            <div class="card">
                <div class="card-header text-white bg-dark">Este checkout aceita pagamentos via:</div>
                <div class="card-body">
                    <div class="row m-t-2">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::checkbox('pay_card') }}
                                <span class="custom-control-description ml-0"><i class="fa fa-credit-card"></i> Cartão</span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ Form::checkbox('pay_ticket') }}
                                <span class="custom-control-description ml-0"><i class="fa fa-file-text-o"></i> Boleto</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header text-white bg-dark">Dados adicionais do checkout</div>
                <div class="card-body">
                    <div class="form-group">
                        {!! Form::label('invoice_desc', 'Descrição na fatura :', ['for' => 'invoice description', 'style' => "margin-bottom:0px" ]) !!}
                        <br><small style="font-size: 9px">Informação que será exibida na fatura do cartão ou do Boleto</small>
                        {!! Form::text('invoice_desc', null, ['class' => 'form-control']) !!}

                        @if ($errors->has('invoice description'))
                        <label class="error" for="invoice description">
                            {{$errors->first('invoice description')}}
                        </label>
                        @endif 
                    </div>
                    <!--
                    <div class="form-group">
                        {!! Form::label('template', 'Template do Checkout :', ['for' => 'template' ]) !!}
                        {!! Form::select('template', [1 => "Modelo 1", 2 => "Modelo 2"], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('template'))
                            <label class="error" for="template">
                                {{$errors->first('template')}}
                            </label>
                        @endif
                    </div>

                    -->

                    <div class="form-group">
                        {!! Form::label('days_expir_ticket', 'Dias até o Boleto Vencer:', ['for' => 'invoice description' ]) !!}
                        {!! Form::select('days_expir_ticket', [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('days_expir_ticket'))
                        <label class="error" for="days_expir_ticket">
                            {{$errors->first('days_expir_ticket')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('request_address', 'Solicitar endereço: ', ['for' => 'request_address' ]) !!}
                        {!! Form::select('request_address', [0 => 'Não', 1 => 'Sim'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('request_address'))
                        <label class="error" for="request_address">
                            {{$errors->first('request_address')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('request_cpf', 'Solicitar CPF: ', ['for' => 'request_cpf' ]) !!}
                        {!! Form::select('request_cpf', [0 => 'Não', 1 => 'Sim'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('request_cpf'))
                        <label class="error" for="request_cpf">
                            {{$errors->first('request_cpf')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                        {!! Form::label('request_phone', 'Solicitar telefone: ', ['for' => 'request_phone' ]) !!}
                        {!! Form::select('request_phone', [0 => 'Não', 1 => 'Sim'], null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}

                        @if ($errors->has('request_phone'))
                        <label class="error" for="request_phone">
                            {{$errors->first('request_phone')}}
                        </label>
                        @endif 
                    </div>

                    <div class="form-group">
                            {!! Form::label('url_page_thank', 'URL página de obrigado via boleto:', ['for' => 'url_page_thank' ]) !!}
                            {!! Form::text('url_page_thank', null, ['class' => 'form-control']) !!}

                            @if ($errors->has('url_page_thank'))
                            <label class="error" for="url_page_thank">
                                {{$errors->first('url_page_thank')}}
                            </label>
                            @endif 
                        </div>

                        <div class="form-group">
                            {!! Form::label('url_page_print_ticket', 'URL página de obrigado via cartão :', ['for' => 'url_page_print_ticket' ]) !!}
                            {!! Form::text('url_page_print_ticket', null, ['class' => 'form-control']) !!}

                            @if ($errors->has('url_page_print_ticket'))
                            <label class="error" for="url_page_print_ticket">
                                {{$errors->first('url_page_print_ticket')}}
                            </label>
                            @endif 
                        </div>

                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-5 col-md-5" style="float: left">
                    {!! Form::label('banner_1', 'Banner Superior',['style'=>'margin-bottom:0px']) !!}
                    <br>
                    <small style="font-size: 10px">A imagem de topo deve conter no mínimo 888 pixels de largura e altura recomendada de 300 pixel.</small>
                    <input type="file" id="input-file-now" class="dropify1" name="banner_1" id="banner_1"
                           data-allowed-file-extensions="jpg jpeg png"
                           data-max-file-size="10M"/>
                    <br>
                </div>
                <div class="form-group col-sm-5 col-md-5" style="float: right">
                    {!! Form::label('banner_2', 'Banner Lateral:',['style'=>'margin-bottom:0px']) !!}
                    <br>
                    <small style="font-size: 10px">A imagem deve conter no mínimo 400 pixel de largura e 1087 de altura.</small>
                    <input type="file" id="input-file-now" class="dropify2" name="banner_2" id="banner_2"
                           data-allowed-file-extensions="jpg jpeg png"
                           data-max-file-size="10M"/>
                    <br>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <a href="{{route('product.checkout.view', [$checkout->plan->product->id])}}" class="btn btn-warning waves-effect waves-light m-1"> 
                        <i class="fa fa-arrow-left"></i>
                        <span>Voltar</span>
                    </a>
                    <button type="submit" class="btn btn-success waves-effect waves-light m-1"> 
                        <i class="fa fa-save"></i>
                        <span>Salvar</span>
                    </button>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
@endsection

@section('scripts')
    <script src="{{asset('assets/plugins/dropify/dropify.min.js')}}"></script>
    <script>
        var banner_1 = {!! json_encode(\App\Helpers\Helper::getPathFileS3('images', $checkout->banner_1)) !!}
        $('.dropify1').dropify({
            defaultFile: banner_1,
            messages: {
                'default': 'Selecione ou arraste e solte um arquivo aqui',
                'replace': 'Selecione ou arraste e solte um arquivo aqui',
                'remove':  'Remover',
                'error':   'Ooops, algume erro aconteceu.'
            },
            error: {
                'fileSize': 'O temanho do arquivo é maior que 1 GB).',
                'imageFormat': 'O formato da imagem não é permitido.'
            }

        });

        var banner_2 = {!! json_encode(\App\Helpers\Helper::getPathFileS3('images', $checkout->banner_2)) !!}
        $('.dropify2').dropify({
            defaultFile: banner_2,
            messages: {
                'default': 'Selecione ou arraste e solte um arquivo aqui',
                'replace': 'Selecione ou arraste e solte um arquivo aqui',
                'remove':  'Remover',
                'error':   'Ooops, algume erro aconteceu.'
            },
            error: {
                'fileSize': 'O temanho do arquivo é maior que 1 GB).',
                'imageFormat': 'O formato da imagem não é permitido.'
            }

        });
    </script>
@endsection