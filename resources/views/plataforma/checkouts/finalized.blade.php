@extends('plataforma.templates.checkout')
@section('content')


{{--    <div class="container-fluid fixed-top fixo-finalizado">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-6 text-left">--}}
{{--                <strong>Informações do seu Pagamento.</strong><br/>--}}
{{--                Total: R$ {!! number_format($sale->transaction_amount,2,",",".") !!}<br/>--}}
{{--                Status: {!! \App\Helpers\Helper::statusPaymentGerenciaNet($sale->status_gateway, true) !!}<br/>--}}
{{--                Detalhes: {!! $sale->status_detail_gateway !!}<br/>--}}
{{--                @if($sale->payment_type_id == 'credit_card')--}}
{{--                    Parcelas: {{ $sale->installments }} X<br/>--}}
{{--                @endif--}}

{{--            </div>--}}
{{--            <div class="col-md-6 text-right">--}}

{{--                @if($sale->payment_type_id == 'banking_billet')--}}
{{--                    Clique no botão abaixo para visualizar seu boleto.<br/><br/>--}}
{{--                    <a target="_blank" class="btn btn-success" href="{{$sale->external_resource_url }}">Visualizar--}}
{{--                        Boleto</a>--}}
{{--                @endif--}}

{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}

    <div class="container-fluid corpo-frame-finalizado">
        <div class="row">
            <div class="col-md-12" style="min-height:1000px;">
                @php $url = ($sale->payment_type_id == 'ticket')? $checkout->url_page_print_ticket : $checkout->url_page_thank @endphp
                @if(filter_var($url, FILTER_VALIDATE_URL))

                <iframe src="{{$url}}" width="100%" height="100%"></iframe>
                    @else
                    <h2 class="text-center">Obrigado por sua Compra!</h2>
                    <div class="col-md-12 text-right">

                        @if($sale->payment_type_id == 'banking_billet')
                            Clique no botão abaixo para visualizar seu boleto.<br/><br/>
                            <a target="_blank" class="btn btn-success" href="{{$sale->external_resource_url }}">Visualizar
                                Boleto</a>
                        @endif

                    </div>
                    @endif
            </div>
        </div>
    </div>
    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-img">
                        <img src="{{asset('assets/images/footerlogo.jpg')}}" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <p>TERMOS E CONDIÇÕES &nbsp;&nbsp;&nbsp;&nbsp;POLÍTICA DE PRIVACIDADE</p>
                    <span>Traspay tda Me | CNPJ 00.000.000/0000-00<br>(11) 99999-9999 | info@colinapay.com.br</span>
                </div>
            </div>
        </div>
    </footer>
    <!--/FOOTER-->

@endsection

@push('scripts')

@endpush
