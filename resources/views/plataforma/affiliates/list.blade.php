@extends('plataforma.templates.template')
@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-selection__choice{
            float: none !important;
            display: inline-block !important;
        }
    </style>
@endsection
@section('content')
    <div class="row pt-2 pb-2">
        <div class="col-sm-9">
            <h4 class="page-title">Minhas Afiliações</h4>

            </ol>
        </div>
        <div class="col-sm-3">
            <div class="btn-group float-sm-right">

                <span class="caret"></span>
                </button>
                <div class="dropdown-menu">

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header text-uppercase">Filtrar por produto</div>
                <div class="card-body">
                    <div class="col-md-12">
                        <form action="{{ route('afilliate.list') }}" method="GET" class="">
                            @csrf
                            <div class="col-lg-9" style="float:left">
                                <div class="form-group">
                                    {!! Form::select('product_name[]',$productsFilter, Illuminate\Support\Facades\Input::get('product_name'), ['class' => 'form-control', 'multiple'=> 'multiple','id'=> 'product_name']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3" style="float:left"><button class="btn btn-success btn-block" type="submit">Buscar</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @if ($products)

            @foreach ($products as $product)
                <div class="col-lg-3">
                    <div class="card">
                        @if ($product->image)
                            <img src="{{ \App\Helpers\Helper::getPathFileS3('images', $product->image)}}" class="card-img-top" alt="Card image cap">
                        @else
                            <img src="{{url('storage/products')}}" class="card-img-top" alt="Card image cap">
                        @endif

                        <div class="card-body">
                            <h5 class="card-title text-dark">{{$product->name}}</h5>
                        </div>
                        <ul class="list-group list-group-flush list shadow-none">
                            <li class="list-group-item d-flex justify-content-between align-items-center">Preço <span>{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">Quantidade de venda: <span style="font-weight: bold">{{$products->whereIn('transaction_mp.status', ['finished', 'completed'])->count()}}</span></li>
                        </ul>
                        <div class="card-body">
                            <a href="{{route('afilliate.detailsProduct', $product->id)}}" class="btn btn-success waves-effect waves-light m-1 card-link"><i class="fa fa-pencil-square-o""></i> Visualizar</a>
                        </div>
                    </div>
                </div>

            @endforeach
        @endif
    </div>


@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#product_name').select2({ width: 'resolve', placeholder:"Selecione os produtos"});
        });

    </script>
@stop