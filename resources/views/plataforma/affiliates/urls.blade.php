@extends('plataforma.templates.template')
@section('content')

    <div class="row">
        <!-- Nav tabs -->
        {!! \App\Helpers\TabsAffiliateHelper::openBoxTabs($product) !!}
        {!! \App\Helpers\TabsAffiliateHelper::openHeader($product, 1) !!}
        {!! \App\Helpers\TabsAffiliateHelper::openContent() !!}



        <div class="row">
            <div class="col-sm-9">
                <h4 class="text-left">Lista URLs para divulgação</h4>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header text-uppercase">URL Página de vendas</div>
                    <div class="card-body">
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                {!! Form::text('sale_page',  url('/r/'. $product->affiliatelogged[0]->pivot->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}
                                {!! Form::label('sale_page', 'URL Destino: ' . $product->salepage->url_page_sale , ['style' => 'TEXT-TRANSFORM: inherit']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">Cliques/Únicos</th>
                        <th class="text-center">Vendas</th>
                        <th class="text-center">CTR</th>
                    </tr>
                    </thead>
                    <tbody class="word-break"><tr>
                        <th colspan="3" class="blank">Suas divulgações</th>
                    </tr> <tr>
                        <td class="text-center cliques_seus">-</td>
                        <td class="text-center vendas_suas">-</td>
                        <td class="text-center ctr_seu">-</td>
                    </tr><tr>
                        <th colspan="3" class="blank">Divulgações dos afiliados</th>
                    </tr>
                    <tr>
                        <td class="text-center cliques">-</td>
                        <td class="text-center vendas">-</td>
                        <td class="text-center ctr">-</td>
                    </tr><tr>
                        <th colspan="3" class="blank">Total</th>
                    </tr>
                    <tr>
                        <td class="text-center tot_cliques">-</td>
                        <td class="text-center tot_vendas">-</td>
                        <td class="text-center tot_ctr">-</td>
                    </tr><tr><th colspan="3" class="blank text-center">
                            <button type="button" data-loading-text="Calculando..." class="btn btn-info btn-gradient btnVerNumUrls" data-chave="98d0c2f95351702ccfe83bf175ef6c70" data-plano="" data-link="" data-tipo="1">
                                <i class="zmdi zmdi-search-in-page"></i> Ver clicks/vendas
                            </button></th></tr></tbody>
                </table>
            </div>
        </div>
        {{--    <hr>--}}
        <div class="col-md-12">

            @foreach ($checkouts as $checkout)
                <div class="row" style="    border-top: 1px solid;
    border-bottom: 1px solid;
    border-color: #ccc6c6;
padding: 30px">
                    <div class="col-md-12" style="padding-bottom: 20px">
                        <div class="col-md-6" style="float: left">
                            <h4>{{ $checkout->desc}}</h4>
                            <div class="card">
                                <div class="card-header text-uppercase">URL para divulgação</div>
                                <div class="card-body">
                                    <div class="form-group has-feedback">
                                        <div class="form-group">
                                            {{--                            {!! Form::label('checkout', 'Checkout - ' . $checkout->desc . ':', ['for' => 'plan_id' ]) !!}--}}
                                            {!! Form::text('checkout',  url('/c/' . $checkout->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" style="float: right">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th class="text-center">Cliques/Únicos</th>
                                    <th class="text-center">Vendas</th>
                                    <th class="text-center">CTR</th>
                                </tr>
                                </thead>
                                <tbody class="word-break">
                                <tr>
                                    <th colspan="3" class="blank">Suas divulgações</th>
                                </tr>
                                <tr>
                                    <td class="text-center cliques_seus">-</td>
                                    <td class="text-center vendas_suas">-</td>
                                    <td class="text-center ctr_seu">-</td>
                                </tr>
                                <tr>
                                    <th colspan="3" class="blank">Divulgações dos afiliados</th>
                                </tr>
                                <tr>
                                    <td class="text-center cliques">-</td>
                                    <td class="text-center vendas">-</td>
                                    <td class="text-center ctr">-</td>
                                </tr>
                                <tr>
                                    <th colspan="3" class="blank">Total</th>
                                </tr>
                                <tr>
                                    <td class="text-center tot_cliques">-</td>
                                    <td class="text-center tot_vendas">-</td>
                                    <td class="text-center tot_ctr">-</td>
                                </tr>
                                <tr>
                                    <th colspan="3" class="blank text-center">
                                        <button type="button" data-loading-text="Calculando..." class="btn btn-info btn-gradient btnVerNumUrls" data-chave="98d0c2f95351702ccfe83bf175ef6c70" data-plano="" data-link="" data-tipo="1">
                                            <i class="zmdi zmdi-search-in-page"></i> Ver clicks/vendas
                                        </button>
                                    </th>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <hr>

        {{--                        <div class="form-group has-feedback">--}}
        {{--                            <div class="form-group">--}}
        {{--                                {!! Form::label('sale_page', 'Página de vendas :', ['for' => 'plan_id' ]) !!}--}}
        {{--                                {!! Form::text('sale_page',  url('/r/'. $product->affiliatelogged[0]->pivot->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}--}}
        {{--                            </div>--}}
        {{--                        </div>--}}

        {{--                        @foreach ($checkouts as $checkout)--}}
        {{--                        <div class="form-group has-feedback">--}}
        {{--                            <div class="form-group">--}}
        {{--                                {!! Form::label('checkout', 'Checkout - ' . $checkout->desc . ':', ['for' => 'plan_id' ]) !!}--}}
        {{--                                @if ( $product->affiliatelogged[0]->pivot->relation_type == 'producer')--}}
        {{--                                    {!! Form::text('checkout',  url('/chk/' . $checkout->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}--}}
        {{--                                @else --}}
        {{--                                    {!! Form::text('checkout',  url('/r/'. $product->affiliatelogged[0]->pivot->code .'/?c=' . $checkout->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}--}}
        {{--                                @endif--}}
        {{--                            </div>--}}
        {{--                        </div>                            --}}
        {{--                        @endforeach--}}

        {!! \App\Helpers\TabsAffiliateHelper::closeContent() !!}
        {!! \App\Helpers\TabsAffiliateHelper::closeHeader() !!}
        {!! \App\Helpers\TabsAffiliateHelper::closeBoxTabs() !!}

    </div>
@endsection