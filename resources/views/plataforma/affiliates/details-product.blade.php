@extends('plataforma.templates.template') 

@section('content')

<div class="row">
                <!-- Nav tabs -->
                {!! \App\Helpers\TabsAffiliateHelper::openBoxTabs($product) !!}
                {!! \App\Helpers\TabsAffiliateHelper::openHeader($product, 0) !!}
                {!! \App\Helpers\TabsAffiliateHelper::openContent() !!}

  
          <div class="row">

              <div class="col-md-3" style="">
                  <div class="card card-primary">
                      <img src="{{ \App\Helpers\Helper::getPathFileS3('images', $product->image) }}" class="card-img-top" alt="Card image cap">
                  </div>
              </div>

              <div class="col-md-9" style="">
                  <h4>{{$product->name}}</h4>
                  <p class="text-dark">

                  @if ($product->payment_type == 'SINGPRICE')
                      <h4 class="peq">Você recebe até: <span style="color:yellowgreen">R$ {{$valueComission}}</span> por venda </h4>
                  @else
                      Veja detalhes nos planos para ver comissões
                      @endif
                      </p>
                      <p><strong>Produtor:</strong><b><a href="{{route('user.perfil', $producer->id)}}">
                                  @if($producer->client && $producer->client->fantasia){{$producer->client->fantasia }}@else{{$producer->name}}
                                  @endif
                              </a></b></p>
                      <p><strong>E-mail de suporte:</strong>
                          @if($product->email_support == null)
                              Naõ definido
                          @else
                              {{ $product->email_support }}
                          @endif
                      </p>
                      <p><strong>Tipo de produto:</strong> {{ $product->product_type == 'V' ? 'Virtual' : 'Físico' }}</p>
                      <p><strong>Entrega do produto:</strong> {{$product->typeDeliverie->desc}} </p>
                      <p><strong>Segmento:</strong> {{$category->desc}} </p>
                      <p><strong>Tipo comissão:</strong> Último clique</p>
                      <p><strong>Validade do Cookie:</strong>
                          @if($product->configAffiliation->expiration_coockie == null)
                              Naõ definido
                          @else
                              {{  \App\Helpers\Helper::expirationCoockie($product->configAffiliation->expiration_coockie) }}
                          @endif
                      </p>
                      <p><strong>Valor do produto: R$</strong> {{ $product->plansWithoutGlobalScope[0]->price }}</p>
                      <p><strong>Garantia:</strong> {{$product->warranty}} dias</p>
                      <p><strong> Url do produto:</strong> <a href="{{$salePage->url_page_sale}}" target="_blank">{{$salePage->url_page_sale}}</a></p>
                      @if(count($checkouts))
                          <p><strong> Url de Checkout:</strong>
                              @foreach ($checkouts as $checkout)
                                  <a href="{{ url('/chk/' . $checkout->code)}}" target="_blank">{{url('/chk/' . $checkout->code)}}</a> <br>
                          @endforeach
                      @endif

          </div>

          <hr>

          <div class="card-body">
              <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="pill" href="#affiliation"><i class="fa fa-users"></i> <span class="hidden-xs">Afiliação</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="pill" href="#about"><i class="icon-user"></i> <span class="hidden-xs">Sobre</span></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="pill" href="#media"><i class="fa fa-picture-o"></i> <span class="hidden-xs">Divulgação</span></a>
                </li>
                @if ($product->payment_type == 'PLAN')
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="pill" href="#plans"><i class="fa fa-money"></i> <span class="hidden-xs">Planos</span></a>
                  </li>                      
                @endif
              </ul>
      
              <!-- Tab panes -->
              <div class="tab-content">
                  <div id="affiliation" class="container tab-pane active">
                    <p>{{$product->configAffiliationWithoutGlobalScope->desc_affiliate}}</p>
                  </div>

                  <div id="about" class="container tab-pane">
                    <p>{{$product->desc}}</p>
                  </div>

                  <div id="media" class="container tab-pane">
                      
                  </div>
                  @if ($product->payment_type == 'PLAN')
                    <div id="plans" class="container tab-pane">
                        
                    </div>
                  @endif
              </div>
            </div>
    

            {!! \App\Helpers\TabsAffiliateHelper::closeContent() !!}
            {!! \App\Helpers\TabsAffiliateHelper::closeHeader() !!}
            {!! \App\Helpers\TabsAffiliateHelper::closeBoxTabs() !!}
          
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.29.2/sweetalert2.all.js"></script>
    
<script>
  
  $(document).on("click",".cancelar_afiliacao",function(){
      var product_id = $('#produto').val();
      var formData = {
              product_id:product_id
          };
          $.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url: '/affiliates/cancel',
          type: 'POST',
          data: formData,
          success: function(result){
            if(result == "true"){
              event.preventDefault();
              Swal.fire(
                      'Sucesso!',
                      "Afiliação cancelada com sucesso",
                      'success'
                  )
              setTimeout(function() {
                location.reload();
              }, 3000);
            }else{
              event.preventDefault();
              Swal.fire(
                      'Erro!',
                      "Afiliação não foi cancelada",
                      'danger'
                  )
              setTimeout(function() {
                location.reload();
              }, 3000);
            }
          },
          error: function(){
              Swal.fire(
                  'Ooops!',
                  result.responseJSON.error,
                  'error'
              )
          }
      }); 
  });
</script> 
@endpush