@extends('plataforma.templates.tabs')


@section('contentTab')

    <div class="row">
        <div class="col-md-12">
            <h4 class="text-left"> Lista de URL's para Divulgação</h4>
            <small>Aqui você pode conferir os links de seu produtos na ColinaPay.</small>
        </div>
    </div>
    <hr>

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-uppercase">URL Página de vendas</div>
                <div class="card-body">
                    <div class="form-group has-feedback">
                        <div class="form-group">
                            {!! Form::text('sale_page',  url('/r/'. $producer->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}
                            {!! Form::label('sale_page', 'URL Destino: ' . $producer->product->salepage->url_page_sale , ['style' => 'TEXT-TRANSFORM: inherit; margin : 10px']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th class="text-center">Cliques/Únicos</th>
                    <th class="text-center">Vendas</th>
                    <th class="text-center">CTR</th>
                </tr>
                </thead>
                <tbody class="word-break"><tr>
                    <th colspan="3" class="blank">Suas divulgações</th>
                </tr> <tr>
                    <td class="text-center cliques_seus">-</td>
                    <td class="text-center vendas_suas">-</td>
                    <td class="text-center ctr_seu">-</td>
                </tr><tr>
                    <th colspan="3" class="blank">Divulgações dos afiliados</th>
                </tr>
                <tr>
                    <td class="text-center cliques">-</td>
                    <td class="text-center vendas">-</td>
                    <td class="text-center ctr">-</td>
                </tr><tr>
                    <th colspan="3" class="blank">Total</th>
                </tr>
                <tr>
                    <td class="text-center tot_cliques">-</td>
                    <td class="text-center tot_vendas">-</td>
                    <td class="text-center tot_ctr">-</td>
                </tr>
                <tr><th colspan="3" class="blank text-center"></th></tr>
                </tbody>
            </table>

            <div class="row">
                <div class="col-md-12 text-right">
                    <button type="button" data-loading-text="Calculando..." class="btn btn-info btn-gradient btnVerNumUrls" data-chave="98d0c2f95351702ccfe83bf175ef6c70" data-plano="" data-link="" data-tipo="1">
                        <i class="zmdi zmdi-search-in-page"></i> Ver clicks/vendas
                    </button>
                </div>
            </div>
        </div>
    </div>

    <hr>

    @foreach ($checkouts as $checkout)
        <div class="row" style="border-top: 1px solid; border-bottom: 1px solid; border-color: #ccc6c6; padding: 30px">

            <div class="col-lg-12">
                <h5 style="margin: 10px; margin-bottom: 15px;">{{ $checkout->desc}}</h5>
                <div class="card">
                    <div class="card-header text-uppercase">URL para divulgação</div>
                    <div class="card-body">
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                {!! Form::text('checkout',  url('/c/' . $checkout->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12" style="float: right">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">Cliques/Únicos</th>
                        <th class="text-center">Vendas</th>
                        <th class="text-center">CTR</th>
                    </tr>
                    </thead>
                    <tbody class="word-break">
                    <tr>
                        <th colspan="3" class="blank">Suas divulgações</th>
                    </tr>
                    <tr>
                        <td class="text-center cliques_seus">-</td>
                        <td class="text-center vendas_suas">-</td>
                        <td class="text-center ctr_seu">-</td>
                    </tr>
                    <tr>
                        <th colspan="3" class="blank">Divulgações dos afiliados</th>
                    </tr>
                    <tr>
                        <td class="text-center cliques">-</td>
                        <td class="text-center vendas">-</td>
                        <td class="text-center ctr">-</td>
                    </tr>
                    <tr>
                        <th colspan="3" class="blank">Total</th>
                    </tr>
                    <tr>
                        <td class="text-center tot_cliques">-</td>
                        <td class="text-center tot_vendas">-</td>
                        <td class="text-center tot_ctr">-</td>
                    </tr>
                    <tr><th colspan="3" class="blank text-center"></th></tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="button" data-loading-text="Calculando..." class="btn btn-info btn-gradient btnVerNumUrls" data-chave="98d0c2f95351702ccfe83bf175ef6c70" data-plano="" data-link="" data-tipo="1">
                            <i class="zmdi zmdi-search-in-page"></i> Ver clicks/vendas
                        </button>
                    </div>
                </div>
            </div>

        </div>
    @endforeach

    <hr>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-uppercase">
                    <h6>URLs Alternativas</h6>
                    <button type="button" class="btn btn-success pull-right" data-toggle="modal" onclick="openModal()"><i class="fa fa-plus"></i> Nova URL </button>
                </div>
            </div>
        </div>
    </div>


    @foreach ($urlAlternatives as $url)
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                {!! Form::label('url', $url->desc . ':', ['for' => 'plan_id' ]) !!}
                                {!! Form::text('url',  url('/' . $producer->code . '?ua=' . $url->code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}
                                {!! Form::label('url','URL Destino: ' . $url->url , ['style' => 'TEXT-TRANSFORM: inherit']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">Cliques/Únicos</th>
                        <th class="text-center">Vendas</th>
                        <th class="text-center">CTR</th>
                    </tr>
                    </thead>
                    <tbody class="word-break"><tr>
                        <th colspan="3" class="blank">Suas divulgações</th>
                    </tr> <tr>
                        <td class="text-center cliques_seus">-</td>
                        <td class="text-center vendas_suas">-</td>
                        <td class="text-center ctr_seu">-</td>
                    </tr><tr>
                        <th colspan="3" class="blank">Divulgações dos afiliados</th>
                    </tr>
                    <tr>
                        <td class="text-center cliques">-</td>
                        <td class="text-center vendas">-</td>
                        <td class="text-center ctr">-</td>
                    </tr><tr>
                        <th colspan="3" class="blank">Total</th>
                    </tr>
                    <tr>
                        <td class="text-center tot_cliques">-</td>
                        <td class="text-center tot_vendas">-</td>
                        <td class="text-center tot_ctr">-</td>
                    </tr>
                    <tr><th colspan="3" class="blank text-center"></th></tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-md-12">
                        <button type="button" data-loading-text="Calculando..." class="btn btn-info btn-gradient btnVerNumUrls" data-chave="98d0c2f95351702ccfe83bf175ef6c70" data-plano="" data-link="" data-tipo="1">
                            <i class="zmdi zmdi-search-in-page"></i> Ver clicks/vendas
                        </button>
                    </div>
                </div>

            </div>
        </div>
    @endforeach


    <!-- Modal -->
    <div class="modal fade" id="modalUrlAlternative" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Adicionar URL Alternativa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    {{ Form::open(['route' => ['product.url.createUrlAlternative', app('request')->product->id], 'method' => 'post']) }}

                    <div class="form-group has-feedback {{ $errors->has('url') ? 'has-error' : '' }}">
                        {!! Form::label('desc', 'Nome da URL:', ['for' => 'desc']) !!}
                        {!! Form::text('desc', old('desc'), ['class' => 'form-control']) !!}

                        @if ($errors->has('desc'))
                            <label class="control-label" for="desc">
                                {{$errors->first('desc')}}
                            </label>
                        @endif
                    </div>

                    <div class="form-group has-feedback {{ $errors->has('url') ? 'has-error' : '' }}">
                        {!! Form::label('url', 'URL:', ['for' => 'url', 'style'=> 'margin-bottom:0px' ]) !!}
                        <br><small style="font-size: 10px">É preciso informar o endereço com http://</small>
                        {!! Form::text('url', old('url'), ['class' => 'form-control']) !!}

                        @if ($errors->has('url'))
                            <label class="control-label" for="url">
                                {{$errors->first('url')}}
                            </label>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('url') ? 'has-error' : '' }}">
                        {!! Form::label('private', 'Privado:', ['for' => 'private', 'style'=> 'margin-bottom:0px' ]) !!}
                        <br><small style="font-size: 10px">Ao definir uma URL como privado, apenas você como produtor terá acesso ao link</small>
                        <br>
                        {!! Form::checkbox('private', null, old('private')) !!}

                        @if ($errors->has('private'))
                            <label class="control-label" for="private">
                                {{$errors->first('private')}}
                            </label>
                        @endif
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="input" class="btn btn-primary" id="btnAddPlano" onclick="closeModal()">Salvar</button>
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    <script>

        function openModal() {
            $('#modalUrlAlternative').modal("show");
        }

        function closeModal() {
            $('#modalUrlAlternative').modal("hide");
            $.preloader.start();
        }

    </script>
@endsection
