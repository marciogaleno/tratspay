@extends('plataforma.templates.template')

@section('content')
    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Gerenciador de Links &nbsp;</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->

        <div class="row">
            <div class="col-md-5">
                <div class="card">
                    <div class="card-header text-uppercase"> Identificar afiliado pelo código de referência
                    </div>
                    <div class="card-body">
                            <form action="" method="GET" class="">
                                @csrf
                                
                                <div class="row">
                                    <div class="col-md-12" style="float:left">
                                        <div class="form-group">
                                            {!! Form::label('complement', 'Código Ref.:') !!}
                                            {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '123456']) !!}
                                        </div>
                                    </div>
                                  
                                </div>

                                <div class="row justify-content-end">
                                    <div class="col-md-12 text-right">
                                        <div class=""><button class="btn btn-success " type="submit"><i class="fa fa-search"></i> Buscar</button></div>
                                    </div>
                                </div>
                            </form>
                      
                    </div>
                </div>
        
            </div>
            <div class="col-md-7">
                    <div class="card">
                        <div class="card-header text-uppercase"> Adicionar rastreamento em uma URL
                        </div>
                        <div class="card-body">
                                <form action="" method="GET" class="">
                                    @csrf
                                  
                                    <div class="form-group">
                                        {!! Form::label('complement', 'URL:') !!}
                                        {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>
                                    
                                    <div class="form-group">
                                        {!! Form::label('complement', 'src:') !!}
                                        {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>
                                
                                    <div class="form-group">
                                        {!! Form::label('complement', 'utm_source:') !!}
                                        {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>
                                
                            
                                    <div class="form-group">
                                        {!! Form::label('complement', 'utm_campaing:') !!}
                                        {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>
                                    
                                
                                    <div class="form-group">
                                        {!! Form::label('complement', 'utm_medium:') !!}
                                        {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('complement', 'utm_content:') !!}
                                        {!! Form::text('complement', null, ['class' => 'form-control', 'placeholder' => '']) !!}
                                    </div>
                                       

                                    <div class="row justify-content-end">
                                        <div class="col-md-12 text-right">
                                            <div class=""><button class="btn btn-success " type="submit">
                                                <i class="fa fa-plus"></i> Adicionar</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                          
                        </div>
                    </div>
            
                </div>
        </div>
     
   </div>
@stop