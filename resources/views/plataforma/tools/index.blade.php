@extends('plataforma.templates.template')
@section('content')


<div class="col-lg-12">
    <div class="card">
        <div class="card-header text-uppercase">O que são as ferramentas?</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>A TratsPay permite que você se integrar com diversas ferramentas, tanto as que nós desenvolvemos,
                        quanto de terceiros, de forma fácil, rápida e prática.&nbsp;</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-header text-uppercase">ferramentas úteis&nbsp;</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4" style="">
                            <div class="col-lg-12 border-warning rounded" style="">
                                <div class="card">
                                    <div class="card-header text-white bg-dark">Notas Fiscais</div>
                                    <div class="card-body border border-warning">
                                        <div class="row">
                                            <div class="col-md-4" style=""><img class="img-fluid d-block"
                                                    src="http://icons.iconarchive.com/icons/papirus-team/papirus-apps/72/standard-notes-icon.png">
                                            </div>
                                            <div class="col-md-8" style="">
                                                <p class="card-text break">Faça a configuração de sua emissão de notas
                                                    fiscais e similares aqui</p>
                                            </div>
                                        </div>
                                        <p class="card-text break" style=""><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;</b><br><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;<a href="notas-fiscais.html"
                                                    class="text-right">CONFIGURAR</a></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="col-lg-12 border-warning rounded" style="">
                                <div class="card">
                                    <div class="card-header text-white bg-dark">API</div>
                                    <div class="card-body border border-warning">
                                        <div class="row">
                                            <div class="col-md-4" style=""><img class="img-fluid d-block"
                                                    src="http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/72/Actions-network-connect-icon.png">
                                            </div>
                                            <div class="col-md-8" style="">
                                                <p class="card-text break">A API permite que você faça integrações
                                                    direto
                                                    com a TratsPay.</p>
                                            </div>
                                        </div>
                                        <p class="card-text break"><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;</b><br><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;<a href="api.html" class="text-right">CONFIGURAR</a></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="col-lg-12 border-warning rounded" style="">
                                <div class="card">
                                    <div class="card-header text-white bg-dark">Postback</div>
                                    <div class="card-body border border-warning">
                                        <div class="row">
                                            <div class="col-md-4" style=""><img class="img-fluid d-block"
                                                    src="http://icons.iconarchive.com/icons/iconshock/real-vista-data/72/connect-icon.png">
                                            </div>
                                            <div class="col-md-8" style="">
                                                <p class="card-text break">Através de nosso postback você pode coonectar
                                                    suas ferramentas conosco.</p>
                                            </div>
                                        </div>
                                        <p class="card-text break"><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;</b><br><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;<a href="{{route('postback.index')}}"
                                                    class="text-right">CONFIGURAR</a></b>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12">

    <div class="card">
        <div class="card-header text-uppercase">automatização de dados&nbsp;</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4" style="">
                            <div class="col-lg-12 border-warning rounded" style="">
                                <div class="card">
                                    <div class="card-header text-white bg-dark">Automatização de Leads</div>
                                    <div class="card-body border border-warning">
                                        <div class="row">
                                            <div class="col-md-4" style=""><img class="img-fluid d-block"
                                                    src="http://icons.iconarchive.com/icons/papirus-team/papirus-apps/72/standard-notes-icon.png">
                                            </div>
                                            <div class="col-md-8" style="">
                                                <p class="card-text break">Integre qualquer ferramenta de e-mail
                                                    marketing.
                                                </p>
                                            </div>
                                        </div>
                                        <p class="card-text break"><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;</b><br><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;<a href="{{route('tool.leadAutomator')}}"
                                                    class="text-right">CONFIGURAR</a></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="col-lg-12 border-warning rounded" style="">
                                <div class="card">
                                    <div class="card-header text-white bg-dark">Tracking de Tráfego</div>
                                    <div class="card-body border border-warning">
                                        <div class="row">
                                            <div class="col-md-4" style=""><img class="img-fluid d-block"
                                                    src="http://icons.iconarchive.com/icons/paomedia/small-n-flat/72/file-code-icon.png">
                                            </div>
                                            <div class="col-md-8" style="">
                                                <p class="card-text break">Faça a configuração de seus pixels de
                                                    conversão
                                                    direto com a Trats.</p>
                                            </div>
                                        </div>
                                        <p class="card-text break"><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;</b><br><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;<a href="tracking-trafego.html"
                                                    class="text-right">CONFIGURAR</a></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" style="">
                            <div class="col-lg-12 border-warning rounded" style="">
                                <div class="card">
                                    <div class="card-header text-white bg-dark">Gerenciador de Links</div>
                                    <div class="card-body border border-warning">
                                        <div class="row">
                                            <div class="col-md-4" style=""><img class="img-fluid d-block"
                                                    src="http://icons.iconarchive.com/icons/custom-icon-design/pretty-office-8/72/Link-icon.png">
                                            </div>
                                            <div class="col-md-8" style="">
                                                <p class="card-text break">Crie links rastreaveis para poder analisar
                                                    melhor
                                                    as suas métricas.</p>
                                            </div>
                                        </div>
                                        <p class="card-text break"><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;</b><br><b>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;&nbsp;<a  href="{{route('tool.linkManager')}}"
                                                    class="text-right">CONFIGURAR</a></b></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection