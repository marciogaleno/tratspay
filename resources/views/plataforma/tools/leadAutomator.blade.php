@extends('plataforma.templates.template')

@section('content')
    <div class="container">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-12 col-md-12">
                <h4 class="page-title">Automatização de Leads &nbsp;</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->
        
        <div class="row text-center">
            
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4 " >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/active_campaign.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/aweber.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/egoi.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/infusionsoft.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/mailchimp.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/mautic.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/ontraport.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 ">
                <div class="mb-4" >
                    <img class="img-fluid img-thumbnail" style="cursor: pointer;" src="{{asset('assets/images/brands/rdstation.jpg')}}" alt="">
                </div>
            </div>
          
           
        </div>

   </div>
@stop