@extends('plataforma.templates.template')

@section('css-dashboard')
    {{--  <link rel="stylesheet" href="{{asset('dist/plugins/chartist-js/chartist.min.css')}}">--}}
    {{--  <link rel="stylesheet" href="{{asset('dist/plugins/chartist-js/chartist-plugin-tooltip.css')}}">--}}
@endsection
@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection
@section('script-dashboard')
    {{--  <script src="{{asset('dist/plugins/chartjs/chart.min.js')}}"></script>--}}
    {{--  <script src="{{asset('dist/plugins/chartjs/chart-int.js')}}"></script>--}}

    {{--  <!-- Chartist JavaScript -->--}}
    {{--  <script src="{{asset('dist/plugins/chartist-js/chartist.min.js')}}"></script>--}}
    {{--  <script src="{{asset('dist/plugins/chartist-js/chartist-plugin-tooltip.js')}}"></script>--}}
    {{--  <script src="{{asset('dist/plugins/functions/chartist-init.js')}}"></script>--}}
@endsection

@section('content')
    <div class="">
        <div class="row" style="">
            <div class="col-md-12 pull-right"  style="padding: 15px">
                <button class="btn btn-success pull-right" data-toggle="collapse" data-target="#demo"><i class="fa fa-filter"></i> Filtrar</button>

            </div>
            <div class="col-md-12">

                <div class="card">
                    <div class="card-body" style="">
                        <div id="demo" class="collapse">
                            @include('plataforma.sales.filter')
                        </div>
                        <div class="table-responsive" style="">
                            <table class="table table-sm">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col" style="width: 5%;">Transação</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col">Forma de Pag.</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Data Compra</th>
                                    <th scope="col">Comissão</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($commissions as $commission)
                                    <tr>
                                        <td>{{$commission->id}}</td>
                                        <td>
                                            <strong>Nome: </strong>{{$commission->name}}<br>
                                            <strong>Nome Comprador: </strong>{{$commission->first_name . ' ' . $commission->las_name}}<br>
                                            <strong>Email comprador: </strong>{{$commission->email}}<br>
                                        </td>
                                        <td>{{App\Helpers\Helper::paymentForms($commission->payment_type_id)}}</td>
                                        <td>{{App\Helpers\Helper::getStatusPayment($commission->status)}}</td>
                                        <td>{{\App\Helpers\Helper::dateToBr($commission->date_created, false)}}</td>
                                        <td>{{App\Helpers\Helper::moneyBR($commission->value)}}</td>
                                        <td>
                                            <!--<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#largesizemodal" href="/myModal"><i aria-hidden="true" class="fa fa-search"></i></button>-->
                                            {{--                              <a href="{{route('sale.details', $commission_id)}}" class="btn btn-primary btn-sm"><i aria-hidden="true" class="fa fa-search"></i></a>--}}
                                            <a  href="{{route('sale.details', $commission->id)}}" target="_blank"  class="btn btn-primary btn-sm"><i aria-hidden="false" style="color:#FFFFFF" class="fa fa-search"></i></a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>Nenhuma comissão encontrada</tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right">{!! $commissions->links() !!}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#form-payment').select2();
            $('#product_name').select2();
            $('#co_affiliates').select2();
        });

    </script>
@stop