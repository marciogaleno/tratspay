@extends('plataforma.templates.template')
@section('content')

@section('styles')
    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
@endsection


<div class="col-lg-12">

    <div class="row ">
        <div class="col-md-6 pb-2">
            <h4 class="page-title">Indicar Afiliados</h4>
        </div>
        <div class="col-md-6  text-right">
            <h6> Você já indicou <span class="text-success"> {{$total}} </span> usuários para a ColinaPay!</h6>
        </div>
    </div>

    <div class="card">

        <div class="card-header text-uppercase">O que é a indicação de afiliados?</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>A indicação de afiliados é a maneira que a ColinaPay recompensa você por conseguir novos usuários para a nossa plataforma.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">

        <div class="card-header text-uppercase">Quanto eu ganho?</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>Para cada venda de novo usuário indicado por você, irá receber 1% das vendas dele, independente se for produtor ou afiliado pelo período de 1 ano.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="card">

        <div class="card-header text-uppercase">Como saber quanto estou ganhando??</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <p>Você pode conferir em seus relatorios de vendas realizadas todas as que forem realizadas pelos seus indicados.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="card">

        <div class="card-header text-uppercase">Qual meu linl?</div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h4>Para divulgar a ColinaPay, utilize o link abaixo</h4>
                </div>
                <div class="col-md-12">
                        {!! Form::text('url',  url('/i/' . Auth::user()->indication_code) , ['class' => 'form-control', 'style' => 'background-color: #FFFFFF', 'readonly']) !!}
                </div>

            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@endpush

@endsection
