@extends('plataforma.templates.page-simples')


@section('content')
<!-- title row -->

<section class="content-header">
   

    <div class="row mt-3">
            <div class="col-6 col-lg-9 col-xl-9">
                <h3>
                {{$product->name}}
                </h3>
            </div>

            @if (Auth::user())
            <div class="col-6 col-lg-3 col-xl-3">
                    @if (!$isAffiliateUserloggedThisProduct || $isAffiliateUserloggedThisProduct->status = "I")
                    <div class="col-md-3 text-white" style="">
                        {!! Form::open(['route' => 'product.affiliate', 'method' => 'POST']) !!}
                          <input type="hidden" value="{{$product->id}}" name="product_id">
                          <input type="hidden" value="{{$product->uuid}}" name="uuid">
                          <button class="btn btn-success waves-effect waves-light m-1" data-toggle="modal" data-target="#solicitModal">Afiliar-se agora!</button>  
                        {!! Form::close() !!}
                    </div>     
                @else
                    <div class="col-md-3 text-white" style="">
                        <button class="btn btn-danger waves-effect waves-light m-1">Você já é afiliado desse produto</button>                     
                    </div>
                @endif
            </div>

            @else
                <a href="{{route('configaffiliation.login', $recruit_code)}}" class="btn btn-danger waves-effect waves-light m-1">Entre para se Afiliar</a>
            @endif
    </div>
</section>

<hr>
<!-- Main content -->
<section class="invoice">

   
    <div class="row">

        <div class="col-3 col-lg-3 col-xl-3" style="">
            <div class="card card-warning">
                <img src="{{ \App\Helpers\Helper::getPathFileS3('images', $product->image) }}" class="card-img-top"
                    alt="Card image cap">
            </div>
        </div>

        <div class="col-6 col-lg-9 col-xl-9" style="">    
            @if ($product->payment_type == 'SINGPRICE')
            <h4>Receba por venda até: <span style="color:yellowgreen"> R$ {{$valueComission}}</span></h4>
            @else
            Veja detalhes nos planos para ver comissões
            @endif
       
            <span>
                <strong>Produtor:</strong><b><a href="{{route('user.perfil', $producer->id)}}"> 
                    @if($producer->client && $producer->client->fantasia){{$producer->client->fantasia }}@else{{$producer->name}}
                    @endif
                  </a></b>
            </span><br>

            <span><strong>E-mail de suporte:</strong>
            @if($product->email_support == null)
                Naõ definido
            @else
                {{ $product->email_support }}
            @endif
            </span><br>

                  <span><strong>Tipo de produto:</strong> {{ $product->product_type == 'V' ? 'Virtual' : 'Físico' }}</span><br>
                  <span><strong>Tipo comissão:</strong> Último clique</span><br>
                  <span><strong>Validade do Cookie:</strong>
                    @if($product->configAffiliation->expiration_coockie == null)
                      Naõ definido
                    @else
                      {{ $product->configAffiliation->expiration_coockie }}
                    @endif
                  </span><br>
                   <span><strong>Valor do produto: R$</strong> {{ $product->plansWithoutGlobalScope[0]->price }}</span><br>
                  <span><strong>Garantia:</strong> {{$product->warranty}} dias</span><br>
                  <span><strong> Url do produto:</strong> <a href="{{$salePage->url_page_sale}}" target="_blank">{{$salePage->url_page_sale}}</a></span><br>
                    @if(count($checkouts))
                      <p><strong> Url de Checkout:</strong>
                      @foreach ($checkouts as $checkout)
                           <a href="{{ url('/chk/' . $checkout->code)}}" target="_blank">{{url('/chk/' . $checkout->code)}}</a> <br>
                      @endforeach
                @endif
        </div>

    </div>

    <hr>
    <h5>Sobre</h5>
    {{$product->desc}}

    <hr>

    <h5>Afiliação</h5>
    
    {{$product->configAffiliationWithoutGlobalScope->desc_affiliate}}

    @if ($product->payment_type == 'PLAN')
        <hr>
        <h5>Afiliação</h5>
    @endif

</section><!-- /.content -->
@endsection