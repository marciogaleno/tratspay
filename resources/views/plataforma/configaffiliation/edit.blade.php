@extends('plataforma.templates.tabs')

@section('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/dropify/dropify.min.css')}}">
    <style>
        .form-personal {
            display: inline;
            width: 90%;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

         .form-personal2 {
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .form-percent {
            font-weight: bold;
            display: inline-block;
            width: 12%;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #000000;
            background-color: #efeaea;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .gray{
            vertical-align: top;
            margin-right: 5px;
            opacity: 0.75;
            -moz-opacity: 0.75;
            filter: alpha(opacity=75);
            -webkit-filter: opacity(0.75);
            filter: gray;
            -webkit-filter: grayscale(100%);
            image-rendering: auto;
        }

        .click{cursor: pointer;}

         .popover-title {
                padding: 8px 14px;
                margin: 0;
                font-size: 14px;
                background-color: #f7f7f7;
                border-bottom: 1px solid #ebebeb;
                border-radius: 5px 5px 0 0;
            }

            select.input-sm {
                height: 30px;
                line-height: 30px;
                margin-top: 5px;
            }
            .input-sm {
                height: 30px;
                padding: 5px 10px;
                font-size: 12px;
                line-height: 1.5;
                border-radius: 3px;
            }

            .popover-body {
                padding: 9px 14px;
            }

            .editable-buttons{
                display: inline-block;
                line-height: 1.5;
                outline: none;
                color: #fff;
                padding: 5px 10px;
                font-size: 12px;
                border-color: #2e6da4;
                display: inline-block;
                vertical-align: top;
                margin-left: 7px;
            }

             button.btn.btn-primary.btn-sm.editable-submit {
                background-color: #337ab7;
                margin-right: 10px;
            }
    </style>
@endsection

@section('scripts')
  <script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>

    <script>

        $(document).ready(function() {
            initializeTable();
        });

        $("#value_commission").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

        $('#accept_affiliation').on('change', function() {
            if (this.value == 1 ) {
            $("#format_commission").attr("disabled", false);
            $("#value_commission").attr("disabled", false);
            $("#expiration_coockie").attr("disabled", false);
            $("#desc_affiliate").attr("disabled", false);
            $("#assignment_type").attr("disabled", false);
            $("#checkboxAfiliado").show();
            } else {
            $("#format_commission").attr("disabled", true);
            $("#value_commission").attr("disabled", true);
            $("#expiration_coockie").attr("disabled", true);
            $("#desc_affiliate").attr("disabled", true);
            $("#assignment_type").attr("disabled", true);
            $("#checkboxAfiliado").hide();
            }
        });

        if ($("#accept_affiliation option:selected").val() == 1) {
            $("#format_commission").attr("disabled", false);
            $("#value_commission").attr("disabled", false);
            $("#validade_coockie").attr("disabled", false);
            $("#desc_afiliado").attr("disabled", false);
            $("#assignment_type").attr("disabled", false);
            $("#checkboxAfiliado").show();
        } else {
            $("#format_commission").attr("disabled", true);
            $("#value_commission").attr("disabled", true);
            $("#expiration_coockie").attr("disabled", true);
            $("#desc_affiliate").attr("disabled", true);
            $("#assignment_type").attr("disabled", true);
            $("#checkboxAfiliado").hide();
        }

        $(document).on("click",".adicionar-termo",function(){
            event.preventDefault();
            $('#modalATC').modal('show');
        });

        document.getElementById("cancel").addEventListener("click", function(event){
            $('#modalATC').modal('hide');
        });

        function images(){
            var element1 = document.getElementById("1");
            var element2 = document.getElementById("2");
            var element3 = document.getElementById("3");
            var element4 = document.getElementById("4");
            var element5 = document.getElementById("5");
            $("#1").click(function(){
                element1.classList.remove("gray");

                element2.classList.add("gray");
                element3.classList.add("gray");
                element4.classList.add("gray");
                element5.classList.add("gray");
                $("#assignment_type").val('1');
                document.getElementById('d-c').style.display = "none";
            });
            $("#2").click(function(){
                $('#primer_click').val("");
                $('#ultimo_click').val("");
                element2.classList.remove("gray");

                element1.classList.add("gray");
                element3.classList.add("gray");
                element4.classList.add("gray");
                element5.classList.add("gray");
                $("#assignment_type").val('2');
                document.getElementById('d-c').style.display = "none";
            });
            $("#3").click(function(){
                element3.classList.remove("gray");

                element1.classList.add("gray");
                element2.classList.add("gray");
                element4.classList.add("gray");
                element5.classList.add("gray");
                $("#assignment_type").val('3');
                document.getElementById('d-c').style.display = "block";
            });
            $("#4").click(function(){
                element4.classList.remove("gray");

                element1.classList.add("gray");
                element2.classList.add("gray");
                element3.classList.add("gray");
                element5.classList.add("gray");
                $("#assignment_type").val('4');
                document.getElementById('d-c').style.display = "none";
            });
            $("#5").click(function(){
                element5.classList.remove("gray");

                element1.classList.add("gray");
                element2.classList.add("gray");
                element3.classList.add("gray");
                element4.classList.add("gray");
                $("#assignment_type").val('5');
                document.getElementById('d-c').style.display = "none";
            });
        }images();

        $('#primer_click').mask("00,00");
        $('#ultimo_click').mask("00,00");

        {{-- START DATA TABLE --}}

        function initializeTable(){
            var html =  '<div class="row">'+
                            '<div class="col-lg-12">'+
                                '<div class="card">'+
                                 '<div class="card-header"><i class="fa fa-table"></i> Lista de Afiliados</div>'+
                                    '<div class="card-body">'+
                                        '<div class="table-responsive">'+
                                            '<table id="default-datatable" class="table table-bordered">'+
                                                '<thead>'+
                                                    '<tr>'+
                                                        '<th>Picture</th>'+
                                                        '<th>Nome / E-mail</th>'+
                                                        '<th>Vendas</th>'+
                                                        '<th>Desde</th>'+
                                                        '<th>Ativo</th>'+
                                                    '</tr>'+
                                                '</thead>'+
                                                '<tbody id="contenido">'+
                                                '</tbody>'+
                                                '<tfoot>'+
                                                    '<tr>'+
                                                        '<th>Picture</th>'+
                                                        '<th>Nome / E-mail</th>'+
                                                        '<th>Vendas</th>'+
                                                        '<th>Desde</th>'+
                                                        '<th>Ativo</th>'+
                                                    '</tr>'+
                                                '</tfoot>'+
                                            '</table>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>';
            $('#mi_contenido').append(html);
            makeDataTable();
        }

        function makeDataTable(){
            var id = $('#p_id').val();
            var formData = { id:id };
             $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/affiliates/my-affiliates',
                type: 'POST',
                data: formData,
                success: function (retorno) {
                    var tamR = retorno.product.length;
                    for (var i = 0; i < tamR; i++) {
                        var image = '';
                        var status = '';
                        var desde = '';
                        if(retorno.product[i].image != null){
                            image = '<img src="{!! \App\Helpers\Helper::getPathFileS3("images", '+ retorno.product[i].image +') !!}" width="50" height="50" alt="avatar">';
                        }else {
                            image = '<img src="https://tratspay-images.s3-sa-east-1.amazonaws.com/default-system/default-avatar.png" width="50" height="50" alt="avatar">';
                        }

                        if(retorno.product[i].pivot.status == "I"){
                            status = '<a class="help click" data-toggle="popover" data-placement="top" data-affiliate = "'+retorno.product[i].pivot.user_id+'" title="Ativo">Não</a>';
                        }else {
                            status = '<a class="help click" data-toggle="popover" data-placement="top" data-affiliate = "'+retorno.product[i].pivot.user_id+'" title="Ativo">Sim</a>';
                        }

                        var created_at = retorno.product[i].created_at;
                        desde = created_at.split(" ");

                        html = '<tr class="odd">'+
                                    '<td class="text-center ">'+
                                        image +
                                    '</td>'+
                                    '<td class="info "><b>'+ retorno.product[i].name+'</b><br>'+
                                        '<span class="text-muted">' +retorno.product[i].email+ '<br>'+
                                        '<label>Comissão: </label>'+
                                        '<input class="real_afiliado" type="text" id="comissao_SN4602136" style="width: 55px;" value="'+retorno.comision+'">%'+
                                        '<button data-ref="SN4602136" data-loading-text="Salvando..." class="btn btn-success btn-sm btn-gradient btnSalvarComissao">Salvar</button>'+
                                        '</span>'+
                                    '</td>'+
                                    '<td class=" ">'+retorno.vendas+'</td>'+
                                    '<td class=" ">'+desde[0]+'</td>'+
                                    '<td class="">'+status +'</td>'+
                                '</tr>';
                        $('#contenido').append(html);
                    }
                    constructTable();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }

        function constructTable(){
            var table = $('#default-datatable').DataTable( {
                buttons: [ 'copy', 'excel', 'pdf', 'print', 'colvis' ],

                "language": {
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Registros por página:  _MENU_",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _PAGE_ até _PAGES_ do total de _PAGES_ registros",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sPrevious": "Anterior",
                        "sNext": "Próxima",
                    }
                },
            } );

            table.buttons().container().appendTo( '#default-datatable_wrapper .col-md-6:eq(0)' );

        }

        $(document).on("click", ".help", function () {
            var id =  $(this).attr("data-affiliate");
            $('#default-datatable_wrapper .help').popover({
            "html": true,
            "content": "<div><select id='estatus_afiliado' class='input-sm'><option value='A'>Sim</option><option value='I'>Não</option></select><div class='editable-buttons'><button type='submit' class='btn btn-primary btn-sm editable-submit' onclick='changeEstatus("+id+")'><i class='zmdi zmdi-check'></i> </button><button type='button' class='btn btn-default btn-sm editable-cancel help' onclick='ocultar()'><i class='zmdi zmdi-close help'></i></button></div></div>",
            });
        });

        function ocultar(){
            $('#default-datatable_wrapper .help').popover('hide');
        }

        function changeEstatus(id){
            var estatus = $("#estatus_afiliado option:selected").val();
            var p_id = $('#p_id').val();
              var formData = {
                product_id : p_id,
                affiliate_id : id,
                estatus:estatus
                };
             $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/affiliates/status-affiliates',
                type: 'POST',
                data: formData,
                success: function (retorno) {
                    event.preventDefault();
                    ocultar();
                    $("#mi_contenido").empty();
                    initializeTable();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
        {{-- END DATA TABLE --}}
    </script>
@endsection
