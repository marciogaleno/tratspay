@extends('plataforma.templates.tabs') 



@section('styles')
  <style>
      .buttonEdit, .buttonDelete
      {
          float:left;
          margin-left: 5px;
          
      }
      .content-wrapper {
        padding-top: 0px;
      }
    </style>
    
    <link href="{{asset('assets/plugins/uploader-master/dist/css/jquery.dm-uploader.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/uploader-master/demo/styles.css')}}" rel="stylesheet">
@endsection

@section('contentTab')

  <div class="row">
      <div class="col-sm-9">
          <h4 class="text-left">Arquivos</h4>
          <small> (Aqui você pode fazer o upload dos arquivos que deseja entregar para os seus clientes, porém lembre-se que o limite máximo de um arquivo é de 99Mb)</small>
      </div>
      
  </div>

  <div class="row">
    <div class="col-md-12 col-sm-12">
      
      <!-- Our markup, the important part here! -->
      <div id="drag-and-drop-zone" class="dm-uploader p-5">
        <h3 class="mb-5 mt-5 text-muted">Arraste ou solte o arquivo aqui</h3>

        <div class="btn btn-primary btn-block mb-5">
            <span>Upload arquivo</span>
            <input type="file" title='Click to add Files' name="fileProduct" />
        </div>
      </div><!-- /uploader -->

    </div>
  </div>

  <div class="row">
    <div class="col-md-12 col-sm-12" style="min-height: 450px">
      <div class="card h-100">
        <div class="card-header">
          Arquivos
        </div>


          
              <div class="table-responsive">
                  <table class="table">
                      <tbody id="files">
                        @if (!$files)
                            <li class="text-muted">Nenhum arquivo encontrado.</li>
                        @else

                            @foreach ($files as $file)
                                  <tr>
                                      <th scope="row" style="width: 10%"> 
                                          <i class="mr-3 mb-2 preview-img {{$file['iconExtension']}}" style="font-size: 30px"></i>
                                      </th>
                                      <td style="width: 50%">                                        
                                          <p style="font-size: 12px">{{$file['name_original']}}</p>
                                      </td>
                                      <td style="width: 10%">                                        
                                          <p>{{$file['size']}}</p>
                                      </td>
                                      <td style="width: 30%">
                                          <div>
                                            {!! Form::open(['route' => ['product.file.download', $product->id], 'method' => 'POST', 'class'=> "buttonEdit"]) !!}
                                              {!! Form::hidden('file_path',  $file['file_path'] ?? '') !!}
                                              {!! Form::hidden('file_name',  $file['name'] ?? '') !!}
                                              <button class="btn btn-primary btn-sm" title="Donwload"><i class="fa fa-download" style="font-size: 15px"></i></button>
                                            {!! Form::close() !!}
                                            {!! Form::open(['route' => ['product.file.delete', $product->id], 'method' => 'DELETE',  'class'=> "buttonDelete"]) !!}
                                              {!! Form::hidden('file_path',  $file['file_path']) !!}
                                              <button class="btn btn-danger btn-sm" title="Excluir" onclick="return confirm('Tem certeza que deseja excluir?')"><i class="fa fa-remove" style="font-size: 15px"></i></button>
                                            {!! Form::close() !!}
                                          </div>
                                      </td>
                                  </tr>
                            @endforeach
                            
                        @endif

                      </tbody>
                  </table>
              </div>             
        
      </div>
    </div>
  </div><!-- /file list -->

  <!--<div class="row">
    <div class="col-12">
        <div class="card h-100">
        <div class="card-header">
          Debug Messages
        </div>

        <ul class="list-group list-group-flush" id="debug">
          <li class="list-group-item text-muted empty">Loading plugin....</li>
            
        </ul>
      </div>
    </div>
  </div> -->

@endsection

@section('scripts')
  <script src="{{asset('assets/plugins/uploader-master/dist/js/jquery.dm-uploader.min.js')}}"></script> 

  <script src="{{asset('assets/plugins/uploader-master/demo/demo-ui.js')}}"></script>

  <!-- File item template -->
  <script type="text/html" id="files-template">
        <tr>
            <td scope="row"  style="width: 10%"> 
                <i class="mr-3 mb-2 preview-img iconExtension" style="font-size: 30px"></i>
            </td>
            <td  style="width: 50%">                                        
              
              <p style="font-size: 12px">
              <strong>%%filename%%</strong>
              </p>
              <p>
                  Status: <span class="text-muted">Waiting</span>
              </p>
              <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" 
                    role="progressbar"
                    style="width: 0%" 
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                </div>
              </div>
                
            </td>
            <td  style="width: 10%">                                        
              <p id="sizeFile"></p>
            </td>
            <td  style="width: 30%">
                <div>
                  {!! Form::open(['route' => ['product.file.download',  $product->id], 'method' => 'POST', 'class'=> "buttonEdit"]) !!}
                      {!! Form::hidden('file_path',  $file['file_path'] ?? '',  ['class' => 'inputFilePathDownload']) !!}
                      {!! Form::hidden('file_name',  $file['name'] ?? '', ['class' => 'inputFileNameDownload']) !!}
                      <button class="btn btn-primary btn-sm" title="Download"><i class="fa fa-download" style="font-size: 15px"></i></button>
                  {!! Form::close() !!}


                  {!! Form::open(['route' => ['product.file.delete', $product->id],'method' => 'DELETE', 'onSubmit' => "return validate(this)", 'class'=> "buttonDelete"]) !!}
                      {!! Form::hidden('file_path',  $file['file_path'] ?? '', ['class' => 'inputFilePathDelete']) !!}  
                      <button class="btn btn-danger btn-sm" title="Excluir" onclick="return confirm('Tem certeza que deseja excluir?')"><i class="fa fa-remove" style="font-size: 15px"></i></button>
                  {!! Form::close() !!}
                </div>
            </td>
          </tr>
  </script>

  <!-- Debug item template -->
  <script type="text/html" id="debug-template">
    <li class="list-group-item text-%%color%%"><strong>%%date%%</strong>: %%message%%</li>
  </script>
  <script>
        $("#drag-and-drop-zone").dmUploader({
            url: {!! json_encode(url('product/' . $product->id . '/files/upload')) !!},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            maxFileSize: 100000000, // 3 Megs 
            extFilter: ["jpg", "jpeg","png","gif", "xls","xlsx", "epub", "pdf", "zip", "txt", 'rar'],
            extraData: [
              
            ],
            onDragEnter: function(){
            // Happens when dragging something over the DnD area
                this.addClass('active');
            },
            onDragLeave: function(){
            // Happens when dragging something OUT of the DnD area
                this.removeClass('active');
            },
            onInit: function(){
                ui_add_log('Penguin initialized :)', 'info');
            },
            onComplete: function(){
            // All files in the queue are processed (success or error)
                ui_add_log('All pending tranfers finished');
            },
            onNewFile: function(id, file){
            // When a new file is added using the file selector or the DnD area
                ui_add_log('New file added #' + id);
                ui_multi_add_file(id, file);

                if (typeof FileReader !== "undefined"){
                    var reader = new FileReader();
            
                    var sizeFile = formatSizeUnits(file.size);
                    var extensionFile = getIconByTypeFile(file.type, file.name);
                    
                    reader.onload = function (e) {
                      $('#sizeFile').html(sizeFile);
                      console.log(extensionFile);
                      $('#uploaderFile' + id).find('.iconExtension').addClass(extensionFile);
                    }

                    reader.readAsDataURL(file);
                }
            },
            onBeforeUpload: function(id){
               // about tho start uploading a file
                ui_add_log('Starting the upload of #' + id);
                ui_multi_update_file_progress(id, 0, '', true);
                ui_multi_update_file_status(id, 'uploading', 'Enviando...');
            },
            onUploadProgress: function(id, percent){
            // Updating file progress
                ui_multi_update_file_progress(id, percent);
            },
            onUploadSuccess: function(id, data){
            // A file was successfully uploaded

                var reponse = JSON.parse(data);
                var uuid =  reponse.data.product_uuid;
                var filename = reponse.data.name;
                var filepath = reponse.data.file_path;
    
                $('#uploaderFile' + id).find('.inputFileNameDownload').val(filename);
                $('#uploaderFile' + id).find('.inputFilePathDownload').val(filepath);
                $('#uploaderFile' + id).find('.inputFilePathDelete').val(filepath);
                
                ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
                ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
                ui_multi_update_file_status(id, 'success', 'Upload Completado');
                ui_multi_update_file_progress(id, 100, 'success', false);
            },
            onUploadError: function(id, xhr, status, message){
                ui_multi_update_file_status(id, 'danger', message);
                ui_multi_update_file_progress(id, 0, 'danger', false);  
            },
            onFallbackMode: function(){
              // When the browser doesn't support this plugin :(
                ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
            },
            onFileSizeError: function(file){
                ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
            },
            onFileTypeError: function(file){
                ui_add_log('File \'' + file.name + '\' cannot be added: must be an image (type error)', 'danger');
            },
            onFileExtError: function(file){
              ui_add_log('File \'' + file.name + '\' cannot be added: must be an image (extension error)', 'danger');
            }
        });

        function formatSizeUnits(bytes) 
        {
            if (bytes >= 1073741824) {
                bytes = parseFloat(bytes / 1073741824).toFixed(2);
                bytes += ' GB';
            } else if (bytes >= 1048576) {
                bytes = parseFloat(bytes / 1048576).toFixed(2);
                bytes += ' MB';
            } else if (bytes >= 1024) {
                bytes = parseFloat(bytes / 1024).toFixed(2);
                bytes += ' KB';
            } else if (bytes > 1) {
                bytes = bytes + ' bytes';
            } else if (bytes == 1) {
                bytes = bytes + ' byte';
            } else {
                bytes = '0 bytes';
            }
            return bytes;
        }

        function getIconByTypeFile(type, filename) 
        {
          console.log(type);
            var icon = null;
            switch (type) {
              case 'application/pdf':
                icon = 'fa fa-file-pdf-o';
                break;
              case 'application/vnd.ms-excel': // XLS
                icon = 'fa fa-file-excel-o';
                break;
              case 'application/vnd.ms-excel.sheet.binary.macroenabled.12': // XLSB
                icon = 'fa fa-file-excel-o';
                break;
              case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': //XLSX
                icon = 'fa fa-file-excel-o';
                break;
              case 'application/epub+zip': //XLSX
                icon = 'fa fa-leanpub';
                break;
              case 'image/jpeg': //XLSX
                icon = 'fa fa-file-image-o';
                break;
              case 'image/x-citrix-jpeg': //XLSX
                icon = 'fa fa-file-image-o';
                break;
              case 'image/png': //XLSX
                icon = 'fa fa-file-image-o';
                break;
              default:
                break;
            }

            if (icon != null) {
               return icon;
            }

            var re = /(?:\.([^.]+))?$/;
            var extensionFile = null;

            extensionFile =  re.exec(filename)[1];  
 

            switch (extensionFile) {
              case 'pdf':
                icon = 'fa fa-file-pdf-o';
                break;
              case 'xls': // XLS
                icon = 'fa fa-file-excel-o';
                break;
              case 'xlsb': // XLSB
                icon = 'fa fa-file-excel-o';
                break;
              case 'xlsx': //XLSX
                icon = 'fa fa-file-excel-o';
                break;
              case 'epub': //EPUB
                icon = 'fa fa-leanpub';
                break;
              case 'jpeg': //jpeg/jpg
                icon = 'fa fa-file-image-o';
                break;
              case 'jpg': //jpeg/jpg
                icon = 'fa fa-file-image-o';
                break;
              case 'png': //png
                icon = 'fa fa-file-image-o';
                break;
              default:
                icon = 'fa fa-file-text-o'
                break;
            }

            return icon;
        }

        function cleanFilemName(filename)
        {
          var file = filename;
          var filenameClean = file.replace(/[|&;$%@"<>()+,]/g, "");
          filenameClean = filenameClean.replace(/ /g,'')

          console.log(filenameClean)
          return filenameClean;
        }

  </script>
@endsection
