<div class="row">
    <div class="col-lg-12">
        <div class="card" style="height: 550px">
            <div class="card-header text-uppercase">Quer fazer uma busca em nossos produtos?</div>
            <div class="card-body">
                <div class="row">
                    <form action="{{ route('market.index') }}" method="GET" class="">
                        @csrf
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input  type="text" name="name" value="{{  Illuminate\Support\Facades\Input::get('name')}}"
                                        class="form-control" id="inputmailinline" placeholder="Faça uma pesquisa avançada." style="width: 100%">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::select('product_type', ['V' => 'Digitais', 'F' => "Fisicos", ],  Illuminate\Support\Facades\Input::get('product_type'), ['class'=> 'form-control', "placeholder" => "Selecionar Tipo"]) !!}
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                {!! Form::select('category_id', $categories,  Illuminate\Support\Facades\Input::get('category_id'), ['class'=> 'form-control', "placeholder" => "Selecionar categoria"]) !!}
                            </div>
                        </div>

                        <div class="col-lg-12"><button class="btn btn-success btn-block" type="submit">Buscar</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
