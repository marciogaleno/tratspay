{{--@extends('plataforma.templates.template')--}}

{{--@section('content')--}}
{{--    <!-- Breadcrumb-->--}}
{{--    <div class="row pt-2 pb-2">--}}
{{--        <div class="col-sm-9 col-md-12">--}}
{{--            <h4 class="page-title">Mercado&nbsp;</h4>--}}
{{--            <ol class="breadcrumb"></ol>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <!-- End Breadcrumb-->--}}
{{--    <!--End Row-->--}}

{{--    <!--Form search-->--}}
{{--    @component('plataforma.Market.includes.form-search')--}}
{{--    @endcomponent--}}
{{--    <!--End form-->--}}

    <div class="row">
        <div class="col-md-12">
            <div class="card-header text-uppercase">Os produtos mais vendidos</div>
        </div>
    </div>
    <!--End Row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-warning nav-justified top-icon">
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('market.index') }}"><i class="fa fa-folder-open-o"></i> <span class="hidden-xs">Todos os produtos</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  active  " href="{{ route('market.digital') }}"><i class="fa fa-cubes"></i> <span class="hidden-xs">digitais</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('market.fisicos') }}"><i class="fa fa-fa truck"></i> <span class="hidden-xs">físicos</span></a>
                        </li>
                    </ul>

                    <!--Table cards-->
                    <div class="col-md-12 mt-4" style="">
                        <div class="col-lg-12">
                            <div class="row">
                                @foreach ($products as $product)
                                    <div class="col-lg-3">
                                        <div class="card">

                                            @if ($product['image'])
                                                <img src="{{ url('storage/products/' . $product['image'])}}" class="card-img-top" alt="Card image cap">
                                            @else
                                                <img src="{{url('storage/products')}}" class="card-img-top" alt="Card image cap">
                                            @endif

                                            <div class="card-body">
                                                <h5 class="card-title text-dark">{{$product->name}}</h5>
                                            </div>
                                            <ul class="list-group list-group-flush list shadow-none">
                                                {{--                                <li class="list-group-item d-flex justify-content-between align-items-center">Preço: <span  style="font-weight: bold">{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>--}}
                                                <li class="list-group-item d-flex justify-content-between align-items-center">Comissão: <span  style="font-weight: bold">R$ 0,00</span></li>
                                                <li class="list-group-item d-flex justify-content-between align-items-center">Produtor: <span style="font-weight: bold">Nome</span></li>
                                            </ul>
                                            <div class="card-body text-center">
                                                <a href="{{ route('product.view', $product->uuid) }}" class="btn btn-success waves-effect waves-light m-1 card-link">Promover</a>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!--End table-->
                    <div class="row">
                        <div class="col-md-4"></div>
                        {{--              {!! $products->links() !!}--}}
                        <div class="col-md-4"></div>
                    </div>
                    <!--End Table cards-->
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card-header text-uppercase">Novos produtos</div>
        </div>
    </div>
    <!--End Row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-12 mt-4" style="">
                        <div class="col-lg-12 mb-3">
                            <div class="row">
                                @foreach ($products as $product)
                                    <div class="col-lg-3">
                                        <div class="card">
                                            @if ($product['image'])
                                                <img src="{{ url('storage/products/' . $product['image'])}}" class="card-img-top" alt="Card image cap">
                                            @else
                                                <img src="{{url('storage/products')}}" class="card-img-top" alt="Card image cap">
                                            @endif

                                            <div class="card-body">
                                                <h5 class="card-title text-dark">{{$product->name}}</h5>
                                            </div>
                                            <ul class="list-group list-group-flush list shadow-none">
                                                {{--                                <li class="list-group-item d-flex justify-content-between align-items-center">Preço: <span  style="font-weight: bold">{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>--}}
                                                <li class="list-group-item d-flex justify-content-between align-items-center">Comissão: <span  style="font-weight: bold">R$ 0,00</span></li>
                                                <li class="list-group-item d-flex justify-content-between align-items-center">Produtor: <span style="font-weight: bold">Nome</span></li>
                                            </ul>
                                            <div class="card-body text-center">
                                                <a href="{{ route('product.view', $product->uuid) }}" class="btn btn-success waves-effect waves-light m-1 card-link">Promover</a>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        {{--              {!! $products->links() !!}--}}
                        <div class="col-md-4"></div>
                    </div>
                    <!-- Tab panes -->
                </div>
            </div>
        </div>
    </div>

{{--@endsection--}}
