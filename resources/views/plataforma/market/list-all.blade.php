@extends('plataforma.templates.template')

@section('content')
    <style>
        @import url('https://fonts.googleapis.com/css?family=Open+Sans');
        @import url('https://fonts.googleapis.com/css?family=Montserrat');

        body {
            font-family: 'Montserrat', sans-serif;

        }

        /* Category Ads */

        #ads {
            margin: 30px 0 30px 0;

        }

        #ads .card-notify-badge {
            position: absolute;
            left: -10px;
            top: -20px;
            background: #f2d900;
            text-align: center;
            border-radius: 30px 30px 30px 30px;
            color: #000;
            padding: 5px 10px;
            font-size: 14px;

        }

        #ads .card-notify-year {
            position: absolute;
            right: -10px;
            top: -20px;
            background: #ff4444;
            border-radius: 50%;
            text-align: center;
            color: #fff;
            font-size: 14px;
            width: 50px;
            height: 50px;
            padding: 15px 0 0 0;
        }


        #ads .card-detail-badge {
            background: #f2d900;
            text-align: center;
            border-radius: 30px 30px 30px 30px;
            color: #000;
            padding: 5px 10px;
            font-size: 14px;
        }



        #ads .card:hover {
            background: #fff;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
            border-radius: 4px;
            transition: all 0.3s ease;
        }

        #ads .card-image-overlay {
            font-size: 20px;

        }


        #ads .card-image-overlay span {
            display: inline-block;
        }


        #ads .ad-btn {
            text-transform: uppercase;
            width: 150px;
            height: 40px;
            border-radius: 80px;
            font-size: 16px;
            line-height: 35px;
            text-align: center;
            display: block;
            text-decoration: none;
            margin: 20px auto 1px auto;
            overflow: hidden;
            position: relative;
            color: #fff;
            background-color: #15ca20;
            border-color: #15ca20;
        }

        #ads .ad-btn:hover {
            background-color: #15ca20;
            color: #15ca20;
            border: 2px solid #15ca20;
            background: transparent;
            transition: all 0.3s ease;
            box-shadow: 12px 15px 20px 0px rgba(46,61,73,0.15);
        }

        #ads .ad-title h5 {
            text-transform: uppercase;
            font-size: 18px;
        }
        .list-group-card {
            padding:1px;
            max-height: 40px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 300px;
            border:none;
            /*background-color: #f5f5f5;*/
            text-align:left;
        }
    </style>

    <div class="container-fluid">
        <!-- Breadcrumb-->
        <div class="row pt-2 pb-2">
            <div class="col-sm-9 col-md-12">
                <h4 class="page-title">Mercado&nbsp;</h4>
                <ol class="breadcrumb"></ol>
            </div>
        </div>
        <!-- End Breadcrumb-->
        <!--End Row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-uppercase">Quer fazer uma busca em nossos produtos?</div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <form action="{{ route('market.index') }}" method="GET" class="">
                                @csrf
                                <div class="col-lg-3" style="float:left">
                                    <div class="form-group">
                                        <input  type="text" name="name" value="{{  Illuminate\Support\Facades\Input::get('name')}}"
                                                class="form-control" id="inputmailinline" placeholder="Nome do produto" style="width: 100%">
                                    </div>
                                </div>
                                <div class="col-lg-3" style="float:left">
                                    <div class="form-group">
                                        {!! Form::select('product_type', ['V' => 'Digitais', 'F' => "Fisicos", ],  Illuminate\Support\Facades\Input::get('product_type'), ['class'=> 'form-control', "placeholder" => "Selecionar Tipo"]) !!}
                                    </div>
                                </div>

                                <div class="col-lg-3" style="float:left">
                                    <div class="form-group">
                                        {!! Form::select('category_id', $categories,  Illuminate\Support\Facades\Input::get('category_id'), ['class'=> 'form-control', "placeholder" => "Selecionar categoria"]) !!}
                                    </div>
                                </div>

                                <div class="col-lg-3" style="float:left"><button class="btn btn-success btn-block" type="submit">Buscar</button></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card-header text-uppercase">Os produtos mais vendidos</div>
            </div>
        </div>
        <!--End Row-->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-tabs-warning nav-justified top-icon">
                            <li class="nav-item">
                                <a class="nav-link @if(Illuminate\Support\Facades\Input::get('product_type')== '') active @endif"  href="{{ url('/market/list-all?product_type=') }}"><i class="fa fa-folder-open-o"></i> <span class="hidden-xs">Todos os produtos</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @if(Illuminate\Support\Facades\Input::get('product_type')== 'V') active @endif" href="{{ url('/market/list-all?product_type=V') }}"><i class="fa fa-cubes"></i> <span class="hidden-xs">digitais</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  @if(Illuminate\Support\Facades\Input::get('product_type')== 'F') active @endif" href="{{ url('/market/list-all?product_type=F') }}"><i class="fa fa-fa truck"></i> <span class="hidden-xs">físicos</span></a>
                            </li>
                        </ul>
                        <div class="row">
                            @if(count($products))
                                @foreach ($products as $product)
                                    @if(!$product->configAffiliationWithoutGlobalScope->product_hide)
                                        <div class="col-md-3" style="padding: 20px;padding-top: 0px;">
                                            <div  id="ads">
                                                <!-- Category Card -->
                                                <div class="">
                                                    <div class="card rounded">
                                                        <div class="card-image">
{{--                                                            <span class="card-notify-badge">Low KMS</span>--}}
{{--                                                            <span class="card-notify-year">2018</span>--}}
                                                            @if ($product['image'])
                                                                <img src="{{ \App\Helpers\Helper::getPathFileS3('images', $product['image']) }}" class="card-img-top" alt="Card image cap"
                                                                     style="display: block; margin-left: auto;margin-right: auto;max-width: 300px;width: auto;height: 180px;">
                                                            @else
                                                                <img src="{{url('storage/app/public/products')}}" class="card-img-top" alt="Card image cap"
                                                                     style="display: block; margin-left: auto;margin-right: auto;max-width: 300px;width: auto;height: 180px;">
                                                            @endif
                                                        </div>
                                                        <div class="card-body text-center">
                                                            <div class="ad-title m-auto">
                                                                <h5 class="card-title text-dark" style="margin-bottom:5px; white-space: nowrap;overflow: hidden;text-overflow: ellipsis;" title="{{$product->name}}">{{$product->name}}</h5>
                                                            </div>
                                                            <ul class="list-group list-group-flush list shadow-none text-dark" style="font-size: 11px">
                                                                <li class="list-group-card  align-items-center" style="max-height: 40px;">Preço: <span  style="font-weight: bold">{{$product->payment_type == 'SINGPRICE' ? 'R$ ' . $product->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>
                                                                <li class="list-group-card  align-items-center" style="">Comissão: <span  style="font-weight: bold">R$ {{\App\Helpers\Helper::getValueCommissionByType($product)}}</span></li>
                                                                <li class="list-group-card  align-items-center" style="">P: <span style="font-weight: bold;">
                                                                       @if(!isset($product->producer->first()->client->fantasia))
                                                                            {{$product->producer->first()->name}}
                                                                        @else
                                                                            {{$product->producer->first()->client->fantasia}}
                                                                        @endif
                                                               </span></li>
                                                            </ul>
                                                            <a class="ad-btn" href="{{ route('product.view', $product->id)}}">Promover</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                               @endforeach
                               @else
                               <div class="col-md-12 text-center" style="padding: 20px">
                                   <h4 class="page-title">Nenhum registro encontrado</h4>
                               </div>
                           @endif
                       </div>
                       <div class="col-md-12">
                              <div class="pull-right">{!! $products->appends(request()->except('page'))->links() !!}</div>
                           </div>
                       </div>
                       <!-- Tab panes -->
                   </div>
               </div>
           </div>
       </div>
       <div class="row">
           <div class="col-lg-12">
               <div class="card">
                   <div class="card-header text-uppercase">Novos produtos</div>
                   <div class="card-body">
                       <!-- Tab panes -->
                       <ul class="nav nav-tabs nav-tabs-warning nav-justified top-icon">
                           <li class="nav-item">
                               <a class="nav-link @if(Illuminate\Support\Facades\Input::get('novo_produtos')== '') active @endif"  href="{{ url('/market/list-all?novo_produtos=') }}"><i class="fa fa-folder-open-o"></i> <span class="hidden-xs">Todos os produtos</span></a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link @if(Illuminate\Support\Facades\Input::get('novo_produtos')== 'V') active @endif" href="{{ url('/market/list-all?novo_produtos=V') }}"><i class="fa fa-cubes"></i> <span class="hidden-xs">digitais</span></a>
                           </li>
                           <li class="nav-item">
                               <a class="nav-link  @if(Illuminate\Support\Facades\Input::get('novo_produtos')== 'F') active @endif" href="{{ url('/market/list-all?novo_produtos=F') }}"><i class="fa fa-fa truck"></i> <span class="hidden-xs">físicos</span></a>
                           </li>
                       </ul>
                       <div class="col-lg-12">
                           <div class="card">
                               <div class="row">
                               @if(count($productsLast))
                                   @foreach ($productsLast as $productLast)
                                       @if(!$productLast->configAffiliationWithoutGlobalScope->product_hide)
                                               <div class="col-md-3" style="padding: 20px;padding-top: 0px;">
                                                   <div  id="ads">
                                                       <!-- Category Card -->
                                                       <div class="">
                                                           <div class="card rounded">
                                                               <div class="card-image">
                                                                   {{--<span class="card-notify-badge">Low KMS</span>--}}
                                                                   {{--<span class="card-notify-year">2018</span>--}}
                                                                   @if ($productLast['image'])
                                                                       <img src="{{ \App\Helpers\Helper::getPathFileS3('images', $productLast['image']) }}" class="card-img-top" alt="Card image cap"
                                                                            style="display: block; margin-left: auto;margin-right: auto;max-width: 300px;width: auto;height: 180px;">
                                                                   @else
                                                                       <img src="{{url('storage/app/public/products')}}" class="card-img-top" alt="Card image cap"
                                                                            style="display: block; margin-left: auto;margin-right: auto;max-width: 300px;width: auto;height: 180px;">
                                                                   @endif
                                                               </div>
                                                               <div class="card-body text-center">
                                                                   <div class="ad-title m-auto">
                                                                       <h5 class="card-title text-dark" style="margin-bottom:5px; white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"title="{{$productLast->name}}" >{{$productLast->name}}</h5>
                                                                   </div>
                                                                   <ul class="list-group list-group-flush list shadow-none text-dark" style="font-size: 11px">
                                                                       <li class="list-group-card  align-items-center" style="max-height: 40px;">Preço: <span  style="font-weight: bold">{{$productLast->payment_type == 'SINGPRICE' ? 'R$ ' . $productLast->plansWithoutGlobalScope[0]->price : 'Planos' }}</span></li>
                                                                       <li class="list-group-card  align-items-center" style="">Comissão: <span  style="font-weight: bold">R$ {{$productLast->configAffiliationWithoutGlobalScope->value_commission}}</span></li>
                                                                       <li class="list-group-card  align-items-center" style="">P: <span style="font-weight: bold;">
                                                                       @if(!isset($productLast->producer->first()->client->fantasia))
                                                                                   {{$productLast->producer->first()->name}}
                                                                               @else
                                                                                   {{$productLast->producer->first()->client->fantasia}}
                                                                               @endif
                                                               </span></li>
                                                                   </ul>
                                                                   <a class="ad-btn" href="{{ route('product.view', $productLast->id)}}">Promover</a>
                                                               </div>
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                       @endif

                                   @endforeach
                               @else
                                   <div class="col-md-12 text-center" style="padding: 20px">
                                       <h4 class="page-title">Nenhum registro encontrado</h4>
                                   </div>
                               @endif
                               </div>
                           </div>
                           <div class="col-md-12">
                               <div class="pull-right">{!! $productsLast->appends(request()->except('page'))->links() !!}</div>
                           </div>
                       </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <!--End Row-->
       <!--End Row-->
       <!--End Row-->
   </div>
@stop