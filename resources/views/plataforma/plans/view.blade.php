@extends('plataforma.templates.tabs') 
@section('contentTab')

    <div class="row">
            <div class="col-sm-9">
                <h4 class="text-left">Lista de Planos</h4>
            </div>
    </div>
    <hr>        
    @if ($product) 

    <div class="card-body">
            <div  class="row m-t-2" id="panelAddPlanos" style="font-size: 14px">
                    <div class="table-responsive">
                        <a href="{{route('product.plan.add', [app('request')->product->id])}}" type="button" class="btn btn-success pull-right" style="margin: 5px 5px"><i class="fa fa-plus"></i> Novo </a>
                        <table class="table" id="tableplans">
                        <thead class="thead-light">
                            <tr>
                            <th scope="col">Nome</th>
                            <th scope="col">Grátis</th>
                            <th scope="col">Periodicade</th>
                            <th scope="col">valor</th>
                            <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product->plans as $plan)
                                <tr>
                                    <td>{{$plan->name}}</td>
                                    <td>{{$plan->free ? 'Sim' : 'Não' }}</td>
                                    <td>{!! \App\Helpers\Helper::frequency($plan->frequency, true) !!}</td>
                                    <td>{{'R$ ' . $plan->price}}</td>
                                    <td>
                                        {!! Form::open(['route' => ['product.plan.delete', $plan->product->id, $plan->code], 'method' => 'DELETE', 'onSubmit' => "return validate(this)"]) !!}
                                                <a href="{{route('product.plan.edit', [$plan->product->id, $plan->code])}}" class="btn btn-primary btn-sm" rel="nofollow"><i class="fa fa-pencil"></i> Editar</a>
                                                <button class="btn btn-danger btn-sm"><i class="fa fa-trash"  onclick="return confirm('Are you sure you want to rollback deletion of candidate table?')"></i> Excluir</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>
            </div>
    </div>

    @endif


@endsection

@section('scripts')

<script>
    
    function validate(form) {
        var valid = confirm('Você tem certeza que deseja excluir?');

        if(!valid) {
            return false;
        } else {
            return true;
        }
    }


</script>
@endsection