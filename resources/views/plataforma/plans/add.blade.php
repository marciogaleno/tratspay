@extends('plataforma.templates.tabs') 
@section('contentTab')

    {{ Form::open(['route' => ['product.plan.create', app('request')->product->id], 'method' => 'post']) }}

    <div class="card-body">
        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
          {!! Form::label('name', 'Nome :', ['for' => 'name' ]) !!}
          {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}

          @if ($errors->has('name'))
            <label class="control-label" for="name">
              {{$errors->first('name')}}
            </label>
          @endif 
        </div>
        <div class="form-group has-feedback {{ $errors->has('desc') ? 'has-error' : '' }}">
            {!! Form::label('desc', 'Descrição:', ['for' => 'desc' ]) !!}
            {!! Form::textarea('desc', old('desc'), ['class' => 'form-control', 'rows' => 5]) !!}
        
            @if ($errors->has('desc'))
              <label class="control-label" for="desc">
                {{$errors->first('desc')}}
              </label>
            @endif 
        </div>
        <div class="form-group">
            {{ Form::checkbox('free', null, null, ['id' => 'free']) }}
            <span class="custom-control-description ml-0">Grátis</span> 
        </div>
        <div class="form-group {{ $errors->has('price') ? 'has-error' : '' }}">
            {!! Form::label('price', 'Valor:', ['for' => 'price' ]) !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        
            @if ($errors->has('price'))
              <label class="control-label" for="price" id="labelPrice">
                {{$errors->first('price')}}
              </label>
            @endif 
        </div>
        <div class="form-group {{ $errors->has('frequency') ? 'has-error' : '' }}">
            {!! Form::label('frequency', 'Periodicidade:', ['for' => 'frequency' ]) !!}
            {!! Form::select('frequency', \App\Helpers\Helper::frequency(), null, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --']) !!}
            
            @if ($errors->has('frequency'))
              <label class="control-label" for="frequency" id="labelFrequency">
                {{$errors->first('frequency')}}
              </label>
            @endif                                         
        </div>
        
          <div class="form-group {{ $errors->has('amount_recurrence') ? 'has-error' : '' }}" 
              id="formGroupAmountRecurrence" style="display:none">
              {!! Form::label('amount_recurrence', 'Quantidade de Recorrências: (Deixe o campo vazio para ficar até o cliente cancelar)', ['for' => 'amount_recurrence', 'disabled' => true ]) !!}
              {!! Form::number('amount_recurrence', null, ['class' => 'form-control',  'min' => '0']) !!}
              
              @if ($errors->has('amount_recurrence'))
                <label class="control-label" for="amount_recurrence" id="labelAmountRecurrence">
                  {{$errors->first('amount_recurrence')}}
                </label>
              @endif                                           
          </div>
        
        <div class="form-group {{ $errors->has('first_installment') ? 'has-error' : '' }}" 
          id="formGroupFirstInstallment" style="display:none">
          {!! Form::label('first_installment', 'Primeira Parcela:', ['for' => 'first_installment' ]) !!}
          {!! Form::select('first_installment', \App\Helpers\Helper::firstInstallment(), 1, ['class' => 'custom-select form-control', 'placeholder' => '-- Selecione --', 'disabled' => true]) !!}
        
          @if ($errors->has('first_installment'))
            <label class="control-label" for="first_installment" id="labelFirstInstallment">
              {{$errors->first('first_installment')}}
            </label>
          @endif   
        </div>


        <div class="form-group {{ $errors->has('free_days') ? 'has-error' : '' }}" 
            id="formGroupFreeDays" style="display:none">
            {!! Form::label('free_days', 'Dias Grátis: (A primeira parcela só iniciará a descontar depois desses dias)', ['for' => 'free_days' ]) !!}
            {!! Form::number('free_days', null, ['class' => 'form-control', 'disabled' => true]) !!}
          
            @if ($errors->has('free_days'))
              <label class="control-label" for="free_days" id="labelFreeDays">
                {{$errors->first('free_days')}}
              </label>
            @endif                                           
        </div>

        <div class="form-group {{ $errors->has('different_value') ? 'has-error' : '' }}" 
            id="formGrouDifferentValue" style="display:none">
            {!! Form::label('different_value', 'Valor Diferenciado:', ['for' => 'different_value' ]) !!}
            {!! Form::text('different_value', null, ['class' => 'form-control', 'disabled' => true]) !!}
            
            @if ($errors->has('different_value'))
              <label class="control-label" for="different_value" id="labelDifferentValue">
                {{$errors->first('different_value')}}
              </label>
            @endif   
        </div>

        <div class="row">
          <div class="col-md-12 text-right">
          <a href="{{route('product.plan.view', [app('request')->product->id])}}" class="btn btn-warning"> Voltar</a>
              <button type="submit" class="btn  btn-primary"> Salvar</button>
          </div>
        </div>
      
    </div>
  {!! Form::close() !!}

@endsection
 
@section('scripts')
<script src="{{asset('js/plataforma/maskMoney.min.js')}}"></script>
<script>

        $("#price").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});
        $("#different_value").maskMoney({allowNegative: false, thousands:'.', decimal:',', affixesStay: false});

        @if (old('free'))
          $('#free').trigger('click');
          $("#price").attr('disabled', true);
          $("#price").closest('.form-group').removeClass('has-error');
          $("#labelPrice").text('');

          $("#frequency").attr('disabled', true);
          $("#frequency").closest('.form-group').removeClass('has-error');
          $("#labelFrequency").text('');
          $("#frequency").val('');
        @endif
        
        $('#free').click(function() {
               if ( $(this).prop('checked')) {
                  $("#price").attr('disabled', true);
                  $("#price").closest('.form-group').removeClass('has-error');
                  $("#labelPrice").text('');

                  $("#frequency").attr('disabled', true);
                  $("#frequency").closest('.form-group').removeClass('has-error');
                  $("#labelFrequency").text('');
                  $("#frequency").val('');
                  
                  $("#formGroupAmountRecurrence").hide();
                  $("#amount_recurrence").attr('disabled', true);
                  $("#amount_recurrence").closest('.form-group').removeClass('has-error');
                  $("#labelAmountRecurrence").text('');
                  
                  $("#formGroupFirstInstallment").hide();
                  $("#first_installment").attr('disabled', true);
                  $("#first_installment").closest('.form-group').removeClass('has-error');
                  $("#labelFirstInstallment").text('');

                  $("#formGrouDifferentValue").hide();
                  $("#different_value").attr('disabled', true);
                  $("#different_value").closest('.form-group').removeClass('has-error');
                  $("#labelDifferentValue").text('');

                  $("#formGroupFreeDays").hide();
                  $("#free_days").attr('disabled', true);   
                  $("#free_days").closest('.form-group').removeClass('has-error');
                  $("#labelFreeDays").text('');                     

               } else {
                  $("#price").attr('disabled', false);
                  $("#frequency").attr('disabled', false);
                  
                  
                  $("#formGroupAmountRecurrence").hide();
                  $("#amount_recurrence").attr('disabled', false);
                  
                  $("#formGroupFirstInstallment").hide();
                  $("#first_installment").attr('disabled', false);

                  $("#formGrouDifferentValue").hide();
                  $("#different_value").attr('disabled', false);                 
               }
            });

            $('#frequency').on('change', function() {
              if (this.value && this.value != 'UNI') {
                $("#formGroupAmountRecurrence").show("slow");
                $("#amount_recurrence").attr('disabled', false);
               
                $("#formGroupFirstInstallment").show("slow");
                $("#first_installment").attr('disabled', false);
              } else {
                $("#formGroupAmountRecurrence").hide("slow");
                $("#amount_recurrence").attr('disabled', true);
               
                $("#formGroupFirstInstallment").hide("slow");
                $("#first_installment").attr('disabled', true);

                $("#formGrouDifferentValue").hide("slow");
                $("#different_value").attr('disabled', true);

                $("#formGroupFreeDays").hide("slow");
                $("#free_days").attr('disabled', true);
              }

            });

            $('#first_installment').on('change', function() {
              if ( this.value && this.value == 3 ) {
                $("#formGrouDifferentValue").show("slow");
                $("#different_value").attr('disabled', false);
              } else {
                $("#formGrouDifferentValue").hide("slow");
                $("#different_value").attr('disabled', true);
              }

              if (this.value && this.value == 4 ) {
                $("#formGroupFreeDays").show("slow");
                $("#free_days").attr('disabled', false);
              } else {
                $("#formGroupFreeDays").hide("slow");
                $("#free_days").attr('disabled', true);
              }

            });

            @if ($errors->any())
              $('#frequency').trigger('change');
              $('#first_installment').trigger('change');
            @endif


</script>
@endsection