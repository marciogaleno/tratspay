@extends('emails.layout')

@section('content')
<div style="background-color:transparent;">
<div class="block-grid"
	style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
	<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
		<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#FFFFFF"><![endif]-->
		<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#FFFFFF;width:650px; border-top: 0px solid transparent; border-left: 8px solid #F1F3F3; border-bottom: 0px solid transparent; border-right: 8px solid #F1F3F3;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 50px; padding-left: 50px; padding-top:35px; padding-bottom:5px;background-color:#FFFFFF;"><![endif]-->
		<div class="col num12"
			style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
			<div style="background-color:#FFFFFF;width:100% !important;">
				<!--[if (!mso)&(!IE)]><!-->
				<div
					style="border-top:0px solid transparent; border-left:8px solid #F1F3F3; border-bottom:0px solid transparent; border-right:8px solid #F1F3F3; padding-top:35px; padding-bottom:5px; padding-right: 50px; padding-left: 50px;">
					<!--<![endif]-->
					<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 15px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
					<div
						style="color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:150%;padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
						<p
							style="font-size: 12px; line-height: 24px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 5;">
							<span style="font-size: 16px;">
								Olá, {{$user->name}}!
							</span>
						</p>
						<p
							style="font-size: 12px; line-height: 24px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 5;">
							<span style="font-size: 16px;">
								Você acabou de fazer uma solicitaçaõ para mudar seu email a {{$email}}!
							</span>
						</p>

						<br>
						
						<div class="content-button">
								<a href="{{route('client.update.email', ['id' => $user->id, 'email' => $email])}}" target="_blank">
								<div class="button-call-action">
									<span class="span-button-call-action">
										<span style="font-size: 16px; line-height: 32px;">Atualizar Email</span>
									</span>
								</div>
								</a>
						</div>


					</div>
					<!--[if mso]></td></tr></table><![endif]-->
					<!--[if (!mso)&(!IE)]><!-->
				</div>
				<!--<![endif]-->
			</div>
		</div>
		<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
		<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
	</div>
</div>
</div>
@endsection	