@extends('emails.layout')

@section('content')
 <div style="background-color:transparent;">
	<div class="block-grid"
		style="Margin: 0 auto; min-width: 320px; max-width: 650px; overflow-wrap: break-word; word-wrap: break-word; word-break: break-word; background-color: #FFFFFF;;">
		<div style="border-collapse: collapse;display: table;width: 100%;background-color:#FFFFFF;">
			<!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:transparent;"><tr><td align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px"><tr class="layout-full-width" style="background-color:#FFFFFF"><![endif]-->
			<!--[if (mso)|(IE)]><td align="center" width="650" style="background-color:#FFFFFF;width:650px; border-top: 0px solid transparent; border-left: 8px solid #F1F3F3; border-bottom: 0px solid transparent; border-right: 8px solid #F1F3F3;" valign="top"><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 50px; padding-left: 50px; padding-top:35px; padding-bottom:5px;background-color:#FFFFFF;"><![endif]-->
			<div class="col num12"
				style="min-width: 320px; max-width: 650px; display: table-cell; vertical-align: top;;">
				<div style="background-color:#FFFFFF;width:100% !important;">
					<!--[if (!mso)&(!IE)]><!-->
					<div
						style="border-top:0px solid transparent; border-left:8px solid #F1F3F3; border-bottom:0px solid transparent; border-right:8px solid #F1F3F3; padding-top:35px; padding-bottom:5px; padding-right: 50px; padding-left: 50px;">
						<!--<![endif]-->
						<!--[if mso]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-right: 10px; padding-left: 10px; padding-top: 15px; padding-bottom: 10px; font-family: Arial, sans-serif"><![endif]-->
						<div
							style="color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;line-height:150%;padding-top:15px;padding-right:10px;padding-bottom:10px;padding-left:10px;">
							<p
								style="font-size: 12px; line-height: 24px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									Parabéns {{$commission->user->name}}, você acabou de realizar uma venda do produto {{$transaction->product_name}}
								</span></p>
							<p
								style="font-size: 12px; line-height: 18px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								 </p>
							<p
								style="font-size: 12px; line-height: 27px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 18px;"><strong>Você recebeu: <span
											style="color: #339966; font-size: 18px; line-height: 27px;">R$
											{{number_format($commission->value, 2, ",", ".")}}</span></strong></span>
							</p>
							
							<p
								style="font-size: 12px; line-height: 27px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
										Confira abaixo alguns dados dessa venda:
								</span>
							</p>

							<br>

							<p
								style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>SRC: </strong>
									@if ($source)
										{{$source->src}}
									@endif
								</span>
							</p>
							<p
								style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
										<strong>Transação: #</strong>{{$transaction->id}}
								</span>
							</p>
							<p
								style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
										<strong>Produto: </strong>{{$transaction->product_name}}
								</span>
							</p>
							<p
								style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>Data Compra: </strong>{{\App\Helpers\Helper::dateToBr($commission->transaction->date_created, true)}}
								</span>
							</p>
							<p
								style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>Valor da venda: </strong> R$ {{number_format($transaction->transaction_amount, 2, ",", ".")}}
								</span>
							</p>

							@if ($commission->commission_type == 'producer' && $commissionsAffiliates)
									<p
										style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
										<span style="font-size: 16px;">
											<strong>Comissão afiliado: </strong>

											@foreach ($commissionsAffiliates as $com)
												{{$com->user->name . ' - '  . 'R$' . number_format($com->value, 2, ",", ".")}}
											@endforeach
										</span>
									</p>

									

							@endif
							<br>

							@if ($commission->commission_type == 'producer' || $commission->commission_type == 'coproducer')

							<p
								style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									 Ah! E aqui segue alguns dados de seu cliente:
								</span>
							</p>

							<br>
							<p
							style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>Nome: </strong>{{$transaction->first_name . ' ' . $transaction->last_name }}
								</span>
							</p>
							<p
							style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>E-mail: </strong>{{$transaction->email}}
								</span>
							</p>
							<p
							style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>Telefone: </strong>{{$transaction->phone ?? ''}}
								</span>
							</p>
							<p
							style="font-size: 12px; line-height: 20px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span style="font-size: 16px;">
									<strong>Endereço: </strong>{{$transaction->address ?? ''}}
									{{', Numero:' . $transaction->address_number ?? ''}} 
									{{', Bairro:' . $transaction->address_district ?? ''}}
									{{', Cidade:' . $transaction->address_city ?? ''}} - {{ $transaction->address_state ?? ''}},
									{{', País:' .  $transaction->address_country ?? ''}}
								</span>
							</p>
                                                  
							@endif
							

							<p
								style="font-size: 12px; line-height: 18px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								 </p>
							<p
								style="line-height: 18px; font-size: 12px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								<span
									style="line-height: 18px; color: #000000; font-size: 12px;"><strong><span
											style="line-height: 18px; font-size: 12px;"><span
												style="font-size: 15px; line-height: 22px;"> </span></span></strong></span>
							</p>
							<p
								style="font-size: 12px; line-height: 18px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								 </p>
							<p
								style="font-size: 12px; line-height: 18px; color: #555555; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; margin: 0;">
								 </p>
						</div>
						<!--[if mso]></td></tr></table><![endif]-->
						<!--[if (!mso)&(!IE)]><!-->
					</div>
					<!--<![endif]-->
				</div>
			</div>
			<!--[if (mso)|(IE)]></td></tr></table><![endif]-->
			<!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
		</div>
	</div>
</div>
@endsection	
