@extends('layouts.app-login')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h5 class="text-center">{{ __('Verifique ativação de seu email') }}</h5>
                <hr/>

                @if (session('resent'))
                    <div class="alert alert-success" role="alert">
                        {{ __('Um novo link de verificação foi enviado para o seu endereço de e-mail.') }}
                    </div>
                @endif

                {{ __('Antes de prosseguir, verifique seu e-mail em busca de um link de ativação.') }}

                <br/>
            </div>
            <div class="col-md-12 text-center" style="margin-top: 20px;">
                {{ __('Se você não recebeu o email') }}.
                <a class="btn btn-info"
                   href="{{ route('verification.resend') }}">{{ __('clique aqui para solicitar outro') }}</a>

            </div>
        </div>
    </div>
@endsection
