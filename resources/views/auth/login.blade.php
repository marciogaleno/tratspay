@extends('layouts.app-login')
@section('content')
@if ($errors->any())
  <div class="alert alert-light-danger alert-dismissible" role="alert">
      <div class="alert-message">
          <span style="text-align:center"><strong>Error</strong> ao fazer login. Verifique os dados informados.</span>
        </div>
  </div>'
@endif
<div class="card-title text-uppercase text-center py-2">Acesse sua conta</div>

{!! Form::open(['route' => Route::currentRouteName() ]) !!}
  <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">

    <div class="position-relative has-icon-left">
      {!! Form::text('email', null, ['class' => 'form-control sty1', 'placeholder' => 'E-mail']) !!}
      <div class="form-control-position">
          <i class="icon-user"></i>
      </div>
      @if ($errors->has('email'))
        <label class="error" for="inputErrorName">{{$errors->first('email')}}</label>
      @endif

    </div>

  </div>

  <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">

    <div class="position-relative has-icon-left">
      {!! Form::password('password', ['class' => 'form-control sty1', 'placeholder' => 'Senha']) !!}
      <div class="form-control-position">
          <i class="icon-lock"></i>
      </div>
      @if ($errors->has('password'))
        <label class="error" for="inputErrorName">{{$errors->first('password')}}</label>
      @endif

    </div>
</div>


<div>
  <div class="col-xs-8">
    <div class="form-group text-left">
      <a href="{{url('/password/reset')}}" class="text-" style="font-family: inherit;"><i class="fa fa-lock"></i> Esqueceu a senha?</a>
    </div>
  </div>
  <!-- /.col -->
  <div class="form-group">
    {!! Form::button('Acessar Conta', ['type' => 'submit', 'class' => 'btn btn-warning btn-block waves-effect waves-light'])!!}
  </div>
  <!-- /.col -->
</div>
{!! Form::close() !!}
<div class="social-auth-links text-center">

</div>
<!-- /.social-auth-links -->
<div class="form-group text-center">
    <p class="text-muted">Ainda não possui uma conta? <a href="./register" style="font-family: inherit;"> Crie a sua conta na TratsPay</a></p>
</div>
@endsection