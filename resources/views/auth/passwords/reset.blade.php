@extends('layouts.app-login')
@section('content')
@if ($errors->any())
<div class="alert alert-light-danger alert-dismissible" role="alert">
    <div class="alert-message">
        <span style="text-align:center"><strong>Error</strong> ao fazer login. Verifique os dados informados.</span>
    </div>
</div>'
@endif
<div class="card-title text-uppercase text-center py-2">Recuperar Senha</div>

{!! Form::open(['route' => 'password.update' ]) !!}
<input type="hidden" name="token" value="{{ $token }}">
<div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">

    <div class="position-relative has-icon-left">
        {!! Form::text('email', $email ?? old('email'), ['class' => 'form-control', 'placeholder' => 'E-mail', 'required']) !!}
        <div class="form-control-position">
            <i class="fa fa-envelope-open-o"></i>
        </div>
        @if ($errors->has('email'))
        <label class="error" for="inputErrorName">{{$errors->first('email')}}</label>
        @endif

    </div>

</div>

<div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">

    <div class="position-relative has-icon-left">
        {!! Form::password('password', ['class' => 'form-control sty1', 'placeholder' => 'Digite sua nova senha', 'required']) !!}
        <div class="form-control-position">
            <i class="icon-lock"></i>
        </div>
        @if ($errors->has('password'))
        <label class="error" for="inputErrorName">{{$errors->first('password')}}</label>
        @endif

    </div>
</div>

<div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">

    <div class="position-relative has-icon-left">
        {!! Form::password('password_confirmation', ['class' => 'form-control sty1', 'placeholder' => 'Confirmar senha', 'required']) !!}
        <div class="form-control-position">
            <i class="icon-lock"></i>
        </div>
        @if ($errors->has('password_confirmation'))
        <label class="error" for="inputErrorName">{{$errors->first('password_confirmation')}}</label>
        @endif

    </div>
</div>


<div>
    <!-- /.col -->
    <div class="form-group">
        {!! Form::button('Alterar Senha', ['type' => 'submit', 'class' => 'btn btn-warning btn-block waves-effect
        waves-light'])!!}
    </div>
    <!-- /.col -->
</div>
{!! Form::close() !!}
<div class="social-auth-links text-center">

</div>

@endsection