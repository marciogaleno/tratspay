@extends('layouts.app-login')
@section('content')
@if ($errors->any())
<div class="alert alert-light-danger alert-dismissible" role="alert">
    <div class="alert-message">
        <span style="text-align:center"><strong>Error</strong> ao fazer login. Verifique os dados informados.</span>
    </div>
</div>'
@endif
<div class="card-title text-uppercase text-center py-2">Recuperar Senha</div>

@if (session('status'))
<div class="alert alert-light-success alert-dismissible" role="alert">
    <div class="alert-message">
        {{ session('status') }}
    </div>
</div>
@endif

{!! Form::open(['route' => 'password.email' ]) !!}
<div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">

    <div class="position-relative has-icon-left">
        {!! Form::text('email', $email ?? old('email'), ['class' => 'form-control sty1', 'placeholder' => 'Digite seu e-mail']) !!}
        <div class="form-control-position">
            <i class="fa fa-envelope-open-o"></i>
        </div>
        @if ($errors->has('email'))
        <label class="error" for="inputErrorName">{{$errors->first('email')}}</label>
        @endif

    </div>

</div>

<div>
    <!-- /.col -->
    <div class="form-group">
        {!! Form::button('Recuperar', ['type' => 'submit', 'class' => 'btn btn-warning btn-block waves-effect
        waves-light'])!!}
    </div>
    <!-- /.col -->
</div>
{!! Form::close() !!}
<div class="social-auth-links text-center">

</div>

<div class="form-group text-center">
        <p class="text-muted">Lembrou sua senha? <a href="{{url('/login')}}" style="font-family: inherit;"> Então faça login</a></p>
    </div>
@endsection