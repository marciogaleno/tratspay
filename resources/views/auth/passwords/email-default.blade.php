@extends('layouts.app-login')

@section('content')
    <style>
        .card-authentication1 {
            max-width: 38rem;
        }
    </style>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Recuperar Senha') }}</div>

                <div class="card-body" >
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail ') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-12" style="padding-right: 0px">
                            <div class=" pull-right">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Recuperar senha') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div><br>
                <div class="form-group text-center">
                    <p class="text-muted">Lembrou sua senha? <a href="{{url('/login')}}" style="font-family: inherit;"> Então faça login</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
