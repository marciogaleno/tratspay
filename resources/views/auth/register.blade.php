@extends('layouts.app-login')

@section('content')
<br> <div class="card-title text-uppercase text-center py-2">Cadastre-se</div>

    {!! Form::open(['route' => 'register']) !!}

      <div class="form-group  has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">
        <div class="position-relative has-icon-left">

        {!! Form::text('name', null, ['class' => 'form-control sty1', 'placeholder' => 'Nome', 'id' => 'inputErrorName']) !!}
        <div class="form-control-position">
            <i class="icon-user"></i>
        </div>
        @if ($errors->has('name'))
        <label class="control-label" for="inputErrorName">{{$errors->first('name')}}</label>
        @endif

        </div>
      </div>

    

      <div class="form-group has-feedback  {{ $errors->has('email') ? 'has-error' : '' }}">

          <div class="position-relative has-icon-left">
            {!! Form::text('email', null, ['class' => 'form-control sty1', 'placeholder' => 'E-mail', 'id' => 'inputErrorEmail']) !!}
            <div class="form-control-position">
              <i class="icon-envelope-open"></i>
            </div>
            @if ($errors->has('email'))
            <label class="control-label" for="inputErrorEmail">{{$errors->first('email')}}</label>
            @endif  
          </div>
      </div>

      <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">

          <div class="position-relative has-icon-left">

            {!! Form::password('password', ['class' => 'form-control sty1', 'placeholder' => 'Senha', 'id' => 'inputErrorEmail']) !!}
            
            <div class="form-control-position">
                <i class="icon-lock"></i>
            </div>
            @if ($errors->has('password'))
            <label class="control-label" for="inputErrorEmail">{{$errors->first('password')}}</label>
            @endif 

          </div>
      </div>

      <div class="form-group has-feedback  {{ $errors->has('password') ? 'has-error' : '' }}">

          <div class="position-relative has-icon-left">

            {!! Form::password('password_confirmation', ['class' => 'form-control sty1', 'placeholder' => 'Confirme a senha', 'id' => 'inputErrorConfirmeEmail']) !!}
            <div class="form-control-position">
                <i class="icon-lock"></i>
            </div>
            @if ($errors->has('password'))
            <label class="control-label" for="inputErrorConfirmeEmail">{{$errors->first('password')}}</label>
            @endif 

            
          </div>

        </div>

        <div>
        <div class="col-xs-8">
            <div class="form-group text-left">
                <label>
                    {!! Form::checkbox('accept_terms') !!} Eu concordo com os
                    <a href="http://www.tratspay.com.br/termos-uso" class="text-muted">Termos de Uso</a>
                </label>
                @if ($errors->any())
                <label class="control-label" style="color:#dd4b39" for="inputError1">{{$errors->first('accept_terms')}}</label>
                @endif 
            </div> 
        </div>
        <!-- /.col -->
        <div class="col-xs-4 m-t-1">
          {!! Form::button('Criar Conta', ['type' => 'submit', 'class' => 'btn btn-warning btn-block waves-effect waves-light'])!!}
        </div>
        <!-- /.col --> 
      </div>
    {!! Form::close() !!}
      <br>
    <div class="form-group text-center">
        <p class="text-muted">Já possui uma conta? <a href="./login"> Faça login</a></p>
    </div>
@endsection
