<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => false]);

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

/*Route::get('mailable', function () {
    $user = App\User::find(1);
    $product = App\Models\Plataforma\Product::find(2);
    $commission = App\Models\Plataforma\Commission::where("id", 263)->with('transaction')->first();
    $commissionsAffiliates =  App\Models\Plataforma\Commission::where('transaction_id',  82)
    ->where('commission_type', 'affiliate')
    ->get();

    return new App\Mail\SendMail($commission, $commission->transaction);
    return new App\Mail\SendMailBuyer("Imprima seu boleto", true, true, '12346', $commission->transaction);
});*/

/*Route::get('getImagem', function () {

    header("Content-type: image/jpg");

    return response(Storage::disk('s3')->get('teste/zCHbXvjg91i3TuixPGOhvEJw7DCTzefFI36RQGYq.jpeg'), 200)
    ->header('Content-Type', 'image/jpg');
    
});*/



/*Route::get('testepostback', function () {
    $transaction = TransactionMp::where('id', 1)->first();

    $status = [
        'pending',
        'refund',
        'abandon_checkout',
        'finished',
        'blocked',
        'canceled',
        'completed'];
        
    $key =  array_rand($status);

    $transaction->update([
        'status' => $status[$key],
    ]);
    return new App\Mail\SendMailBuyer("Imprima seu boleto", false, false, '12346', $commission->transaction);
});*/

Route::group(['namespace' => 'Plataforma'], function() {
    Route::get('chk/{codCht}', 'CheckoutsController@front')
        ->name('checkout.front');
        
    Route::get('c/{codCht}', 'CheckoutsController@front')
        ->name('checkout.front');

    Route::post('chk/payment/{codCht}', 'CheckoutsController@payment')->name('checkout.payment');
    Route::get('chk/payment/finalizado', 'CheckoutsController@testeFinalizacao');
    Route::get('chk/payment/ticket/{transaction_id}', 'CheckoutsController@ticket')->name('checkout.payment.ticket');

    Route::get('/r/{promotionCode}', 'ResolveUrlController@resolve')
        ->name('resolve.url')
        ->where('codeTracking', '[a-zA-Z0-9]+');

    Route::get('convite/{code}', 'ConfigAffiliationController@recruitAffiliate')
        ->name('configaffiliation.convite'); 

    Route::get('convitelogin/{code}', 'ConfigAffiliationController@login')
        ->name('configaffiliation.login');   

    Route::get('i/{indication_code}', 'IndicationController@tracking')
        ->name('indication.tracking');   
});

Route::group(['namespace' => 'Plataforma', 'middleware' => 'auth'], function() {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::post('/updatepassword', 'UsersController@updatePassword');
    Route::get('/getmessages', 'TalkAffiliatesController@getMessagesApi');
    Route::get('/getnotifications', 'NotificationController@getNotificationsApi');
    //products

    Route::get('products/view/{id}', 'ProductsController@view')->name('product.view');
    Route::get('products', 'ProductsController@list')->name('product.list');
    Route::get('products/add', 'ProductsController@add')->name('product.add');
    Route::post('products/store', 'ProductsController@store')->name('product.store');
    Route::put('products/update/{id}', 'ProductsController@update')->name('product.update');
    Route::post('products/uploadFile', 'ProductsController@uploadFile');
    Route::delete('products/deleteFile/{uuid}/{file}', 'ProductsController@deleteFile')->name('product.deleteFile');
    Route::post('products/downloadFile/{uuid}/{file}', 'ProductsController@downloadFile')->name('product.downloadFile');
    Route::post('products/affiliate', 'ProductsController@affiliate')->name('product.affiliate');
    Route::get('products/tabs/{id}', 'ProductsController@tabs')->name('product.tabs');

    Route::any('products/affiliates', 'ProductsController@complaints')->name('product.complaints');


    //MEUS AFILIADOS - PRODUTOR
    Route::get('myaffiliates', 'MyAffiliatesController@index')->name('myaffiliates.list');

    //FALAR COM AFILIADOS
    Route::get('talktoaffiliates', 'TalkAffiliatesController@index')->name('talktoaffiliates.index');
    Route::get('talktoaffiliates/write', 'TalkAffiliatesController@getCreate')->name('talktoaffiliates.write');
    Route::get('talktoaffiliates/view/{id}', 'TalkAffiliatesController@getView')->name('talktoaffiliates.view');
    Route::get('talktoaffiliates/allmessages', 'TalkAffiliatesController@allMessages')->name('talktoaffiliates.allmessages');
    Route::put('talktoaffiliates/allmessages/edit', 'TalkAffiliatesController@updateMessage')->name('talktoaffiliates.allmessages.put');
    Route::post('talktoaffiliates/send', 'TalkAffiliatesController@send')->name('talktoaffiliates.send');


    //NOTIFICATIONS
    Route::get('notifications', 'NotificationController@index')->name('notifications.index');
    Route::get('notifications/view/{id}', 'NotificationController@getView')->name('notifications.view');

    // Route::post('products/afilliate', function(){
    //     $statusafiliate = Input::has('statusafiliate') ? true : false;

    //     ProducerAffiliate::update([
    //         'statusafiliate' => $statusafiliate
    //     ]);

    //     return 'created';
    // });


     //Usuários
     Route::get('user/perfil/{id}', 'UsersController@userPerfil')->name('user.perfil');
     Route::get('user/myProfile', 'UsersController@userPerfil')->name('user.myprofile');

     //Cliente
     Route::get('client/perfil', 'UsersController@perfil')->name('client.perfil');
     Route::get('client/edit', 'ClientsController@edit')->name('client.edit');
     Route::put('client/update', 'ClientsController@update')->name('client.update');
     Route::get('client/update/email', 'ClientsController@updateEmail')->name('client.update.email');
     Route::post('client/bank-account/add', 'ClientsController@addBankAccount')->name('client.add.bank.account');
     Route::delete('client/bank-account/delete', 'ClientsController@removeBankAccount')->name('client.delete.bank.account');
     Route::post('client/change-image', 'ClientsController@changeImage')->name('client.change.image');


    // Market
    Route::get('market/list-all', 'MarketController@index')->name('market.index');
    Route::get('market/last', 'MarketController@getLastProducts')->name('market.last');

    //Report
    Route::get('report/salesaffiliate', 'ReportController@salesAffiliates')->name('report.salesaffiliate');
    Route::get('report/salescoproducer', 'ReportController@salesCoProducer')->name('report.salescoproducer');
    Route::get('report/salescoaffiliate', 'ReportController@salesCoAffiliates')->name('report.salescoaffiliate');
    Route::get('report/salesmanager', 'ReportController@salesManager')->name('report.salesmanager');
    Route::get('report/salesIndication', 'ReportController@salesIndication')->name('report.salesIndication');


    // Afiliates
    Route::get('affiliates/detailsProduct/{productUuid}', 'AffiliatesController@detailsProduct')->name('afilliate.detailsProduct');
    Route::get('affiliates/urls/{productUuid}', 'AffiliatesController@urls')->name('afilliate.urls');
    Route::get('affiliates', 'AffiliatesController@list')->name('afilliate.list');
    Route::get('affiliates/list-request', 'AffiliatesController@myRequest')->name('afilliate.list-request');
    Route::post('affiliates/cancel', 'AffiliatesController@cancel')->name('afilliate.cancel');
    Route::post('affiliates/my-affiliates', 'AffiliatesController@listMyAffiliates')->name('afilliate.listMyAffiliates');
    Route::post('affiliates/status-affiliates', 'AffiliatesController@statusAffiliate')->name('afilliate.statusAffiliate');

    Route::get('affiliates-solicitacao/list', 'AffiliatesController@solicitacao')->name('afilliate-solicitacao.list');

    Route::post('coproducers/approve/{coproducerId}', 'CoProducersController@approve')->name('coproducer.approve');
    Route::post('coproducers/cancel/{coproducerId}', 'CoProducersController@cancel')->name('coproducer.cancel');
    Route::get('coproducers/received', 'CoProducersController@received')->name('coproducer.received');

    //CoAffiliates
    Route::get('coaffiliates/add/{product_id}', 'CoAffiliatesController@add')->name('coaffiliate.add');
    Route::get('coaffiliates/edit/{product_id}', 'CoAffiliatesController@edit')->name('coaffiliate.edit');
    Route::post('coaffiliates/create/{product_id}', 'CoAffiliatesController@create')->name('coaffiliate.create');
    Route::get('coaffiliates/list/{product_id}', 'CoAffiliatesController@list')->name('coaffiliate.list');
    Route::post('coaffiliates/approve/{product_id}', 'CoAffiliatesController@approve')->name('coaffiliate.approve');
    Route::post('coaffiliates/cancel/{product_id}', 'CoAffiliatesController@cancel')->name('coaffiliate.cancel');
    Route::delete('coaffiliates/delete', 'CoAffiliatesController@delete')->name('coaffiliate.delete');
    Route::get('coaffiliates/received', 'CoAffiliatesController@received')->name('coaffiliate.received');
    Route::get('coaffiliates/report', 'CoAffiliatesController@report')->name('coaffiliate.report');

    // Purchases

    Route::get('purchases', 'PurchasesController@index')->name('purchase.list');
    Route::post('purchases/download', 'PurchasesController@download')->name('purchase.download');

    // Sales
    Route::get('sales', 'SalesController@list')->name('sale.list');
    Route::get('sales/detail', 'SalesController@detailRelatorio')->name('sale.detailsales');
    Route::get('sales/details/{transactionId}', 'SalesController@details')->name('sale.details');
    Route::get('sales/affiliatesByProduct/{product_id}', 'SalesController@affiliatesByProduct')->name('sale.affiliatesByProduct');
    Route::post('sales/reprocessCommission', 'SalesController@reprocessCommission')->name('sale.reprocessCommission');
    Route::post('sales/editEmailUser', 'SalesController@editEmailUser')->name('sale.editEmailUser');
    Route::post('sales/resendDataAffiliate', 'SalesController@resendDataAffiliate')->name('sale.resendDataAffiliate');
    Route::post('sales/resendTicket', 'SalesController@resendTicket')->name('sale.resendTicket');
    Route::get('sales/tracking', 'SalesController@tracking')->name('sale.tracking');

    // Indicated
    Route::get('indication', 'IndicationController@index')->name('indication.index');

    // Saques
    Route::get('cashouts/', 'CashoutController@index')->name('cashout.index');
    Route::post('cashouts/create', 'CashoutController@create')->name('cashout.create');

    //Tools
    Route::get('tools', 'ToolsController@index')->name('tool.index');
    Route::get('tools/linkManager', 'ToolsController@linkManager')->name('tool.linkManager');
    Route::get('tools/leadAutomator', 'ToolsController@leadAutomator')->name('tool.leadAutomator');

    //Postback
    Route::get('tools/postback', 'PostbackController@index')->name('postback.index');
    Route::post('tools/create', 'PostbackController@create')->name('postback.create');
    Route::get('tools/postback/{id}', 'PostbackController@edit')->name('postback.edit');
    Route::put('tools/postback/{id}', 'PostbackController@update')->name('postback.update');
    Route::delete('tools/postback/delete/{id}', 'PostbackController@delete')->name('postback.delete');
    Route::post('tools/postback/test', 'PostbackController@test')->name('postback.test');

    //Reembolso
    Route::get('reembolso', 'ReembolsoController@index')->name('reembolso.index');
    Route::get('reembolso/view/{id}', 'ReembolsoController@view')->name('reembolso.view');
    Route::get('reembolso/edit/{id}/{status}', 'ReembolsoController@edit')->name('reembolso.edit');
    Route::post('reembolso/save', 'ReembolsoController@create');
});



// Products Tabs
Route::group(['namespace' => 'Plataforma', 'prefix' => 'product/{product_id}', 'middleware' => ['auth', 'product.exists']], function($id) {
    Route::get('edit', 'ProductsController@edit')->name('product.edit');
    Route::get('memberArea', 'ProductsController@memberArea')->name('product.memberArea');
    Route::put('updateMemberArea', 'ProductsController@updateMemberArea')->name('product.updateMemberArea');

    //sales
    Route::get('sales', 'ProductsController@sales')->name('product.sales');

    //plans
    Route::get('plans', 'PlansController@view')->name('product.plan.view');
    Route::get('plans/add', 'PlansController@add')->name('product.plan.add');
    Route::post('plans/create', 'PlansController@create')->name('product.plan.create');
    Route::get('plans/{codePlan}', 'PlansController@edit')->name('product.plan.edit');
    Route::put('plans/update/{codePlan}', 'PlansController@update')->name('product.plan.update');
    Route::delete('plans/delete/{codePlan}', 'PlansController@delete')->name('product.plan.delete');

    //Checkouts
    Route::get('checkouts/{planId?}', 'CheckoutsController@view')->name('product.checkout.view');
    Route::get('checkout/add/', 'CheckoutsController@add')->name('product.checkout.add');
    Route::post('checkouts/create', 'CheckoutsController@create')->name('product.checkout.create');
    Route::get('checkouts/search/{planId}', 'CheckoutsController@search')->name('product.checkout.search');
    Route::get('checkouts/edit/{checkoutId}', 'CheckoutsController@edit')->name('product.checkout.edit');
    Route::put('checkouts/update/{codCheckout}', 'CheckoutsController@update')->name('product.checkout.update');
    Route::get('checkouts/search/{planId}', 'CheckoutsController@search')->name('product.checkout.search');
    Route::delete('checkouts/delete/{codCheckout}', 'CheckoutsController@delete')->name('product.checkout.delete');

    //URLs
    Route::get('urls', 'UrlsController@index')->name('product.url.index');
    Route::post('urls/createUrlAlternative', 'UrlsController@createUrlAlternative')->name('product.url.createUrlAlternative');

    //CoProducers
    Route::get('coproducers', 'CoProducersController@list')->name('product.coproducer.list');
    Route::get('coproducers/add', 'CoProducersController@add')->name('product.coproducer.add');
    Route::get('coproducers/{coproducerId}', 'CoProducersController@edit')->name('product.coproducer.edit');
    Route::post('coproducers/create', 'CoProducersController@create')->name('product.coproducer.create');
    Route::put('coproducers/update/{coproducerId}', 'CoProducersController@update')->name('product.coproducer.update');
    Route::delete('coproducers/delete/{coproducerId}', 'CoProducersController@delete')->name('product.coproducer.delete');

    //Files
    Route::get('files', 'FilesController@index')->name('product.file.index');
    Route::post('files/upload', 'FilesController@upload')->name('product.file.upload');
    Route::delete('files/delete', 'FilesController@delete')->name('product.file.delete');
    Route::post('files/downnlad', 'FilesController@download')->name('product.file.download');

    //ConfigAffiliation
    Route::get('configaffiliation', 'ConfigAffiliationController@edit')->name('product.configaffiliation.edit');
    Route::put('configaffiliation/update/{id}', 'ConfigAffiliationController@update')->name('product.configaffiliation.update');

    // Media
    Route::get('media/add', 'MediaController@add')->name('product.media.add');
    Route::post('media/create', 'MediaController@create')->name('product.media.create');
    Route::delete('media/delete/{id}', 'MediaController@delete')->name('product.media.delete');

    // Campaings
    Route::get('campaigns', 'CampaignsController@index')->name('campaign.index');
    Route::get('campaigns/add', 'CampaignsController@add')->name('campaign.add');
    Route::get('campaigns/edit/{code}', 'CampaignsController@edit')->name('campaign.edit');
    Route::post('campaigns/create', 'CampaignsController@create')->name('campaign.create');
    Route::put('campaigns/update/{code}', 'CampaignsController@update')->name('campaign.update');
    Route::delete('campaigns/delete/{code}', 'CampaignsController@delete')->name('campaign.delete');

    // Coupons
    Route::get('coupons', 'CouponsController@index')->name('product.coupon.index');
    Route::get('coupons/add', 'CouponsController@add')->name('product.coupon.add');
    Route::post('coupons/create', 'CouponsController@create')->name('product.coupon.create');
    Route::get('coupons/edit/{id}', 'CouponsController@edit')->name('product.coupon.edit');
    Route::put('coupons/update/{id}', 'CouponsController@update')->name('product.coupon.update');
    Route::delete('coupons/delete/{id}', 'CouponsController@delete')->name('product.coupon.delete');
    Route::get('coupons/urls/{id}', 'CouponsController@urls')->name('product.coupon.urls');
});

/**===============================
* Admin
==================================*/
Route::group(['namespace' => 'Admin', 'prefix' => 'origin', 'middleware' => 'security'], function() {
    /**===============================
    * Dashboard Admin
    ==================================*/
    Route::get('/', 'DashboardController@index')->name('admin.index');
    Route::get('index/index', 'DashboardController@index')->name('admin.index');
    /**===============================
    * Module
    ==================================*/
    Route::group(['prefix'=> 'module'], function(){
        Route::get('index', 'ModuleController@index')->name('admin.module.index');
        Route::get('create', 'ModuleController@create')->name('admin.module.create');
        Route::post('create', 'ModuleController@store');
        Route::post('delete', 'ModuleController@destroy')->name('admin.module.delete');
    });
    /**===============================
    * Category Resource
    ==================================*/
    Route::group(['prefix'=> 'category'], function(){
        Route::get('index', 'CategoryResourceController@index')->name('admin.category.resource.index');
        Route::get('create', 'CategoryResourceController@create')->name('admin.category.resource.create');
        Route::post('create', 'CategoryResourceController@store');
        Route::post('delete', 'CategoryResourceController@destroy')->name('admin.category.resource.delete');
    });
    /**===============================
    * Resource
    ==================================*/
    Route::group(['prefix'=> 'resource'], function(){
        Route::get('index', 'ResourceController@index')->name('admin.resource.index');
        Route::get('create', 'ResourceController@create')->name('admin.resource.create');
        Route::post('create', 'ResourceController@store');
        Route::post('delete', 'ResourceController@destroy')->name('admin.resource.delete');
    });
    /**===============================
    * Roles
    ==================================*/
    Route::group(['prefix'=> 'roles'], function(){
        Route::get('index', 'RoleController@index')->name('admin.role.index');
        Route::get('create', 'RoleController@create')->name('admin.role.create');
        Route::post('create', 'RoleController@store');
        Route::post('delete', 'RoleController@destroy')->name('admin.role.delete');
    });
    /**===============================
    * Type Account
    ==================================*/
    Route::group(['prefix'=> 'type-account'], function(){
        Route::get('index', 'TypeAccountController@index')->name('admin.type.account.index');
        Route::get('create', 'TypeAccountController@create')->name('admin.type.account.create');
        Route::post('create', 'TypeAccountController@store');
        Route::post('delete', 'TypeAccountController@destroy')->name('admin.type.account.delete');
    });
    /**===============================
    * Type Account Roles
    ==================================*/
    Route::group(['prefix'=> 'type-account-roles'], function(){
        Route::get('index', 'TypeAccountRoleController@index')->name('admin.type.account.roles.index');
        Route::get('sync/{id}', 'TypeAccountRoleController@sync')->name('admin.type.account.roles.sync');
        Route::post('sync', 'TypeAccountRoleController@syncRole')->name('admin.type.account.roles.syncrole');
    });
    /**===============================
    * Type Account User
    ==================================*/
    Route::group(['prefix'=> 'type-account-user'], function(){
        Route::get('index', 'TypeAccountUserController@index')->name('admin.type.account.user.index');
        Route::get('assign/{id}', 'TypeAccountUserController@assign')->name('admin.type.account.user.assign');
        Route::get('type-accounts', 'TypeAccountUserController@typeAccounts')->name('admin.type.account.user.typeaccount');
        Route::post('attach', 'TypeAccountUserController@attach')->name('admin.type.account.user.attach');
        Route::post('detach', 'TypeAccountUserController@detach')->name('admin.type.account.user.detach');
    });
    /**===============================
    * Login
    ==================================*/
    Route::get('login', 'AuthController@showLoginForm')->name('admin.login.form');
    Route::post('login', 'AuthController@login')->name('admin.login.index');
    /**===============================
    * Client
    ==================================*/
    Route::group(['prefix'=> 'configuration'], function(){
        Route::get('index', 'SystemConfigurationController@index')->name('admin.configuration.index');
        Route::put('update', 'SystemConfigurationController@update')->name('admin.configuration.update');
        Route::post('category/{category}', 'SystemConfigurationController@category')->name('admin.configuration.category');
    });
    /**===============================
    * Platform
    ==================================*/
    /*Route::group(['prefix'=> 'platform'], function(){
        Route::get('index', 'PlatformController@index')->name('admin.platform.index');
    });*/
    /**===============================
    * Cashouts
    ==================================*/
    Route::group(['prefix'=> 'cashouts'], function(){
        Route::get('index', 'CashoutController@index')->name('admin.cashout.index');
        Route::put('process/{id}', 'CashoutController@process')->name('admin.cashout.process');
    });
    /**===============================
    * User Admins
    ==================================*/
    Route::group(['prefix'=> 'user-admin'], function(){
        Route::get('index', 'UserAdminController@index')->name('admin.user.admin.index');
    });
    /**===============================
    * Users
    ==================================*/
    Route::group(['prefix'=> 'users'], function(){
        Route::get('index', 'UserController@index')->name('admin.users.index');
        Route::get('index/{id}', 'UserController@show')->name('admin.users.show');
    });
    /**===============================
    * Approve Account
    ==================================*/
    Route::group(['prefix'=> 'approve-account'], function(){
        Route::get('index', 'AccountController@index')->name('admin.account.approve.index');
    });
    /**===============================
    * Profiles
    ==================================*/
    /*Route::group(['prefix'=> 'profiles'], function(){
        Route::get('index', 'ProfileController@index')->name('admin.profile.index');
    });*/
    /**===============================
    * Product All
    ==================================*/
    Route::group(['prefix'=> 'products'], function(){
        Route::get('index', 'ProductController@index')->name('admin.product.index');
        Route::get('edit/{id}', 'ProductController@edit')->name('admin.product.edit');
        Route::put('update/{id}', 'ProductController@update')->name('admin.product.update');
    });
    /**===============================
    * Product Pendent
    ==================================*/
    Route::group(['prefix'=> 'product-pendent'], function(){
        Route::get('index', 'ProductController@pendent')->name('admin.product.pendent');
    });
    /**===============================
    * Product Disapprove
    ==================================*/
    Route::group(['prefix'=> 'product-disapprove'], function(){
        Route::get('index', 'ProductController@disapprove')->name('admin.product.disapprove');
    });
    /**===============================
    * Report Sales
    ==================================*/
    Route::group(['prefix'=> 'report-sales'], function(){
        Route::get('index', 'ReportController@sales')->name('admin.report.sales');
    });
    /**===============================
    * Report Extract
    ==================================*/
    Route::group(['prefix'=> 'report-extract'], function(){
        Route::get('index', 'ReportController@extract')->name('admin.report.extract');
    });
    /**===============================
    * Tool
    ==================================*/
    Route::group(['prefix'=> 'tool'], function(){
        Route::get('index', 'ToolController@index')->name('admin.tool.index');
    });

    Route::group(['prefix'=> 'reembolso'], function(){
        Route::get('index', 'ReembolsoController@index')->name('admin.reembolso.index');
        Route::get('view/{id}', 'ReembolsoController@view')->name('admin.reembolso.view');
        Route::get('edit/{id}/{status}', 'ReembolsoController@edit')->name('admin.reembolso.edit');
    });


});
/**===============================
* End Admin
==================================*/
Route::get('/storage/{filename}', function ($filename)
{
    $path = storage_path('app/public/products/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
